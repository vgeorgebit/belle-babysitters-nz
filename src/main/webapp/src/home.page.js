import React from 'react';

import { withRouter } from 'react-router-dom';

class HomePage extends React.Component {

    constructor(props, context) {
        super(props);
        this.navCustomer = this.navCustomer.bind(this);
        this.navBabysitter = this.navBabysitter.bind(this);
        this.navAdmin = this.navAdmin.bind(this);
    }

    navCustomer(e) {
        e.preventDefault();
        this.props.history.push("/customer_login");
    }

    navBabysitter(e) {
        e.preventDefault();
        this.props.history.push("/babysitter_login");
    }

    navAdmin(e) {
        e.preventDefault();
        this.props.history.push("/admin_login");
    }

    render() {
        return (
            <div>
                   <a id="admin" onClick={this.navAdmin}>Admin Login</a>
                <div className="homepage">
                    {/* <nav className="navbar navbar-expand-md navbar-light ">
                <img alt="Belle Babysitters Logo" className="navbar-brand" src="/logo_menu.jpg" width="auto" height="40" />
            </nav> */}
               

                    <div >
                        <img alt="Belle Babysitters Logo" src="/logo.jpg" className="img-fluid center" />
                    </div>

                    <div>
                        <h3>DUNEDIN BASED BABYSITTING AGENCY </h3>
                        <h1>You can relax when we provide a babysitter for you!</h1>

                    </div>

                    <div>
                        <button className="btn loginBtnCust" onClick={this.navCustomer}><i style={{color:"White"}} className="fa fa-home" aria-hidden="true">&nbsp;&nbsp;</i>Customer Login</button>
                        <button className="btn loginBtnBaby" onClick={this.navBabysitter}><i style={{color:"White"}} className="fa fa-users" aria-hidden="true">&nbsp;&nbsp;</i>Babysitter Login</button>
                    </div>




                </div>
            </div>

        )

    }
}
/**
 * Kurt Weston was here
 */
export default withRouter(HomePage);