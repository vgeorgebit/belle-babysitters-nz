import React from 'react';

import { withRouter } from 'react-router-dom';
import Accordion from "./components/Accordion";
import BabysitterNavigation from './components/BabysitterNavigation';


class BabysitterHelpPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedIndex: -1
        }
    }

    render() {
        return (
            <div>
                <BabysitterNavigation />

                <div className="container" style={{ marginTop: "30px" }}>
                    <h1 style={{ paddingBottom: "10px" }}> Babysitter Information</h1>



                    <Accordion className="accordion" selectedIndex={this.state.selectedIndex} >
                        <div data-header="Upcoming Jobs" className="accordionItem">
                            <div className="panel">
                                <p> When a job is available and you meet the criteria it will apear in <a href="/babysitter">upcoming jobs</a>.</p>
                                <p class="badge badge-pill badge-warning">PENDING</p>
                                <p> The job has been assigned to you but we are waiting on payment from the customer. </p>
                                <p class="badge badge-pill badge-success">CONFIRMED</p>
                                <p> We have recieved payment from the customer and you are now fully assigned to the job.</p>
                                {/* <img src="/img/upcomingJobs_confirmed.png"></img> */}
                            </div>
                        </div>
                    </Accordion >

                    <Accordion className="accordion" selectedIndex={this.state.selectedIndex} >
                        <div data-header="Job Offers" className="accordionItem">
                            <div className="panel">
                                <p>Set whether or not you are available for the upcoming job.</p>
                                <img className="jobOffers" src="/img/pendingOffers.png"></img>
                                <p> </p>
                                <p> View the jobs where you have set yourself to available. You are able to set yourself unavailable by clicking <i style={{color:"black"}} className="fa fa-times" aria-hidden="true" title="Decline Job">&nbsp;&nbsp;</i></p>
                                <img className="jobOffers" src="/img/available.png"></img>
                                <p> </p>
                                <p> View jobs where you have set yourself to unavailable. You are able to set yourself available by clicking <i style={{color:"black"}} className="fa fa-check" aria-hidden="true" title="Accept Job">&nbsp;&nbsp;</i></p>
                                <img className="jobOffers" src="/img/unavailable.png"></img>
                            </div>

                        </div>
                    </Accordion >

                    <Accordion className="accordion" selectedIndex={this.state.selectedIndex} >
                        <div data-header="Job History" className="accordionItem">
                            <p> This page will show all of your historical bookings. This is for archiving purposes.</p>
                        </div>
                    </Accordion >


                    <br></br>

                    <p> More information can be found on our  <a href="http://www.bellebabysitters.co.nz/information-for-babysitters-1">main website  </a>
                        or by contacting Annabelle.</p>

                </div>
            </div>
        )
    }
}

export default withRouter(BabysitterHelpPage);