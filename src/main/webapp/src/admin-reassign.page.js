import React from 'react';

import { withRouter } from 'react-router-dom';
import AdminAssignPage from './admin-assign.page.js';

class AdminReassign extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            job: new URL(window.location.href).searchParams.get('job'),
        }
    }

    componentDidMount() {
        const url_string = window.location.href;
        const url = new URL(url_string);
        const username = url.searchParams.get('job');
        this.setState({
            username: username
        });

        if (!username) {
            this.props.history.replace("/admin");
        }
    }

    render() {
        return (
            <div>
                <AdminAssignPage job={this.state.job} modify={true} history={this.props.history}/>
            </div>
        )
    }
}

export default withRouter(AdminReassign);