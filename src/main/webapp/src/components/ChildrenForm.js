import React from 'react';

const textRegex = RegExp(
    /^[A-Za-z\s\\-]*$/
);



class ChildrenForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            children: [
                { firstName: "", lastName: "", gender: "", dob: "", medical: "" }
            ],
            formErrors: {
                children: [
                    { firstName: "", lastName: "", gender: "", dob: "", medical: "" }
                ],
            },
            addDisabled: false
        }

        this.addChild = this.addChild.bind(this);
        this.handleEditChild = this.handleEditChild.bind(this);
        this.validateAndUpdateChildren = this.validateAndUpdateChildren.bind(this);
        // this.validate = this.validate.bind(this);
    }

    addChild(event) {
        // Maximum of 25 children
        if (this.state.children.length >= 25) {
            this.setState({
                addDisabled: true
            });
        }

        // event.preventDefault();
        // this.setState((prevState) => ({
        //     children: [...prevState.children, { firstName: "", lastName: "", gender: "", dob: "", medical: "" }]
        // }), () => {
        //     this.props.onChange(this.state.children);
        // });

        event.preventDefault();
        let children = this.state.children;
        children.push({
            firstName: "", lastName: "", gender: "", dob: "", medical: ""
        });

        this.validateAndUpdateChildren(children, this.state.children.length);

    }

    removeChild(event, index) {
        event.preventDefault();
        if (this.state.children.length > 1 && window.confirm("Are you sure you want to remove this child?")) {
            let children = this.state.children;
            children.splice(index, 1);
            this.setState({
                children
            }, () => {
                this.validateAndUpdateChildren(this.state.children);
            });
        }
    }

    handleEditChild(event, index, field) {
        let c = this.state.children;
        c[index][field] = event.target.value;

        this.validateAndUpdateChildren(c, index, field);

        this.setState({
            children: c
        }, () => {
            this.props.onChange(this.state.children);
        });
    }

    validateAndUpdateChildren(children, restrictIndex = null, restrictName = null, callback = null) {
        let childrenErrors = [];

        children.forEach((c, index) => {
            let errors = this.state.formErrors.children[index] ? this.state.formErrors.children[index] : {
                firstName: "", lastName: "", gender: "", dob: "", medical: ""
            };

            if (restrictIndex === null || restrictIndex === index) {
                // Check errors
                for (let field in c) {
                    let value = c[field];

                    if (restrictName === null || restrictName === field) {
                        switch (field) {

                            case "firstName":
                                errors.firstName =
                                    value.length < 1 ? "First name is required" :
                                        !textRegex.test(value) ? "First name may only contain alphabetic characters" :
                                            value.length > 64 ? "First name is too long" :
                                                null;
                                break;
                            case "lastName":
                                errors.lastName =
                                    value.length < 1 ? "Last name is required" :
                                        !textRegex.test(value) ? "Last name may only contain alphabetic characters" :
                                            value.length > 64 ? "Last name is too long" :
                                                null;
                                break;
                            case "gender":
                                errors.gender =
                                    value === null || value.length === 0 ? "Gender is required." :
                                        null
                                break;
                            case "dob":
                                errors.dob =
                                    value === null || value.length === 0 ? "Date of birth is required." :
                                        Date.parse(value) >= new Date() ? "Date of birth must be in the past" :
                                            null
                                break;
                            case "medical":
                                errors.medical =
                                    value === null || value.length === 0 ? null :
                                        value.length > 500 ? "Medical notes are too long"
                                            : null;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            childrenErrors.push(errors);
        });

        let formErrors = this.state.formErrors;
        formErrors.children = childrenErrors;

        this.setState({
            children: children,
            formErrors: formErrors
        }, () => {
            if (callback)
                callback();
        });
    }


    render() {
        return (
            <div className="child-form">
                {/* <h3>Children Information</h3> */}

                {this.state.children.map((child, index) => {
                    return (
                        <div key={index}>
                            <hr />

                            <div className="form-row">
                                <div className="form-group col-md-10">
                                    <h4 className="title"> {"Child " + (index + 1)} </h4>
                                </div>
                                <div className="form-group col-md-2">
                                    { this.state.children.length > 1 && <button href="#" className="btn btn-outline-danger removeBtn" onClick={(e) => { this.removeChild(e, index) }}>Remove</button>}
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label>First Name</label>
                                    <input value={this.state.children[index].firstName} className={"form-control" + (this.state.formErrors.children[index].firstName === null ? " is-valid" : this.state.formErrors.children[index].firstName ? " is-invalid" : "")} onChange={(e) => { this.handleEditChild(e, index, "firstName") }} type="text" name={"child-firstName-" + index} />
                                    {this.state.formErrors.children[index].firstName && <span className="formError">{this.state.formErrors.children[index].firstName}</span>}
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Last Name</label>
                                    <input value={this.state.children[index].lastName}  className={"form-control" + (this.state.formErrors.children[index].lastName === null ? " is-valid" : this.state.formErrors.children[index].lastName ? " is-invalid" : "")} onChange={(e) => { this.handleEditChild(e, index, "lastName") }} type="text" name={"child-lastName-" + index} />
                                    {this.state.formErrors.children[index].lastName && <span className="formError">{this.state.formErrors.children[index].lastName}</span>}
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label>Gender</label>
                                    <select value={this.state.children[index].gender} className={"form-control" + (this.state.formErrors.children[index].gender ? " is-invalid" : this.state.children[index].gender ? " is-valid" : "")} onChange={(e) => { this.handleEditChild(e, index, "gender") }} >
                                        <option value="" disabled>Gender</option>
                                        <option value="FEMALE">Female</option>
                                        <option value="MALE">Male</option>
                                        <option value="OTHER">Other</option>
                                    </select>
                                    {this.state.formErrors.children[index].gender && <span className="formError">{this.state.formErrors.children[index].gender}</span>}

                                </div>
                                {/* <input onChange={(e) => { this.handleEditChild(e, index, "gender") }} type="text" name={"child-gender-" + index} placeholder={"Child " + (index + 1) + " Gender"} /> */}
                                <div className="form-group col-md-6">
                                    <label>Date of Birth</label>
                                    <input value={this.state.children[index].dob.toString().split("T")[0]}  className={"form-control" + (this.state.formErrors.children[index].dob === null ? " is-valid" : this.state.formErrors.children[index].dob ? " is-invalid" : "")} onChange={(e) => { this.handleEditChild(e, index, "dob") }} type="date" name={"child-dob-" + index} />
                                    {this.state.formErrors.children[index].dob && <span className="formError">{this.state.formErrors.children[index].dob}</span>}
                                </div>

                            </div>
                            <div className="form-group">
                                <label>Medical Notes</label>
                                <textarea placeholder="E.g. Medical conditions, allergies and medications..." value={this.state.children[index].medical}  className={"form-control" + (this.state.formErrors.children[index].medical ? " is-invalid" : this.state.children[index].medical ? " is-valid" : "")} onChange={(e) => { this.handleEditChild(e, index, "medical") }} type="text" name={"child-medical-" + index} rows="3" />
                                {this.state.formErrors.children[index].medical && <span className="formError">{this.state.formErrors.children[index].medical}</span>}
                            </div>
                        </div>
                    )
                })}

                <button className="btn addBtn" disabled={this.state.addDisabled} onClick={this.addChild}><i className="fa fa-plus-circle" aria-hidden="true"></i> Add Child</button>
            </div>
        );
    }

}

export default ChildrenForm;
