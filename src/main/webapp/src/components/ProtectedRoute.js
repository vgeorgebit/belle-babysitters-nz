import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import api from './../api.js';
import * as constants from '../constants.js';

export class ProtectedRoute extends React.Component {
    constructor(props) {
        super(props);

        let {allowedRole, component, ...passProps} = props;
        this.allowedRole = allowedRole;
        this.component = component;
        this.passProps = passProps;

        this.state = {
            redirect: false
        }
    }
    
    render() {
        if (this.state.redirect) {
            return <Redirect to={{
                pathname: this.getLoginPageForRole(this.allowedRole)
            }}/>;
        }

        return (
        <Route {...this.passProps} render={(props) => {
            if (api.hasRole(this.allowedRole)) {
                api.isTokenValid().then(valid => {
                    if (!valid) {
                        this.setState({
                            redirect: true
                        });
                        api.clearTokens();
                    }
                });
                return <this.component {...props} />;
            } else {
                return <Redirect to={{
                    pathname: this.getLoginPageForRole(this.allowedRole)
                }} />
            }
        }}
        />
        );
    }

    getLoginPageForRole(role) {
        switch (role) {
            case constants.ROLE_ADMIN:
                return "/admin_login";
            case constants.ROLE_SUPER_ADMIN:
                return "/admin";
            case constants.ROLE_BABYSITTER:
                return "/babysitter_login";
            case constants.ROLE_CUSTOMER:
                return "/customer_login";
            default:
                return "/";
        }
    }
}
