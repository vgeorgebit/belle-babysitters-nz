import React from 'react';
import { withRouter } from "react-router-dom";
import * as constants from '../constants';

import api from '../api.js';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);

        this.handleLogin = this.handleLogin.bind(this);
        this.homepage = this.homepage.bind(this);

        this.state = {
            userType: "",
            usernameInput: "",
            passwordInput: "",
            errorMessage: null,
            loggingIn: false
        };
    }

    homepage(e){
        e.preventDefault();
        this.props.history.push("/");
    }

    componentDidMount() {
        if (api.hasRole(constants.ROLE_ADMIN)) {
            this.props.history.push("/admin");
        } else if (api.hasRole(constants.ROLE_BABYSITTER)) {
            this.props.history.push("/babysitter");
        } else if (api.hasRole(constants.ROLE_CUSTOMER)) {
            this.props.history.push("/customer");
        }

        if (!["admin", "babysitter", "customer"].includes(this.props.userType))
            throw new Error("Invalid user type for login form");

        this.setState({
            userType: this.props.userType
        });
    }

    componentWillReceiveProps(nextProps, nextState) {
        this.setState({
            userType: this.nextProps["userType"]
        });
    }

    handleLogin = (event) => {
        event.preventDefault();

        this.setState({
            errorMessage: null,
            loggingIn: true
        });

        api.fetchAccessToken(this.state.userType + "\\" + this.state.usernameInput, this.state.passwordInput).then((response) => {
            this.setState({
                errorMessage: null,
                loggingIn: false
            });

            this.props.history.push("/" + this.state.userType);
        }).catch((error) => {
            const newState = error.response && error.response.data && error.response.data.error_description ? error.response.data.error_description : "An unknown error occurred";
            this.setState({
                errorMessage: newState,
                loggingIn: false
            });
        })

    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        let infoText = "";

        switch (String(this.state.userType)) {
            case "admin":
                infoText = "Admin Login";
                break;
            case "babysitter":
                infoText = "Babysitter Login";
                break;
            case "customer":
                infoText = "Customer Login";
                break;
            default:
                break;
        }

        let customerExtraDetails = (
            <p>Don't have an account? <a href="#" onClick={e => { e.preventDefault(); this.props.history.replace("register")}}>Sign up!</a></p>
        );

        return (
            <div className="loginContainer container" >
                
                <img src="logo.jpg" className="img-fluid center" alt="Belle Babysitters Logo" />
                
                <div className="loginForm">
                    <h1 className="font-weight-light">{infoText}</h1>
                    {this.state.userType === "customer" && customerExtraDetails}
                    <form onSubmit={this.handleLogin}>
                        <input type="text" disabled={this.state.loggingIn} name="usernameInput" value={this.state.usernameInput} onChange={this.handleChange.bind(this)} className="form-control"  placeholder={this.props.userType === "customer" ? "Email Address" : "Username"} />
                        <input type="password" disabled={this.state.loggingIn} name="passwordInput" value={this.state.passwordInput} onChange={this.handleChange.bind(this)} className="form-control" placeholder="Password" />
                        {(this.props.userType === "customer" || this.props.userType === "babysitter") && <a target="_blank" href={this.props.userType + "/forgotpassword"}>Forgot password?</a>}
                        <button type="submit" disabled={this.state.loggingIn} className="btn btn-primary btn-block">{this.state.loggingIn ? 'Logging In...' : 'Login'}</button>
                    </form>
                    {this.state.errorMessage ? <p className="alert alert-danger">{this.state.errorMessage}</p> : null}
                    <div id="loginHomeLinkContainer">
                        <a id="loginHomeLink" onClick={this.homepage}>Go back</a>
                    </div>
                </div>
        
            </div>
        );
    }
}

export default withRouter(LoginForm);