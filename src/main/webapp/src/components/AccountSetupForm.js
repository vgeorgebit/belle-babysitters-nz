import React from 'react';
import * as constants from '../constants.js';
import { ax } from '../api.js';

class AccountSetupForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            processing: false,
            disabled: true,
            password: '',
            confirmPassword: '',
            errorMessage: null,
            success: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSetup = this.handleSetup.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        }, () => {
            if (this.state.password.length < 6 || this.state.password.length > 32) {
                this.setState({
                    disabled: true,
                    errorMessage: "Password must contain between 6 and 32 characters."
                });
            } else if (this.state.confirmPassword !== this.state.password) {
                this.setState({
                    disabled: true,
                    errorMessage: "Passwords must match."
                });
            } else if (!this.state.password.match("[0-9A-Za-z!@#$%^&*/_-]+")) {
                this.setState({
                    disabled: true,
                    errorMessage: "Password may only contain alphanumeric characters and !@#$%^&*/_-"
                });
            } else {
                this.setState({
                    disabled: false,
                    errorMessage: null
                })
            }
        });
    }

    handleSetup(event) {
        event.preventDefault();
        this.setState({
            processing: true
        });

        const form = new FormData();
        form.set("code", window.location.search.substring(6))
        form.set("password", this.state.password)

        ax.post(constants.PUBLIC_API_BASE_URL + (this.props.userType === "babysitter" ? "babysitter/setup" : "customer/setup"), form).then(response => {
            this.setState({
                success: true
            });
        }).catch(error => {
            this.setState({
                processing: false,
                errorMessage: error.response && error.response.data && error.response.data.error_description ? error.response.data.error_description : "An unknown error occurred"
            });
        });
    }

    render() {
        return (
            <div className="container setupContainer" >
                <img src="/logo.jpg" className="img-fluid center" alt="Belle Babysitters Logo" />
                <h1>{this.props.userType === "babysitter" ? "Babysitter" : "Customer"} Account Setup</h1>

                <form onSubmit={this.handleSetup}>
                    {this.state.success ?
                        (
                            <div id="setupSuccess">
                                <p>Your account has been setup successfully!</p>
                                <a href={this.props.userType === "babysitter" ? "/babysitter_login" : "/customer_login"}>Click here to login to your account.</a>
                            </div>
                        ) : (
                            <div>
                                <p>Please setup a password for your account.</p>
                                <input type="password" disabled={this.state.processing} placeholder="Password" name="password" value={this.state.password} onChange={this.handleChange} className="form-control" />
                                <input type="password" disabled={this.state.processing} placeholder="Confirm Password" name="confirmPassword" value={this.state.confirmPassword} onChange={this.handleChange} className="form-control" />
                                <button type="submit" disabled={this.state.processing || this.state.disabled} className="btn btn-primary btn-block">{this.state.processing ? 'Processing...' : 'Submit'}</button>
                                <p id="setupError">{this.state.errorMessage}</p>
                            </div>
                        )
                    }
                </form>
            </div>
        )
    }
}

export default AccountSetupForm;