 import React from 'react';

 import { ax } from '../api.js'
 import * as constants from '../constants.js'

class CusotmerInquiryForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        }, () => {
            if (this.state.value.length > 20) {
                this.setState({
                    disabled: true,
                    errorMessage: "Enquiry must be longer than 20 characters."
                });
            } else {
                this.setState({
                    disabled: false,
                    errorMessage: null
                })
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        ax.post(constants.API_BASE_URL + "customer/inquiry?inquiryMessage=" +  this.state.value).then(response => {
            this.setState({
                success: true
            });        
            alert('Enquiry has been sent to Belle Babysittters');
            this.setState({
                value: ''
            });
    
        }).catch(error => {
            this.setState({
                processing: false,
                errorMessage: error.response && error.response.data && error.response.data.error_description ? error.response.data.error_description : "An unknown error occurred"
        });
        alert('Enquiry must be longer than 20 characters.');
     });
    }

    render() {
        return (

            <div className = 'container inquiryContainer'>
                <h1>Send An Enquiry</h1>  
                              
                <form onSubmit={this.handleSubmit}>
                <h2>Type your enquiry below: </h2>
                <label for="value" ></label>
                
                <textarea name="value" placeholder = "Type enquiry here." value={this.state.value} onChange={this.handleChange}/>

                <p>Your enquiry will be sent automatically to Belle Babbysitters and be responded to using the<br>
                </br>email address associated with your account.</p>
                
                <button type="submit" class="btn btn-primary mb-2">Submit Enquiry</button>

                
                 
                </form>
            </div>
                
        );
    }
}
export default CusotmerInquiryForm;
