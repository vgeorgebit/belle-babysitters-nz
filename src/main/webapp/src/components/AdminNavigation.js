import React from 'react';
import api, { ax } from '../api.js';
import * as constants from '../constants.js';


class AdminNavigation extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            accountDetails: {}
        };

        ax.get(constants.API_BASE_URL + "admin/owndetails").then(response => {
            this.setState({
                accountDetails: response.data
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });;
    }
    //logo 
    static logout() {
        //console.log('logout click happened')
        api.clearTokens();
    };

    render() {

        return (

            <nav className="navbar navbar-expand-md navbar-light">
                <a href="/admin"><img alt="Belle Babysitters Logo" className="navbar-brand" src="/logo_menu.jpg" width="auto" height="40" /></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/admin">Pending Bookings</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/admin/confirmed">Confirmed Bookings</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/admin/historical">Historical Bookings</a>
                        </li>
                       { api.hasRole(constants.ROLE_SUPER_ADMIN) && // role based menu visibility giving full access to super admin
                        <div className="nav-item dropdown">
                            <a className="navProfile nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Manage Accounts
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a className="dropdown-item" href="/admin/admin">Admin</a>
                                <a className="dropdown-item" href="/admin/customer">Customer</a>
                                <a className="dropdown-item" href="/admin/babysitter">Babysitter</a>
                            </div>
                        </div>
                       } 
                    </ul>
                    <div className="nav-item dropdown">
                        <a className="navProfile nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fa fa-user-circle fa-lg"></i>
                            <span>{this.state.accountDetails.firstName} {this.state.accountDetails.lastName} ({this.state.accountDetails.username})</span>
                        </a>
                        <div className="dropdown-menu" id="adminProfileDropdown" aria-labelledby="navbarDropdown">
                            <a className="dropdown-item" href="/admin/changepassword">Change Password</a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="#" onClick={AdminNavigation.logout} >Logout</a>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

export default AdminNavigation;