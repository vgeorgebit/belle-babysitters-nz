import React from 'react';
import api, { ax } from '../api.js';
import * as constants from '../constants.js';


/**
 * Kurt Weston was here
 */
class BadgeDisplay extends React.Component {



    render() {
        let babysitter = this.props.babysitter;
        return (
            <table style={{margin:0}}>
                <tr>
                    <td style={{all:"unset"}}>    
                        {babysitter.license == "RESTRICTED" && <i style={{color:"ORANGE"}} className="fa fa-id-card-o" aria-hidden="true" title="Restricted Drivers License">&nbsp;</i>} 
                        {babysitter.license == "FULL" && <i style={{color:"BLACK"}} className="fa fa-id-card-o" aria-hidden="true" title="Full Drivers License">&nbsp;</i>} 

                    </td>
                    <td style={{all:"unset"}}>
                        {(new Date(babysitter.medicalCertDate) > new Date())  && <i className="fa fa-heartbeat" style={{color:"Red"}} aria-hidden="true" title={"First Aid Certificate (Expires: "+babysitter.medicalCertDate.split("T")[0]+")"}/>}
                    </td>
                </tr>
                <tr>
                    <td style={{all:"unset"}}> 
                        {babysitter.car == 'ALWAYS' && <i style={{color:"Green"}} className="fa fa-car" aria-hidden="true" title="ALWAYS Has Vehicle Access">&nbsp;</i>}
                        {babysitter.car == 'SOMETIMES' && <i style={{color:"Orange"}} className="fa fa-car" aria-hidden="true" title="SOMETIMES Has Vehicle Access">&nbsp;</i>}
                    </td>
                    <td style={{all:"unset"}}> 
                    {console.log((new Date(babysitter.policeCheck).getMonth() - new Date().getMonth() 
                                    + (12 * (new Date(babysitter.policeCheck).getFullYear() - new Date().getFullYear()))
                                    ))}
                        {babysitter.policeCheck && 
                            <i style={{color:(babysitter.policeCheck 
                                    && (new Date().getMonth() - new Date(babysitter.policeCheck).getMonth()
                                    + (12 * ( new Date().getFullYear() - new Date(babysitter.policeCheck).getFullYear()))
                                    ) > 36) ?
                                "Orange" : "Blue"}}
                                className="fa fa-gavel" aria-hidden="true" title={"Police Check (Completed: " + (babysitter.policeCheck.split("T")[0])+")"}/>}
                    </td>
                </tr>
                </table>
        );
    }
}

export default BadgeDisplay;