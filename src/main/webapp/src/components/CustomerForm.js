import React from 'react';
import { Redirect } from 'react-router-dom';

import ChildrenForm from './ChildrenForm.js';
import { withRouter } from 'react-router-dom';

import { ax } from '../api.js'
import * as constants from '../constants.js'

const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const textRegex = RegExp(
    /^[A-Za-z\s\\-]*$/
);

const addressRegex = RegExp(
    // /^[a-zA-Z0-9,.\\-\\s]*$/
    /^[a-zA-Z0-9\s,.'-]*$/
);

const numberRegex = RegExp(
    /^\d+$/
);

const postCodeRegex = RegExp(
    /^[0-9\-A-Za-z]+$/
);

class AdminCustomerFormPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            customer: {
                familySurname: '',
                email: '',
                membershipEndDate: '',
                caregivers: [
                    {
                        firstName: '',
                        lastName: '',
                        phoneNumber: '',
                        streetAddress: '',
                        addressLine2: '',
                        city: '',
                        region: '',
                        postCode: '',
                        addressNotes: '',
                        relationshipToChild: '',
                    }
                ],
                emgConFName: '', emgConLName: '', emgConNo: '', emgConRel: '',
                extraNotes: '',
                pets: '',
                adminNotesAdmin: '',
                adminNotesBabysitter: '',
                disabled: false,
                children: [
                    { firstName: "", lastName: "", gender: "", dob: "", medical: "" },
                ],
            },
            formErrors: {
                familySurname: '',
                email: '',
                membershipEndDate: '',
                caregivers: [
                    {
                        firstName: '',
                        lastName: '',
                        phoneNumber: '',
                        streetAddress: '',
                        addressLine2: '',
                        city: '',
                        region: '',
                        postCode: '',
                        addressNotes: null,
                        relationshipToChild: null,
                    }, {
                        firstName: '',
                        lastName: '',
                        phoneNumber: '',
                        streetAddress: '',
                        addressLine2: '',
                        city: '',
                        region: '',
                        postCode: '',
                        addressNotes: null,
                        relationshipToChild: null,
                    }],
                emgConFName: '', emgConLName: '', emgConNo: '', emgConRel: '',
                extraNotes: '',
                pets: '',
                children: [{ firstName: "", lastName: "", gender: "", dob: "", medical: "" }],
            },
            processing: false,
            hasSubmitErrors: false,
            submitError: '',
            redirect: false,
            revisions: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.addCaregiver = this.addCaregiver.bind(this);
        this.handleEditCaregiver = this.handleEditCaregiver.bind(this);
        this.validateAndUpdateCaregivers = this.validateAndUpdateCaregivers.bind(this);
        this.validate = this.validate.bind(this);

        this.childrenForm = React.createRef();
        this.autofillAddress = this.autofillAddress.bind(this);

        if (this.props.username) {
            ax.get(constants.API_BASE_URL + "customer/" + this.props.username).then(response => {

                let formErrors = this.state.formErrors;
                formErrors.caregivers = [];
                response.data.caregivers.forEach(() => {
                    formErrors.caregivers.push({
                        firstName: '',
                        lastName: '',
                        phoneNumber: '',
                        streetAddress: '',
                        addressLine2: '',
                        city: '',
                        region: '',
                        postCode: '',
                        addressNotes: null,
                        relationshipToChild: null,
                    });
                });

                this.setState({
                    formErrors: formErrors,
                    customer: response.data,
                }, () => {
                    let childrenFormErrors = this.childrenForm.current.state.formErrors;
                    childrenFormErrors.children = [];
                    response.data.children.forEach(() => {
                        childrenFormErrors.children.push({
                            firstName: "", lastName: "", gender: "", dob: "", medical: ""
                        })
                    });
                    this.childrenForm.current.setState({
                        formErrors: childrenFormErrors,
                        children: response.data.children
                    });
                });
            }).catch(error => {
                this.props.history.replace("/admin/customer");
            });

            ax.get(constants.API_BASE_URL + "audit/customer/" + this.props.username).then(response => {
                let r = response.data;
                r.sort((a, b) => {
                    return a.revisionDate < b.revisionDate ? 1 : a.revisionDate == b.revisionDate ? 0 : -1;
                });

                this.setState({
                    revisions: r
                });
            });
        }
    }

    handleSubmit = event => {
        event.preventDefault();

        this.validateAndUpdateCaregivers(this.state.customer.caregivers, null, null, () => {
            this.childrenForm.current.validateAndUpdateChildren(this.childrenForm.current.state.children, null, null, () => {
                let fe = this.state.formErrors;
                fe.children = this.childrenForm.current.state.formErrors.children;

                this.setState({
                    formErrors: fe,
                    submitError: false,
                    hasSubmitErrors: false
                }, () => {

                    this.validate(null, () => {
                        let errors = false;

                        Object.keys(this.state.formErrors).forEach((field) => {
                            let value = this.state.formErrors[field];
                            if (field === "caregivers" || field === "children")
                                return;
                            if (!(value === null || value.length === 0)) {
                                errors = true;
                            }
                        });

                        for (const index in this.state.formErrors.caregivers) {
                            for (const field in this.state.formErrors.caregivers[index]) {
                                if (this.state.formErrors.caregivers[index][field] != null) {
                                    errors = true;
                                }
                            }
                        }

                        for (const index in this.state.formErrors.children) {
                            for (const field in this.state.formErrors.children[index]) {
                                if (this.state.formErrors.children[index][field] != null) {
                                    errors = true;
                                }
                            }
                        }

                        this.setState({
                            hasSubmitErrors: errors
                        });

                        if (!errors) {
                            this.setState({
                                processing: true
                            });
                            if (this.props.username) {
                                ax.put(constants.API_BASE_URL + "customer", this.state.customer).then(response => {
                                    // redirect to customer page
                                    this.setState({
                                        redirect: true
                                    });
                                }).catch(error => { // if there is an error
                                    let msg = '';
                                    if (error.response && error.response.data && error.response.data.constraintViolations != null && error.response.data.constraintViolations.length > 0) {
                                        error.response.data.constraintViolations.forEach(v => {
                                            msg += v + '  ';
                                        });
                                    } else if (error.response && error.response.data) {
                                        msg = error.response.data.error_description;
                                    } else {
                                        msg = "An unknown error occurred";
                                    }

                                    this.setState({
                                        submitError: msg,
                                        processing: false
                                    });
                                });
                            } else {
                                // Submitting the form to backend
                                ax.post(constants.API_BASE_URL + "customer", this.state.customer).then(response => {
                                    // redirect to customer page
                                    this.setState({
                                        redirect: true
                                    });
                                }).catch(error => { // if there is an error
                                    let msg = '';
                                    if (error.response && error.response.data && error.response.data.constraintViolations != null && error.response.data.constraintViolations.length > 0) {
                                        error.response.data.constraintViolations.forEach(v => {
                                            msg += v + '  ';
                                        });
                                    } else if (error.response && error.response.data) {
                                        msg = error.response.data.error_description;
                                    } else {
                                        msg = "An unknown error occurred";
                                    }

                                    this.setState({
                                        submitError: msg,
                                        processing: false
                                    });
                                });
                            }
                        }
                    });
                });
            });
        });


    }

    validate(name = null, callback = null) {
        let formErrors = this.state.formErrors;

        Object.keys(this.state.customer).forEach(field => {
            if (field === "children" || field === "caregivers")
                return;

            if (name === null || name === field) {
                let value = this.state.customer[field];

                switch (field) {
                    case "familySurname":
                        formErrors.familySurname =
                            value.length < 1 ? "Family surname is required" :
                                !textRegex.test(value) ? "Family surname may only contain alphabetic characters" :
                                    value.length > 64 ? "Family surname is too long" :
                                        null;
                        break;
                    case "email":
                        formErrors.email =
                            value.length < 1 ? "Email address is required" :
                                !emailRegex.test(value) ? "Email address must be valid" :
                                    value.length > 255 ? "Email address is too long" :
                                        null;
                        break;
                    case "extraNotes":
                        formErrors.extraNotes =
                            value === null || value.length === 0 ? null :
                                value.length > 500 ? "Other requirements are too long" :
                                    null;
                        break;
                    case "pets":
                        formErrors.pets =
                            value === null || value.length < 1 ? "Details of pets are required" :
                                value.length > 500 ? "Details of pets is too long" :
                                    null;
                        break;
                    case "emgConFName":
                        formErrors.emgConFName =
                            value.length < 1 ? "First name is required" :
                                !textRegex.test(value) ? "First name may only contain alphabetic characters" :
                                    value.length > 64 ? "First name is too long" :
                                        null;
                        break;
                    case "emgConLName":
                        formErrors.emgConLName =
                            value.length < 1 ? "Last name is required" :
                                !textRegex.test(value) ? "Last name may only contain alphabetic characters" :
                                    value.length > 64 ? "Last name is too long" :
                                        null;
                        break;
                    case "emgConNo":
                        formErrors.emgConNo =
                            value.length < 1 ? "Phone number is required" :
                                !numberRegex.test(value) ? "Phone number may only contain digits" :
                                    value.length > 15 ? "Phone number is too long" :
                                        null;
                        break;
                    case "membershipEndDate":
                        formErrors.membershipEndDate = null;
                        break;
                    case "emgConRel":
                        formErrors.emgConRel =
                            value === null || value.length === 0 ? "Relationship to child is required." :
                                null
                        break;
                    default:
                        break;
                }
            }

        });

        this.setState({
            formErrors: formErrors
        }, () => {
            if (callback)
                callback();
        });


    }

    handleChange(event) {
        event.preventDefault();
        const { name, value } = event.target;

        let c = this.state.customer;
        c[name] = value;

        this.setState({
            customer: c,
            hasSubmitErrors: false,
            submitError: null
        }, () => {
            this.validate(name);
        });
    }

    handleCheckChange(event) {
        let cust = this.state.customer;
        cust.disabled = event.target.checked;
        this.setState({
            customer: cust
        }, () => {
            console.log(this.state.customer);
        });
    }

    handleEditCaregiver(event, index, field) {
        let c = this.state.customer.caregivers;
        c[index][field] = event.target.value;

        this.validateAndUpdateCaregivers(c, index, field);

    }

    validateAndUpdateCaregivers(caregivers, restrictIndex = null, restrictName = null, callback = null) {
        let caregiverErrors = [];

        caregivers.forEach((cg, index) => {
            let errors = this.state.formErrors.caregivers[index] ? this.state.formErrors.caregivers[index] : {
                firstName: '',
                lastName: '',
                phoneNumber: '',
                streetAddress: '',
                addressLine2: '',
                city: '',
                region: '',
                postCode: '',
                addressNotes: '',
                relationshipToChild: '',
            };

            if (restrictIndex === null || restrictIndex === index) {
                // Check errors
                for (let field in cg) {
                    let value = cg[field];

                    if (restrictName === null || restrictName === field) {

                        switch (field) {
                            case "firstName":
                                errors.firstName =
                                    value.length < 1 ? "First name is required" :
                                        !textRegex.test(value) ? "First name may only contain alphabetic characters" :
                                            value.length > 64 || value.length < 1 ? "Caregiver first name is too long" :
                                                null;
                                break;
                            case "lastName":
                                errors.lastName =
                                    value.length < 1 ? "Last name is required" :
                                        !textRegex.test(value) ? "Last name may only contain alphabetic characters" :
                                            value.length > 64 || value.length < 1 ? "Last name is too long" :
                                                null;
                                break;
                            case "phoneNumber":
                                errors.phoneNumber =
                                    value.length < 1 ? "Phone number is required" :
                                        !numberRegex.test(value) ? "Phone number may only contain digits" :
                                            value.length > 15 || value.length < 1 ? "Phone number is too long" :
                                                null;
                                break;
                            case "streetAddress":
                                errors.streetAddress =
                                    value.length < 1 ? "Street address is required" :
                                        !addressRegex.test(value) ? "Street address contains invalid characters" :
                                            value.length > 64 ? "Street address is too long" :
                                                null;
                                break;
                            case "addressLine2":
                                errors.addressLine2 =
                                    value.length < 1 ? "Suburb is required" :
                                        !textRegex.test(value) ? "Suburb contains invalid characters" :
                                            value.length > 64 ? "Suburb is too long" :
                                                null;
                                break;
                            case "city":
                                errors.city =
                                    value.length < 1 ? "City name is required" :
                                        !textRegex.test(value) ? "City name may only contain alphabetic characters" :
                                            value.length > 64 ? "City name is too long" :
                                                null;
                                break;
                            case "region":
                                errors.region =
                                    value.length < 1 ? "Region name is required" :
                                        !textRegex.test(value) ? "Region name may only contain alphabetic characters" :
                                            value.length > 64 ? "Region name is too long" :
                                                null;
                                break;
                            case "postCode":
                                errors.postCode =
                                    value.length < 1 ? "Post code is required" :
                                        !postCodeRegex.test(value) ? "Post code may only contain alphanumeric characters." :
                                            value.length < 4 ? "Post code is too short" :
                                                value.length > 10 ? "Post code is too long" :
                                                    null;
                                break;
                            case "addressNotes":
                                errors.addressNotes =
                                    value === null || value.length === 0 ? null :
                                        value.length < 500 ? null :
                                            "Address notes are too long";
                                break;
                            case "relationshipToChild":
                                errors.relationshipToChild =
                                    value === null || value.length === 0 ? "Relationship to child is required." :
                                        null
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            caregiverErrors.push(errors);
        });

        let customer = this.state.customer;
        customer.caregivers = caregivers;

        let formErrors = this.state.formErrors;
        formErrors.caregivers = caregiverErrors;

        this.setState({
            customer: customer,
            formErrors: formErrors
        }, () => {
            if (callback)
                callback();
        });
    }

    removeCaregiver(event, index) {
        event.preventDefault();
        if (this.state.customer.caregivers.length > 1 && window.confirm("Are you sure you want to remove this caregiver?")) {
            let customer = this.state.customer;
            customer.caregivers.splice(index, 1);
            this.setState({
                customer
            }, () => {
                this.validateAndUpdateCaregivers(this.state.customer.caregivers);
            });
        }
    }

    addCaregiver(event) {
        event.preventDefault();
        let caregivers = this.state.customer.caregivers;
        caregivers.push({
            firstName: '',
            lastName: '',
            phoneNumber: '',
            streetAddress: '',
            addressLine2: '',
            city: '',
            region: '',
            postCode: '',
            addressNotes: '',
            relationshipToChild: '',
        });

        this.validateAndUpdateCaregivers(caregivers, this.state.customer.caregivers.length);
    }

    autofillAddress(event, srcIndex, destIndex) {
        event.preventDefault();

        let cust = this.state.customer;
        cust.caregivers[destIndex].streetAddress = cust.caregivers[srcIndex].streetAddress;
        cust.caregivers[destIndex].addressLine2 = cust.caregivers[srcIndex].addressLine2;
        cust.caregivers[destIndex].city = cust.caregivers[srcIndex].city;
        cust.caregivers[destIndex].region = cust.caregivers[srcIndex].region;
        cust.caregivers[destIndex].postCode = cust.caregivers[srcIndex].postCode;
        cust.caregivers[destIndex].addressNotes = cust.caregivers[srcIndex].addressNotes;

        this.setState({
            caregivers: cust
        });
    }

    render() {
        return (
            <div>
                {
                    this.state.redirect && <Redirect to={{
                        pathname: "/admin/customer"
                    }} />
                }

                <div className="formContainer">

                    <form className="card">
                        <div className="card-header">
                            <div style={{width: "100%", textAlign: "right"}}>
                                <button className="btn btn-danger" onClick={() => {this.props.history.push("/admin/customer")}}>Cancel</button>
                            </div>
                            <h1 className="customer-header">{this.props.username ? "Modify Customer Account" : "Create Customer Account"} </h1>
                        </div>
                        <div className="card-body">
                            <h4>Account Details</h4>
                            <div className="form-group">
                                <div className="row">
                                    <div className="col-md-6">
                                        <label>Family Surname</label>
                                        <input type="text" name="familySurname" className={"form-control" + (this.state.formErrors.familySurname === null ? " is-valid" : this.state.formErrors.familySurname ? " is-invalid" : "")} value={this.state.customer.familySurname} onChange={this.handleChange} />
                                        {this.state.formErrors.familySurname && <span className="formError">{this.state.formErrors.familySurname}</span>}
                                    </div>

                                    {
                                        this.props.username &&
                                        <div className="col-md-6" style={{ marginTop: 30, paddingLeft: 50 }}>
                                            <input type="checkbox" name="disabled" checked={this.state.customer.disabled} onChange={this.handleCheckChange} className="form-check-input" />
                                            <label className="form-check-label" htmlFor="disabled">Disable Account</label>
                                        </div>
                                    }
                                </div>
                            </div>
                            <div style={{ paddingLeft: 0 }} className="form-group col-md-6">
                                <label> Email Address  </label>
                                <input disabled={this.props.username ? true : false} type="email" name="email" className={"form-control placeholder" + (this.state.formErrors.email === null ? " is-valid" : this.state.formErrors.email ? " is-invalid" : "")} value={this.state.customer.email} onChange={this.handleChange} placeholder="name@example.com" />
                                {this.state.formErrors.email && <span className="formError">{this.state.formErrors.email}</span>}
                            </div>
                            <div style={{ paddingLeft: 0 }} className="form-group col-md-6">
                                <label> Membership End Date</label>
                                <input type="date" name="membershipEndDate" className={"form-control placeholder" + (this.state.formErrors.membershipEndDate === null ? " is-valid" : this.state.formErrors.membershipEndDate ? " is-invalid" : "")} value={this.state.customer.membershipEndDate ? this.state.customer.membershipEndDate.split('T')[0] : ""} onChange={this.handleChange} required />
                                {this.state.formErrors.membershipEndDate && <span className="formError">{this.state.formErrors.membershipEndDate}</span>}
                            </div>
                            {/* Adding another parent/caregiver */}
                            {this.state.customer.caregivers.map((caregiver, index) => {
                                return (

                                    <div key={index}>
                                        <hr />

                                        <div className="form-row">
                                            <div style={{ marginBottom: 0, paddingBottom: 0 }} className="form-group col-md-10">
                                                <h4 className="header-customerform"> {"Parent / Caregiver " + (index + 1)} </h4>
                                            </div>
                                            <div className="form-group col-md-2">
                                                {this.state.customer.caregivers.length > 1 && <button href="#" className="btn btn-outline-danger removeBtn" onClick={(e) => { this.removeCaregiver(e, index) }}>Remove</button>}
                                            </div>
                                        </div>

                                        <div className="form-row">
                                            <div className="form-group col-md-6">
                                                <label> First Name </label>
                                                <input value={this.state.customer.caregivers[index].firstName} className={"form-control" + (this.state.formErrors.caregivers[index].firstName === null ? " is-valid" : this.state.formErrors.caregivers[index].firstName ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "firstName") }} type="text" name={"caregiver-firstName-" + index} />
                                                {this.state.formErrors.caregivers[index].firstName && <span className="formError">{this.state.formErrors.caregivers[index].firstName}</span>}
                                            </div>
                                            <div className="form-group col-md-6">
                                                <label> Last Name </label>
                                                <input value={this.state.customer.caregivers[index].lastName} className={"form-control" + (this.state.formErrors.caregivers[index].lastName === null ? " is-valid" : this.state.formErrors.caregivers[index].lastName ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "lastName") }} type="text" name={"caregiver-lastName-" + index} />
                                                {this.state.formErrors.caregivers[index].lastName && <span className="formError">{this.state.formErrors.caregivers[index].lastName}</span>}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label> Phone Number </label>
                                            <input value={this.state.customer.caregivers[index].phoneNumber} className={"form-control col-md-6" + (this.state.formErrors.caregivers[index].phoneNumber === null ? " is-valid" : this.state.formErrors.caregivers[index].phoneNumber ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "phoneNumber") }} type="text" name={"caregiver-phoneNumber-" + index} />
                                            {this.state.formErrors.caregivers[index].phoneNumber && <span className="formError">{this.state.formErrors.caregivers[index].phoneNumber}</span>}
                                        </div>


                                        <div className="form-group">
                                            <label> Relationship To Child  </label>
                                            <select value={this.state.customer.caregivers[index].relationshipToChild} className={"form-control col-md-6 placeholder" + (this.state.formErrors.caregivers[index].relationshipToChild ? " is-invalid" : this.state.customer.caregivers[index].relationshipToChild ? " is-valid" : "")} name="relationshipToChild" onChange={(e) => { this.handleEditCaregiver(e, index, "relationshipToChild") }}>
                                                <option value="" disabled>Please select</option>
                                                <option value="MOTHER">Mother</option>
                                                <option value="FATHER">Father</option>
                                                <option value="STEP_MOTHER">Step-Mother</option>
                                                <option value="STEP_FATHER">Step-Father</option>
                                                <option value="GRANDPARENT">Grandparent</option>
                                                <option value="OTHER_FAMILY">Other Family</option>
                                                <option value="OTHER">Other</option>
                                            </select>
                                            {this.state.formErrors.caregivers[index].relationshipToChild && <span className="formError">{this.state.formErrors.caregivers[index].relationshipToChild}</span>}
                                        </div>



                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-12">
                                                    <h5 className="header-customerform" style={{ paddingBottom: 0, display: "inline-block" }}> Home Address </h5>
                                                </div>
                                                <div className="col-12">
                                                    <div style={{ textAlign: "left", fontSize: 12 }}>
                                                        {
                                                            this.state.customer.caregivers.map((value, i) => {
                                                                return (
                                                                    i != index && value.firstName && value.lastName &&
                                                                    <div>
                                                                        <span><a href="#" onClick={(event) => { this.autofillAddress(event, i, index) }}>{"Copy address from " + value.firstName + " " + value.lastName}</a></span>
                                                                        <br />
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div className="form-row">
                                                <div className="form-group col-md-6">
                                                    <label>Street Address</label>
                                                    <input value={this.state.customer.caregivers[index].streetAddress} className={"form-control" + (this.state.formErrors.caregivers[index].streetAddress === null ? " is-valid" : this.state.formErrors.caregivers[index].streetAddress ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "streetAddress") }} type="text" name={"caregiver-streetAddress-" + index} />
                                                    {this.state.formErrors.caregivers[index].streetAddress && <span className="formError">{this.state.formErrors.caregivers[index].streetAddress}</span>}
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Suburb</label>
                                                    <input value={this.state.customer.caregivers[index].addressLine2} className={"form-control" + (this.state.formErrors.caregivers[index].addressLine2 === null ? " is-valid" : this.state.formErrors.caregivers[index].addressLine2 ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "addressLine2") }} type="text" name={"caregiver-addressLine2-" + index} />
                                                    {this.state.formErrors.caregivers[index].addressLine2 && <span className="formError">{this.state.formErrors.caregivers[index].addressLine2}</span>}
                                                </div>
                                            </div>
                                            <div className="form-row">
                                                <div className="form-group col-md-6">
                                                    <label>City</label>
                                                    <input value={this.state.customer.caregivers[index].city} className={"form-control" + (this.state.formErrors.caregivers[index].city === null ? " is-valid" : this.state.formErrors.caregivers[index].city ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "city") }} type="text" name={"caregiver-city-" + index} />
                                                    {this.state.formErrors.caregivers[index].city && <span className="formError">{this.state.formErrors.caregivers[index].city}</span>}
                                                </div>
                                                <div className="form-group col-md-4">
                                                    <label>Region</label>
                                                    <input value={this.state.customer.caregivers[index].region} className={"form-control" + (this.state.formErrors.caregivers[index].region === null ? " is-valid" : this.state.formErrors.caregivers[index].region ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "region") }} type="text" name={"caregiver-region-" + index} />
                                                    {this.state.formErrors.caregivers[index].region && <span className="formError">{this.state.formErrors.caregivers[index].region}</span>}
                                                </div>
                                                <div className="form-group col-md-2">
                                                    <label>Post Code</label>
                                                    <input value={this.state.customer.caregivers[index].postCode} className={"form-control" + (this.state.formErrors.caregivers[index].postCode === null ? " is-valid" : this.state.formErrors.caregivers[index].postCode ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "postCode") }} type="text" name={"caregiver-postCode-" + index} />
                                                    {this.state.formErrors.caregivers[index].postCode && <span className="formError">{this.state.formErrors.caregivers[index].postCode}</span>}
                                                </div>
                                            </div>
                                            <div>
                                                <label>Address Notes</label>
                                                <textarea placeholder="E.g. Tips on finding address, notes on parking..." value={this.state.customer.caregivers[index].addressNotes} className={"form-control" + (this.state.formErrors.caregivers[index].addressNotes ? " is-invalid" : this.state.customer.caregivers[index].addressNotes ? " is-valid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "addressNotes") }} type="text" name={"caregiver-addressNotes-" + index} rows="3" />
                                                {this.state.formErrors.caregivers[index].addressNotes && <span className="formError">{this.state.formErrors.caregivers[index].addressNotes}</span>}
                                            </div>
                                        </div>

                                    </div> //closing div for key=index
                                )
                            })}

                            <button className="btn addBtn" disabled={this.state.customer.caregivers.length > 5} onClick={this.addCaregiver}><i className="fa fa-plus-circle" aria-hidden="true"></i> Add Parent/Caregiver</button>

                            <div className="form-wrapper ">
                                <ChildrenForm ref={this.childrenForm} onChange={(c) => {
                                    let cust = this.state.customer;
                                    cust.children = c;
                                    this.setState({
                                        customer: cust
                                    })
                                }} />
                            </div>

                            <hr />

                            <div className="form-group">

                                <h4 className="header-customerform"> Emergency Contact </h4>

                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <label> First Name </label>
                                        <input value={this.state.customer.emgConFName} className={"form-control" + (this.state.formErrors.emgConFName === null ? " is-valid" : this.state.formErrors.emgConFName ? " is-invalid" : "")} type="text" name="emgConFName" onChange={this.handleChange} />
                                        {this.state.formErrors.emgConFName && <span className="formError">{this.state.formErrors.emgConFName}</span>}
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label> Last Name </label>
                                        <input value={this.state.customer.emgConLName} className={"form-control" + (this.state.formErrors.emgConLName === null ? " is-valid" : this.state.formErrors.emgConLName ? " is-invalid" : "")} type="text" name="emgConLName" onChange={this.handleChange} />
                                        {this.state.formErrors.emgConLName && <span className="formError">{this.state.formErrors.emgConLName}</span>}
                                    </div>
                                </div>


                                <div className="form-group">
                                    <label> Phone Number </label>
                                    <input value={this.state.customer.emgConNo} className={"form-control col-md-6" + (this.state.formErrors.emgConNo === null ? " is-valid" : this.state.formErrors.emgConNo ? " is-invalid" : "")} type="text" name="emgConNo" onChange={this.handleChange} />
                                    {this.state.formErrors.emgConNo && <span className="formError">{this.state.formErrors.emgConNo}</span>}
                                </div>

                                <div className="form-group">
                                    <label> Relationship To Child  </label>
                                    <select value={this.state.customer.emgConRel} className={"form-control col-md-6" + (this.state.formErrors.emgConRel ? " is-invalid" : this.state.customer.emgConRel ? " is-valid" : "")} type="text" name="emgConRel" onChange={this.handleChange} >
                                        <option value="" disabled>Please select</option>
                                        <option value="AUNT">Aunt</option>
                                        <option value="UNCLE">Uncle</option>
                                        <option value="GRANDMOTHER">Grandmother</option>
                                        <option value="GRANDFATHER">Grandfather</option>
                                        <option value="OTHERFAMILY">Other Family</option>
                                        <option value="FAMILYFRIEND">Family Friend</option>
                                        <option value="OTHER">Other</option>
                                    </select>

                                    {this.state.formErrors.emgConRel && <span className="formError">{this.state.formErrors.emgConRel}</span>}
                                </div>
                            </div>


                            <hr />
                            <h4 className="header-customerform"> Pets </h4>
                            <div className="form-group">
                                <textarea placeholder="E.g. Details of any pets..." value={this.state.customer.pets} className={"form-control" + (this.state.formErrors.pets ? " is-invalid" : this.state.customer.pets ? " is-valid" : "")} type="text" name="pets" onChange={this.handleChange} rows="4" />
                                {this.state.formErrors.pets && <span className="formError">{this.state.formErrors.pets}</span>}
                            </div>

                            <hr />

                            <h4 className="header-customerform"> Any Other Requirements </h4>
                            <div className="form-group">
                                <textarea placeholder="E.g. What do you expect/value in a babysitter or is there anything else we need to know?" value={this.state.customer.extraNotes} className={"form-control" + (this.state.formErrors.extraNotes ? " is-invalid" : this.state.customer.extraNotes ? " is-valid" : "")} type="text" name="extraNotes" onChange={this.handleChange} rows="5" />
                                {this.state.formErrors.extraNotes && <span className="formError">{this.state.formErrors.extraNotes}</span>}
                            </div>
                            <br></br>
                            <hr />
                            <br></br>
                            <h4 className="header-customerform">Admin Only Notes</h4>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label>Notes only visible to admins</label>
                                    <textarea value={this.state.customer.adminNotesAdmin} className="form-control" type="text" name="adminNotesAdmin" onChange={this.handleChange} rows="5" />
                                </div>
                                <div className="form-group col-md-6">
                                    <label>Notes sent to babysitters before job</label>
                                    <textarea value={this.state.customer.adminNotesBabysitter} className="form-control" type="text" name="adminNotesBabysitter" onChange={this.handleChange} rows="5" />
                                </div>
                            </div>

                            <div>
                                <button disabled={this.state.processing} className="btn submit-btn" type="submit" value="Submit" onClick={this.handleSubmit}>{this.state.processing ? "Processing..." : this.props.username ? "Update Account" : "Create Account"}</button>
                            </div>

                            <br></br>
                            <br></br>
                            <div>
                                {this.state.hasSubmitErrors && <p style={{ float: "right", fontSize: 16 }} className="formError">Please fix the errors highlighted in the form.</p>}
                                {this.state.submitError && <p style={{ float: "right", fontSize: 16 }} className="formError">{this.state.submitError}</p>}
                            </div>
                        </div>

                    </form>

                    {this.props.username &&
                        <div style={{ maxHeight: 650, margin: '0 auto', backgroundColor: '#FFF', marginTop: 50, width: "100%", maxWidth: '920px', whiteSpace: "nowrap", boxShadow: "0 0 30px #d6d6d6" }}>
                            <h4 style={{ paddingLeft: 20, paddingTop: 15 }}>Revision History</h4>
                            {this.state.revisions.length > 0 ?
                                <div style={{ margin: '20px', maxHeight: 450, overflow: "scroll" }}>
                                    <table className="table table-bordered table-hover" style={{ width: '100%', borderCollapse: "collapse" }}>
                                        <thead>
                                            <tr>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Date</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Action</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Editor</th>

                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Family Surname</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Email</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Membership End</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Emg First</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Emg Last</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Emg Phone</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Emg Relation</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Pets</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Other Requirements</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Disabled</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Admin Notes (Admin)</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Admin Notes (Babysitter)</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Caregivers <span style={{ fontWeight: "normal", fontSize: 10 }}>(changes not highlighted)</span></th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Children <span style={{ fontWeight: "normal", fontSize: 13 }}>(changes not highlighted)</span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.revisions.map((value, index) => {
                                                    return (
                                                        <tr key={value.revisionId}>
                                                            <td>{new Date(Date.parse(value.revisionDate)) + ""}</td>
                                                            <td>{value.action}</td>
                                                            <td>{value.editor}</td>

                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.familySurname !== value.entity.familySurname ? "text-info" : ""}>{value.entity.familySurname}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.email !== value.entity.email ? "text-info" : ""}>{value.entity.email}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.membershipEndDate !== value.entity.membershipEndDate ? "text-info" : ""}>{value.entity.membershipEndDate && value.entity.membershipEndDate.split("T")[0]}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.emgConFName !== value.entity.emgConFName ? "text-info" : ""}>{value.entity.emgConFName}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.emgConLName !== value.entity.emgConLName ? "text-info" : ""}>{value.entity.emgConLName}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.emgConNo !== value.entity.emgConNo ? "text-info" : ""}>{value.entity.emgConNo}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.emgConRel !== value.entity.emgConRel ? "text-info" : ""}>{value.entity.emgConRel}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.pets !== value.entity.pets ? "text-info" : ""}>{value.entity.pets.split("\n").map((value, index) => {
                                                                return <span key={index}>{value}<br /></span>
                                                            })}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.extraNotes !== value.entity.extraNotes ? "text-info" : ""}>{value.entity.extraNotes.split("\n").map((value, index) => {
                                                                return <span key={index}>{value}<br /></span>
                                                            })}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.disabled !== value.entity.disabled ? "text-info" : ""}>{value.entity.disabled ? "Yes" : "No"}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.adminNotesAdmin !== value.entity.adminNotesAdmin ? "text-info" : ""}>{value.entity.adminNotesAdmin && value.entity.adminNotesAdmin.split("\n").map((value, index) => {
                                                                return <span key={index}>{value}<br /></span>
                                                            })}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.adminNotesBabysitter !== value.entity.adminNotesBabysitter ? "text-info" : ""}>{value.entity.adminNotesBabysitter && value.entity.adminNotesBabysitter.split("\n").map((value, index) => {
                                                                return <span key={index}>{value}<br /></span>
                                                            })}</td>
                                                            <td>
                                                                <table>
                                                                    <thead>
                                                                        <tr>
                                                                            <th>First Name</th>
                                                                            <th>Last Name</th>
                                                                            <th>Phone</th>
                                                                            <th>Street Address</th>
                                                                            <th>Address 2</th>
                                                                            <th>City</th>
                                                                            <th>Region</th>
                                                                            <th>Post Code</th>
                                                                            <th>Address Notes</th>
                                                                            <th>Relationship</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {this.state.revisions[index].entity && this.state.revisions[index].entity.caregivers.map((value, index) => {
                                                                            return <tr key={index}>
                                                                                <td>{value.firstName}</td>
                                                                                <td>{value.lastName}</td>
                                                                                <td>{value.phoneNumber}</td>
                                                                                <td>{value.streetAddress}</td>
                                                                                <td>{value.addressLine2}</td>
                                                                                <td>{value.city}</td>
                                                                                <td>{value.region}</td>
                                                                                <td>{value.postCode}</td>
                                                                                <td>{value.addressNotes.split("\n").map((value, index) => {
                                                                                    return <span key={index}>{value}<br /></span>
                                                                                })}</td>
                                                                                <td>{value.relationshipToChild}</td>
                                                                            </tr>
                                                                        })}
                                                                    </tbody>
                                                                </table>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <thead>
                                                                        <tr>
                                                                            <th>First Name</th>
                                                                            <th>Last Name</th>
                                                                            <th>Gender</th>
                                                                            <th>DOB</th>
                                                                            <th>Medical</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {this.state.revisions[index].entity && this.state.revisions[index].entity.children.map((value, index) => {
                                                                            return <tr key={index}>
                                                                                <td>{value.firstName}</td>
                                                                                <td>{value.lastName}</td>
                                                                                <td>{value.gender}</td>
                                                                                <td>{value.dob.split("T")[0]}</td>
                                                                                <td>{value.medical.split("\n").map((value, index) => {
                                                                                    return <span key={index}>{value}<br /></span>
                                                                                })}</td>
                                                                            </tr>
                                                                        })}
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                                :
                                <p>No revision history available</p>
                            }
                        </div>
                    }
                </div>
            </div >
        );
    }
}

export default withRouter(AdminCustomerFormPage);