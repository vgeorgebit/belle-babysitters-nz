import React from 'react';
import api, { ax } from '../api.js';
import * as constants from '../constants.js';
import { withRouter } from 'react-router-dom';

class CustomerNavigation extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            accountDetails: {}
        };

        ax.get(constants.API_BASE_URL + "customer/owndetails").then(response => {
            this.setState({
                accountDetails: response.data
            });
        }).catch(error => {
            api.clearTokens();
            this.props.history.replace("/customer_login");
        });
    }

    logout() {
        api.clearTokens();
    };

    render() {

        return (
            <nav className="navbar navbar-expand-md navbar-light bg-light">
                <a href="/admin"> <img alt="Belle Babysitters Logo" className="navbar-brand" src="/logo_menu.jpg" width="auto" height="40" /></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/customer">Home</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/customer/jobs/form">Create Booking</a>
                        </li>
                        <li className="navbar-nav mr-auto">
                            <a className="nav-link" href="/customer/enquiry">Make An Enquiry</a>
                        </li>
                    </ul>
                    <div className="nav-item dropdown">
                        <a className="navProfile nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fa fa-user-circle fa-lg"></i>
                            <span>{this.state.accountDetails.familySurname + " Family"}</span>
                        </a>
                        <div className="dropdown-menu dropdown-menu-right" id="" aria-labelledby="navbarDropdown">
                            <a className="dropdown-item disabled text-secondary"><small>{this.state.accountDetails.email}</small></a>
                            <a className="dropdown-item" href="/customer/account">Account</a>
                            <a className="dropdown-item" href="/customer/help">Help</a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="/customer_login" onClick={this.logout} >Logout</a>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

export default withRouter(CustomerNavigation);