import React from 'react';
import { withRouter } from "react-router-dom";
import * as constants from '../constants';
import { ax } from './../api.js';

const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

class PasswordResetRequest extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            errorMessage: null,
            submitting: false,
            validationMsg: "",
            success: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        }, () => {
            let validMsg = null;
            if (!emailRegex.test(this.state.email)) {
                validMsg = "Invalid email address";
            }
            this.setState({
                validationMsg: validMsg
            });
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        // TODO: Validate
        console.log(this.state);

        const formData = new FormData();
        formData.set("email", this.state.email);
        this.setState({
            submitting: true
        });

        ax.post(constants.PUBLIC_API_BASE_URL + this.props.userType + "/passwordreset", formData).then(response => {

            this.setState({
                submitting: false,
                success: true
            });
        }).catch(error => {
            let msg = '';
            if (error.response && error.response.data && error.response.data.constraintViolations) {
                error.response.data.constraintViolations.map(v => {
                    msg += v + '  ';
                });
            } else if (error.response && error.response.data && error.response.data.error_description) {
                msg = error.response.data.error_description;
            }
            else {
                msg = "An unknown error occurred";
            }

            this.setState({
                errorMessage: msg,
                submitting: false
            });
        });
    }

    render() {

        return (
            <div className="container passwordResetContainer" >
                <h1> Reset Password</h1>
                {this.state.success ? <div><p>Password reset successfully requested.</p><p>A password reset link has been emailed to you. This link will be valid for 12 hours.</p></div> :
                    <div>
                        <p>Please enter the email address associated with your account</p>
                        <form onSubmit={this.handleSubmit}>
                            <div>
                                <input type="email" disabled={this.state.submitting} name="email" value={this.state.email} onChange={this.handleChange.bind(this)} className={"form-control" + (this.state.validationMsg == null ? " is-valid" : this.state.validationMsg.length > 0 ? " is-invalid" : "")} placeholder="email" />
                                {<p style={{ color: "red", fontSize: 12, height: 20 }}>{this.state.validationMsg}</p>}
                            </div>
                            <button type="submit" disabled={this.state.submitting || this.state.validationMsg != null} className="btn btn-primary btn-block">{this.state.submitting ? 'Processing...' : 'Request Reset'}</button>
                        </form>
                        {this.state.errorMessage ? <p className="alert alert-danger">{this.state.errorMessage}</p> : null}
                    </div>
                }
            </div>
        );
    }
}

export default withRouter(PasswordResetRequest);