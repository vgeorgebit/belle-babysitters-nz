import React from 'react';
import api, { ax } from '../api.js';
import * as constants from '../constants.js';


class ItemListTable extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            page: {},
            loading: true,
            pageNum: 0,
            search: '',
            show: false,
            jobDisplay : {},
            
        };

        ax.get(constants.API_BASE_URL + this.props.url+ "?page=0&filterBy=" + this.props.filter).then(response => {
            this.setState({
                page: response.data,
                loading: false
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });;

        this.loadPage = this.loadPage.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.respondOffer = this.respondOffer.bind(this);
    }

    loadPage(pageNum) {
        this.setState({
            loading: true
        });

        ax.get(constants.API_BASE_URL + this.props.url +"?page=" + pageNum + "&filterBy=" + this.props.filter).then(response => {
            this.setState({
                page: response.data,
                loading: false,
                pageNum: pageNum
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });;
    }

    handleSearch(event) {
        this.setState({
            search: event.target.value,
            pageNum: 0
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }

    showModal(e,job){
        this.setState({ 
            jobDisplay: job,
            show:true
        });
      };
    
    
    hideModal = (e) => {
        this.setState({ 
            show: false
         });
      };

    respondOffer(e, id, boolean){
        ax.post(constants.API_BASE_URL + "jobs/respond/"+id + "?accepted="+boolean).then(response => {
            this.setState({ 
                show: false
            });
            window.location.reload()
        }).catch(e =>{
            alert("ERROR OCCURED");
        });; 

    };

/**
 * Kurt Weston was here
 */
    render() {
        return (
        <div>
            <div className="container adminsContainer" style={{maxWidth: 1100}}>
                    <div className="row">
                        <div className="col">
                            <h1>
                            {this.props.filter == "PENDING" && <i style={{color:"Blue"}} className="fa fa-info-circle" aria-hidden="true" title="Available Jobs">&nbsp;</i>}                                
                            {this.props.filter == "AVAILABLE" && <i style={{color:"Green"}} className="fa fa-check" aria-hidden="true" title="Accepted Jobs">&nbsp;</i>}                                
                            {this.props.filter == "UNAVAILABLE" && <i style={{color:"Red"}} className="fa fa-times" aria-hidden="true" title="Declined Jobs">&nbsp;</i>}                                
                            {this.props.title}
                            
                            </h1>
                        </div>
                    </div>
                    {!this.props.noSearch &&
                    <div className="row">
                         <div className="col">
                            <input onChange={this.handleSearch} className="form-control" type="text" placeholder="Search" />
                        </div>
                    </div>
                    }
                    <div>
                        {
                            this.state.loading ?
                                (
                                    <div className="spinner-border text-primary" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                ) :
                                <div>
                                    <table className="table">
                                        <thead>
                                            <tr >
                                                <th>Job ID</th>
                                                <th style={{width:"20%"}}>Start Date</th>
                                                <th style={{width:"20%"}}>End Date</th>
                                                <th style={{width:"30%"}}>Children Details</th>
                                                <th style={{width:"10%"}}>Requirements</th>
                                                <th style={{width:"30%"}}></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        {   
                                            this.state.page.content.map(offer => {
                                                return (
                                                    <tr key={offer.id} onClick={ e=> {this.showModal(e,offer.job)}} className="pleaseDontHover">
                                                        
                                                        <td style={{color:"gray"}}>#{offer.job.id}</td>
                                                        <td>{offer.job.startTime.split("-")[2].substring(0,2)+"-"+offer.job.startTime.split("-")[1]+"-"+offer.job.startTime.split("-")[0]} <br/> <span className="text-primary">{offer.job.startTime.substring(12,17)}</span></td>
                                                        <td>{offer.job.endTime.split("-")[2].substring(0,2)+"-"+offer.job.endTime.split("-")[1]+"-"+offer.job.endTime.split("-")[0]} <br/> <span className="text-primary" >{offer.job.endTime.substring(12,17)}</span></td>
                                                        <td>{offer.job.childrenDetails.split("\n").map((child, index) => {
                                                                return <span key={index} style={{display: "block"}}>
                                                                {child} 
                                                                
                                                                
                                                                </span>
                                                        })}
                                                        
                                                        
                                                        </td>
                                                        <td>
                                                            <tr style={{display:"inline"}}>
                                                                <td style={{all:"unset", display:"inline"}}>    
                                                                    {offer.job.fullLicenseRequired && <i className="fa fa-id-card-o" aria-hidden="true" title="Full Drivers License Required">&nbsp;</i>} 
                                                                    
                                                                </td>
                                                                <td style={{all:"unset", display:"inline"}}> 
                                                                    {offer.job.ownCarRequired && <i style={{color:"Green"}} className="fa fa-car" aria-hidden="true" title="Vehicle Required">&nbsp;</i>}
                                                                </td>
                                                                
                                                            </tr>
                                                        </td>    
                                                        <td>
                                                            { (this.props.filter === "UNAVAILABLE" || this.props.filter === "PENDING") &&
                                                            <label onClick={(e) => {this.respondOffer(e, offer.id, true)}} className={"btn btn-"+(this.props.filter === "PENDING" ? "success" : "secondary")} style={{ margin:0, float:"none", textAlign:"Center",  height:"100%", width:"46%", border:"none", cursor:"pointer"}}><i style={{color:"White"}} className="fa fa-check" aria-hidden="true" title="Accept Job">&nbsp;&nbsp;</i></label>
                                                            }
                                                            {   
                                                            (this.props.filter === "AVAILABLE" || this.props.filter === "PENDING") &&
                                                            <label onClick={(e) => {this.respondOffer(e, offer.id, false)}} className={"btn btn-"+(this.props.filter === "PENDING" ? "danger" : "secondary")} style={{ margin:0, marginLeft:5, float:"none", textAlign:"Center", height:"100%",  width:"46%", border:"none", cursor:"pointer"}}><i style={{color:"White"}} className="fa fa-times" aria-hidden="true" title="Decline Job">&nbsp;&nbsp;</i></label>
                                                            
                                                            }
                                                            
                                                        </td>

                                                        
                                                    
                                                    </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <center>
                                        {!this.state.page.first && <span><a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum - 1) }} href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>}
                                        <span>Page {this.state.pageNum + 1} of {this.state.page.totalPages}</span>
                                        {!this.state.page.last && <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum + 1) }} href="">Next</a></span>}
                                    </center>
                                </div>
                        }
                    </div>
                </div>
                {false &&  this.state.show && this.state.jobDisplay &&
                <div className="blackout" >
                <div className="modalContainer" >
                    
                    <div className="row">
                        
                        <div className="col col-12">
                            <button style={{height:'auto', width:'100%'}} type="button" className="btn btn-danger" onClick={this.hideModal}>Back to list...</button>
                        </div>
                    </div>
                    <div className="modalContent" >
                        <table style={{width:"100%", marginTop:30, marginBottom:20, marginLeft:"auto", marginRight:"auto", textAlign:"center"}}>

                                    
                            <tr style={{width:"100%"}}>
                                <td>
                                
                                    <label onClick={(e) => {this.respondOffer(e, this.state.jobDisplay.id, true)}} className="btn btn-success" style={{ margin:0, float:"none", textAlign:"Center",  height:"100%", width:200, border:"none", cursor:"pointer"}}><i style={{color:"White"}} className="fa fa-check" aria-hidden="true" title="Accepted Jobs">&nbsp;&nbsp;</i>Available</label>
                                </td>
                                <td>
                                    <label onClick={(e) => {this.respondOffer(e, this.state.jobDisplay.id, false)}} className="btn btn-warning" style={{ margin:0, float:"none", textAlign:"Center", height:"100%", width:200, border:"none", cursor:"pointer"}}><i style={{color:"White"}} className="fa fa-times" aria-hidden="true" title="Declined Jobs">&nbsp;&nbsp;</i>Unavailable</label>
                                </td>
                            </tr>
                        </table>
                        
                    <table className="table">
                       
                        <tbody>
                            <tr><th>ID</th><td>#{this.state.jobDisplay.id}</td></tr>
                            <tr><th>Start Date and Time</th><td>{this.state.jobDisplay.startTime.split("-")[2].substring(0,2)+"-"+this.state.jobDisplay.startTime.split("-")[1]+"-"+this.state.jobDisplay.startTime.split("-")[0]} <br/> <span className="text-primary">{this.state.jobDisplay.startTime.substring(12,17)}</span></td></tr>
                            <tr><th>End Date and Time</th><td>{this.state.jobDisplay.endTime.split("-")[2].substring(0,2)+"-"+this.state.jobDisplay.endTime.split("-")[1]+"-"+this.state.jobDisplay.endTime.split("-")[0]} <br/> <span className="text-primary" >{this.state.jobDisplay.endTime.substring(12,17)}</span></td></tr>
                            <tr><th>Family Surname</th><td>{this.state.jobDisplay.customer.familySurname}</td></tr>
                            <tr><th>Number of Children</th><td>{this.state.jobDisplay.numChildren}</td></tr>
                            
                        </tbody>
                    </table>

                </div>
                </div>
                </div>

                }
        </div>
        )
    }



}


export default ItemListTable;