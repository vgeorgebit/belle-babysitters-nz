import React from 'react';
import { Redirect } from 'react-router-dom';

import ChildrenForm from './ChildrenForm.js';
import { withRouter } from 'react-router-dom';

import { ax } from '../api.js'
import * as constants from '../constants.js'

const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const textRegex = RegExp(
    /^[A-Za-z\s\\-]*$/
);

const addressRegex = RegExp(
    // /^[a-zA-Z0-9,.\\-\\s]*$/
    /^[a-zA-Z0-9\s,.'-]*$/
);

const numberRegex = RegExp(
    /^\d+$/
);

const passwordRegex = RegExp(
    /^[0-9A-Za-z!@#$%^&*/_-]+$/
);

const postCodeRegex = RegExp(
    /^[0-9\-A-Za-z]+$/
);

class CustomerCustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            accountCreationSuccess: null,
            customer: {
                familySurname: '',
                email: '',
                membershipEndDate: '',
                password: '',
                howDidYouHear: '',
                caregivers: [
                    {
                        firstName: '',
                        lastName: '',
                        phoneNumber: '',
                        streetAddress: '',
                        addressLine2: '',
                        city: '',
                        region: '',
                        postCode: '',
                        addressNotes: '',
                        relationshipToChild: '',

                    }
                ],
                emgConFName: '', emgConLName: '', emgConNo: '', emgConRel: '',
                extraNotes: '',
                pets: '',
                adminNotesAdmin: '',
                adminNotesBabysitter: '',
                disabled: false,
                children: [
                    { firstName: "", lastName: "", gender: "", dob: "", medical: "" },
                ],
            },
            formErrors: {
                familySurname: '',
                email: '',
                membershipEndDate: '',
                password: '',
                howDidYouHear: '',
                caregivers: [
                    {
                        firstName: '',
                        lastName: '',
                        phoneNumber: '',
                        streetAddress: '',
                        addressLine2: '',
                        city: '',
                        region: '',
                        postCode: '',
                        addressNotes: null,
                        relationshipToChild: null,
                    }, {
                        firstName: '',
                        lastName: '',
                        phoneNumber: '',
                        streetAddress: '',
                        addressLine2: '',
                        city: '',
                        region: '',
                        postCode: '',
                        addressNotes: null,
                        relationshipToChild: null,
                    }],
                emgConFName: '', emgConLName: '', emgConNo: '', emgConRel: '',
                extraNotes: '',
                pets: '',
                children: [{ firstName: "", lastName: "", gender: "", dob: "", medical: "" }],
            },
            processing: false,
            hasSubmitErrors: false,
            submitError: '',
            redirect: false,
            revisions: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.addCaregiver = this.addCaregiver.bind(this);
        this.handleEditCaregiver = this.handleEditCaregiver.bind(this);
        this.validateAndUpdateCaregivers = this.validateAndUpdateCaregivers.bind(this);
        this.validate = this.validate.bind(this);

        this.childrenForm = React.createRef();
        this.autofillAddress = this.autofillAddress.bind(this);

        if (this.props.editMode) {
            ax.get(constants.API_BASE_URL + "customer/owndetails").then(response => {

                let formErrors = this.state.formErrors;
                formErrors.caregivers = [];
                response.data.caregivers.forEach(() => {
                    formErrors.caregivers.push({
                        firstName: '',
                        lastName: '',
                        phoneNumber: '',
                        streetAddress: '',
                        addressLine2: '',
                        city: '',
                        region: '',
                        postCode: '',
                        addressNotes: null,
                        relationshipToChild: null,
                    });
                });

                this.setState({
                    formErrors: formErrors,
                    customer: response.data,
                }, () => {
                    let childrenFormErrors = this.childrenForm.current.state.formErrors;
                    childrenFormErrors.children = [];
                    response.data.children.forEach(() => {
                        childrenFormErrors.children.push({
                            firstName: "", lastName: "", gender: "", dob: "", medical: ""
                        })
                    });
                    this.childrenForm.current.setState({
                        formErrors: childrenFormErrors,
                        children: response.data.children
                    });
                });
            }).catch(error => {
                this.props.history.replace("/customer");
            });
        }
    }

    handleSubmit = event => {
        event.preventDefault();

        this.validateAndUpdateCaregivers(this.state.customer.caregivers, null, null, () => {
            this.childrenForm.current.validateAndUpdateChildren(this.childrenForm.current.state.children, null, null, () => {
                let fe = this.state.formErrors;
                fe.children = this.childrenForm.current.state.formErrors.children;

                this.setState({
                    formErrors: fe,
                    submitError: false,
                    hasSubmitErrors: false
                }, () => {

                    this.validate(null, () => {
                        let errors = false;

                        Object.keys(this.state.formErrors).forEach((field) => {
                            let value = this.state.formErrors[field];
                            if (field === "caregivers" || field === "children")
                                return;
                            if (!(value === null || value.length === 0)) {
                                errors = true;
                            }
                        });

                        for (const index in this.state.formErrors.caregivers) {
                            for (const field in this.state.formErrors.caregivers[index]) {
                                if (this.state.formErrors.caregivers[index][field] != null) {
                                    errors = true;
                                }
                            }
                        }

                        for (const index in this.state.formErrors.children) {
                            for (const field in this.state.formErrors.children[index]) {
                                if (this.state.formErrors.children[index][field] != null) {
                                    errors = true;
                                }
                            }
                        }

                        this.setState({
                            hasSubmitErrors: errors
                        });

                        if (!errors) {
                            this.setState({
                                processing: true
                            });
                            if (this.props.editMode) {
                                ax.put(constants.API_BASE_URL + "customer/owndetails", this.state.customer).then(response => {
                                    // redirect to customer page
                                    this.setState({
                                        redirect: true
                                    });
                                }).catch(error => { // if there is an error
                                    let msg = '';
                                    if (error.response && error.response.data && error.response.data.constraintViolations != null && error.response.data.constraintViolations.length > 0) {
                                        error.response.data.constraintViolations.forEach(v => {
                                            msg += v + '  ';
                                        });
                                    } else if (error.response && error.response.data) {
                                        msg = error.response.data.error_description;
                                    } else {
                                        msg = "An unknown error occurred";
                                    }

                                    this.setState({
                                        submitError: msg,
                                        processing: false
                                    });
                                });
                            } else {
                                // Submitting the form to backend
                                ax.post(constants.PUBLIC_API_BASE_URL + "customer", this.state.customer).then(response => {
                                    // redirect to customer page
                                    this.setState({
                                        accountCreationSuccess: true
                                    });
                                }).catch(error => { // if there is an error
                                    let msg = '';
                                    if (error.response && error.response.data && error.response.data.constraintViolations != null && error.response.data.constraintViolations.length > 0) {
                                        error.response.data.constraintViolations.forEach(v => {
                                            msg += v + '  ';
                                        });
                                    } else if (error.response && error.response.data) {
                                        msg = error.response.data.error_description;
                                    } else {
                                        msg = "An unknown error occurred";
                                    }

                                    this.setState({
                                        submitError: msg,
                                        processing: false
                                    });
                                });
                            }
                        }
                    });
                });
            });
        });


    }

    validate(name = null, callback = null) {
        let formErrors = this.state.formErrors;

        Object.keys(this.state.customer).forEach(field => {
            if (field === "children" || field === "caregivers")
                return;

            if (name === null || name === field) {
                let value = this.state.customer[field];

                switch (field) {
                    case "familySurname":
                        formErrors.familySurname =
                            value.length < 1 ? "Family surname is required" :
                                !textRegex.test(value) ? "Family surname may only contain alphabetic characters" :
                                    value.length > 64 ? "Family surname is too long" :
                                        null;
                        break;
                    case "email":
                        formErrors.email =
                            value.length < 1 ? "Email address is required" :
                                !emailRegex.test(value) ? "Email address must be valid" :
                                    value.length > 255 ? "Email address is too long" :
                                        null;
                        break;
                    case "extraNotes":
                        formErrors.extraNotes =
                            value === null || value.length === 0 ? null :
                                value.length > 500 ? "Other requirements are too long" :
                                    null;
                        break;
                    case "pets":
                        formErrors.pets =
                            value === null || value.length < 1 ? "Details of pets are required" :
                                value.length > 500 ? "Details of pets is too long" :
                                    null;
                        break;
                    case "emgConFName":
                        formErrors.emgConFName =
                            value.length < 1 ? "First name is required" :
                                !textRegex.test(value) ? "First name may only contain alphabetic characters" :
                                    value.length > 64 ? "First name is too long" :
                                        null;
                        break;
                    case "emgConLName":
                        formErrors.emgConLName =
                            value.length < 1 ? "Last name is required" :
                                !textRegex.test(value) ? "Last name may only contain alphabetic characters" :
                                    value.length > 64 ? "Last name is too long" :
                                        null;
                        break;
                    case "emgConNo":
                        formErrors.emgConNo =
                            value.length < 1 ? "Phone number is required" :
                                !numberRegex.test(value) ? "Phone number may only contain digits" :
                                    value.length > 15 ? "Phone number is too long" :
                                        null;
                        break;
                    case "membershipEndDate":
                        formErrors.membershipEndDate = null;
                        break;
                    case "emgConRel":
                        formErrors.emgConRel =
                            value === null || value.length === 0 ? "Relationship to child is required." :
                                null
                        break;
                    case "howDidYouHear":
                        formErrors.howDidYouHear = 
                            value === null || value.length === 0 ? "How you heard about us is required" :
                            null;
                        break;
                    case "password":
                        formErrors.password =
                            !value ? null :
                                value.length < 6 ? "Password is too short" :
                                    value.length > 32 ? "Password is too long" :
                                        !passwordRegex.test(value) ? "Password may only contain alphanumeric characters and !@#$%^&*/_-" :
                                            null;
                        break;
                    default:
                        break;
                }
            }

        });

        this.setState({
            formErrors: formErrors
        }, () => {
            if (callback)
                callback();
        });


    }

    handleChange(event) {
        event.preventDefault();
        const { name, value } = event.target;

        let c = this.state.customer;
        c[name] = value;

        this.setState({
            customer: c,
            hasSubmitErrors: false,
            submitError: null
        }, () => {
            this.validate(name);
        });
    }

    handleCheckChange(event) {
        let cust = this.state.customer;
        cust.disabled = event.target.checked;
        this.setState({
            customer: cust
        }, () => {
            console.log(this.state.customer);
        });
    }

    handleEditCaregiver(event, index, field) {
        let c = this.state.customer.caregivers;
        c[index][field] = event.target.value;

        this.validateAndUpdateCaregivers(c, index, field);

    }

    validateAndUpdateCaregivers(caregivers, restrictIndex = null, restrictName = null, callback = null) {
        let caregiverErrors = [];

        caregivers.forEach((cg, index) => {
            let errors = this.state.formErrors.caregivers[index] ? this.state.formErrors.caregivers[index] : {
                firstName: '',
                lastName: '',
                phoneNumber: '',
                streetAddress: '',
                addressLine2: '',
                city: '',
                region: '',
                postCode: '',
                addressNotes: '',
                relationshipToChild: '',
            };

            if (restrictIndex === null || restrictIndex === index) {
                // Check errors
                for (let field in cg) {
                    let value = cg[field];

                    if (restrictName === null || restrictName === field) {

                        switch (field) {
                            case "firstName":
                                errors.firstName =
                                    value.length < 1 ? "First name is required" :
                                        !textRegex.test(value) ? "First name may only contain alphabetic characters" :
                                            value.length > 64 || value.length < 1 ? "Caregiver first name is too long" :
                                                null;
                                break;
                            case "lastName":
                                errors.lastName =
                                    value.length < 1 ? "Last name is required" :
                                        !textRegex.test(value) ? "Last name may only contain alphabetic characters" :
                                            value.length > 64 || value.length < 1 ? "Last name is too long" :
                                                null;
                                break;
                            case "phoneNumber":
                                errors.phoneNumber =
                                    value.length < 1 ? "Phone number is required" :
                                        !numberRegex.test(value) ? "Phone number may only contain digits" :
                                            value.length > 15 || value.length < 1 ? "Phone number is too long" :
                                                null;
                                break;
                            case "streetAddress":
                                errors.streetAddress =
                                    value.length < 1 ? "Street address is required" :
                                        !addressRegex.test(value) ? "Street address contains invalid characters" :
                                            value.length > 64 ? "Street address is too long" :
                                                null;
                                break;
                            case "addressLine2":
                                errors.addressLine2 =
                                    value.length < 1 ? "Suburb is required" :
                                        !textRegex.test(value) ? "Suburb contains invalid characters" :
                                            value.length > 64 ? "Suburb is too long" :
                                                null;
                                break;
                            case "city":
                                errors.city =
                                    value.length < 1 ? "City name is required" :
                                        !textRegex.test(value) ? "City name may only contain alphabetic characters" :
                                            value.length > 64 ? "City name is too long" :
                                                null;
                                break;
                            case "region":
                                errors.region =
                                    value.length < 1 ? "Region name is required" :
                                        !textRegex.test(value) ? "Region name may only contain alphabetic characters" :
                                            value.length > 64 ? "Region name is too long" :
                                                null;
                                break;
                            case "postCode":
                                errors.postCode =
                                    value.length < 1 ? "Post code is required" :
                                        !postCodeRegex.test(value) ? "Post code may only contain alphanumeric characters." :
                                            value.length < 4 ? "Post code is too short" :
                                                value.length > 10 ? "Post code is too long" :
                                                    null;
                                break;
                            case "addressNotes":
                                errors.addressNotes =
                                    value === null || value.length === 0 ? null :
                                        value.length < 500 ? null :
                                            "Address notes are too long";
                                break;
                            case "relationshipToChild":
                                errors.relationshipToChild =
                                    value === null || value.length === 0 ? "Relationship to child is required." :
                                        null
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            caregiverErrors.push(errors);
        });

        let customer = this.state.customer;
        customer.caregivers = caregivers;

        let formErrors = this.state.formErrors;
        formErrors.caregivers = caregiverErrors;

        this.setState({
            customer: customer,
            formErrors: formErrors
        }, () => {
            if (callback)
                callback();
        });
    }

    removeCaregiver(event, index) {
        event.preventDefault();
        if (this.state.customer.caregivers.length > 1 && window.confirm("Are you sure you want to remove this caregiver?")) {
            let customer = this.state.customer;
            customer.caregivers.splice(index, 1);
            this.setState({
                customer
            }, () => {
                this.validateAndUpdateCaregivers(this.state.customer.caregivers);
            });
        }
    }

    addCaregiver(event) {
        event.preventDefault();
        let caregivers = this.state.customer.caregivers;
        caregivers.push({
            firstName: '',
            lastName: '',
            phoneNumber: '',
            streetAddress: '',
            addressLine2: '',
            city: '',
            region: '',
            postCode: '',
            addressNotes: '',
            relationshipToChild: '',
        });

        this.validateAndUpdateCaregivers(caregivers, this.state.customer.caregivers.length);
    }

    autofillAddress(event, srcIndex, destIndex) {
        event.preventDefault();

        let cust = this.state.customer;
        cust.caregivers[destIndex].streetAddress = cust.caregivers[srcIndex].streetAddress;
        cust.caregivers[destIndex].addressLine2 = cust.caregivers[srcIndex].addressLine2;
        cust.caregivers[destIndex].city = cust.caregivers[srcIndex].city;
        cust.caregivers[destIndex].region = cust.caregivers[srcIndex].region;
        cust.caregivers[destIndex].postCode = cust.caregivers[srcIndex].postCode;
        cust.caregivers[destIndex].addressNotes = cust.caregivers[srcIndex].addressNotes;

        this.setState({
            caregivers: cust
        });
    }

    render() {
        if (this.state.accountCreationSuccess) {
            return (
                <div className="container setupContainer" >
                    <img src="/logo.jpg" className="img-fluid center" alt="Belle Babysitters Logo" />
                    <div id="setupSuccess">
                        <h2>Thank you for signing up with Belle Babysitters!</h2>
                        <br/>
                        <p>Please check your email to setup your account password and begin making bookings.</p>
                    </div>
                </div>
            );
        }
        return (
            <div>
                {
                    this.state.redirect && <Redirect to={{
                        pathname: "/customer/account"
                    }} />
                }

                <div className="formContainer">

                    <form className="card">
                        <div className="card-header">
                            {this.props.editMode && 
                                <div style={{width: "100%", textAlign: "right"}}>
                                    <button className="btn btn-danger" style={{float: "none"}} onClick={() => {this.props.history.push("/customer/account")}}>Cancel</button>
                                </div>
                            }
                            <h1 className="customer-header">{this.props.editMode ? "Modify Customer Account" : "Customer Sign Up"} </h1>
                        </div>
                        <div className="card-body">
                            <h4>Account Details</h4>
                            <div className="form-group">
                                <div className="row">
                                    <div className="col-md-6">
                                        <label>Family Surname</label>
                                        <input disabled={this.props.editMode ? true : false} type="text" name="familySurname" className={"form-control" + (this.state.formErrors.familySurname === null ? " is-valid" : this.state.formErrors.familySurname ? " is-invalid" : "")} value={this.state.customer.familySurname} onChange={this.handleChange} />
                                        {this.state.formErrors.familySurname && <span className="formError">{this.state.formErrors.familySurname}</span>}
                                    </div>

                                </div>
                            </div>
                            <div style={{ paddingLeft: 0 }} className="form-group col-md-6">
                                <label> Email Address  </label>
                                {/* <input disabled={this.props.editMode ? true : false} type="email" name="email" className={"form-control placeholder" + (this.state.formErrors.email === null ? " is-valid" : this.state.formErrors.email ? " is-invalid" : "")} value={this.state.customer.email} onChange={this.handleChange} placeholder="name@example.com" /> */}
                                <input disabled={this.props.editMode ? true : false} type="email" name="email" className={"form-control placeholder" + (this.state.formErrors.email === null ? " is-valid" : this.state.formErrors.email ? " is-invalid" : "")} value={this.state.customer.email} onChange={this.handleChange} placeholder="name@example.com" />
                                {this.state.formErrors.email && <span className="formError">{this.state.formErrors.email}</span>}
                            </div>

                            {!this.props.editMode &&
                                <div className="form-group">
                                    <label>How Did You Hear About Us?</label>
                                    <select value={this.state.customer.howDidYouHear} className={"form-control col-md-6" + (this.state.formErrors.howDidYouHear ? " is-invalid" : this.state.customer.howDidYouHear ? " is-valid" : "")} type="text" name="howDidYouHear" onChange={this.handleChange} >
                                        <option value="" disabled>Please select</option>
                                        <option value="Newspaper">Newspaper</option>
                                        <option value="Yellow Pages">Yellow Pages</option>
                                        <option value="Website / Google">Website / Google</option>
                                        <option value="Flyers / Noticeboard">Flyers / Noticeboard</option>
                                        <option value="Family / Friends">Family / Friends</option>
                                        <option value="School / Kindergarten">School / Kindergarten</option>
                                        <option value="Other">Other</option>
                                    </select>

                                    {this.state.formErrors.howDidYouHear && <span className="formError">{this.state.formErrors.howDidYouHear}</span>}
                                </div>
                            }

                            {this.props.editMode && 
                                <div style={{ paddingLeft: 0 }} className="form-group col-md-6">
                                    <label> Membership End Date</label>
                                    <input disabled type="date" name="membershipEndDate" className={"form-control placeholder" + (this.state.formErrors.membershipEndDate === null ? " is-valid" : this.state.formErrors.membershipEndDate ? " is-invalid" : "")} value={this.state.customer.membershipEndDate ? this.state.customer.membershipEndDate.split('T')[0] : ""} onChange={this.handleChange} required />
                                    {this.state.formErrors.membershipEndDate && <span className="formError">{this.state.formErrors.membershipEndDate}</span>}
                                </div>
                            }

                            {this.props.editMode && 
                                <div style={{ paddingLeft: 0 }} className="form-group col-md-6">
                                    <label> Password </label>
                                    <input type="password" placeholder="(Unchanged)" name="password" className={"form-control placeholder" + (this.state.formErrors.password === null ? " is-valid" : this.state.formErrors.password ? " is-invalid" : "")} value={this.state.customer.password} onChange={this.handleChange} />
                                    {this.state.formErrors.password && <span className="formError">{this.state.formErrors.password}</span>}

                                </div>
                            }

                            {/* Adding another parent/caregiver */}
                            {this.state.customer.caregivers.map((caregiver, index) => {
                                return (

                                    <div key={index}>
                                        <hr />

                                        <div className="form-row">
                                            <div style={{ marginBottom: 0, paddingBottom: 0 }} className="form-group col-md-10">
                                                <h4 className="header-customerform"> {"Parent / Caregiver " + (index + 1)} </h4>
                                            </div>
                                            <div className="form-group col-md-2">
                                                {this.state.customer.caregivers.length > 1 && <button href="#" className="btn btn-outline-danger removeBtn" onClick={(e) => { this.removeCaregiver(e, index) }}>Remove</button>}
                                            </div>
                                        </div>

                                        <div className="form-row">
                                            <div className="form-group col-md-6">
                                                <label> First Name </label>
                                                <input value={this.state.customer.caregivers[index].firstName} className={"form-control" + (this.state.formErrors.caregivers[index].firstName === null ? " is-valid" : this.state.formErrors.caregivers[index].firstName ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "firstName") }} type="text" name={"caregiver-firstName-" + index} />
                                                {this.state.formErrors.caregivers[index].firstName && <span className="formError">{this.state.formErrors.caregivers[index].firstName}</span>}
                                            </div>
                                            <div className="form-group col-md-6">
                                                <label> Last Name </label>
                                                <input value={this.state.customer.caregivers[index].lastName} className={"form-control" + (this.state.formErrors.caregivers[index].lastName === null ? " is-valid" : this.state.formErrors.caregivers[index].lastName ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "lastName") }} type="text" name={"caregiver-lastName-" + index} />
                                                {this.state.formErrors.caregivers[index].lastName && <span className="formError">{this.state.formErrors.caregivers[index].lastName}</span>}
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label> Phone Number </label>
                                            <input value={this.state.customer.caregivers[index].phoneNumber} className={"form-control col-md-6" + (this.state.formErrors.caregivers[index].phoneNumber === null ? " is-valid" : this.state.formErrors.caregivers[index].phoneNumber ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "phoneNumber") }} type="text" name={"caregiver-phoneNumber-" + index} />
                                            {this.state.formErrors.caregivers[index].phoneNumber && <span className="formError">{this.state.formErrors.caregivers[index].phoneNumber}</span>}
                                        </div>

                                        <div className="form-group">
                                            <label> Relationship To Child  </label>
                                            <select value={this.state.customer.caregivers[index].relationshipToChild} className={"form-control col-md-6 placeholder" + (this.state.formErrors.caregivers[index].relationshipToChild ? " is-invalid" : this.state.customer.caregivers[index].relationshipToChild ? " is-valid" : "")} name="relationshipToChild" onChange={(e) => { this.handleEditCaregiver(e, index, "relationshipToChild") }}>
                                                <option value="" disabled>Please select</option>
                                                <option value="MOTHER">Mother</option>
                                                <option value="FATHER">Father</option>
                                                <option value="STEP_MOTHER">Step-Mother</option>
                                                <option value="STEP_FATHER">Step-Father</option>
                                                <option value="GRANDPARENT">Grandparent</option>
                                                <option value="OTHER_FAMILY">Other Family</option>
                                                <option value="OTHER">Other</option>
                                            </select>
                                            {this.state.formErrors.caregivers[index].relationshipToChild && <span className="formError">{this.state.formErrors.caregivers[index].relationshipToChild}</span>}
                                        </div>



                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-12">
                                                    <h5 className="header-customerform" style={{ paddingBottom: 0, display: "inline-block" }}> Home Address </h5>
                                                </div>
                                                <div className="col-12">
                                                    <div style={{ textAlign: "left", fontSize: 12 }}>
                                                        {
                                                            this.state.customer.caregivers.map((value, i) => {
                                                                return (
                                                                    i != index && value.firstName && value.lastName &&
                                                                    <div>
                                                                        <span><a href="#" onClick={(event) => { this.autofillAddress(event, i, index) }}>{"Copy address from " + value.firstName + " " + value.lastName}</a></span>
                                                                        <br />
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div className="form-row">
                                                <div className="form-group col-md-6">
                                                    <label>Street Address</label>
                                                    <input value={this.state.customer.caregivers[index].streetAddress} className={"form-control" + (this.state.formErrors.caregivers[index].streetAddress === null ? " is-valid" : this.state.formErrors.caregivers[index].streetAddress ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "streetAddress") }} type="text" name={"caregiver-streetAddress-" + index} />
                                                    {this.state.formErrors.caregivers[index].streetAddress && <span className="formError">{this.state.formErrors.caregivers[index].streetAddress}</span>}
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label>Suburb</label>
                                                    <input value={this.state.customer.caregivers[index].addressLine2} className={"form-control" + (this.state.formErrors.caregivers[index].addressLine2 === null ? " is-valid" : this.state.formErrors.caregivers[index].addressLine2 ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "addressLine2") }} type="text" name={"caregiver-addressLine2-" + index} />
                                                    {this.state.formErrors.caregivers[index].addressLine2 && <span className="formError">{this.state.formErrors.caregivers[index].addressLine2}</span>}
                                                </div>
                                            </div>
                                            <div className="form-row">
                                                <div className="form-group col-md-6">
                                                    <label>City</label>
                                                    <input value={this.state.customer.caregivers[index].city} className={"form-control" + (this.state.formErrors.caregivers[index].city === null ? " is-valid" : this.state.formErrors.caregivers[index].city ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "city") }} type="text" name={"caregiver-city-" + index} />
                                                    {this.state.formErrors.caregivers[index].city && <span className="formError">{this.state.formErrors.caregivers[index].city}</span>}
                                                </div>
                                                <div className="form-group col-md-4">
                                                    <label>Region</label>
                                                    <input value={this.state.customer.caregivers[index].region} className={"form-control" + (this.state.formErrors.caregivers[index].region === null ? " is-valid" : this.state.formErrors.caregivers[index].region ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "region") }} type="text" name={"caregiver-region-" + index} />
                                                    {this.state.formErrors.caregivers[index].region && <span className="formError">{this.state.formErrors.caregivers[index].region}</span>}
                                                </div>
                                                <div className="form-group col-md-2">
                                                    <label>Post Code</label>
                                                    <input value={this.state.customer.caregivers[index].postCode} className={"form-control" + (this.state.formErrors.caregivers[index].postCode === null ? " is-valid" : this.state.formErrors.caregivers[index].postCode ? " is-invalid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "postCode") }} type="text" name={"caregiver-postCode-" + index} />
                                                    {this.state.formErrors.caregivers[index].postCode && <span className="formError">{this.state.formErrors.caregivers[index].postCode}</span>}
                                                </div>
                                            </div>
                                            <div>
                                                <label>Address Notes</label>
                                                <textarea placeholder="E.g. Tips on finding address, notes on parking..." value={this.state.customer.caregivers[index].addressNotes} className={"form-control" + (this.state.formErrors.caregivers[index].addressNotes ? " is-invalid" : this.state.customer.caregivers[index].addressNotes ? " is-valid" : "")} onChange={(e) => { this.handleEditCaregiver(e, index, "addressNotes") }} type="text" name={"caregiver-addressNotes-" + index} rows="3" />
                                                {this.state.formErrors.caregivers[index].addressNotes && <span className="formError">{this.state.formErrors.caregivers[index].addressNotes}</span>}
                                            </div>
                                        </div>

                                    </div> //closing div for key=index
                                )
                            })}

                            <button className="btn addBtn" disabled={this.state.customer.caregivers.length > 5} onClick={this.addCaregiver}><i className="fa fa-plus-circle" aria-hidden="true"></i> Add Parent/Caregiver</button>

                            <div className="form-wrapper ">
                                <ChildrenForm ref={this.childrenForm} onChange={(c) => {
                                    let cust = this.state.customer;
                                    cust.children = c;
                                    this.setState({
                                        customer: cust
                                    })
                                }} />
                            </div>

                            <hr />

                            <div className="form-group">

                                <h4 className="header-customerform"> Emergency Contact </h4>

                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <label> First Name </label>
                                        <input value={this.state.customer.emgConFName} className={"form-control" + (this.state.formErrors.emgConFName === null ? " is-valid" : this.state.formErrors.emgConFName ? " is-invalid" : "")} type="text" name="emgConFName" onChange={this.handleChange} />
                                        {this.state.formErrors.emgConFName && <span className="formError">{this.state.formErrors.emgConFName}</span>}
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label> Last Name </label>
                                        <input value={this.state.customer.emgConLName} className={"form-control" + (this.state.formErrors.emgConLName === null ? " is-valid" : this.state.formErrors.emgConLName ? " is-invalid" : "")} type="text" name="emgConLName" onChange={this.handleChange} />
                                        {this.state.formErrors.emgConLName && <span className="formError">{this.state.formErrors.emgConLName}</span>}
                                    </div>
                                </div>


                                <div className="form-group">
                                    <label> Phone Number </label>
                                    <input value={this.state.customer.emgConNo} className={"form-control col-md-6" + (this.state.formErrors.emgConNo === null ? " is-valid" : this.state.formErrors.emgConNo ? " is-invalid" : "")} type="text" name="emgConNo" onChange={this.handleChange} />
                                    {this.state.formErrors.emgConNo && <span className="formError">{this.state.formErrors.emgConNo}</span>}
                                </div>

                                <div className="form-group">
                                    <label> Relationship To Child  </label>
                                    <select value={this.state.customer.emgConRel} className={"form-control col-md-6" + (this.state.formErrors.emgConRel ? " is-invalid" : this.state.customer.emgConRel ? " is-valid" : "")} type="text" name="emgConRel" onChange={this.handleChange} >
                                        <option value="" disabled>Please select</option>
                                        <option value="AUNT">Aunt</option>
                                        <option value="UNCLE">Uncle</option>
                                        <option value="GRANDMOTHER">Grandmother</option>
                                        <option value="GRANDFATHER">Grandfather</option>
                                        <option value="OTHERFAMILY">Other Family</option>
                                        <option value="FAMILYFRIEND">Family Friend</option>
                                        <option value="OTHER">Other</option>
                                    </select>

                                    {this.state.formErrors.emgConRel && <span className="formError">{this.state.formErrors.emgConRel}</span>}
                                </div>
                            </div>


                            <hr />
                            <h4 className="header-customerform"> Pets </h4>
                            <div className="form-group">
                                <textarea placeholder="E.g. Details of any pets..." value={this.state.customer.pets} className={"form-control" + (this.state.formErrors.pets ? " is-invalid" : this.state.customer.pets ? " is-valid" : "")} type="text" name="pets" onChange={this.handleChange} rows="4" />
                                {this.state.formErrors.pets && <span className="formError">{this.state.formErrors.pets}</span>}
                            </div>

                            <hr />

                            <h4 className="header-customerform"> Any Other Requirements </h4>
                            <div className="form-group">
                                <textarea placeholder="E.g. What do you expect/value in a babysitter or is there anything else we need to know?" value={this.state.customer.extraNotes} className={"form-control" + (this.state.formErrors.extraNotes ? " is-invalid" : this.state.customer.extraNotes ? " is-valid" : "")} type="text" name="extraNotes" onChange={this.handleChange} rows="5" />
                                {this.state.formErrors.extraNotes && <span className="formError">{this.state.formErrors.extraNotes}</span>}
                            </div>
                            <br></br>

                            <div>
                                <button disabled={this.state.processing} className="btn submit-btn" type="submit" value="Submit" onClick={this.handleSubmit}>{this.state.processing ? "Processing..." : this.props.editMode ? "Update Account" : "Create Account"}</button>
                            </div>

                            <br></br>
                            <br></br>
                            <div>
                                {this.state.hasSubmitErrors && <p style={{ float: "right", fontSize: 16 }} className="formError">Please fix the errors highlighted in the form.</p>}
                                {this.state.submitError && <p style={{ float: "right", fontSize: 16 }} className="formError">{this.state.submitError}</p>}
                            </div>
                        </div>

                    </form>

                </div>
            </div >
        );
    }
}

export default withRouter(CustomerCustomerForm);