import React from 'react';
import { ax } from '../api.js';
import { withRouter } from "react-router-dom";
import * as constants from '../constants';

import api from '../api.js';

class ChangePassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            accountDetails: {},
            username: "",
            newPassword : "",
            confirmPassword : "",
            errorMessage: null,
            updating : false
        };

        ax.get(constants.API_BASE_URL + "admin/owndetails").then(response => {
            this.setState({
                accountDetails: response.data,
                username : response.data.username,
                userType : response.data.userType
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });;
    }

    handlePasswordChange = (event) => {
        event.preventDefault();

        this.setState({
            errorMessage: null,
            updating: true
        });
        if(this.state.newPassword !== this.state.confirmPassword){
            this.setState({
                errorMessage : "Both password fields must match!",
                updating: false
            });
        } else {
            api.changePassword(this.state.username, this.state.newPassword).then((response) => {
                this.setState({
                    errorMessage: null
                });

                this.props.history.push("/admin");

            }).catch((error) => {
                console.log(error);
                const newState = error.response && error.response.data && error.response.data.error_description ? error.response.data.error_description : "An unknown error occurred";
                this.setState({
                    errorMessage: newState,
                    loggingIn: false,
                    updating: false
                });
            
            })
        }
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        let infoText = "";

        switch (String(this.state.userType)) {
            case "admin":
                infoText = "Admin Change Password";
                break;
            case "babysitter":
                infoText = "Babysitter Change Password";
                break;
            case "customer":
                infoText = "Customer Change Password";
                break;
            default:
                break;
        }

        return (
            <div className="passwordContainer container" style={{marginLeft:"auto", marginRight:"auto", textAlign:"center"}} >
                <br/>
                <h2 style={{marginTop:20}}> Change Password </h2>
             
                <div className="passwordForm">
                    <h1 className="font-weight-light">{infoText}</h1>
                    
                    <form onSubmit={this.handlePasswordChange} style={{width:"50%", marginLeft:"auto", marginRight:"auto"}} >
                        <p>Change password for user: <strong>{this.state.username}</strong></p>
                        <input type="password" style={{width:"70%", marginLeft:"auto", marginRight:"auto"}} disabled={this.state.updating} name="newPassword" value={this.state.newPassword} onChange={this.handleChange.bind(this)} className="form-control" placeholder="New password" />
                        <br/>
                        <input type="password" style={{width:"70%", marginLeft:"auto", marginRight:"auto"}} disabled={this.state.updating} name="confirmPassword" value={this.state.confirmPassword} onChange={this.handleChange.bind(this)} className="form-control" placeholder="Confirm password" />
                        <br/>
                        <br/>
                        <button type="submit" style={{width:"50%", marginLeft:"auto", marginRight:"auto"}} disabled={this.state.updating} className="btn btn-success btn-block">{this.state.updating ? 'Changing password...' : 'Submit'}</button>
                    </form>
                    {this.state.errorMessage ? <p className="alert alert-danger">{this.state.errorMessage}</p> : null}
                    
                </div>
            </div>
        );
    }
}

export default withRouter(ChangePassword);