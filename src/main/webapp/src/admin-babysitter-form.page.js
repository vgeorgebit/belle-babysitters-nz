import React from 'react';
import AdminNavigation from './components/AdminNavigation.js';
import { ax } from './api.js'
import { withRouter } from 'react-router-dom';
import * as constants from './constants.js'


const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const nameRegex = RegExp(
    /^[ A-Za-z\\-]+$/
);

const phoneRegex = RegExp(
    /^[0-9]+$/
);

const addressRegex = RegExp(
    /^[a-zA-Z0-9,.\-\s]*$/
);

const usernameRegex = RegExp(
    /^[A-Za-z0-9]+$/
);

const passwordRegex = RegExp(
    /^[0-9A-Za-z!@#$%^&*/_-]+$/
);

class AdminBabysitterFormPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            dob: '',
            address: '',
            contactPhone: '',
            email: '',
            emgName: '',
            emgPhone: '',
            emgRelation: '',
            contractSignDate: '',
            username: '',
            password: '',
            file: null,
            disabled: false,
            license: '',
            car : '',
            policeCheck: '',
            medicalCertDate: '',
            bio: '',

            formErrors: {
                firstName: '',
                lastName: '',
                dob: '',
                address: '',
                contactPhone: '',
                email: '',
                emgName: '',
                emgPhone: '',
                emgRelation: '',
                contractSignDate: '',
                username: '',
                password: '',
                license: '',
                car : '',
                policeCheck: '',
                medicalCertDate: '',
                bio: '',
                
            },

            revisions: [],

            submitError: '',
            processing: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateFormInput = this.validateFormInput.bind(this);
        this.isFormValid = this.isFormValid.bind(this);
        this._handleImageChange = this._handleImageChange.bind(this);
        // this._handleSubmit = this._handleSubmit.bind(this);
        this.getImage = this.getImage.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);

        if (this.props.username != null) {
            ax.get(constants.API_BASE_URL + "babysitter/" + this.props.username).then(response => {
                const data = response.data;
                this.setState({
                    firstName: data.firstName,
                    lastName: data.lastName,
                    dob: data.dob,
                    address: data.address,
                    contactPhone: data.contactPhone,
                    email: data.email,
                    emgName: data.emgName,
                    emgPhone: data.emgPhone,
                    emgRelation: data.emgRelation,
                    contractSignDate: data.contractSignDate,
                    username: data.username,
                    disabled: data.disabled,
                    license: data.license,
                    car : data.car,
                    policeCheck: data.policeCheck,
                    medicalCertDate: data.medicalCertDate,
                    bio: data.bio
                });
                this.getImage(data.username)

            }).catch(error => {
                this.props.history.replace("/admin/babysitter");
            });

            ax.get(constants.API_BASE_URL + "audit/babysitter/" + this.props.username).then(response => {
                let r = response.data;
                r.sort((a, b) => {
                    return a.revisionDate < b.revisionDate ? 1 : a.revisionDate == b.revisionDate ? 0 : -1;
                });

                this.setState({
                    revisions: r
                });
            }).catch(e =>{
                alert("ERROR OCCURED");
            });
        }
    }

    getImage(username) {
        ax.get(constants.API_BASE_URL + "babysitter/image/" + username, { responseType: 'arraybuffer' })
            .then(
                response => {
                    const base64 = btoa(
                        new Uint8Array(response.data).reduce(
                            (data, byte) => data + String.fromCharCode(byte),
                            '',
                        ),
                    );

                    this.setState({
                        image: "data:;base64," + base64
                    });
                });
    }

    _handleSubmit() {
        const formData = new FormData()

        formData.append("file", this.state.file)

        ax.put(constants.API_BASE_URL + "babysitter/image/" + this.state.username, formData, { headers: {} }).then(response => {
            this.setState({
                file: null
            });
        }).catch(error => {
            let msg = '';
            if (error.response && error.response.data && error.response.data.constraintViolations) {
                error.response.data.constraintViolations.forEach(v => {
                    msg += v + '  ';
                });
            } else if (error.response && error.response.data && error.response.data.error_description) {
                msg = error.response.data.error_description;
            } else {
                msg = "An unknown error occurred with the image";
            }

            this.setState({
                submitError: msg,
                processing: false
            });
        });


    }

    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let img = e.target.files[0];

        if (img && img.type.match('image.*')) {
            reader.readAsDataURL(img);
        } else {
            img = null;
        }

        reader.onloadend = () => {
            this.setState({
                file: img,
                image: reader.result

            });
        };

    }

    isFormValid() {
        let valid = true;
        Object.keys(this.state.formErrors).forEach(key => {
            if (this.state.formErrors[key] != null)
                if (!(!this.props.username && key === "password"))
                    valid = false;
        });

        return valid;
    };

    handleChange(event) {
        event.preventDefault();
        const { name, value } = event.target;

        this.setState({
            [name]: value,
            submitError: null
        }, () => {
            this.validateFormInput(name);
        });
    }

    validateFormInput(name = null, callback = null) {
        let formErrors = { ...this.state.formErrors };

        let values = {}
        if (name != null) {
            values[name] = this.state[name];
        } else {
            values = this.state;
        }

        for (const key in values) {
            let value = values[key];
            switch (key) {
                case "firstName":
                    formErrors.firstName =
                        value.length > 64 ? "First name is too long" :
                            value.length < 1 ? "First name is required" :
                                !nameRegex.test(value) ? "First name may only contain letters" :
                                    null;
                    break;
                case "lastName":
                    formErrors.lastName =
                        value.length > 64 ? "Last name is too long" :
                            value.length < 1 ? "Last name is required" :
                                !nameRegex.test(value) ? "Last name may only contain letters" :
                                    null;
                    break;
                case "dob":
                    const diffMs = Date.now() - new Date(Date.parse(value)).getTime();
                    const ageDt = new Date(diffMs);
                    const age = Math.abs(ageDt.getUTCFullYear() - 1970);

                    formErrors.dob =
                        !value ? "Date of birth is required" :
                            Date.parse(value) >= new Date() ? "Date of birth must be in the past" :
                            age > 120 ? "Date of birth is too long ago" :
                                age < 14 ? "Babysitters must be at least 14 years old" :
                                null;
                    break;

                case "contactPhone":
                    formErrors.contactPhone =
                        value.length > 15 ? "Phone number is too long" :
                            value.length < 1 ? "Phone number is required" :
                                !phoneRegex.test(value) ? "Phone number may only contain numbers" :
                                    null;
                    break;
                case "email":
                    formErrors.email =
                        value.length > 255 ? "Email address is too long" :
                            value.length < 1 ? "Email address is required" :
                                !emailRegex.test(value) ? "Email address must be valid" :

                                    null;
                    break;
                case "address":
                    formErrors.address =
                        value.trim().length < 1 ? "Address is required" :
                            value.length > 512 ? "Address is too long" :
                                value.split(/\r\n|\r|\n/).length > 3 ? "Address can only contain a maximum of 3 lines" :
                                    !addressRegex.test() ? "Address contains invalid characters" :
                                        null;
                    break;
                case "emgName":
                    formErrors.emgName =
                        value.length < 1 ? "Emergency contact name is required" :
                            value.length > 64 ? "Emergency contact name is too long" :
                                !nameRegex.test(value) ? "Emergency contact name may only contain letters" :
                                    null;
                    break;
                case "emgRelation":
                    formErrors.emgRelation =
                        value.length < 1 ? "Emergency contact relationship is required" :
                            value.length > 64 ? "Emergency contact relationship is too long" :
                                !nameRegex.test(value) ? "Emergency relationship may only contain letters" :
                                    null;
                    break;
                case "emgPhone":
                    formErrors.emgPhone =
                        value.length > 15 ? "Emergency phone number is too long" :
                            value.length < 1 ? "Emergency phone number is required" :
                                !phoneRegex.test(value) ? "Emergency phone number may only contain numbers" :
                                    null;
                    break;
                case "username":
                    formErrors.username =
                        value.length < 1 ? "Username is required" :
                            value.length < 4 ? "Username is too short" :
                                value.length > 16 ? "Username is too long" :
                                    !usernameRegex.test(value) ? "Username may only contain alphanumeric characters" :
                                        null;
                    break;
                case "contractSignDate":
                    formErrors.contractSignDate =
                        !value ? "Contract sign date is required" :
                            null;
                    break;
                case "password":
                    formErrors.password =
                        !value ? null :
                            value.length < 6 ? "Password is too short" :
                                value.length > 32 ? "Password is too long" :
                                    !passwordRegex.test(value) ? "Password may only contain alphanumeric characters and !@#$%^&*/_-" :
                                        null
                    break;

                case "license":
                    formErrors.license = !value ? "Driver license is required" : null;
                break;

                case "medicalCertDate":
                    formErrors.medicalCertDate = null;
                break;

                case "policeCheck":
                    formErrors.policeCheck = null;
                break;
                
                case "car":
                    formErrors.car = ! value ? "Car access is required" : null;
                break;

                case "bio":
                    formErrors.bio = null;
                break;
                    
                    
                default:
                    break;
            }
        }
        this.setState({
            formErrors,
        }, () => {
            if (callback)
                callback();
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.validateFormInput(null, () => {
            if (!this.isFormValid()) {
                this.setState({
                    submitError: "Please fix the errors highlighted in the form."
                });
            } else {
                this.setState({
                    processing: true
                });

                const babysitter = {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    dob: this.state.dob,
                    address: this.state.address,
                    contactPhone: this.state.contactPhone,
                    email: this.state.email,
                    emgName: this.state.emgName,
                    emgPhone: this.state.emgPhone,
                    emgRelation: this.state.emgRelation,
                    contractSignDate: this.state.contractSignDate,
                    username: this.state.username,
                    disabled: this.state.disabled,
                    license: this.state.license,
                    car : this.state.car,
                    policeCheck: this.state.policeCheck,
                    medicalCertDate: this.state.medicalCertDate,
                    bio: this.state.bio
                };

                if (!this.props.username) {
                    ax.post(constants.API_BASE_URL + "babysitter", babysitter).then(response => {
                        if (this.state.file !== null) {
                            this._handleSubmit();
                        }
                        this.props.history.push("/admin/babysitter");
                    }).catch(error => {
                        let msg = '';
                        if (error.response && error.response.data && error.response.data.constraintViolations) {
                            error.response.data.constraintViolations.forEach(v => {
                                msg += v + '  ';
                            });
                        } else if (error.response && error.response.data && error.response.data.error_description) {
                            msg = error.response.data.error_description;
                        } else {
                            msg = "An unknown error occurred";
                        }

                        this.setState({
                            submitError: msg,
                            processing: false
                        });
                    });
                } else {
                    const updated = {
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        dob: this.state.dob,
                        address: this.state.address,
                        contactPhone: this.state.contactPhone,
                        email: this.state.email,
                        emgName: this.state.emgName,
                        emgPhone: this.state.emgPhone,
                        emgRelation: this.state.emgRelation,
                        contractSignDate: this.state.contractSignDate,
                        username: this.state.username,
                        password: this.state.password ? this.state.password : null,
                        disabled: this.state.disabled,
                        license: this.state.license,
                        car : this.state.car,
                        policeCheck: this.state.policeCheck,
                        medicalCertDate: this.state.medicalCertDate,
                        bio: this.state.bio
                    };
                    ax.put(constants.API_BASE_URL + "babysitter", updated).then(response => {
                        if (this.state.file !== null) {
                            this._handleSubmit();
                        }
                        this.props.history.push("/admin/babysitter");
                    }).catch(error => {
                        let msg = '';
                        if (error.response && error.response.data && error.response.data.constraintViolations) {
                            error.response.data.constraintViolations.forEach(v => {
                                msg += v + '  ';
                            });
                        } else if (error.response && error.response.data && error.response.data.error_description) {
                            msg = error.response.data.error_description;
                        } else {
                            msg = "An unknown error occurred";
                        }

                        this.setState({
                            submitError: msg,
                            processing: false
                        });
                    });

                }
            }
        });
    }

    getInputClasses(formError) {
        return "form-control" + (formError === null ? " is-valid" : formError ? " is-invalid" : "");
    }

    deleteClicked(event) {
        event.preventDefault();

        const del = window.confirm("Are you sure you want to remove this babysitter?"); //dialog box

        if (del) {
            ax.delete(constants.API_BASE_URL + "babysitter/" + this.state.username).then(response => {
                this.props.history.push("/admin/babysitter"); // redirect to babysitter list
            }).catch(error => {
                alert("Error: Unable to delete babysitter");
            });
        }
    }

    handleCheckChange(event) {
        this.setState({
            [event.target.name]: event.target.checked
        })
    }

    render() {
        return (
            <div>
                <AdminNavigation />

                <div className="babysitterFormContainer container">
                    <div style={{width: "100%", textAlign: "right"}}>
                        <button className="btn btn-danger" onClick={() => {this.props.history.push("/admin/babysitter")}}>Cancel</button>
                    </div>
                    <h1>{!this.props.username ? 'New Babysitter Account' : 'Modify Babysitter Account'}</h1>

                    <form onSubmit={this.handleSubmit}>
                        <div className="input-group row">
                            <div className="col col-6">
                                <label>First Name:</label>
                                <input type="text" name="firstName" className={this.getInputClasses(this.state.formErrors.firstName)} value={this.state.firstName} onChange={this.handleChange} />
                                {this.state.formErrors.firstName && <span className="formError">{this.state.formErrors.firstName}</span>}

                                <label>Last Name:</label>
                                <input type="text" name="lastName" className={this.getInputClasses(this.state.formErrors.lastName)} value={this.state.lastName} onChange={this.handleChange} />
                                {this.state.formErrors.lastName && <span className="formError">{this.state.formErrors.lastName}</span>}

                                <label>DOB:</label>
                                <input type="date" name="dob" className={this.getInputClasses(this.state.formErrors.dob)} value={this.state.dob.split('T')[0]} onChange={this.handleChange} placeholder="dd/mm/yyyy" />
                                {this.state.formErrors.dob && <span className="formError">{this.state.formErrors.dob}</span>}
                            </div>
                            <div className="col col-6">
                                <p style={{ textAlign: "center" }}>Profile Picture</p>

                                <div style={{ width: 150, marginLeft: "auto", marginRight: "auto", display: "block", background: (!this.props.username ? "gray" : null) }}>
                                    <div>
                                        <img src={this.state.image} style={{ width: 150, height: 150 }} />
                                        <input type="file" accept='.jpeg,.jpg,.png' id="file" className="btn btn-secondary" onChange={this._handleImageChange} />
                                        <label htmlFor="file" className={this.state.file == null ? "btn btn-secondary" : "btn btn-success"} id="uploadImage" style={{ width: 150 }}><i className={this.state.file == null ? "fa fa-camera" : "fa fa-check"} aria-hidden="true"></i>{this.state.file == null ? `  Attach Image` : (this.state.file.name.length > 13 ? '   ...' + this.state.file.name.substr(this.state.file.name.length - 10) : '   ' + this.state.file.name)}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-12">
                                <label>Babysitter Bio</label>
                                <textarea className={this.getInputClasses(this.state.formErrors.bio)} onChange={ this.handleChange } type="text" name="bio" rows="4" value={this.state.bio} />
                                {this.state.formErrors.bio && <span className="formError">{this.state.formErrors.bio}</span>}
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-6">
                                <label>Contact Phone:</label>
                                <input type="text" name="contactPhone" className={this.getInputClasses(this.state.formErrors.contactPhone)} value={this.state.contactPhone} onChange={this.handleChange} />
                                {this.state.formErrors.contactPhone && <span className="formError">{this.state.formErrors.contactPhone}</span>}
                                <label> Email Address:  </label>
                                <input type="email" name="email" className={this.getInputClasses(this.state.formErrors.email)} value={this.state.email} onChange={this.handleChange} placeholder="name@example.com" />
                                {this.state.formErrors.email && <span className="formError">{this.state.formErrors.email}</span>}
                            </div>
                            <div className="col col-6">
                                <label>Address:</label>
                                <textarea rows="4" style={{ resize: "none" }} maxLength="512" name="address" className={this.getInputClasses(this.state.formErrors.address)} value={this.state.address} onChange={this.handleChange}></textarea>
                                {this.state.formErrors.address && <span className="formError">{this.state.formErrors.address}</span>}
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-6">
                                <label> Emergency Contact Name:  </label>
                                <input type="text" name="emgName" className={this.getInputClasses(this.state.formErrors.emgName)} value={this.state.emgName} onChange={this.handleChange} />
                                {this.state.formErrors.emgName && <span className="formError">{this.state.formErrors.emgName}</span>}
                                <label>Emergency Contact Phone:</label>
                                <input type="text" name="emgPhone" className={this.getInputClasses(this.state.formErrors.emgPhone)} value={this.state.emgPhone} onChange={this.handleChange} />
                                {this.state.formErrors.emgPhone && <span className="formError">{this.state.formErrors.emgPhone}</span>}

                            </div>
                            <div className="col col-6">
                                <label>Emergency Contact Relationship:</label>
                                <input type="text" name="emgRelation" className={this.getInputClasses(this.state.formErrors.emgRelation)} value={this.state.emgRelation} onChange={this.handleChange} />
                                {this.state.formErrors.emgRelation && <span className="formError">{this.state.formErrors.emgRelation}</span>}
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-6">
                                <label> Username:  </label>
                                <input type="text" name="username" disabled={this.props.username != null} className={this.getInputClasses(this.state.formErrors.username)} value={this.state.username} onChange={this.handleChange} />
                                {this.state.formErrors.username && <span className="formError">{this.state.formErrors.username}</span>}
                            </div>
                            <div className="col col-6">
                                <label> Contract Sign Date:  </label>
                                <input type="date" name="contractSignDate" className={this.getInputClasses(this.state.formErrors.contractSignDate)} value={this.state.contractSignDate.split('T')[0]} onChange={this.handleChange} placeholder="dd/mm/yyyy" />
                                {this.state.formErrors.contractSignDate && <span className="formError">{this.state.formErrors.contractSignDate}</span>}
                            </div>
                        </div>

                        {this.props.username &&
                            <div className="row">
                                <div className="col col-6">
                                    <label>Password:</label>
                                    <input type="password" placeholder="(Unchanged)" name="password" className={this.getInputClasses(this.state.formErrors.password)} onChange={this.handleChange} />
                                    {this.state.formErrors.password && <span className="formError">{this.state.formErrors.password}</span>}
                                </div>
                                
                                    <div className="col col-6">
                                    <div className="row" >
                                        <input type="checkbox" name="disabled" checked={this.state.disabled} onChange={this.handleCheckChange} className="form-check-input" />
                                        <label className="form-check-label" htmlFor="disabled">Disable Account: </label>
                                    </div>
                                </div>

                            </div>
                        }

                        <div className="row">
                        <div className="col col-6">
                                <label>Police Check Date:</label>
                                <input type="date" name="policeCheck" className={this.getInputClasses(this.state.formErrors.policeCheck)} value={this.state.policeCheck && this.state.policeCheck.split('T')[0]} onChange={this.handleChange} placeholder="dd/mm/yyyy" />
                                {this.state.formErrors.policeCheck && <span className="formError">{this.state.formErrors.policeCheck}</span>}
                                
                                
                                <label>First Aid Certificate Expiry:</label>
                                <input type="date" name="medicalCertDate" className={this.getInputClasses(this.state.formErrors.medicalCertDate)} value={this.state.medicalCertDate && this.state.medicalCertDate.split('T')[0]} onChange={this.handleChange} placeholder="dd/mm/yyyy" />
                                {this.state.formErrors.medicalCertDate && <span className="formError">{this.state.formErrors.medicalCertDate}</span>}

                            </div>
                            <div className="col col-6">
                                <label>Car Access:</label>
                                    <select className={this.getInputClasses(this.state.formErrors.car)} type="text" name="car" value={this.state.car} onChange={this.handleChange} >
                                        <option value="" disabled>Please select</option>
                                        <option value="NONE">None</option>
                                        <option value="SOMETIMES">Sometimes</option>
                                        <option value="ALWAYS">Always</option>
                                    </select>
                                    {this.state.formErrors.car && <span className="formError">{this.state.formErrors.car}</span>}

                                
                                <label style={{marginTop:8}}>Drivers License:</label>
                                    <select className={this.getInputClasses(this.state.formErrors.license)} type="text" name="license" value={this.state.license} onChange={this.handleChange} >
                                        <option value="" disabled>Please select</option>
                                        <option value="NONE">None</option>
                                        <option value="LEARNERS">Learners</option>
                                        <option value="RESTRICTED">Restricted</option>
                                        <option value="FULL">Full License</option>
                                    </select>
                                    {this.state.formErrors.license && <span className="formError">{this.state.formErrors.license}</span>}
                            </div>
                            
                        </div>
                        <div>
                            {this.props.username &&
                                <button style={{ float: "left" }} type="submit" className="btn btn-danger" onClick={e => this.deleteClicked(e)}> Delete </button>
                            }
                            <input style={{ float: "right" }} disabled={this.state.processing} type="submit" value={this.state.processing ? "Processing..." : (this.props.username != null ? "Update Babysitter" : "Add Babysitter")} className="btn btn-success" />
                        </div>

                        <br></br>
                        <br></br>
                        <div>

                            {this.state.submitError && <p style={{ float: "right", fontSize: 16 }} className="formError">{this.state.submitError}</p>}
                        </div>
                    </form>
                    <br /><br /><br />
                    {this.props.username &&
                        <div>
                            <h4>Revision History</h4>
                            {this.state.revisions.length > 0 ?
                                <div style={{ width: "100%", maxHeight: 400, overflow: "scroll", whiteSpace: "nowrap" }}>
                                    <table className="table table-bordered table-hover" style={{ borderCollapse: "collapse", width: "100%" }}>
                                        <thead>
                                            <tr>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Date</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Action</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Editor</th>

                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>First Name</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Last Name</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>DOB</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Bio</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Address</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Phone</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Email</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Emg Name</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Emg Phone</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Emg Relationship</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Username</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Contract Sign Date</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Account Disabled</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Police Check Date</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>Car Access</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>License</th>
                                                <th style={{ background: "#EEE", position: "sticky", top: 0 }}>First Aid Expiry</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.revisions.map((value, index) => {
                                                    return (
                                                        <tr key={value.revisionId}>
                                                            <td>{new Date(Date.parse(value.revisionDate)) + ""}</td>
                                                            <td>{value.action}</td>
                                                            <td>{value.editor}</td>

                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.firstName !== value.entity.firstName ? "text-info" : ""}>{value.entity.firstName}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.lastName !== value.entity.lastName ? "text-info" : ""}>{value.entity.lastName}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.dob !== value.entity.dob ? "text-info" : ""}>{value.entity.dob.split("T")[0]}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.bio !== value.entity.bio ? "text-info" : ""}>{value.entity.bio.split("\n").map(value => {
                                                                return <span>{value}<br /></span>
                                                            })}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.address !== value.entity.address ? "text-info" : ""}>{value.entity.address.split("\n").map(value => {
                                                                return <span>{value}<br /></span>
                                                            })}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.contactPhone !== value.entity.contactPhone ? "text-info" : ""}>{value.entity.contactPhone}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.email !== value.entity.email ? "text-info" : ""}>{value.entity.email}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.emgName !== value.entity.emgName ? "text-info" : ""}>{value.entity.emgName}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.emgPhone !== value.entity.emgPhone ? "text-info" : ""}>{value.entity.emgPhone}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.emgRelation !== value.entity.emgRelation ? "text-info" : ""}>{value.entity.emgRelation}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.username !== value.entity.username ? "text-info" : ""}>{value.entity.username}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.contractSignDate !== value.entity.contractSignDate ? "text-info" : ""}>{value.entity.contractSignDate.split("T")[0]}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.disabled !== value.entity.disabled ? "text-info" : ""}>{value.entity.disabled ? "Yes" : "No"}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.policeCheck !== value.entity.policeCheck ? "text-info" : ""}>{value.entity.policeCheck ? value.entity.policeCheck.split("T")[0] : "-"}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.car !== value.entity.car ? "text-info" : ""}>{value.entity.car}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.license !== value.entity.license ? "text-info" : ""}>{value.entity.license}</td>
                                                            <td className={index === this.state.revisions.length - 1 || this.state.revisions[index + 1].entity.medicalCertDate !== value.entity.medicalCertDate ? "text-info" : ""}>{value.entity.medicalCertDate ? value.entity.medicalCertDate.split("T")[0] : "-"}</td>


                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                                :
                                <p>No revision history available</p>
                            }
                        </div>
                    }
                </div>
            </div>
        );
    }
}

export default withRouter(AdminBabysitterFormPage);