import React from 'react';

import CustomerForm from './components/CustomerForm.js'
import AdminNavigation from './components/AdminNavigation.js';

class AdminCustomerFormPage extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <div>
                <AdminNavigation/>
                <CustomerForm/>
            </div>
        )
    }
}

export default AdminCustomerFormPage;