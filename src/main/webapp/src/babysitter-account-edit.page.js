import React from 'react';
import BabysitterNavigation from './components/BabysitterNavigation.js';
import { ax } from './api.js'
import { withRouter } from 'react-router-dom';
import * as constants from './constants.js'


const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const nameRegex = RegExp(
    /^[ A-Za-z\\-]+$/
);

const phoneRegex = RegExp(
    /^[0-9]+$/
);

const addressRegex = RegExp(
    /^[a-zA-Z0-9,.\-\s]*$/
);

const passwordRegex = RegExp(
    /^[0-9A-Za-z!@#$%^&*/_-]+$/
);

class BabysitterAccountEditPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            address: '',
            contactPhone: '',
            email: '',
            password: '',

            formErrors: {
                address: '',
                contactPhone: '',
                email: '',
                password: '',
            },

            submitError: '',
            processing: false,
            images: {},

        };
        this.handleChange = this.handleChange.bind(this);

        this.validateFormInput = this.validateFormInput.bind(this);
        this.isFormValid = this.isFormValid.bind(this);

        this.getImages = this.getImages.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    getDetails() {
        ax.get(constants.API_BASE_URL + "babysitter/owndetails").then(response => {
            const data = response.data;
            this.setState({
                firstName: data.firstName,
                lastName: data.lastName,
                dob: data.dob,
                address: data.address,
                contactPhone: data.contactPhone,
                email: data.email,
                emgName: data.emgName,
                emgPhone: data.emgPhone,
                emgRelation: data.emgRelation,
                contractSignDate: data.contractSignDate,
                username: data.username,
                bio: data.bio
            });
            this.getImages(response.data.username);
        }).catch(e =>{
            alert("ERROR OCCURED");
        });

    };

    getImages(username) {
        ax.get(constants.API_BASE_URL + "babysitter/image/" + username, { responseType: 'arraybuffer' })
            .then(
                response => {
                    const base64 = btoa(
                        new Uint8Array(response.data).reduce(
                            (data, byte) => data + String.fromCharCode(byte),
                            '',
                        ),
                    );

                    this.setState(prevState => {
                        let images = Object.assign({}, prevState.images);
                        images[username] = "data:;base64," + base64;
                        return { images };
                    });
                }).catch(e =>{
                    alert("ERROR OCCURED");
                });

    }

    componentDidMount() {
        this.getDetails()
    }


    isFormValid() {
        let valid = true;
        Object.keys(this.state.formErrors).forEach(key => {
            if (this.state.formErrors[key] != null)
                valid = false;
        });

        return valid;
    };

    handleChange(event) {
        event.preventDefault();
        const { name, value } = event.target;

        this.setState({
            [name]: value,
            submitError: null
        }, () => {
            this.validateFormInput(name);
        });
    }

    validateFormInput(name = null, callback = null) {
        let formErrors = { ...this.state.formErrors };

        let values = {}
        if (name != null) {
            values[name] = this.state[name];
        } else {
            values = this.state;
        }

        for (const key in values) {
            let value = values[key];
            switch (key) {

                case "contactPhone":
                    formErrors.contactPhone =
                        value.length > 15 ? "Phone number is too long" :
                            value.length < 1 ? "Phone number is required" :
                                !phoneRegex.test(value) ? "Phone number may only contain numbers" :
                                    null;
                    break;
                case "email":
                    formErrors.email =
                        value.length > 255 ? "Email address is too long" :
                            value.length < 1 ? "Email address is required" :
                                !emailRegex.test(value) ? "Email address must be valid" :

                                    null;
                    break;
                case "address":
                    formErrors.address =
                        value.trim().length < 1 ? "Address is required" :
                            value.length > 512 ? "Address is too long" :
                                value.trim().split(/\r\n|\r|\n/).length > 3 ? "Address can only contain a maximum of 3 lines" :
                                    !addressRegex.test() ? "Address contains invalid characters" :
                                        null;
                    break;
                case "password":
                    formErrors.password =
                        !value ? null :
                            value.length < 6 ? "Password is too short" :
                                value.length > 32 ? "Password is too long" :
                                    !passwordRegex.test(value) ? "Password may only contain alphanumeric characters and !@#$%^&*/_-" :
                                        null;
                    break;
                default:
                    break;
            }
        }
        this.setState({
            formErrors,
        }, () => {
            if (callback)
                callback();
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.validateFormInput(null, () => {
            if (!this.isFormValid()) {
                this.setState({
                    submitError: "Please fix the errors highlighted in the form."
                });
            } else {
                let fd = new FormData();
                fd.set("email", this.state.email);
                fd.set("contactPhone", this.state.contactPhone);
                fd.set("address", this.state.address);
                fd.set("password", this.state.password);

                this.setState({
                    processing: true
                });

                ax.put(constants.API_BASE_URL + "babysitter/owndetails", fd).then(response => {
                    this.props.history.push("/babysitter");
                }).catch(error => {
                    let msg = '';

                    if (error.response && error.response.data && error.response.data.constraintViolations) {
                        error.response.data.constraintViolations.forEach(v => {
                            msg += v + '  ';
                        });
                    } else if (error.response && error.response.data && error.response.data.error_description) {
                        msg = error.response.data.error_description;
                    } else {
                        msg = "An unknown error occurred";
                    }
                    this.setState({
                        submitError: msg,
                        processing: false
                    });
                });

            }
        });

    }

    getInputClasses(formError) {
        return "form-control" + (formError === null ? " is-valid" : formError ? " is-invalid" : "");
    }

    render() {
        return (
            <div>
                <BabysitterNavigation />
                <div className="babysitterFormContainer container">
                    <div style={{width: "100%", textAlign: "right"}}>
                        <button className="btn btn-danger" style={{float: "none"}} onClick={() => {this.props.history.push("/babysitter/account")}}>Cancel</button>
                    </div>
                    <h1>Modify Account</h1>
                    <form onSubmit={this.handleSubmit}>
                        <div className="input-group row">
                            <div className="col col-6">
                                <label>First Name:</label>
                                <input disabled type="text" name="firstName" className={this.getInputClasses(this.state.formErrors.lastName)} placeholder={this.state.firstName} />

                                <label>Last Name:</label>
                                <input disabled type="text" name="lastName" className={this.getInputClasses(this.state.formErrors.lastName)} placeholder={this.state.lastName} />

                                <label>DOB:</label>
                                <input disabled type="text" name="dob" className={this.getInputClasses(this.state.formErrors.dob)} placeholder={this.state.dob && this.state.dob.toString().split('T')[0]} />
                            </div>
                            <div className="col col-6">
                                <p style={{ textAlign: "center" }}>Profile Picture</p>
                                <div style={{ width: "100%" }}>
                                    <img style={{ width: 150, height: 150, margin: "0 auto", display: "block" }} src={this.state.images[this.state.username]} />
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-12">
                                <label>Babysitter Bio</label>
                                <textarea disabled="true" style={{resize: "none"}} className="form-control" onChange={this.handleChange} type="text" name="bio" rows="4" value={this.state.bio} />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-6">
                                <label>Contact Phone:</label>
                                <input type="text" name="contactPhone" className={this.getInputClasses(this.state.formErrors.contactPhone)} value={this.state.contactPhone} onChange={this.handleChange} />
                                {this.state.formErrors.contactPhone && <span className="formError">{this.state.formErrors.contactPhone}</span>}
                                <label> Email Address:  </label>
                                <input type="email" name="email" className={this.getInputClasses(this.state.formErrors.email)} value={this.state.email} onChange={this.handleChange} />
                                {this.state.formErrors.email && <span className="formError">{this.state.formErrors.email}</span>}
                            </div>
                            <div className="col col-6">
                                <label>Address:</label>
                                <textarea rows="4" style={{ resize: "none" }} maxLength="512" name="address" className={this.getInputClasses(this.state.formErrors.address)} value={this.state.address} onChange={this.handleChange}></textarea>
                                {this.state.formErrors.address && <span className="formError">{this.state.formErrors.address}</span>}
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-6">
                                <label> Emergency Contact Name:  </label>
                                <input disabled type="text" name="emgName" className={this.getInputClasses(this.state.formErrors.emgName)} placeholder={this.state.emgName} onChange={this.handleChange} />
                                <label>Emergency Contact Phone:</label>
                                <input disabled type="text" name="emgPhone" className={this.getInputClasses(this.state.formErrors.emgPhone)} placeholder={this.state.emgPhone} onChange={this.handleChange} />

                            </div>
                            <div className="col col-6">
                                <label>Emergency Contact Relationship:</label>
                                <input disabled type="text" name="emgRelation" className={this.getInputClasses(this.state.formErrors.emgRelation)} placeholder={this.state.emgRelation} onChange={this.handleChange} />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-6">
                                <label> Username:  </label>
                                <input disabled type="text" name="username" className={this.getInputClasses(this.state.formErrors.username)} placeholder={this.state.username} onChange={this.handleChange} />
                            </div>
                            <div className="col col-6">
                                <label> Contract Sign Date:  </label>
                                <input disabled type="date" name="contractSignDate" className={this.getInputClasses(this.state.formErrors.contractSignDate)} placeholder={this.state.contractSignDate && this.state.contractSignDate.toString().split('T')[0]} onChange={this.handleChange} placeholder="dd/mm/yyyy" />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col col-6">
                                <label>Password:</label>
                                <input type="password" placeholder="(Unchanged)" name="password" className={this.getInputClasses(this.state.formErrors.password)} onChange={this.handleChange} />
                                {this.state.formErrors.password && <span className="formError">{this.state.formErrors.password}</span>}
                            </div>
                        </div>
                        <div>
                            <input style={{ float: "right" }} disabled={this.state.processing} type="submit" value={this.state.processing ? "Processing..." : (this.props.username != null ? "Update Babysitter" : "Update Babysitter")} className="btn submit-btn" />
                        </div>

                        <br></br>
                        <br></br>
                        <div>
                            {this.state.submitError && <p style={{ float: "right", fontSize: 16 }} className="formError">{this.state.submitError}</p>}
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default withRouter(BabysitterAccountEditPage);