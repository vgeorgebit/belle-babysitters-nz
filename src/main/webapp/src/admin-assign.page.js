import React from 'react';


import AdminNavigation from './components/AdminNavigation.js';
import { withRouter } from 'react-router-dom';
import { ax } from './api.js';
import * as constants from './constants.js';
import BadgeDisplay from './components/BadgeDisplay.js'

const feeRegex = RegExp(
    /\d+.\d{2}/
);

class AdminAssignPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            job: {},
            assignedBabysitters: null,
            availableBabysitters: null,
            unavailableBabysitters: null,
            fee: null,
            feeError: '',

            submitError: '',
            processing: false,
            page: '',
            search: null,
            pageNum: 0,
            images: [],
        };


        this.handleChange = this.handleChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.loadPage = this.loadPage.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.addAssigned = this.addAssigned.bind(this);
        this.removeAssigned = this.removeAssigned.bind(this);

        this.getImages = this.getImages.bind(this);
        this.handleSearch = this.handleSearch.bind(this);

    }

    componentDidMount() {
        const url_string = window.location.href;
        const url = new URL(url_string);
        const job = url.searchParams.get('job');
        ax.get(constants.API_BASE_URL + "jobs/" + job).then(response => {
            this.setState({
                job: response.data,
                loading: false,
                assignedBabysitters: response.data.assignedBabysitters
            }, () => {
                if(response.data.assignedBabysitters){
                    this.state.assignedBabysitters.forEach(b=> {
                        if (!this.state.images[b.username]) {
                            this.getImages(b.username);
                        }
                    });
                }
                
            });
            

        }).catch(e => {
            this.props.history.replace("/admin")
        });

        

        ax.get(constants.API_BASE_URL + "jobs/" + job + "/offers/AVAILABLE").then(response => {
            let babysitterList = [];
            let list = [];
            response.data.forEach(o => {
                list.push(o.babysitter);
                if (!this.state.images[o.babysitter.username]) {
                    this.getImages(o.babysitter.username);
                }
            });

            //fill available babysitter list with non-assigned
            if(list){
                list.forEach(available => {
                    let valid = true;
                    if(this.state.assignedBabysitters){
                        this.state.assignedBabysitters.forEach(assigned => {
                            if(assigned.username === available.username){
                                valid = false;
                            }
                        });
                    }
                    if(valid){
                        babysitterList.push(available);
                    }
                });
            }
            this.setState({
                availableBabysitters: babysitterList,
                loadingAvailable: false
            });
        }).catch(e => {
            alert("ERROR: Could not acquire available babysitters, please refresh the page.");
        });

        

        ax.get(constants.API_BASE_URL + "jobs/" + job + "/offers/UNAVAILABLE").then(response => {
            let babysitterList = [];
            let list = [];
            response.data.forEach(o => {
                list.push(o.babysitter);
                if (!this.state.images[o.babysitter.username]) {
                    this.getImages(o.babysitter.username);
                }
            })

            //only fill unavailable with babysitters who are unassigned
            if(list){
            list.forEach(unavailable => {
                    let valid = true;
                    if(this.state.assignedBabysitters){
                        this.state.assignedBabysitters.forEach(assigned => {
                            if(assigned.username === unavailable.username){
                                valid = false;
                            }
                        });
                    }
                    if(valid){
                        babysitterList.push(unavailable);
                    }
                });
            }

            this.setState({
                unavailableBabysitters: babysitterList,
            });
        }).catch(e => {
            alert("ERROR: Could not acquire unavailable babysitters, please refresh the page.");
        });
    }

    getImages(username) {
        ax.get(constants.API_BASE_URL + "babysitter/image/" + username, { responseType: 'arraybuffer' })
            .then(
                response => {
                    const base64 = btoa(
                        new Uint8Array(response.data).reduce(
                            (data, byte) => data + String.fromCharCode(byte),
                            '',
                        ),
                    );

                    this.setState(prevState => {
                        let images = Object.assign({}, prevState.images);
                        images[username] = "data:;base64," + base64;
                        return { images };
                    });
                }).catch(e => {
                    console.log("IMAGE ERROR");
                });

    }

    handleSubmit(event) {
        event.preventDefault();

        if (this.state.assignedBabysitters.length == 0) {
            alert("You must assign at least one babysitter to the job");
        } else if (!feeRegex.test(this.state.fee) && !this.props.modify) {
            alert("You must enter a valid booking fee");
        } else {

            let usernames = []
            this.state.assignedBabysitters.forEach(b => {
                usernames.push(b.username);
            })

            let request = new FormData();
            request.set("babysitterUsernames", usernames);
            request.set("bookingFee", this.state.fee);

            if(!this.props.modify){
                ax.post(constants.API_BASE_URL + "jobs/" + this.state.job.id + "/assign", request).then(response => {
                    this.props.history.push("/admin");
                }).catch(error => {
                    let msg = '';
                    if (error.response && error.response.data && error.response.data.constraintViolations) {
                        error.response.data.constraintViolations.map(v => {
                            msg += v + '  ';
                        });
                    } else if (error.response && error.response.data && error.response.data.error_description) {
                        msg = error.response.data.error_description;
                    } else {
                        msg = "An unknown error occurred";
                    }

                    this.setState({
                        submitError: msg
                    });
                });
            } else {
                ax.put(constants.API_BASE_URL + "jobs/" + this.state.job.id + "/reassign", request).then(response => {
                    if(this.state.job.status === "PENDING_PAYMENT"){
                        this.props.history.push("/admin/");
                    } else {
                        this.props.history.push("/admin/confirmed");
                    }
                }).catch(error => {
                    let msg = '';
                    if (error.response && error.response.data && error.response.data.constraintViolations) {
                        error.response.data.constraintViolations.map(v => {
                            msg += v + '  ';
                        });
                    } else if (error.response && error.response.data && error.response.data.error_description) {
                        msg = error.response.data.error_description;
                    } else {
                        msg = "An unknown error occurred";
                    }

                    this.setState({
                        submitError: msg
                    });
                });
            }




        }

    }

    getImages(username) {
        ax.get(constants.API_BASE_URL + "babysitter/image/" + username, { responseType: 'arraybuffer' })
            .then(
                response => {
                    const base64 = btoa(
                        new Uint8Array(response.data).reduce(
                            (data, byte) => data + String.fromCharCode(byte),
                            '',
                        ),
                    );

                    this.setState(prevState => {
                        let images = Object.assign({}, prevState.images);
                        images[username] = "data:;base64," + base64;
                        return { images };
                    });
                }).catch(e => {
                    alert("ERROR: Could not find images, please refresh the page.");
                });

    }

    loadPage(pageNum) {
        this.setState({
            loading: true
        });

        ax.get(constants.API_BASE_URL + "babysitters?page=" + pageNum + "&search=" + this.state.search).then(response => {
            this.setState({
                page: response.data,
                loading: false,
                pageNum: pageNum
            });
            response.data.content.forEach(b => {

                if (!this.state.images[b.username]) {
                    this.getImages(b.username);
                }
            })


        }).catch(e => {
            alert("ERROR: Loading page, please refresh.");
        });
    }

    getInputClasses(formError) {
        return "form-control" + (formError === null ? " is-valid" : formError ? " is-invalid" : "");
    }

    handleCheckChange(event) {
        this.setState({
            [event.target.name]: event.target.checked
        })
    }

    handleSearch(event) {
        this.setState({
            search: event.target.value,
            pageNum: 0
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }

    handleChange(event) {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            submitError: null
        });
    }

    addAssigned(b) {
        let valid = true;
        let list = this.state.assignedBabysitters;

        //unavailable check
        
        this.state.unavailableBabysitters.forEach(unavailable => {
            if(unavailable.username == b.username){
                if(!window.confirm("This babysitter has set themselves as UNAVAILABLE, do you want to assign them anyway?")){
                    valid=false;
                }
            }
        });
        if(!valid){return;} //if they do not want to assign an unavailable

        list.forEach(babysitter => {
            if (babysitter.username === b.username) {
                valid = false
            }
        }); //if already in assigned, do not let them

        if ((this.state.job.fullLicenseRequired && b.license !== "FULL") || (this.state.job.ownCarRequired && b.car === 'NONE')) {
            if (!window.confirm("Warning: " + b.firstName + " does NOT meet the transport requirements for this job.\nAre you sure you want to assign them?\n\n(Note that they will not be assigned until the confirm button at the top of the page is clicked)")) {
                return;
            }
        } 
        let updatedBabysitters = this.state.availableBabysitters;

        let i = updatedBabysitters.indexOf(b);

        if (valid) {
            list.push(b);
        }

        if (i !== -1) {
            updatedBabysitters.splice(i, 1);
        }

        this.setState({
            assignedBabysitters: list,
            availableBabysitters: updatedBabysitters
        });

            
    }

    removeAssigned(b) {
        let valid = true;
        let available = this.state.availableBabysitters;
        let assigned = this.state.assignedBabysitters;

        available.forEach(babysitter => {
            if (babysitter.username === b.username) {
                valid = false;
            }
        }); //if in available, do not 

        this.state.unavailableBabysitters.forEach(babysitter => {
            if (babysitter.username === b.username) {
                valid = false;
            }
        });

        let i = assigned.indexOf(b);

        if (i !== -1) { assigned.splice(i, 1); } //if it does exist, cut
        if (valid) { available.push(b); } //if it does not exist, push

        this.setState({
            assignedBabysitters: assigned,
            availableBabysitters: available
        })
    }


    handleSearch(event) {
        this.setState({
            search: event.target.value,
            pageNum: 0
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }

    render() {
        const job = this.state.job;

        return (
            <div>
                <AdminNavigation />

                <div className="adminFormContainer container">
                    <h1>{this.props.modify ? "Reassign Job" : "Assign Job"}</h1>

                    <form onSubmit={this.handleSubmit}>

                        <div>
                            <div className="row" style={{ marginBottom: 10 }}>
                                <div className="col col-6">
                                    <h4>&nbsp;<i style={{ color: "Blue" }} className="fa fa-info-circle" aria-hidden="true" />&nbsp;Details</h4>
                                </div>
                                <div className="col col-6">
                                    <input style={{ float: "right" }} disabled={this.state.processing} type="submit" value={this.state.processing ? "Processing..." : this.props.modify ? "Confirm Reassignment" : "Confirm Job"} className="btn btn-success" />
                                </div>
                            </div>
                            <table className="table">
                                {this.state.job && this.state.job.customer &&
                                    <tbody>


                                        <tr><th style={{ width: "30%" }}>ID</th><td>#{job.id}</td></tr>
                                        <tr><th>Start Time</th><td>{job.startTime.split("-")[2].substring(0, 2) + "-" + job.startTime.split("-")[1] + "-" + job.startTime.split("-")[0]} <br /> <span className="text-primary">{job.startTime.substring(12, 17)}</span></td></tr>
                                        <tr><th>End Time</th><td>{job.endTime.split("-")[2].substring(0, 2) + "-" + job.endTime.split("-")[1] + "-" + job.endTime.split("-")[0]} <br /> <span className="text-primary" >{job.endTime.substring(12, 17)}</span></td></tr>
                                        <tr><th>Duration</th><td>{(new Date(Date.parse(job.endTime.replace(" NZDT",  "").split("-").join(" "))) - new Date(Date.parse(job.startTime.replace(" NZDT",  "").split("-").join(" ")))) / 36e5} hours</td></tr>
                                        <tr><th>Date Requested</th><td>{job.dateRequested.split("-")[2].substring(0, 2) + "-" + job.dateRequested.split("-")[1] + "-" + job.dateRequested.split("-")[0]} {job.dateRequested.substring(12, 17)}</td></tr>
                                        <tr><th>Job Notes</th><td>{job.jobNotes}</td></tr>
                                        <tr><th>Job Address</th><td>{job.jobAddress.split('\n').map(line => {
                                            return line && <span>{line}<br /></span>
                                        })}</td></tr>
                                        <tr><th>Address Notes</th><td>{job.addressNotes}</td></tr>

                                        <tr><th># Children</th><td>{job.numChildren}</td></tr>
                                        <tr><th>Children Details</th><td>{job.childrenDetails.split('\n').map(line => {
                                            return line && <span>{line}<br /></span>
                                        })}</td></tr>
                                        <tr><th>Requirements</th>
                                            <td>
                                                <table style={{ margin: 0 }}>
                                                    <tr>
                                                        <td style={{ all: "unset" }}>
                                                            {job.fullLicenseRequired && <div><i className="fa fa-id-card-o" aria-hidden="true" title="Full Drivers License">&nbsp;</i><span>Full License Required</span></div>}

                                                        </td>
                                                        <td style={{ all: "unset" }}>
                                                            {job.ownCarRequired && <div><i style={{ color: "Green" }} className="fa fa-car" aria-hidden="true" title="Babysitter Must Have Vehicle Access">&nbsp;</i><span>Vehicle Access Required</span></div>}
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <tr><th>Membership</th>
                                            {(job.customer.membershipEndDate && (new Date(job.customer.membershipEndDate) > new Date())) ?
                                                <td style={{ color: "green", fontWeight: "bold" }}>This customer is a member</td> :
                                                <td style={{ color: "red", fontWeight: "bold" }}>This customer is NOT a member</td>}
                                        </tr>
                                        <tr><th>Booking Fee</th>
                                        {   this.props.modify ? 
                                            <td>
                                                    <span>${this.state.job.bookingFeePrice}</span>
                                            </td>
                                            :
                                            <td>
                                                <span style={{ display: "inline" }}>$</span><input type="number" step="0.01" name="fee" style={{ width: "40%", display: "inline" }} className={this.getInputClasses(this.state.feeError)} value={this.state.fee} onChange={this.handleChange} placeholder="0.00" />
                                                {this.state.feeError && <span className="formError">{this.state.feeError}</span>}

                                            </td>
                                        }</tr>

                                    </tbody>
                                }
                            </table>
                            <br />
                            <br />
                            <h4>&nbsp;<i style={{ color: "Green" }} className="fa fa-check" aria-hidden="true" />&nbsp;Assigned Babysitters</h4>
                            <table className="table">
                                <thead>
                                    <tr>

                                        <th></th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Badges</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                {this.state.assignedBabysitters &&
                                    <tbody>
                                        {this.state.assignedBabysitters.map((b, i) => {

                                            return (
                                                <tr key={b.username}>
                                                    <td><img alt="Babysitter Profile" className="adminsProfile" src={this.state.images[b.username]} /></td>
                                                    <td>{b.firstName + " " + b.lastName}</td>
                                                    <td>{b.username}</td>
                                                    <td>
                                                        <BadgeDisplay babysitter={b}/>
                                                    </td>
                                                    <td>
                                                        <label onClick={(e) => { this.removeAssigned(b) }} className="btn btn-danger assign-button">Remove</label>
                                                    </td>
                                                </tr>

                                            );
                                        })}


                                    </tbody>}
                            </table>

                            <br />
                            <br />
                            <br />
                            <h4>&nbsp;<i style={{ color: "black" }} className="fa fa-users" aria-hidden="true" />&nbsp;Available Babysitters</h4>
                            <table className="table">
                                <thead>
                                    <tr>

                                        <th></th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Badges</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                {this.state.availableBabysitters &&
                                    <tbody>
                                        {this.state.availableBabysitters.map((b, i) => {
                                            //if babysitter in assigned list then dont

                                            return (
                                                <tr key={b.username}>
                                                    <td><img alt="Babysitter Profile" className="adminsProfile" src={this.state.images[b.username]} /></td>
                                                    <td>{b.firstName + " " + b.lastName}</td>
                                                    <td>{b.username}</td>
                                                    <td>
                                                        <BadgeDisplay babysitter={b}/>
                                                    </td>
                                                    <td>
                                                        <label onClick={(e) => { this.addAssigned(b) }} className="btn btn-success assign-button">Add</label>
                                                    </td>
                                                </tr>

                                            );
                                        })}


                                    </tbody>
                                }
                            </table>

                            <br /><br /><br />
                            <h4>&nbsp;<i style={{ color: "red" }} className="fa fa-times" aria-hidden="true" />&nbsp;Unavailable Babysitters</h4>
                            <table className="table">
                                <thead>
                                    <tr>

                                        <th></th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Badges</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                {this.state.unavailableBabysitters &&
                                    <tbody>
                                        {this.state.unavailableBabysitters.map((b, i) => {
                                            return (
                                                <tr key={b.username}>
                                                    <td><img alt="Babysitter Profile" className="adminsProfile" src={this.state.images[b.username]} /></td>
                                                    <td>{b.firstName + " " + b.lastName}</td>
                                                    <td>{b.username}</td>
                                                    <td>
                                                        <BadgeDisplay babysitter={b}/>
                                                    </td>
                                                </tr>

                                            );
                                        })}


                                    </tbody>
                                }
                            </table>
                        </div>

                        <br /><br />
                        <p>Assign another babysitter:</p>
                        <h4>&nbsp;<i style={{ color: "gray" }} className="fa fa-search" aria-hidden="true" />&nbsp;Search Babysitters</h4>



                        <div className="row">
                            <div className="col col-4">
                                <input onChange={this.handleSearch} className="form-control" type="text" placeholder="Search" />
                            </div>
                        </div>
                        {this.state.page &&
                            <div>
                                {
                                    this.state.loading ?
                                        (
                                            <div className="spinner-border text-primary" role="status">
                                                <span className="sr-only">Loading...</span>
                                            </div>
                                        ) :
                                        <div>
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>First Name</th>
                                                        <th>Surname</th>
                                                        <th>Username</th>
                                                        <th>Badges</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    {
                                                        this.state.page.content.map(babysitter => {

                                                            let exists = false;
                                                            this.state.assignedBabysitters.forEach(b => {
                                                                if (b.username === babysitter.username) {
                                                                    exists = true;
                                                                }
                                                            });
                                                            this.state.availableBabysitters.forEach(b => {
                                                                if (b.username === babysitter.username) {
                                                                    exists = true;
                                                                }
                                                            });



                                                            return (

                                                                <tr key={babysitter.username} >

                                                                    <td><img alt="Babysitter Profile" className="adminsProfile" src={this.state.images[babysitter.username]} /></td>
                                                                    <td style={{textDecorationLine: (babysitter.disabled ? 'line-through' : '')}}>{babysitter.firstName}</td>
                                                                    <td style={{textDecorationLine: (babysitter.disabled ? 'line-through' : '')}}>{babysitter.lastName}</td>
                                                                    <td style={{textDecorationLine: (babysitter.disabled ? 'line-through' : '')}}>{babysitter.username}</td>
                                                                    <td>
                                                                        {babysitter.disabled ? <span style={{color:"red", textDecorationLine:'underline'}}>DISABLED</span> : <BadgeDisplay babysitter={babysitter}/>}
                                                                    </td>
                                                                    <td>
                                                                        {exists || babysitter.disabled ? "" : <label onClick={(e) => { this.addAssigned(babysitter) }} className="btn btn-success assign-button">Add</label>}
                                                                    </td>
                                                                </tr>)
                                                        })
                                                    }
                                                </tbody>
                                            </table>
                                            <center>
                                                {!this.state.page.first && <span><a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum - 1) }} href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>}
                                                <span>Page {this.state.pageNum + 1} of {this.state.page.totalPages}</span>
                                                {!this.state.page.last && <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum + 1) }} href="">Next</a></span>}
                                            </center>
                                        </div>
                                }
                            </div>
                        }







                    </form>



                </div>

            </div>
        )
    }
}

export default AdminAssignPage;