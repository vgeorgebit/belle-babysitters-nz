import React from 'react';

import { withRouter } from 'react-router-dom'
import * as constants from './constants.js';

import axios from 'axios';

class CustomerProcessPaymentPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            success: null
        };
    }

    componentDidMount() {
        const url = new URL(window.location.href);
        axios.post(constants.PUBLIC_API_BASE_URL + "job/payment/complete" + url.search).then(response => {
            this.setState({
                success: true
            });
        }).catch(error => {
            this.setState({
                success: false
            });
        });
    }

    render() {
        return (
            <div className="d-flex justify-content-center text-center">
                <div style={{ marginTop: 50 }}>
                    <img style={{ maxWidth: "70%" }} src="/logo.jpg" className="img-fluid center" alt="Belle Babysitters Logo" />
                    <br />
                    <br />
                    {this.state.success === null ?
                        <div>
                            <div className="spinner-border text-primary" style={{ width: "3.5rem", height: "3.5rem" }} role="status"></div>
                            <br />
                            <br />
                            <p style={{ fontSize: 18 }}><b>Processing payment&nbsp;&nbsp;</b><i className="fa fa-credit-card cc-animated"></i></p>
                            <p>Please do not close or refresh this page.</p>
                        </div>
                        : this.state.success === true ?
                            <div>
                                <i style={{ color: "green", fontSize: 60 }} class="fa fa-check-circle"></i>
                                <br />
                                <br />
                                <p>Payment successfully completed.</p>
                                <p><a href="/customer">Back to Homepage</a></p>
                            </div>
                            :
                            <div>
                                <i style={{ color: "#FD1F2F", fontSize: 60 }} class="fa fa-times-circle"></i>
                                <br />
                                <br />
                                <p>Failed to complete the payment.</p>
                                <p><a href="/customer">Back to Homepage</a></p>
                            </div>
                    }
                </div>
            </div>
        )
    }
}

export default withRouter(CustomerProcessPaymentPage);