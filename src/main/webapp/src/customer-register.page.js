import React from 'react';

import CustomerCustomerForm from './components/Customer-customerForm.js'
import CustomerNavigation from './components/CustomerNavigation';
import api, { ax } from './api.js';
import * as constants from './constants.js';
import { withRouter } from 'react-router-dom';

class CustomerRegisterPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <CustomerCustomerForm />
            </div>
        )
    }
}

export default withRouter(CustomerRegisterPage);