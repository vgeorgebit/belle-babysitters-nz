
import React from 'react';

import AdminNavigation from './components/AdminNavigation.js';
import { withRouter } from 'react-router-dom';
import { ax } from './api.js';
import * as constants from './constants.js';

class AdminCustomerPage extends React.Component {

    constructor(props, context) {
        super(props);
        this.nav = this.nav.bind(this);
        this.state = {
            page: {},
            loading: true,
            pageNum: 0,
            search: '',
            show: false,
            accountDisplay: {},
        };

        ax.get(constants.API_BASE_URL + "customers?page=0&search=" + this.state.search).then(response => {
            this.setState({
                page: response.data,
                loading: false
            });
        }).catch(e => {
            alert("ERROR OCCURED");
        });

        this.loadPage = this.loadPage.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    loadPage(pageNum) {
        this.setState({
            loading: true
        });

        ax.get(constants.API_BASE_URL + "customers?page=" + pageNum + "&search=" + this.state.search).then(response => {
            this.setState({
                page: response.data,
                loading: false,
                pageNum: pageNum
            });
        }).catch(e => {
            alert("ERROR OCCURED");
        });
    }


    handleSearch(event) {
        this.setState({
            search: event.target.value,
            pageNum: 0
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }
    nav(e) {
        e.preventDefault();
        this.props.history.push("/admin/customer/form");
    }

    showModal(e, customer) {

        this.setState({
            accountDisplay: customer,
            show: true
        });

    };


    hideModal = (e) => {
        this.setState({
            show: false,
            file: null
        });
    };


    render() {
        return (
            <div>
                <AdminNavigation />
                <div className="container adminsContainer customersContainer">
                    <div className="row">
                        <div className="col">
                            <h1>Customer Accounts</h1>
                        </div>
                        <div className="col">
                            <button className="btn btn-success" onClick={this.nav}><i className="fa fa-plus-circle" aria-hidden="true"></i> Add New Customer</button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <input onChange={this.handleSearch} className="form-control" type="text" placeholder="Search" />
                        </div>
                    </div>
                    <div>
                        {
                            this.state.loading ?
                                (
                                    <div className="spinner-border text-primary" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                ) :
                                <div>
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th>Family Name</th>
                                                <th>Email Address</th>
                                                <th>Number of Children</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.page.content.map(customer => {
                                                    return (
                                                        <tr key={customer.email} style={{ background: customer.disabled ? "#F0EFEF" : null }} onClick={e => { this.showModal(e, customer) }}>

                                                            <td>{customer.familySurname}</td>
                                                            <td>{customer.email}</td>
                                                            <td>{customer.children.length}</td>
                                                        </tr>)
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <center>
                                        {!this.state.page.first && <span><a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum - 1) }} href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>}
                                        <span>Page {this.state.pageNum + 1} of {this.state.page.totalPages}</span>
                                        {!this.state.page.last && <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum + 1) }} href="">Next</a></span>}
                                    </center>
                                </div>
                        }
                    </div>
                </div>

                {
                    this.state.show &&
                    <div className="blackout" >
                        <div className="modalContainer" >

                            <div className="row">
                                <div className="col col-6">
                                    <h2>{this.state.accountDisplay.familySurname} Family</h2>


                                </div>
                                <div className="col col-4">
                                    <a style={{ height: 'auto', width: '100%' }} id="modifyButton" className="btn btn-warning" href={"/admin/customer/edit?username=" + this.state.accountDisplay.email}><i className="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Modify</a>

                                </div>
                                <div className="col col-2">
                                    <button style={{ height: 'auto', width: '100%' }} type="button" className="btn btn-danger" onClick={this.hideModal}><i className="fa fa-times" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <div class="modalContent">
                                <table className="table" style={{ marginTop: '10%' }}>
                                    <thead>
                                        {this.state.accountDisplay.disabled && <tr><th style={{ textAlign: "center", background: "#FE4040", color: "white" }} colSpan="2"> THIS ACCOUNT IS DISABLED </th></tr>}
                                    </thead>
                                    <tbody>

                                        <tr><th>Family Surname</th><td> {this.state.accountDisplay.familySurname}</td></tr>
                                        <tr><th>Caregivers</th>
                                            <td>
                                            {this.state.accountDisplay.caregivers.map((caregiver) => {
                                                return <span> {caregiver.firstName} {caregiver.lastName}<br />
                                                                Relation: {caregiver.relationshipToChild}<br />
                                                                Ph: {caregiver.phoneNumber} <br />
                                                </span>
                                            })}
                                            </td>
                                        </tr>


                                        <tr><th>Email</th><td> {this.state.accountDisplay.email}</td></tr>
                                        
                                        <tr><th>Membership Expiry Date </th><td> {this.state.accountDisplay.membershipEndDate && this.state.accountDisplay.membershipEndDate.split("-")[2].substring(0, 2) + "-" + this.state.accountDisplay.membershipEndDate.split("-")[1] + "-" + this.state.accountDisplay.membershipEndDate.split("-")[0]}</td></tr>
                                        <tr><th>Emergency Contact</th><td> <span>
                                        {this.state.accountDisplay.emgConFName} {this.state.accountDisplay.emgConLName} <br/>
                                        Relation: {this.state.accountDisplay.emgConRel} <br/>
                                        Ph: {this.state.accountDisplay.emgConNo} <br/>
                                        </span>
                                        </td></tr>
                                        <tr><th>Children</th>
                                            <td>{this.state.accountDisplay.children.map((child) => {
                                            return <span key={child.firstName}>{child.firstName} 
                                            {" (" + (((new Date().getFullYear() - new Date(child.dob).getFullYear()) > 1) ? 
                                            (new Date().getFullYear() - new Date(child.dob).getFullYear())
                                                :(((new Date().getFullYear() - new Date(child.dob).getFullYear()) * 12) + new Date().getMonth() - new Date(child.dob).getMonth() +" months"))
                                        
                                            } {child.gender[0]})<br/>
                                            </span>
                                            })}
                                            </td>
                                        </tr>
                                        <tr><th>Pets </th>
                                            <td> {this.state.accountDisplay.pets.split("\n").map((pet, index) => {
                                                return <span key={index}>{pet}<br /></span>
                                            })}</td>
                                        </tr>
                                        <tr><th>Extra Notes </th><td> {this.state.accountDisplay.extraNotes}</td></tr>
                                        <tr><th>Admin Notes </th><td> {this.state.accountDisplay.adminNotesAdmin}</td></tr>
                                        <tr><th>Babysitter Notes </th><td> {this.state.accountDisplay.adminNotesBabysitter}</td></tr>
                                        <tr><th>How Customer Heard </th><td> {this.state.accountDisplay.howDidYouHear}</td></tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                }

            </div>
        )
    }
}

export default withRouter(AdminCustomerPage);