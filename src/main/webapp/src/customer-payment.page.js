import React from 'react';

import { withRouter } from 'react-router-dom'
import CustomerNavigation from './components/CustomerNavigation.js';
import { ax } from './api.js';
import * as constants from './constants.js';
import BadgeDisplay from './components/BadgeDisplay.js'

class CustomerPaymentPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loadingMessage: "Loading Booking Details...",
            job: null,
            babysitterImages: {},
            processing: false
        };

        this.buttonClick = this.buttonClick.bind(this);
    }

    componentDidMount() {
        const url = new URL(window.location.href);
        const jobId = url.searchParams.get('job');

        ax.get(constants.API_BASE_URL + "jobs/customer/" + jobId).then(response => {
            if (response.data.status != "PENDING_PAYMENT") {
                this.props.history.replace("/customer");
            } else {
                response.data.assignedBabysitters.forEach(babysitter => {
                    ax.get(constants.API_BASE_URL + "babysitter/image/" + babysitter.username, { responseType: 'arraybuffer' }).then(response => {
                        let key = "img_" + babysitter.username;

                        const base64 = btoa(
                            new Uint8Array(response.data).reduce(
                                (data, byte) => data + String.fromCharCode(byte),
                                '',
                            ),
                        );

                        this.setState({
                            [key]: base64
                        });
                    });
                });
                this.setState({
                    job: response.data,
                    loadingMessage: null
                });
            }
        }).catch(error => {
            this.props.history.replace("/customer");
        });
    }

    buttonClick(event) {
        event.preventDefault();

        this.setState({
            processing: true
        });

        ax.post(constants.API_BASE_URL + "job/payment/" + this.state.job.id).then(response => {
            window.location.assign(response.data.redirect_url);
        }).catch(error => {
            console.log(error);
            alert("ERROR!");
        });
    }

    render() {
        let job = this.state.job;
        return (
            <div>
                <CustomerNavigation />
                <div className="container" style={{ maxWidth: 800 }}>
                    <h1 style={{ textAlign: "center", marginTop: 30 }}><i className="fa fa-calendar"></i>&nbsp;&nbsp;Booking Payment &amp; Confirmation</h1>

                    {
                        this.state.loadingMessage ?
                            <div className="text-center" style={{ marginTop: 100 }}>
                                <div className="spinner-grow text-primary" role="status"></div>
                                <p>{this.state.loadingMessage}</p>
                            </div>
                            :
                            <div>
                                <br />
                                {this.state.job && this.state.job.customer && <div>
                                <div className="col">
                                    <h2 style={{ fontSize: 26 }}>Booking Details <span style={{fontSize:18}}>(#{job.id})</span></h2>
                                </div>

                          

                                <div className="babysitterFormContainer container">

                                    <div className="row">
                                        
                                        <div className="col col-4">
                                            <span style={{fontWeight:"bold", textAlign:"center"}}>Start</span><br/>
                                            <span>{job.startTime.split("-")[2].substring(0,2)+"-"+job.startTime.split("-")[1]+"-"+job.startTime.split("-")[0]} <span className="text-primary">{job.startTime.substring(12,17)}</span></span>
                                        </div>
                                        <div className="col col-4">
                                            <span style={{fontWeight:"bold", textAlign:"center"}}>End</span><br/>
                                            <span>{job.endTime.split("-")[2].substring(0,2)+"-"+job.endTime.split("-")[1]+"-"+job.endTime.split("-")[0]} <span className="text-primary" >{job.endTime.substring(12,17)}</span></span>
                                        </div>
                                        <div className="col col-4">
                                            <table style={{ margin: 0 }}>
                                                    <tr>
                                                        <td style={{ all: "unset" }}>
                                                            {job.fullLicenseRequired && <div><i className="fa fa-id-card-o" aria-hidden="true" title="Full Drivers License">&nbsp;</i><span>Full License Required</span></div>}

                                                        </td>
                                                        <td style={{ all: "unset" }}>
                                                            {job.ownCarRequired && <div><i style={{ color: "Green" }} className="fa fa-car" aria-hidden="true" title="Babysitter Must Have Vehicle Access">&nbsp;</i><span>Vehicle Access Required</span></div>}
                                                        </td>
                                                    </tr>
                                                </table>

                                        </div>

                                    </div>
                                    <hr/>

                                    <div className="row">
                                        <div className="col col-12">
                                            <span style={{fontWeight:"bold"}}> Job Description</span><br/>
                                            <span>{job.jobNotes}</span>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col col-lg-4 col-sm-12">
                                        <span style={{fontWeight:"bold"}}>Address</span><br/>
                                        {job.jobAddress.split('\n').map( line => {
                                            return line && <span>{line}<br/></span>
                                        })}
                                        </div>
                                        <div className="col col-lg-4 col-sm-12">
                                            <span style={{fontWeight:"bold"}}>Address Notes</span><br/>
                                            <span>{job.addressNotes}</span>
                                        </div>
                                        <div className="col col-lg-4 col-sm-12">
                                        <span style={{fontWeight:"bold"}}>Children</span><br/>
                                        {job.childrenDetails.split('\n').map(line =>{
                                            return line && <span>{line}<br/></span>
                                        })}
                                        </div>
                                        <div className="col col-6">

                                        </div>
                                    </div>

                                
                                    
                                    </div>
                                </div>
                                }








                                <hr/>
                                <h2 style={{ fontSize: 26 }}>{"Assigned Babysitter" + (this.state.job.assignedBabysitters.length > 1 ? "s" : "")}</h2>
                                <div className="row">
                                    {this.state.job.assignedBabysitters.map(b => {
                                        return (
                                            <div key={b.username} style={{ marginTop: 20 }} className="col-6">
                                                <div className="row">
                                                    <div className="col">
                                                        <h3 style={{ fontSize: 20 }}>{b.firstName}</h3>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col row">
                                                        <div className="col-6">

                                                            <img src={"data:;base64, " + this.state["img_" + b.username]} width="100" height="100" style={{ borderRadius: "100%" }} />
                                                        </div>
                                                        <div className="col-6">
                                                            <b style={{ fontSize: 9 }}>BADGES</b><br />
                                                            <BadgeDisplay babysitter={b}/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col">
                                                        <p style={{ marginTop: 10 }}> {b.bio}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                    })}
                                </div>
                                <hr />
                                <h2 style={{ fontSize: 26 }}>Payment</h2>
                                <div className="row">
                                    <div className="col-6">
                                        <h3 style={{ fontSize: 20 }} className="text-secondary">Babysitter Rates</h3>
                                        <p>Babysitters standard rates of $18 per hour (minimum $30 total per booking) are <b>payable to the babysitter separately at the conclusion on the babysitting occasion</b> (cash preferred).</p>
                                        <p>Some exceptions to the standard rate apply in such cases as, $22 per hour for short notice bookings (less than 12 hours notice), public holidays and any other unusual circumstances.</p>
                                        <p>Petrol charges will also apply if you expect the babysitter to transport your children or travel more than 30 minutes from the centre of Dunedin to get to you.</p>

                                    </div>
                                    <div className="col-6">
                                        <h3 style={{ fontSize: 20 }} className="text-secondary">Booking Fee</h3>
                                        <p>The Booking Fee is required to secure a booking for one of our babysitters, payable in advance, via credit card (using PayPal).</p>
                                        <span className="" style={{ textAlign: "right" }}>Booking Fee: </span>
                                        <span className="badge badge-pill badge-success" style={{ padding: 10, marginLeft: 10 }}>${this.state.job.bookingFeePrice}</span>
                                        <p className="text-danger">Payment is required to confirm booking</p>
                                        <div className="col" style={{ textAlign: "right" }}>
                                            <p style={{ fontSize: 14, marginTop: 10 }}>By confirming you agree to the <a href="http://www.bellebabysitters.co.nz/policy/1" target="_blank">terms and conditions</a></p>
                                            <button disabled={this.state.processing} onClick={this.buttonClick} style={{ marginTop: 10 }} className="btn btn-primary">{this.state.processing ? <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> : <i class="fa fa-credit-card"></i>}{this.state.processing ? <span>&nbsp;&nbsp;Redirecting to PayPal...</span> : <span>&nbsp;&nbsp;Pay Now &amp; Confirm Booking</span>}</button>
                                            <table style={{ marginTop: 20 }} border="0" cellpadding="0" cellspacing="0" align="right"><tr><td align="right"></td></tr><tr><td align="right"><a href="https://www.paypal.com/c2/webapps/mpp/paypal-popup?locale.x=en_C2" title="PayPal Acceptance Mark" onclick="javascript:window.open('https://www.paypal.com/c2/webapps/mpp/paypal-popup?locale.x=en_C2','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/digitalassets/c/website/marketing/apac/C2/logos-buttons/optimize/Full_Online_Tray_RGB.png" width="200" border="0" alt="PayPal Acceptance Mark" /></a></td></tr></table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                    }
                </div>
            </div >
        )
    }
}

export default withRouter(CustomerPaymentPage);