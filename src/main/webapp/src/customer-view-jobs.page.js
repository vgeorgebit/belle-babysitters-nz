import React from 'react';

import { withRouter } from 'react-router-dom';
import { ax } from './api.js';
import * as constants from './constants.js';

import CustomerNavigation from './components/CustomerNavigation.js';

class CustomerJobViewPage extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            page: {},
            loading: true,
            pageNum: 0,
            search: '',
            show: false,
            jobDisplay : {},
            status: ''
            
        };

        ax.post(constants.API_BASE_URL + "jobs/babysitter?page=0&search=" + this.state.search +"&filterBy=").then(response => {
            this.setState({
                page: response.data,
                loading: false
            });
           
        }).catch(e =>{
            alert("ERROR OCCURED");
        });

        this.loadPage = this.loadPage.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    loadPage(pageNum) {
        this.setState({
            loading: true
        });

        ax.post(constants.API_BASE_URL + "jobs/babysitter?page=" + pageNum + "&search=" + this.state.search +"&filterBy="+ this.state.status).then(response => {
            this.setState({
                page: response.data,
                loading: false,
                pageNum: pageNum
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });
    }

    handleSearch(event) {
        this.setState({
            search: event.target.value.replace("#", "%23"),
            pageNum: 0
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }

    showModal(e,job){
        
        this.setState({ 
            jobDisplay: job
        });
      
        this.setState({ show: true });

      };
    
    
    hideModal = (e) => {
        this.setState({ 
            show: false
         });
      };

    render() {
        return (
        <div>
            <CustomerNavigation />

            
            <div className="container adminsContainer" style={{maxWidth: 1000}}>
                    <div className="row">
                        <div className="col">
                            <h1>Upcoming Jobs</h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <input style={{display: "inline"}} onChange={this.handleSearch} className="form-control" type="text" placeholder="Search" />
                            <div style={{display: "inline"}}>
                                <label style={{marginLeft:30, marginRight:10}}>Filter By:</label>
                                <select style={{width: 250, display: "inline"}} type="text" className="form-control" name="status" value={this.state.status} onChange={this.handleChange} >
                                    <option value="">NONE</option>
                                    <option value="PENDING_ASSIGNMENT">PENDING ASSIGNMENT</option>
                                    <option value="PENDING_PAYMENT">PENDING PAYMENT</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div>
                        { 
                            this.state.loading ?
                                (
                                    <div className="spinner-border text-primary" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                ) :
                                <div>
                                    <table className="table">
                                        <thead>
                                            <tr>        
                                                <th>ID</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Customer</th>
                                                <th>Email</th>
                                                <th>Requirements</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            {   this.state.page.content &&
                                                this.state.page.content.map(job => {
                                                    
                                                    return (
                                                        <tr key={job.id} onClick={ e=> {this.showModal(e,job)}}>
                                                            
                                                            {/* <td>{job.dateRequested.split("T")[0]}</td> */}
                                                            <td style={{color:"gray"}}>#{job.id}</td>
                                                            <td>{job.startTime.split("-")[2].substring(0,2)+"-"+job.startTime.split("-")[1]+"-"+job.startTime.split("-")[0]} <br/> <span className="text-primary">{job.startTime.substring(12,17)}</span></td>
                                                            <td>{job.endTime.split("-")[2].substring(0,2)+"-"+job.endTime.split("-")[1]+"-"+job.endTime.split("-")[0]} <br/> <span className="text-primary" >{job.endTime.substring(12,17)}</span></td>
                                                            <td>{job.customer.familySurname}</td>
                                                            <td>{job.customer.email}</td>
                                                            <td>
                                                                <tr style={{display:"inline"}}>
                                                                    <td style={{all:"unset", display:"inline"}}>    
                                                                        {job.fullLicenseRequired && <i className="fa fa-id-card-o" aria-hidden="true" title="Full Drivers License Required">&nbsp;</i>} 
                                                                        
                                                                    </td>
                                                                    <td style={{all:"unset", display:"inline"}}> 
                                                                        {job.ownCarRequired && <i style={{color:"Green"}} className="fa fa-car" aria-hidden="true" title="Vehicle Required">&nbsp;</i>}
                                                                    </td>
                                                                    
                                                                </tr>
                                                            </td>
                                                            <td><span className={"badge badge-pill badge-"+(job.status == "CONFIRMED" ? "success" : "warning")}>{job.status == "CONFIRMED" ? job.status.replace("_"," ") : "PENDING"}</span></td>

                                                            
                                                        </tr>)
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <center>
                                        {!this.state.page.first && <span><a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum - 1) }} href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>}
                                        <span>Page {this.state.pageNum + 1} of {this.state.page.totalPages}</span>
                                        {!this.state.page.last && <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum + 1) }} href="">Next</a></span>}
                                    </center>
                                </div>
                        }
                    </div>
                </div>
                        
                {                
                this.state.show  &&
                <div className="blackout" >
                <div className="modalContainer" >
                    
                    <div className="row">
                        <div className="col col-10">
                            <h2>{this.state.jobDisplay.customer.familySurname} Family</h2>
                            
                        </div>
                        <div className="col col-2">
                            <button style={{height:'auto', width:'100%'}} type="button" className="btn btn-danger" onClick={this.hideModal}><i className="fa fa-times" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div className="modalContent">
                    
                    <table className="table">
                        
                            
                       
                        <tbody>
                            <tr><th>ID</th><td>#{this.state.jobDisplay.id}</td></tr>
                            <tr><th>Status</th><td>{this.state.jobDisplay.status}</td></tr>
                            <tr><th>Date Confirmed</th><td>{this.state.jobDisplay.dateConfirmed ? (this.state.jobDisplay.dateConfirmed.split("-")[2].substring(0,2)+"-"+this.state.jobDisplay.dateConfirmed.split("-")[1]+"-"+this.state.jobDisplay.dateConfirmed.split("-")[0] + " " + this.state.jobDisplay.dateConfirmed.substring(12,17)) : "-"}</td></tr>
                            <tr><th>Job Notes</th><td>{this.state.jobDisplay.jobNotes}</td></tr>
                            <tr><th>Number of Children</th><td>{this.state.jobDisplay.numChildren}</td></tr>
                            <tr><th>Children Notes</th><td>{this.state.jobDisplay.childrenDetails.split("\n").map((child, index) => {
                                return <span key={index} style={{display: "block"}}>{child}</span>
                            })}</td></tr>
                            <tr><th>Start Time</th><td>{this.state.jobDisplay.startTime.split("-")[2].substring(0,2)+"-"+this.state.jobDisplay.startTime.split("-")[1]+"-"+this.state.jobDisplay.startTime.split("-")[0]} {this.state.jobDisplay.startTime.substring(12,17)}</td></tr>
                            <tr><th>End Time</th><td>{this.state.jobDisplay.endTime.split("-")[2].substring(0,2)+"-"+this.state.jobDisplay.endTime.split("-")[1]+"-"+this.state.jobDisplay.endTime.split("-")[0]} {this.state.jobDisplay.endTime.substring(12,17)}</td></tr>
                            <tr><th>Job Address</th><td>{this.state.jobDisplay.jobAddress.split("\n").map((item, index) => {
                                return <span key={index} style={{display:"block"}}>{item}</span>
                            })}</td></tr>
                            <tr><th>Address Notes</th><td>{this.state.jobDisplay.addressNotes}</td></tr>
                            <tr><th>Own Car Required</th><td>{this.state.jobDisplay.ownCarRequired ? "Yes" : "No"}</td></tr>
                            <tr><th>Full License Required</th><td>{this.state.jobDisplay.fullLicenseRequired ? "Yes" : "No"}</td></tr>
                            <tr><th>Date Requested</th><td>{this.state.jobDisplay.dateRequested.split("-")[2].substring(0,2)+"-"+this.state.jobDisplay.dateRequested.split("-")[1]+"-"+this.state.jobDisplay.dateRequested.split("-")[0]} {this.state.jobDisplay.dateRequested.substring(12,17)}</td></tr>

                            


                           
                        </tbody>
                    </table>

                </div>
                </div>
                </div>

                }
        
        </div>
        )
    }
}

export default withRouter(CustomerJobViewPage);