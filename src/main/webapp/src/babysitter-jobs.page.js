import React from 'react';

import { withRouter } from 'react-router-dom';
import { ax } from './api.js';
import * as constants from './constants.js';

import AdminNavigation from './components/AdminNavigation.js';
import ItemListTable from './components/ItemListTable.js';
import BabysitterNavigation from './components/BabysitterNavigation.js';

class BabysitterJobPage extends React.Component {


    render() {
        return (
            <div>
                <BabysitterNavigation/>
                <ItemListTable 
                    title="Pending Job Offers"
                    filter="PENDING"
                    url="jobs/offers"
                    noSearch
                    
                    
                />
                <ItemListTable
                    title="You Are Available For"
                    filter="AVAILABLE"
                    url="jobs/offers"
                    noSearch
                />
                <ItemListTable
                    title="You Are Unavailable For  "
                    filter="UNAVAILABLE"
                    url="jobs/offers"
                    noSearch
                />{/**
                    * Kurt Weston was here
                    */}
            </div>
        )
    }
}

export default withRouter(BabysitterJobPage);