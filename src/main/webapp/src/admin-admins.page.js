import React from 'react';

import AdminNavigation from './components/AdminNavigation.js';
import { withRouter } from 'react-router-dom';
import { ax } from './api.js';
import * as constants from './constants.js';

class AdminAdminsPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            page: {},
            loading: true,
            pageNum: 0,
            search: '',
            show: false,
            accountDisplay : {},
            
        };

        ax.get(constants.API_BASE_URL + "admins?page=0&search=" + this.state.search).then(response => {
            this.setState({
                page: response.data,
                loading: false
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });

        this.loadPage = this.loadPage.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleAddClick = this.handleAddClick.bind(this);
    }

    loadPage(pageNum) {
        this.setState({
            loading: true
        });

        ax.get(constants.API_BASE_URL + "admins?page=" + pageNum + "&search=" + this.state.search).then(response => {
            this.setState({
                page: response.data,
                loading: false,
                pageNum: pageNum
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });
    }

    handleAddClick(event) {
        event.preventDefault();
        this.props.history.push("/admin/admin/form")
    }

    handleSearch(event) {
        this.setState({
            search: event.target.value,
            pageNum: 0
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }

    showModal(e,admin){
        this.setState({ 
            accountDisplay: admin,
            show: true
        });


      };
    
    
    hideModal = (e) => {
        this.setState({ 
            show: false,
            file : null
         });
      };


    render() {
        return (
            <div>
                <AdminNavigation />

                <div class="container adminsContainer">
                    <div class="row">
                        <div class="col">
                            <h1>Admin Accounts</h1>
                        </div>
                        <div class="col">
                            <button onClick={this.handleAddClick} type="button" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Add Admin Account</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input onChange={this.handleSearch} class="form-control" type="text" placeholder="Search" />
                        </div>
                    </div>
                    <div>
                        {
                            this.state.loading ?
                                (
                                    <div class="spinner-border text-primary" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                ) :
                                <div>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>First Name</th>
                                                <th>Surname</th>
                                                <th>Username</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.page.content.map(admin => {
                                                    return (
                                                        <tr key={admin.username} onClick={ e=> {this.showModal(e,admin)}}>
                                                            <td></td>
                                                            <td>{admin.firstName}</td>
                                                            <td>{admin.lastName}</td>
                                                            <td>{admin.username}</td>
                                                            

                                                            {/* {index != 0 && <button href="#" className="btn btn-outline-danger removeBtn" onClick={(e) => { this.removeCaregiver(e, index) }}>Remove</button>} */}
                                                        </tr>)
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <center>
                                        {!this.state.page.first && <span><a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum - 1) }} href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>}
                                        <span>Page {this.state.pageNum + 1} of {this.state.page.totalPages}</span>
                                        {!this.state.page.last && <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum + 1) }} href="">Next</a></span>}
                                    </center>
                                </div>
                        }
                    </div>
                </div>

                { 

                
                this.state.show &&
                <div className="blackout" >
                <div className="modalContainer" >
                    
                    <div className="row">
                        <div className="col col-6">
                            <h2>{this.state.accountDisplay.firstName} {this.state.accountDisplay.lastName}</h2>
                            
                            
                        </div>
                        <div className="col col-4">
                            <a style={{height:'auto', width:'100%'}} id="modifyButton" className="btn btn-warning" href={"/admin/admin/edit?username=" + this.state.accountDisplay.username}><i className="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Modify</a>

                        </div>
                        <div className="col col-2">
                            <button style={{height:'auto', width:'100%'}} type="button" className="btn btn-danger" onClick={this.hideModal}><i className="fa fa-times" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div class="modalContent">
                    <table className="table" style={{marginTop:'10%'}}>

                        <tbody>
                            <tr><th> First Name</th><td> {this.state.accountDisplay.firstName}</td></tr>
                            <tr><th> Last Name</th><td> {this.state.accountDisplay.lastName}</td></tr>
                            <tr><th> Username</th><td> {this.state.accountDisplay.username}</td></tr>
                            <tr><th> Super Admin</th><td> {this.state.accountDisplay.superAdmin ? "True" : "False"}</td></tr>
                        </tbody>
                    </table>

                </div>
                </div>
                </div>

                }

            </div>
        )
    }



}

export default withRouter(AdminAdminsPage);