import React from 'react';

import AdminNavigation from './components/AdminNavigation.js';
import { withRouter } from 'react-router-dom';
import { ax } from './api.js';
import * as constants from './constants.js';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
function parseMonth(item) {
    let month = "";
    switch (item) {
        case "Jan":
            month = "01";
            break;
        case "Feb":
            month = "02";
            break;
        case "Mar":
            month = "03";
            break;
        case "Apr":
            month = "04";
            break;
        case "May":
            month = "05";
            break;
        case "Jun":
            month = "06";
            break;
        case "Jul":
            month = "07";
            break;
        case "Aug":
            month = "08";
            break;
        case "Sep":
            month = "09";
            break;
        case "Oct":
            month = "10";
            break;
        case "Nov":
            month = "11";
            break;
        case "Dec":
            month = "12";
            break;
        default:
            month = "01";
            break;
    }
    return month;
}

const textRegex = RegExp(
    /^[A-Za-z\s\\-]*$/
);

const addressRegex = RegExp(
    /^[a-zA-Z0-9\s,.'-]*$/
);

const numberRegex = RegExp(
    /^\d+$/
);

const postCodeRegex = RegExp(
    /^[0-9\-A-Za-z]+$/
);

class AdminJobsForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            customer: null,

            childrenDetails: '',
            jobAddress: '',
            addressNotes: '',
            jobNotes: '',
            transportRequired: '',
            licenseRequired: '',
            startTime: null,
            endTime: null,
            streetAddress: '',
            addressLine2: '',
            region: '',
            postCode: '',
            city: '',
            endSelected: false,
            startSelected: false,
            children: '',
            responseEndTime: '',
            responseStartTime: '',
            requirements: '',
            status: '',

            formErrors: {
                startTime: '',
                endTime: '',
                children: '',
                addressNotes: '',
                jobNotes: '',
                streetAddress: '',
                addressLine2: '',
                region: '',
                postCode: '',
                city: '',
                requirements: ''
            },

            submitError: '',
            processing: false,
            page: {},
            search: null,
            pageNum: 0,
        };

        if (this.props.job) {
            ax.get(constants.API_BASE_URL + "jobs/" + this.props.job).then(response => {

                let job = response.data;

                if (job.status === "CANCELLED" || job.status === "EXPIRED" || job.status === "COMPLETED") {
                    this.props.history.push("/admin");
                }

                let hour = job.startTime.split(" ")[1].split(":")[0];
                let ampm = parseInt(hour) >= 12 ? "PM" : "AM";
                hour = ampm == "PM" ? "" + (parseInt(hour) - 12) : hour;

                let start = job.startTime.split("-")[2].split(" ")[0] + "/" + parseMonth(job.startTime.split("-")[1]) + "/" + job.startTime.split("-")[0] + " " + hour + ":" + job.startTime.split(" ")[1].split(":")[1] + " " + ampm;
                let startUS = parseMonth(job.startTime.split("-")[1]) + "/" + job.startTime.split("-")[2].split(" ")[0] + "/" + job.startTime.split("-")[0] + " " + hour + ":" + job.startTime.split(" ")[1].split(":")[1] + " " + ampm;


                hour = job.endTime.split(" ")[1].split(":")[0];
                ampm = parseInt(hour) >= 12 ? "PM" : "AM";
                hour = ampm == "PM" ? "" + (parseInt(hour) - 12) : hour;

                let end = job.endTime.split("-")[2].split(" ")[0] + "/" + parseMonth(job.endTime.split("-")[1]) + "/" + job.endTime.split("-")[0] + " " + hour + ":" + job.endTime.split(" ")[1].split(":")[1] + " " + ampm;
                let endUS = parseMonth(job.endTime.split("-")[1]) + "/" + job.endTime.split("-")[2].split(" ")[0] + "/" + job.endTime.split("-")[0] + " " + hour + ":" + job.endTime.split(" ")[1].split(":")[1] + " " + ampm;

                this.setState({
                    responseEndTime: new Date(endUS),
                    responseStartTime: new Date(startUS),

                    customer: job.customer,
                    childrenDetails: job.childrenDetails,
                    startTime: start,
                    endTime: end,
                    addressNotes: job.addressNotes,
                    jobNotes: job.jobNotes,
                    transportRequired: '',
                    licenseRequired: '',
                    streetAddress: job.jobAddress.split('\n')[0],
                    addressLine2: job.jobAddress.split('\n')[1],
                    region: job.jobAddress.split('\n')[2],
                    postCode: job.jobAddress.split('\n')[3],
                    city: job.jobAddress.split('\n')[4],
                    requirements: (job.ownCarRequired && job.fullLicenseRequired) ? "DRIVEN_REQUIRED" : ((job.fullLicenseRequired) ? "DRIVEN_PROVIDED" : "NONE"),
                    status: job.status

                });
            });
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.loadPage = this.loadPage.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);
        this.selectCustomer = this.selectCustomer.bind(this);
        this.handleChildCheckChange = this.handleChildCheckChange.bind(this);
        this.searchFocus = this.searchFocus.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.deleteJob = this.deleteJob.bind(this);

        this.isFormValid = this.isFormValid.bind(this);
        this.validateFormInput = this.validateFormInput.bind(this);

    }

    validateFormInput(name = null, callback = null) {
        let formErrors = { ...this.state.formErrors };
        let values = {}
        if (name != null) {
            values[name] = this.state[name];
        } else {
            values = this.state;
        }

        for (const key in values) {
            let value = values[key];
            switch (key) {



                case "startTime":
                    formErrors.startTime = null;
                    // this.props.job ? null :
                    // !value ? "Start Time is required" :
                    // !this.state.startSelected && !this.state.endSelected ? null : 
                    //     value.length < 1 ? "Start Time is required" :
                    //         new Date() >= new Date(value) ? "Start Time must be in the future":
                    //         (new Date().getDate() === new Date(value).getDate() 
                    //         && new Date().getMonth() === new Date(value).getMonth()
                    //         && new Date().getFullYear() === new Date(value).getFullYear()
                    //         && (new Date().getHours() + 1) >= new Date(value).getHours()) ? "Please contact us for a short notice booking" :
                                    
                    //                 (this.state.endTime && new Date(value) >= new Date(this.state.endTime)) ? "Start Time must be before End Time" :
                    //                 null; 
                     break;
                case "endTime":
                    formErrors.endTime = null;
                    // this.props.job ? null :
                    // !value ? "End Time is required" :
                    // !this.state.startSelected && !this.state.endSelected ? null :
                    //     value.length < 5 ? "End Time is required" :
                    //         new Date()  >= new Date(value) ? "End Time must be in the future" :
                    //             this.state.startTime && new Date(value) <= new Date(this.state.startTime) ? "End Time must be after Start Time" :
                    //             null;
                    break;
                case "streetAddress":
                    formErrors.streetAddress =
                        value.length < 1 ? "Street address is required" :
                            !addressRegex.test(value) ? "Street address contains invalid characters" :
                                value.length > 64 ? "Street address is too long" :
                                    null;
                    break;
                case "addressLine2":
                    formErrors.addressLine2 =
                        value.length < 1 ? "Suburb is required" :
                            !addressRegex.test(value) ? "Suburb contains invalid characters" :
                                value.length > 64 ? "Suburb is too long" :
                                    null;
                    break;
                case "city":
                    formErrors.city =
                        value.length < 1 ? "City name is required" :
                            !textRegex.test(value) ? "City name may only contain alphabetic characters" :
                                value.length > 64 ? "City name is too long" :
                                    null;
                    break;
                case "region":
                    formErrors.region =
                        value.length < 1 ? "Region name is required" :
                            !textRegex.test(value) ? "Region name may only contain alphabetic characters" :
                                value.length > 64 ? "Region name is too long" :
                                    null;
                    break;
                case "postCode":
                    formErrors.postCode =
                        value.length < 1 ? "Post code is required" :
                            !postCodeRegex.test(value) ? "Post code may only contain alphanumeric characters." :
                                value.length < 4 ? "Post code is too short" :
                                    value.length > 10 ? "Post code is too long" :
                                        null;
                    break;
                case "addressNotes":
                    formErrors.addressNotes =
                        value.length > 512 ? "Address Notes is too long" :
                            !addressRegex.test(value) ? "Address Notes has invalid characters" :
                                null
                    break;
                case "children":
                    formErrors.children =
                        this.props.job ? null : "At least one child must be selected";
                    this.state.customer.children.forEach(child => {
                        if (child.selected) {
                            formErrors.children = null;
                        }

                    })
                    break;
                case "jobNotes":
                    formErrors.jobNotes =  null;
                        // value.length < 1 ? "Booking Notes is required" :
                        //     value.length > 512 ? "Booking Notes is too long" : 
                        //         !addressRegex.test(value) ? "Booking Notes has invalid characters" :
                    break;

                case "requirements":
                    formErrors.requirements =
                        !value ? "Transport requirements is required" : null;
                    break;


                default:
                    break;
            }
        }
        this.setState({
            formErrors,
        }, () => {
            if (callback)
                callback();
        });
    }

    isFormValid() {
        let valid = true;
        Object.keys(this.state.formErrors).forEach(key => {

            if (this.state.formErrors[key] != null)
                valid = false;
        });

        return valid;
    };

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            processing: true
        });

        this.validateFormInput(null, () => {
            if (!this.isFormValid()) {
                this.setState({
                    submitError: "Please fix the highlighted errors in the form",
                    processing: false
                });
            } else {
                let duration = !this.props.job ?  
                Math.round(((new Date(this.state.endTime) - new Date(this.state.startTime)) / 36e5) * 100) / 100
            :  Math.round(((new Date( this.state.endSelected ? this.state.endTime : this.state.responseEndTime) 
                - new Date( this.state.startSelected ? this.state.startTime : this.state.responseStartTime)) / 36e5)*100)/100;
            
                if( duration < 2){
                    if(!window.confirm("This booking is less than two hours long, are you sure you want to continue?")){
                        this.setState({processing:false});
                        return;
                    }
                }
                
                
                let request = {
                    childrenDetails: "",
                    startTime: this.state.startSelected ? this.state.startTime : this.state.responseStartTime,
                    endTime: this.state.endSelected ? this.state.endTime : this.state.responseEndTime,

                    addressNotes: this.state.addressNotes,
                    jobAddress: this.state.streetAddress + "\n"
                        + this.state.addressLine2 + "\n"
                        + this.state.region + "\n"
                        + this.state.postCode + "\n"
                        + this.state.city + "\n",
                    jobNotes: this.state.jobNotes,
                    ownCarRequired: this.state.requirements == "DRIVEN_REQUIRED",
                    fullLicenseRequired: this.state.requirements == "DRIVEN_REQUIRED" || this.state.requirements == "DRIVEN_PROVIDED",
                    numChildren: 0
                };

                this.state.customer.children.forEach(child => {
                    if (child.selected) {
                        request["childrenDetails"] += child.firstName + " (" +  
                        (((new Date().getFullYear() - new Date(child.dob).getFullYear()) > 1) ? 
                        (new Date().getFullYear() - new Date(child.dob).getFullYear())
                        :(((new Date().getFullYear() - new Date(child.dob).getFullYear()) * 12) + new Date().getMonth() - new Date(child.dob).getMonth() +" months"))
                     
                            + " "+child.gender[0] + ")\nNotes: " + child.medical + "\n\n";
                        request["numChildren"]++;
                    }
                });

                if (!this.props.job) {
                    ax.post(constants.API_BASE_URL + "jobs/" + this.state.customer.email, request).then(response => {
                        this.props.history.push("/admin");
                    }).catch(error => {
                        let msg = '';
                        if (error.response && error.response.data && error.response.data.constraintViolations) {
                            error.response.data.constraintViolations.map(v => {
                                msg += v + '  ';
                            });
                        } else if (error.response && error.response.data && error.response.data.error_description) {
                            msg = error.response.data.error_description;
                        } else {
                            msg = "An unknown error occurred";
                        }

                        this.setState({
                            submitError: msg,
                            processing: false
                        });
                    });
                } else {
                    request["childrenDetails"] = this.state.childrenDetails;

                    ax.put(constants.API_BASE_URL + "jobs/" + this.props.job, request).then(response => {
                        this.props.history.push("/admin");
                    }).catch(error => {
                        let msg = '';
                        if (error.response && error.response.data && error.response.data.constraintViolations) {
                            error.response.data.constraintViolations.map(v => {
                                msg += v + '  ';
                            });
                        } else if (error.response && error.response.data && error.response.data.error_description) {
                            msg = error.response.data.error_description;
                        } else {
                            msg = "An unknown error occurred";
                        }

                        this.setState({
                            submitError: msg,
                            processing: false
                        });
                    });

                }
            }
        });

    }

    selectCustomer(cust) {
        cust.children.forEach((value) => {
            value["selected"] = true;
        });
        this.setState({
            customer: cust,
        });
    }

    loadPage(pageNum) {
        this.setState({
            loading: true
        });

        ax.get(constants.API_BASE_URL + "customers?page=" + pageNum + "&search=" + this.state.search).then(response => {
            this.setState({
                page: response.data,
                loading: false,
                pageNum: pageNum
            });

        }).catch(e => {
            alert("ERROR OCCURED");
        });
    }

    getInputClasses(formError) {
        return "form-control" + (formError === null ? " is-valid" : formError ? " is-invalid" : "");
    }

    handleChildCheckChange(event, i) {
        let customer = this.state.customer;
        customer.children[i]["selected"] = event.target.checked;

        this.setState({
            customer
        });
    }

    handleCheckChange(event) {
        this.setState({
            [event.target.name]: event.target.checked
        })
    }

    handleSearch(event) {
        this.setState({
            search: event.target.value,
            pageNum: 0
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }

    handleChange(event) {
        event.preventDefault();

        const { name, value } = event.target;
        this.setState({
            [name]: value,
            submitError: null
        }, () => {
            this.validateFormInput(name);
        });
    }

    handleDateChange(time, isStart) {
        //startSelected & endSelected for modify date pickers to work 
        if (isStart && !this.state.startSelected) {
            this.setState({
                startTime: null,
                startSelected: true
            }, () => {
                this.validateFormInput("startTime");

            });
        }

        if (!isStart && !this.state.endSelected) {
            this.setState({
                endTime: null,
                endSelected: true
            }, () => {
                this.validateFormInput("endTime");

            });
        }

        if (isStart) {
            this.setState({
                startTime: time,
                startSelected: true
            }, () => {
                this.validateFormInput("startTime");

            });
        } else {
            this.setState({
                endTime: time,
                endSelected: true
            }, () => {
                this.validateFormInput("endTime");

            });
        }

    }

    searchFocus(bool) {
        this.setState({
            searchFocused: bool
        })
    }

    autofillAddress(caregiver) {

        this.setState({
            streetAddress: caregiver.streetAddress,
            addressLine2: caregiver.addressLine2,
            addressNotes: caregiver.addressNotes,
            region: caregiver.region,
            postCode: caregiver.postCode,
            city: caregiver.city
        })
    }



    deleteJob() {
        ax.post(constants.API_BASE_URL + "jobs/" + this.props.job + "/cancel").then(response => {
            this.props.history.push("/admin");

        }).catch(e => {
            console.log(e);
            alert("ERROR OCCURED");
        });
    }

    render() {
        return (
            <div>
                <AdminNavigation />


                <div className="babysitterFormContainer container">
                    <button className="btn btn-danger" style={{ float: "right" }} onClick={() => { this.props.history.push(this.state.status === "CONFIRMED" ? "/admin/confirmed" : "/admin") }}>Cancel</button>
                    <h1>{!this.props.job ? 'New Booking' : 'Modify Booking'}</h1>
                    <form onSubmit={this.handleSubmit}>
                        <div className="input-group row">
                            <div className="col col-6">
                                <label>Customer:</label>
                                <input type="text" autoComplete="off" disabled={this.state.customer} name="customer" className={this.props.job ? "form-control" : this.state.customer ? "form-control is-valid" : "form-control"} onFocus={(e) => { this.searchFocus(true) }} value={!this.state.customer ? this.state.search : this.state.customer.familySurname + ", (" + this.state.customer.email + ")"} onChange={this.handleSearch} placeholder="Search" />



                            </div>
                            <div className="col col-6">
                                {!this.props.job && this.state.search === null &&
                                    <span style={{ minHeight: "50%", display: "inline-flex", alignItems: "center" }}>Please select a customer to continue...</span>
                                }

                                {this.state.customer === null && this.state.searchFocused ?
                                    this.state.loading ?
                                        (
                                            <div className="spinner-border text-primary" role="status">
                                                <span className="sr-only">Loading...</span>
                                            </div>
                                        ) :
                                        <div>
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th>Family Name</th>
                                                        <th>Email Address</th>
                                                    </tr>
                                                </thead>
                                                <tbody className="jobCustomerContainer">
                                                    {this.state.page.content && this.state.page.content.map(customer => {
                                                        return (
                                                            <tr key={customer.email} style={{ background: customer.disabled ? "#F0EFEF" : null }} onClick={(e) => { this.selectCustomer(customer) }} >
                                                                {/* onClick={ e=> {this.showModal(e,customer.email)}} */}

                                                                <td>{customer.familySurname}</td>
                                                                <td>{customer.email}</td>
                                                            </tr>)
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>
                                    : null}

                            </div>
                        </div>
                        {/* FORM CONTENT STARTS HERE */}
                        {
                            this.state.customer &&
                            <div>
                                <div className="form-group" style={{padding:0}}>
                                <div className="row" style={{margin:0, padding:0}}>
                                    <div className="form-group col-md-6" style={{margin:0, padding:0}}>
                                        <label>Start Date:</label>
                                        <div className="datepicker-custom">
                                            <DatePicker value={this.state.startTime} onChange={(date) => { this.handleDateChange(date, true) }}
                                                selected={!this.props.job ? this.state.startTime : this.state.startSelected ? this.state.startTime : null}
                                                showTimeSelect timeFormat="HH:mm"
                                                timeIntervals={15} timeCaption="time"
                                                dateFormat="dd/MM/yyyy h:mm aa" name="startTime"
                                                placeholder="dd/mm/yyyy"
                                                name="startTime"
                                                className={this.getInputClasses(this.state.formErrors.startTime)} placeholder="dd/mm/yyyy"

                                                autoComplete="off" placeholder="dd/mm/yyyy"
                                                popperModifiers={{
                                                    
                                                    preventOverflow: {
                                                        enabled: true,
                                                        escapeWithReference: false,
                                                        boundariesElement: "viewport" 
                                                    },
                                                  }}

                                            />
                                    </div >
                                    
                                    {this.state.formErrors.startTime && <span className="formError">{this.state.formErrors.startTime}</span>}
                                    
                                    {!this.state.formErrors.startTime && !this.state.formErrors.endTime && 
                                    this.state.startTime && this.state.endTime &&
                                        <span>Duration: {
                                            !this.props.job ?  
                                            Math.round(((new Date(this.state.endTime) - new Date(this.state.startTime)) / 36e5) * 100) / 100
                                            :  Math.round(((new Date( this.state.endSelected ? this.state.endTime : this.state.responseEndTime) 
                                                - new Date( this.state.startSelected ? this.state.startTime : this.state.responseStartTime)) / 36e5)*100)/100
                                            } hours</span>
                                    }
                                </div>

                                <div className="form-group col-md-6" style={{margin:0, padding:0}}>
                                    <label>End Date:</label>
                                    <div className="datepicker-custom">
                                    <DatePicker value={this.state.endTime} onChange={ (date) => {this.handleDateChange(date, false)}}
                                            selected={!this.props.job ? this.state.endTime : this.state.endSelected ? this.state.endTime : null}
                                            showTimeSelect timeFormat="HH:mm"
                                            timeIntervals={15} timeCaption="time"
                                            dateFormat="dd/MM/yyyy h:mm aa" name="endTime"
                                            className={this.getInputClasses(this.state.formErrors.endTime)} placeholder="dd/mm/yyyy"
                                            name="endTime"
                                            popperModifiers={{
                                                preventOverflow: {
                                                    enabled: true,
                                                    escapeWithReference: false,
                                                    boundariesElement: "viewport" 
                                                },
                                              }}
                                            autoComplete="off"/>
                                    </div>
                                    {this.state.formErrors.endTime && <span className="formError">{this.state.formErrors.endTime}</span>}
                                </div>
                                </div>
                                </div>
                                <h4>Address for Booking</h4>
                                <div className="row">
                                    <div className="col col-6" style={{ float: "left", textAlign: "left", fontSize: 12 }}>
                                        {
                                            this.state.customer.caregivers.map((value, i) => {
                                                return (
                                                    value.firstName && value.lastName &&
                                                    <div>
                                                        <span><a href="#" onClick={(event) => { this.autofillAddress(value) }}>{"Copy address from " + value.firstName + " " + value.lastName}</a></span>
                                                        <br />
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                    <br />
                                </div>
                                <div className="form-row" autoComplete="off">

                                    <div className="form-group col-md-6">

                                        <label>Street Address</label>
                                        <input autoComplete="off" className={"form-control" + (this.state.formErrors.streetAddress === null ? " is-valid" : this.state.formErrors.streetAddress ? " is-invalid" : "")} onChange={this.handleChange} type="text" name={"streetAddress"} value={this.state.streetAddress} />
                                        {this.state.formErrors.streetAddress && <span className="formError">{this.state.formErrors.streetAddress}</span>}
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label>Suburb</label>
                                        <input autoComplete="off" className={"form-control" + (this.state.formErrors.addressLine2 === null ? " is-valid" : this.state.formErrors.addressLine2 ? " is-invalid" : "")} onChange={this.handleChange} type="text" name={"addressLine2"} value={this.state.addressLine2} />
                                        {this.state.formErrors.addressLine2 && <span className="formError">{this.state.formErrors.addressLine2}</span>}
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <label>City</label>
                                        <input autoComplete="off" className={"form-control" + (this.state.formErrors.city === null ? " is-valid" : this.state.formErrors.city ? " is-invalid" : "")} onChange={this.handleChange} type="text" name={"city"} value={this.state.city} />
                                        {this.state.formErrors.city && <span className="formError">{this.state.formErrors.city}</span>}
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label>Region</label>
                                        <input autoComplete="off" className={"form-control" + (this.state.formErrors.region === null ? " is-valid" : this.state.formErrors.region ? " is-invalid" : "")} onChange={this.handleChange} type="text" name={"region"} value={this.state.region} />
                                        {this.state.formErrors.region && <span className="formError">{this.state.formErrors.region}</span>}
                                    </div>
                                    <div className="form-group col-md-2">
                                        <label>Post Code</label>
                                        <input autoComplete="off" className={"form-control" + (this.state.formErrors.postCode === null ? " is-valid" : this.state.formErrors.postCode ? " is-invalid" : "")} onChange={this.handleChange} type="text" name={"postCode"} value={this.state.postCode} />
                                        {this.state.formErrors.postCode && <span className="formError">{this.state.formErrors.postCode}</span>}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col col-12">
                                        <label>Address Notes</label>
                                        <textarea autoComplete="off" placeholder="E.g. difficulty finding address, car parks, hazards, etc" className={this.getInputClasses(this.state.formErrors.addressNotes)} onChange={this.handleChange} type="text" name="addressNotes" rows="4" value={this.state.addressNotes}></textarea>
                                        {this.state.formErrors.addressNotes && <span className="formError">{this.state.formErrors.addressNotes}</span>}
                                    </div>
                                </div>

                                <div className="row">
                                    {this.props.job ?
                                        <div className="col col-12" >
                                            <span>You must create another booking to change children</span><br />
                                            <span style={{ color: "gray" }}>{this.state.childrenDetails.split("\n").map(item => {

                                                return item ? <div>{item} </div> : "";

                                        })}</span>
                                    
                                    </div> :
                                    <div className="col col-12">
                                        <label>Children Present</label> 
                                        <br/>
                                        <ul style={{listStyleType:"none"}}  name="children">
                                        {this.state.customer && this.state.customer.children.map((child, i) => {
                                            return (<li key={child.firstName}><input type="checkbox" name={"child-"+i} checked={this.state.customer.children[i].selected} onChange={(e) => {this.handleChildCheckChange(e, i)}} className="form-check-input" />
                                            <label className="form-check-label" htmlFor={"child-"+i}>
                                            {child.firstName} 
                                            {" (" + (((new Date().getFullYear() - new Date(child.dob).getFullYear()) > 1) ? 
                                            (new Date().getFullYear() - new Date(child.dob).getFullYear())
                                                :(((new Date().getFullYear() - new Date(child.dob).getFullYear()) * 12) + new Date().getMonth() - new Date(child.dob).getMonth() +" months"))
                                        
                                        } {child.gender[0]})
                                            
                                            </label>
                                            </li>
                                        );
                                        })
                                        }
                                        </ul> 
                                        {this.state.formErrors.children && <span className="formError">{this.state.formErrors.children}</span>}
                                   
                                    </div>
                                }
                                </div>

                                <div className="row">
                                    <div className="col col-12">
                                        <label>Booking Notes</label>
                                        <textarea autoComplete="off" placeholder="E.g. purpose of booking, dinner times, bed times, etc" className={this.getInputClasses(this.state.formErrors.jobNotes)} onChange={ this.handleChange } type="text" name="jobNotes" rows="4" value={this.state.jobNotes} />
                                        {this.state.formErrors.jobNotes && <span className="formError">{this.state.formErrors.jobNotes}</span>}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col col-12">
                                        <label>Transport Requirements:</label>
                                        <select className={this.props.job ? "form-control" : this.state.requirements ? "form-control is-valid" : "form-control"} type="text" name="requirements" value={this.state.requirements} onChange={this.handleChange} >
                                            <option value="" disabled>Please select</option>
                                            <option value="NONE">None</option>
                                            <option value="DRIVEN_REQUIRED">Children Must Be Driven and Own Car Required</option>
                                            <option value="DRIVEN_PROVIDED">Children Must Be Driven and Car Provided</option>
                                        </select>
                                        {this.state.formErrors.requirements && <span className="formError">{this.state.formErrors.requirements}</span>}

                                    </div>

                                </div>
                                <div className="row">
                                    <div className="col col-6">
                                        {this.props.job &&
                                            <input style={{ float: "left" }} disabled={this.state.processing} type="button" onClick={(e) => { if (window.confirm("Are you sure you want to cancel this job?")) this.deleteJob() }} value={this.state.processing ? "Cancelling..." : "Cancel Booking"} className="btn btn-danger" />
                                        }
                                    </div>
                                    <div className="col col-6">
                                        <input style={{ float: "right" }} disabled={this.state.processing} type="submit" value={this.state.processing ? "Processing..." : (this.props.job != null ? "Update Booking" : "Add Booking")} className="btn btn-success" />

                                    </div>
                                </div>


                            </div>
                        }
                    </form>

                </div>

            </div>
        )
    }
}

export default withRouter(AdminJobsForm);