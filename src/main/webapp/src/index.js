import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter, Route} from 'react-router-dom';
import { ProtectedRoute } from './components/ProtectedRoute'
import * as constants from './constants.js'

import HomePage from './home.page.js';
import ErrorPage from './error.page.js';

import AdminIndexPage from './admin-index.page.js';
import AdminCustomerPage from './admin-customers.page.js';
import AdminCustomerFormPage from './admin-customer-form.page.js';
import AdminCustomerEditPage from './admin-customer-edit.page.js';
import AdminUpdatePasswordPage from './admin-update-password.page.js';
import AdminBabysittersPage from './admin-babysitters.page.js';
import AdminBabysittersEditPage from './admin-babysitter-edit.page.js'
import CreateAdminPage from './admin-admin-form.page.js';
import AdminAdminEditPage from './admin-admin-edit.page.js';
import adminJobsFormPage from './admin-jobs-form.page.js';

import BabysitterIndexPage from './babysitter-index.page.js';
import BabysitterAccountPage from './babysitter-account.page.js';
import BabysitterAccountEditPage from './babysitter-account-edit.page.js';
import BabysitterJobPage from './babysitter-jobs.page.js';
import BabysitterUpcomingPage from './babysitter-upcoming-jobs.page.js';
import BabysitterCompletedPage from './babysitter-completed-jobs.page.js';
import BabysitterHelpPage from './babysitter-help.page';


import CreateBabysitterPage from './admin-babysitter-form.page'

import CustomerIndexPage from './customer-index.page.js';
import CustomerInquiryPage from './customer-inquiry-page'
import CustomerJobPaymentPage from './customer-payment.page.js'
import CustomerProcessPaymentPage from './customer-payment-process.page.js'
import CustomerJobForm from './customer-job-form.page.js'
import CustomerAccountPage from './customer-account.page.js'
import CustomerAccountEditPage from './customer-account-edit.page.js'
import CustomerHelpPage from './customer-help.page';
import CustomerRegisterPage from './customer-register.page.js';


import AccountSetupForm from './components/AccountSetupForm.js';
import LoginForm from './components/LoginForm.js';
import PasswordResetRequest from './components/PasswordResetRequest';
import PasswordResetForm from './components/PasswordResetForm';
import AdminAdminsPage from './admin-admins.page';
import AdminConfirmedJobs from './admin-confirmed-jobs.page';
import AdminAssignPage from './admin-assign.page';
import AdminReassignPage from './admin-reassign.page.js';
import AdminEditJobPage from './admin-jobs-edit.page.js';
import AdminPastJobPage from './admin-past-jobs.page.js';


import './styles.css';
import NoPageFound from './components/NoPageFound';


function App() {
    return (
        
        <BrowserRouter>
            <Route exact path="/" component={HomePage} />

            <Route exact path="/admin_login" component={() => <LoginForm userType="admin" />} />

            <ProtectedRoute exact path="/admin" component={AdminIndexPage} allowedRole={constants.ROLE_ADMIN} />
            <ProtectedRoute exact path="/admin/customer" component={AdminCustomerPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/customer/form" component={AdminCustomerFormPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/customer/edit" component={AdminCustomerEditPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/admin/form" component={CreateAdminPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/changepassword" component={AdminUpdatePasswordPage} allowedRole={constants.ROLE_ADMIN} />
            <ProtectedRoute exact path="/admin/babysitter" component={AdminBabysittersPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            {/* <ProtectedRonpute exact path="/admin/babysitter/view" component={AdminBabysittersViewPage} allowedRole={constants.ROLE_SUPER_ADMIN} /> */}
            <ProtectedRoute exact path="/admin/admin" component={AdminAdminsPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/confirmed" component={AdminConfirmedJobs} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/historical" component={AdminPastJobPage} allowedRole={constants.ROLE_SUPER_ADMIN} />

            <ProtectedRoute exact path="/admin/babysitter/edit" component={AdminBabysittersEditPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/admin/edit" component={AdminAdminEditPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/babysitter/form" component={CreateBabysitterPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/jobs/form" component={adminJobsFormPage} allowedRole={constants.ROLE_ADMIN} />
            <ProtectedRoute exact path="/admin/jobs/assign" component={AdminAssignPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/jobs/edit" component={AdminEditJobPage} allowedRole={constants.ROLE_SUPER_ADMIN} />
            <ProtectedRoute exact path="/admin/jobs/reassign" component={AdminReassignPage} allowedRole={constants.ROLE_SUPER_ADMIN} />

            <Route exact path="/babysitter_login" component={() => <LoginForm userType="babysitter"/>} />
            <Route exact path="/babysitter/setup" component={() => <AccountSetupForm userType="babysitter" /> } />
            <Route exact path="/babysitter/forgotpassword" component={() => <PasswordResetRequest userType="babysitter"/>} />
            <Route exact path="/babysitter/reset" component={() => <PasswordResetForm userType="babysitter"/>} />

            <ProtectedRoute exact path="/babysitter/account" component={BabysitterAccountPage} allowedRole={constants.ROLE_BABYSITTER} />
            <ProtectedRoute exact path="/babysitter/account/edit" component={BabysitterAccountEditPage} allowedRole={constants.ROLE_BABYSITTER} />
            <Route exact path="/babysitter/help" component={BabysitterHelpPage} />
            
            <ProtectedRoute exact path="/babysitter/jobs" component={BabysitterJobPage} allowedRole={constants.ROLE_BABYSITTER} />
            <ProtectedRoute exact path="/babysitter" component={BabysitterUpcomingPage} allowedRole={constants.ROLE_BABYSITTER} />
            <ProtectedRoute exact path="/babysitter/completed" component={BabysitterCompletedPage} allowedRole={constants.ROLE_BABYSITTER} />

            <ProtectedRoute exact path="/customer" component={CustomerIndexPage} allowedRole={constants.ROLE_CUSTOMER} />
            <Route exact path="/register" component={CustomerRegisterPage} />
            <Route exact path="/customer_login" component={() => <LoginForm userType="customer"/>} />
            <Route exact path="/customer/setup" component={() => <AccountSetupForm userType="customer" /> } />
            <Route exact path="/customer/forgotpassword" component={() => <PasswordResetRequest userType="customer"/>} />
            <Route exact path="/customer/reset" component={() => <PasswordResetForm userType="customer"/>} />
            <Route exact path="/customer/pay" component={CustomerJobPaymentPage} allowedRole={constants.ROLE_CUSTOMER} />
            <Route exact path="/customer/processpayment" component={CustomerProcessPaymentPage} />
            <Route exact path="/customer/jobs/form" component={CustomerJobForm} />
            <Route exact path="/customer/help" component={CustomerHelpPage} />
            

            <ProtectedRoute exact path="/customer/account" component={CustomerAccountPage} allowedRole={constants.ROLE_CUSTOMER} />
            <ProtectedRoute exact path="/customer/account/edit" component={CustomerAccountEditPage} allowedRole={constants.ROLE_CUSTOMER} />
            <ProtectedRoute exact path="/customer/Enquiry" component={CustomerInquiryPage} allowedRole={constants.ROLE_CUSTOMER} />

        </BrowserRouter>
        
   
    );
}

ReactDOM.render(<App />, document.getElementById('root'));