import React from 'react';

import CreateAdminPage from './admin-admin-form.page'


class AdminAdminEditPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: new URL(window.location.href).searchParams.get('username'),

        }

        
    }

    componentDidMount() {
        const url_string = window.location.href;
        const url = new URL(url_string);
        const username = url.searchParams.get('username');
        //  console.log(username);
        this.setState({
            username: username
        });

    }

    render() {
        return (
            <div>
                <CreateAdminPage username={this.state.username}/>
               
            </div>
        )
    }
}

export default AdminAdminEditPage;