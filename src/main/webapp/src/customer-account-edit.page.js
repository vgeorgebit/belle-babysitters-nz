import React from 'react';

import CustomerCustomerForm from './components/Customer-customerForm.js'
import CustomerNavigation from './components/CustomerNavigation';
import api, { ax } from './api.js';
import * as constants from './constants.js';
import { withRouter } from 'react-router-dom';

class CustomerAccountEditPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: ''
        }
    }

    componentDidMount() {
        ax.get(constants.API_BASE_URL + "customer/owndetails").then(response => {
            this.setState({
                username: response.data.username
            });
        }).catch(error => {
            api.clearTokens();
            this.props.history.replace("/customer");
        });
    }

    render() {
        return (
            <div>
                <CustomerNavigation />
                <CustomerCustomerForm editMode={true}/>
                
            </div>
        )
    }
}

export default withRouter(CustomerAccountEditPage);