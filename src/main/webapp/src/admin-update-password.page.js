import React from 'react';
import AdminNavigation from './components/AdminNavigation.js';
import ChangePassword from './components/ChangePassword.js';

class AdminUpdatePasswordPage extends React.Component {


    render() {
        return (<div>
            <AdminNavigation />
            <ChangePassword />
        </div> )
    }
}

export default AdminUpdatePasswordPage;