import axios from 'axios';
import * as constants from './constants.js';

export const ax = axios.create({});

let alreadyRefreshing = false;
let waitingRequests = [];

ax.interceptors.request.use(function (config) {
    config.headers["Content-Type"] = "application/json";
    config.params = { access_token: localStorage.getItem("access") };
    return config;
});

ax.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {
        const { config } = error;
        const originalRequest = config;

        // If error was due to an expired token, try to refresh it...
        if (error.response && error.response.data.error === "invalid_token") {
            if (!alreadyRefreshing) {
                alreadyRefreshing = true;

                const formData = new FormData();
                formData.set("grant_type", "refresh_token");
                formData.set("refresh_token", localStorage.getItem("refresh"));

                axios.post(constants.API_OAUTH_URL + "token", formData, {
                    headers: {
                        "Authorization": "Basic YmVsbGViYWJ5c2l0dGVycy1jbGllbnQ6YjNsbGVCNGJ5c2lUVGVyJA==",
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }).then(response => {
                    // Successfully refresh token
                    processAndStoreTokenResponse(response);
                    alreadyRefreshing = false;
                    waitingRequests.map(cb => cb()); // Perform all buffered requests
                    waitingRequests = [];
                }).catch(error => {
                    // Clear tokens & reload page to trigger redirect to login screen
                    localStorage.removeItem("access");
                    localStorage.removeItem("refresh");
                    localStorage.removeItem("authorities");
                    window.location.reload();
                });
            }

            // Buffer any requests that occur while already refreshing
            const retryOrigReq = new Promise((resolve, reject) => {
                waitingRequests.push(token => {
                    // Replace the expired token and retry
                    originalRequest.params.access_token = localStorage.getItem("access");
                    resolve(axios(originalRequest));
                });
            });
            return retryOrigReq;
        } else {
            return Promise.reject(error);
        }
    }
);

function processAndStoreTokenResponse(response) {
    localStorage.setItem("access", response.data.access_token);
    localStorage.setItem("refresh", response.data.refresh_token);

    var base64Url = localStorage.getItem("access").split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    localStorage.setItem("authorities", JSON.parse(jsonPayload).authorities);
}

class Api {
    hasRole(roleName) {
        const authorities = localStorage.getItem("authorities");
        return authorities && authorities.includes(roleName);
    }

    fetchAccessToken(username, password) {
        const formData = new FormData();
        formData.set("grant_type", "password");
        formData.set("username", username);
        formData.set("password", password);


        const a = axios.create({
            headers: {
                "Authorization": "Basic YmVsbGViYWJ5c2l0dGVycy1jbGllbnQ6YjNsbGVCNGJ5c2lUVGVyJA==",
                "Content-Type": "application/x-www-form-urlencoded"
            }
        });

        a.interceptors.response.use(response => {
            processAndStoreTokenResponse(response);
            return response;
        }, error => {
            return Promise.reject(error);
        });

        return a.post(constants.API_OAUTH_URL + "token", formData);
    }

    async isTokenValid() {
        let resp = true;

        await axios.post(constants.API_OAUTH_URL + "check_token/?token=" + localStorage.getItem("access"), {}, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
        }).then(async response => {
            resp = true;
        }).catch(async error => {
            // If invalid try & refresh first...
            const formData = new FormData();
            formData.set("grant_type", "refresh_token");
            formData.set("refresh_token", localStorage.getItem("refresh"));

            await axios.post(constants.API_OAUTH_URL + "token", formData, {
                headers: {
                    "Authorization": "Basic YmVsbGViYWJ5c2l0dGVycy1jbGllbnQ6YjNsbGVCNGJ5c2lUVGVyJA==",
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(response => {
                // Refresh succeeded - store new token
                processAndStoreTokenResponse(response);
                resp = true;
            }).catch(error => {
                // Refresh failed - token is not valid so return false
                resp = false;
            });
        });

        return Promise.resolve(resp);
    }

    clearTokens() {
        localStorage.removeItem("access");
        localStorage.removeItem("refresh");
        localStorage.removeItem("authorities");
    }

    // TODO: Method to invalidate token on backend here!

    changePassword(username, password) {

        return ax.put(constants.API_BASE_URL + "admin/ownpassword", {
            "password" : password
        }
        );
    }
}

export default new Api();