import React from 'react';

import AdminNavigation from './components/AdminNavigation.js';
import ChildrenForm from './components/ChildrenForm.js';

const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const textRegex = RegExp(
    /^[A-Za-z\\-]*$/
);

const addressRegex = RegExp(
    /^[a-zA-Z0-9,.\\-\\s]*$/
);

const numberRegex = RegExp(
    /^\d+$/
);

const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
        val === null && (valid = false);
    });

    // validate the form was filled out
    Object.values(rest).forEach(val => {
        val.length === 0 && (valid = false);
    });

    return valid;

};

class AdminCustomerFormPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            surname: '',
            parent1First: '', parent1Last: '',
            parent2First: '', parent2Last: '',
            other: '',
            streetAddress: '', addressLine2: '', city: '', region: '', postCode: '', addressNotes: '',
            email: '',
            parent1ContactNo: '', parent2ContactNo: '',
            additionalContactNo: '',
            emgConFName: '', emgConLName: '', emgConNo: '', emgConRel: '',
            pets: '',
            extraNotes: '',
            membershipEndDate:'',
            formErrors: {
                surname: "",
                parent1First: "", parent1Last: "",
                parent2First: "", parent2Last: "",
                other: "",
                streetAddress: "", addressLine2: "", city: "", region: "", postCode: "", addressNotes: "",
                email: "",
                parent1ContactNo: "", parent2ContactNo: "",
                additionalContactNo: "",
                emgConFName: "", emgConLName: "", emgConNo: "", emgConRel: "",
                pets: "",
                extraNotes: "",
                membershipEndDate:'',
                children: [],
            },

            children: []

        };
        this.handleChange = this.handleChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = event => {
        event.preventDefault();

        if (formValid(this.state)) {
            console.log(`
            --SUBMITTING--
            Family Surname: ${this.state.surname}
            Parent 1 First Name: ${this.state.parent1First}
            Parent 1 Surname: ${this.state.parent1Last}
            Parent 2 First Name: ${this.state.parent2First}
            Parent 2 Surname: ${this.state.parent2Last}
            streetAddress: ${this.state.streetAddress}
            addressLine2: ${this.state.addressLine2}
            city: ${this.state.city}
            postCode: ${this.state.postCode}
            addressNotes: ${this.state.addressNotes}
            email: ${this.state.email}
            parent1ContactNo: ${this.state.parent1ContactNo}
            parent2ContactNo: ${this.state.parent2ContactNo}
            emgConFName: ${this.state.emgConFName}
            emgConLName: ${this.state.emgConLName}
            emgConRel: ${this.state.emgConRel}
            pets: ${this.state.pets}
            extraNotes: ${this.state.extraNotes}
            children: ${this.state.children}
          `);
        } else {
            console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
        }


    }

    handleChange(event) {
        event.preventDefault();
        const { name, value } = event.target;
        let formErrors = { ...this.state.formErrors };

        switch (event.target.name) {
            case "surname":
                formErrors.surname = !textRegex.test(value)
                    ? "Surname must only contain alphanumeric characters." : value.length < 64 && value.length > 0
                        ? null
                        : "Surname must contain between 1 and 64 characters.";
                break;
            case "parent1First":
                formErrors.parent1First = !textRegex.test(value)
                ? "Name must only contain alphanumeric characters.": value.length < 64 && value.length > 0
                    ? null
                    : "Parent 1 first name must contain between 1 and 64 characters.";
                break;
            case "parent1Last":
                formErrors.parent1Last = textRegex.test(value) && value.length < 64 && value.length > 0
                    ? null
                    : "Parent 1 last name must contain between 1 and 64 characters.";
                break;
            case "parent2First":
                formErrors.parent2First = textRegex.test(value) && value.length < 64 && value.length > 0
                    ? ""
                    : "Parent 2 first name must contain between 1 and 64 characters.";
                break;
            case "parent2Last":
                formErrors.parent2Last = textRegex.test(value) && value.length < 64
                    ? ""
                    : "Parent 1 last name must contain between 1 and 64 characters.";
                break;
            case "other":
                formErrors.other = textRegex.test(value) && value.length < 64
                    ? ""
                    : "Other caregivers name must contain between 1 and 64 characters.";
                break;
            case "streetAddress":
                formErrors.streetAddress = addressRegex.test(value) && value.length < 512
                    ? ""
                    : "Address must contain between 1 and 512 characters.";
                break;
            case "addressLine2":
                formErrors.addressLine2 = addressRegex.test(value) && value.length < 512
                    ? ""
                    : "Address line 2 must contain between 1 and 512 characters.";
                break;
            case "city":
                formErrors.city = textRegex.test(value) && value.length < 64
                    ? ""
                    : "City must contain between 1 and 64 characters.";
                break;
            case "region":
                formErrors.region = textRegex.test(value) && value.length < 64
                    ? ""
                    : "Region must contain between 1 and 64 characters";
                break;
            case "postCode":
                formErrors.postCode = numberRegex.test(value) && value.length < 4
                    ? ""
                    : "Post Code must be 4 numeric values";
                break;
            case "addressNotes":
                formErrors.addressNotes = value.length < 500
                    ? ""
                    : "Address notes must be less than 500 characters.";
                break;
            case "email":
                formErrors.email = emailRegex.test(value)
                    ? ""
                    : "Email name must contain between 1 and 255 characters.";
                break;
            case "parent1ContactNo":
                formErrors.parent1ContactNo = numberRegex.test(value) && value.length < 15
                    ? ""
                    : "Parent 1 contact phone number must be between 1 to 15 numeric values.";
                break;
            case "parent2ContactNo":
                formErrors.parent2ContactNo = numberRegex.test(value) && value.length < 15
                    ? ""
                    : "Parent 2 contact phone number must be between 1 to 15 numeric values";
                break;
            case "additionalContactNo":
                formErrors.additionalContactNo = numberRegex.test(value) && value.length < 15
                    ? ""
                    : "Additional contact phone number must be between 1 to 15 numeric values";
                break;
            case "emgConFName":
                formErrors.emgConFName = textRegex.test(value) && value.length < 64
                    ? ""
                    : "First name must contain between 1 and 64 characters.";
                break;
            case "emgConLName":
                formErrors.emgConLName = textRegex.test(value) && value.length < 64
                    ? ""
                    : "Last name must contain between 1 and 64 characters.";
                break;
            case "emgConNo":
                formErrors.emgConNo = numberRegex.test(value) && value.length < 15
                    ? "Surname must contain between 1 and 64 characters."
                    : "";
                break;
            case "emgConRel":
                formErrors.emgConRel = textRegex.test(value) && value.length < 64
                    ? ""
                    : "Emergency Relationship must contain between 1 and 64 characters.";
                break;
            case "pets":
                formErrors.pets = textRegex.test(value) && value.length < 64
                    ? ""
                    : "Pets must contain between 1 and 64 characters.";
                break;
            case "extraNotes":
                formErrors.extraNotes = value.length < 500
                    ? ""
                    : "Extra notes must be less than 500 characters.";
                break;
            default:
                break;
        }

        this.setState({
            formErrors, [name]: value
        }, () => {
            console.log(this.state);
        });
        console.log(this.state);

    }

    // handleSubmit(event){
    //     alert('A name was submitted: ' + this.state.surname);
    //     event.preventDefault();
    // }

    render() {
        // const { formErrors } = this.state;
        return (
            <div>
                <AdminNavigation />

                <div className="formContainer">
                    <h1> Add new customer </h1>
                    <form onSubmit={this.handleSubmit} noValidate>
                        <div className="form-wrapper">
                            <div>
                                <label>
                                    Family Surname:
                            <input type="text" name="surname" value={this.state.surname} onChange={this.handleChange} noValidate />
                                    <p className="errorMessage">{this.state.formErrors.surname}</p>
                                </label>
                            </div>
                            <div>
                                <label>
                                    Parent 1:
                            <input type="text" name="parent1First" value={this.state.parent1First} onChange={this.handleChange} placeholder="first" required />
                                    <input type="text" name="parent1Last" value={this.state.parent1Last} onChange={this.handleChange} placeholder="last" required />
                                    <p className="errorMessage">{this.state.formErrors.parent1First}</p>
                                    <p className="errorMessage">{this.state.formErrors.parent1Last}</p>
                                </label>
                            </div>
                            <div>
                                <label>
                                    Parent 2:
                            <input type="text" name="parent2First" value={this.state.parent2First} onChange={this.handleChange} placeholder="first" />
                                    <input type="text" name="parent2Last" value={this.state.parent2Last} onChange={this.handleChange} placeholder="last" />
                                    <p className="errorMessage">{this.state.formErrors.parent2First}</p>
                                    <p className="errorMessage">{this.state.formErrors.parent2Last}</p>
                                </label>
                            </div>
                            <div>
                                <label>
                                    Other Caregivers of Children:
                            <input type="text" name="other" value={this.state.other} onChange={this.handleChange} />
                                    <p className="errorMessage">{this.state.formErrors.other}</p>
                                </label>
                            </div>
                            <div>
                                <label>
                                    Home Address:
                                    <div>
                                        <input type="text" name="streetAddress" value={this.state.streetAddress} onChange={this.handleChange} placeholder="Street Address" required />
                                        <p className="errorMessage">{this.state.formErrors.streetAddress}</p>
                                    </div>
                                    <div>
                                        <input type="text" name="addressLine2" value={this.state.addressLine2} onChange={this.handleChange} placeholder="Address Line 2" />
                                        <p className="errorMessage">{this.state.formErrors.addressLine2}</p>
                                    </div>
                                    <div>
                                        <input type="text" name="city" value={this.state.city} onChange={this.handleChange} placeholder="City" />
                                        <p className="errorMessage">{this.state.formErrors.city}</p>
                                        <input type="text" name="region" value={this.state.region} onChange={this.handleChange} placeholder="Region" />
                                    </div>
                                    <div>
                                        <input name="postCode" value={this.state.postCode} onChange={this.handleChange} placeholder="Postal/Zip Code" />
                                        <p className="errorMessage">{this.state.formErrors.postCode}</p>
                                    </div>
                                    <div>
                                        <input type="text" name="addressNotes" value={this.state.addressNotes} onChange={this.handleChange} placeholder="Optional notes on the family" />
                                    </div>
                                </label>
                            </div>
                            <div>
                                <label>
                                    Email Address:
                            <input type="email" name="email" value={this.state.email} onChange={this.handleChange} required />
                                </label>
                            </div>
                            <div>
                                <label>
                                    Parent 1 Contact Number:
                            <input type="text" name="parent1ContactNo" value={this.state.parent1ContactNo} onChange={this.handleChange} required />
                                </label>
                            </div>
                            <div>
                                <label>
                                    Parent 2 Contact Number:
                            <input type="text" name="parent2ContactNo" value={this.state.parent2ContactNo} onChange={this.handleChange} />
                                </label>
                            </div>
                            <div>
                                <label>
                                    Additional Contact Number (if applicable):
                            <input type="text" name="additionalContactNo" value={this.state.additionalContactNo} onChange={this.handleChange} />
                                </label>
                            </div>
                            <div>
                                <label>
                                    <h4> Emergency Contact: </h4>
                                    Name:
                            <input type="text" name="emgConFName" value={this.state.emgConFName} onChange={this.handleChange} placeholder="First" required />
                                    <input type="text" name="emgConLName" value={this.state.emgConLName} onChange={this.handleChange} placeholder="Last" required />
                                    <div>
                                        Phone Number:
                                <input type="number" name="additionalContactNo" value={this.state.additionalContactNo} onChange={this.handleChange} required />
                                    </div>
                                    <div>
                                        Relationship to you:
                                <input type="text" name="emgConRel" value={this.state.emgConRel} onChange={this.handleChange} required />
                                    </div>
                                </label>
                            </div>
                            <div>
                                <label>
                                    Pets:
                            <input type="text" name="pets" value={this.state.pets} onChange={this.handleChange} />
                                </label>
                            </div>
                            <div>
                                <label>
                                    Extra Notes:
                            <textarea type="text" name="extraNotes" value={this.state.extraNotes} onChange={this.handleChange} /> 
                                </label>
                            </div>
                        </div>
                        <div className="form-wrapper">
                            {/* <h1>Children Information</h1>  */}
                            <ChildrenForm onChange={(c) => {
                                this.setState({
                                    children: c
                                })
                            }} />
                        </div>
                        <button type="submit" value="Submit">Create Account </button>
                    </form>


                </div>
            </div>
        );
    }
}

export default AdminCustomerFormPage;