import React from 'react';

import CustomerForm from './components/CustomerForm.js'
import AdminNavigation from './components/AdminNavigation.js';
import { withRouter } from 'react-router-dom';

class AdminCustomerFormPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: new URL(window.location.href).searchParams.get('username'),
        }
    }

    componentDidMount() {
        const url_string = window.location.href;
        const url = new URL(url_string);
        const username = url.searchParams.get('username');
        this.setState({
            username: username
        });

        if (!username) {
            this.props.history.replace("/admin/customer");
        }
    }

    render() {
        return (
            <div>
                <AdminNavigation />
                <CustomerForm username={this.state.username}/>
            </div>
        )
    }
}

export default withRouter(AdminCustomerFormPage);