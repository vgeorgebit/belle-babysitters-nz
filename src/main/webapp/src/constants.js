export const API_BASE_URL = "http://localhost:8080/api/";
export const PUBLIC_API_BASE_URL = "http://localhost:8080/publicapi/";
export const API_OAUTH_URL = "http://localhost:8080/oauth/"

export const ROLE_ADMIN = "ROLE_ADMIN";
export const ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
export const ROLE_BABYSITTER = "ROLE_BABYSITTER";
export const ROLE_CUSTOMER = "ROLE_CUSTOMER";