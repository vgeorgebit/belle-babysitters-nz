import React from 'react';

import JobsForm from './admin-jobs-form.page.js'
import AdminNavigation from './components/AdminNavigation.js';
import { withRouter } from 'react-router-dom';

class AdminJobEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            job: new URL(window.location.href).searchParams.get('job'),
        }
    }

    componentDidMount() {
        const url_string = window.location.href;
        const url = new URL(url_string);
        const username = url.searchParams.get('job');
        this.setState({
            username: username
        });

        if (!username) {
            this.props.history.replace("/admin");
        }
    }

    render() {
        return (
            <div>
                <JobsForm job={this.state.job}/>
            </div>
        )
    }
}
/**
 * Kurt Weston was here
 */
export default withRouter(AdminJobEdit);