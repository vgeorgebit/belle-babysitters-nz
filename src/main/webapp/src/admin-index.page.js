import React from 'react';

import { withRouter } from 'react-router-dom';
import { ax } from './api.js';
import * as constants from './constants.js';

import AdminNavigation from './components/AdminNavigation.js';

class AdminIndexPage extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            page: {},
            loading: true,
            pageNum: 0,
            search: '',
            show: false,
            jobDisplay : {},
            status : '',
            images: []
            
        };

        ax.get(constants.API_BASE_URL + "jobs?page=0&search=" + this.state.search +"&filterBy=").then(response => {
            this.setState({
                page: response.data,
                loading: false
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });

        this.loadPage = this.loadPage.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleAddClick = this.handleAddClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.confirmBooking = this.confirmBooking.bind(this);
    }

    getImages(username) {
        ax.get(constants.API_BASE_URL + "babysitter/image/" + username, { responseType: 'arraybuffer' })
            .then(
                response => {
                    const base64 = btoa(
                        new Uint8Array(response.data).reduce(
                            (data, byte) => data + String.fromCharCode(byte),
                            '',
                        ),
                    );

                    this.setState(prevState => {
                        let images = Object.assign({}, prevState.images);
                        images[username] = "data:;base64," + base64;
                        return { images };
                    });
                }).catch(e => {
                    console.log("IMAGE ERROR");
                }
                );

    }

    loadPage(pageNum) {
        this.setState({
            loading: true
        });

        ax.get(constants.API_BASE_URL + "jobs?page=" + pageNum + "&search=" + this.state.search +"&filterBy="+ this.state.status).then(response => {
            this.setState({
                page: response.data,
                loading: false,
                pageNum: pageNum
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });
    }


    confirmBooking(id){
        ax.post(constants.API_BASE_URL + "jobs/"+id+"/confirm").then(response => {
            this.props.history.push("/admin/confirmed")
        }).catch(e =>{
            alert("ERROR OCCURED");
        });
    }

    handleAddClick(event) {
        event.preventDefault();
        this.props.history.push("/admin/jobs/form")
    }

    handleSearch(event) {
        this.setState({
            search: event.target.value.replace("#", "%23"),
            pageNum: 0
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }

    handleChange(event) {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            submitError: null
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }

    showModal(e,job){
        this.setState({ 
            jobDisplay: job,
            show:true
        }, () => {
            if(job.assignedBabysitters.length > 0){
                job.assignedBabysitters.forEach(b =>{
                    if(!this.state.images[b.username]){
                        this.getImages(b.username);
                    }
                })
            }
        });
      };
    
    
    hideModal = (e) => {
        this.setState({ 
            show: false
         });
      };


    render() {
        return (
        <div>
            <AdminNavigation />

            
        
            <div className="container adminsContainer" style={{maxWidth: 1000}}>
                    <div className="row">
                        <div className="col">
                            <h1>Pending Bookings</h1>
                        </div>
                        <div className="col">
                            <button className="btn btn-success" onClick={this.handleAddClick}><i className="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Create Booking</button>    
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <input style={{display: "inline"}} onChange={this.handleSearch} className="form-control" type="text" placeholder="Search" />
                            <div style={{display: "inline"}}>
                            <label style={{marginLeft:30, marginRight:10}}>Filter By:</label>
                            <select style={{width: 250, display: "inline"}} type="text" className="form-control" name="status" value={this.state.status} onChange={this.handleChange} >
                                <option value="">NONE</option>
                                <option value="PENDING_ASSIGNMENT">PENDING ASSIGNMENT</option>
                                <option value="PENDING_PAYMENT">PENDING PAYMENT</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div>
                        {
                            this.state.loading ?
                                (
                                    <div className="spinner-border text-primary" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                ) :
                                <div>
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                
                                                <th>ID</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Customer</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                


                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.page.content.map(job => {
                                                    return (
                                                            this.createTableRow(job)
                                                            )
                                                })
                                            }
                                            
                                        </tbody>
                                    </table>
                                    <center>
                                        {!this.state.page.first && <span><a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum - 1) }} href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>}
                                        <span>Page {this.state.pageNum + 1} of {this.state.page.totalPages}</span>
                                        {!this.state.page.last && <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum + 1) }} href="">Next</a></span>}
                                    </center>
                                </div>
                        }
                    </div>
                </div>
                        
                {                                 
                this.state.show && this.state.jobDisplay &&
                <div className="blackout" >
                <div className="modalContainer" >
                    
                    <div className="row">
                        <div className="col col-6">
                            <h2>{this.state.jobDisplay.customer.familySurname} Family</h2>
                            
                            
                            
                        </div>
                        <div className="col col-4">
                            <a style={{height:'auto', width:'100%', cursor:"pointer"}} id="modifyButton" className="btn btn-warning" onClick={(e) => {this.props.history.push("/admin/jobs/edit?job="+this.state.jobDisplay.id)}}><i className="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Modify</a>

                        </div>
                        <div className="col col-2">
                            <button style={{height:'auto', width:'100%'}} type="button" className="btn btn-danger" onClick={this.hideModal}><i className="fa fa-times" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div className="modalContent">
                    {this.state.jobDisplay.status == "PENDING_ASSIGNMENT" ? 
                        <div colSpan="2" onClick={(e) => {this.props.history.push("/admin/jobs/assign?job="+this.state.jobDisplay.id)}} className="btn btn-success" style={{textAlign:"Center", width:"100%", border:"none", marginTop:30, marginBottom:10, cursor:"pointer"}}>Assign</div>
                        :
                        <div colSpan="2" onClick={(e) => {if (window.confirm("Are you sure you want to confirm this booking?")) this.confirmBooking(this.state.jobDisplay.id)}} style={{textAlign:"Center", width:"100%", border:"none", marginTop:30, marginBottom:10, cursor:"pointer"}} className="btn btn-success"><i className="fa fa-credit-card" aria-hidden="true"></i>&nbsp;&nbsp;Confirm Payment</div>
                    }
                    <table className="table">
                        
                            
                       
                        <tbody>
                        {this.state.jobDisplay.assignedBabysitters.length > 0 ? <tr><td colSpan={2} style={{textAlign:"center", fontWeight:"bold"}}>Assigned Babysitters</td></tr> : "" }
                                {   
                                    this.state.jobDisplay.assignedBabysitters.map(babysitter => {
                                        return <tr>
                                                    <td>
                                                    <img src={this.state.images[babysitter.username]} width={50} height={50} style={{borderRadius:"100%"}}/>
                                                    &nbsp;&nbsp;{babysitter.firstName}
                                                    </td>
                                                    <td>
                                        
                                                        {babysitter.bio}
                                                    </td>
                                                </tr>
                                    })
                                }
                            
                        

                            {this.state.jobDisplay.assignedBabysitters.length > 0 ? <tr><td colSpan={2} style={{textAlign:"center", fontWeight:"bold"}}>Job Details</td></tr> : "" }
                            
                            <tr><th>ID</th><td>#{this.state.jobDisplay.id}</td></tr>
                            <tr><th>Status</th><td>{this.state.jobDisplay.status}</td></tr>
                            <tr><th>Job Notes</th><td>{this.state.jobDisplay.jobNotes}</td></tr>
                            <tr><th>Caregivers</th>
                                <td>
                                {this.state.jobDisplay.customer.caregivers.map((caregiver) => {
                                    return <span> {caregiver.firstName} {caregiver.lastName}<br />
                                                    Relation: {caregiver.relationshipToChild}<br />
                                                    Ph: {caregiver.phoneNumber} <br />
                                    </span>
                                })}
                                </td></tr>
                            <tr><th>Number of Children</th><td>{this.state.jobDisplay.numChildren}</td></tr>
                            <tr><th>Children Notes</th><td>{this.state.jobDisplay.childrenDetails.split("\n").map((child, index) => {
                                return <span key={index} style={{display: "block"}}>{child}</span>
                            })}</td></tr>
                            <tr><th>Start Time</th><td>{this.state.jobDisplay.startTime.split("-")[2].substring(0,2)+"-"+this.state.jobDisplay.startTime.split("-")[1]+"-"+this.state.jobDisplay.startTime.split("-")[0]} {this.state.jobDisplay.startTime.substring(12,17)}</td></tr>
                            <tr><th>End Time</th><td>{this.state.jobDisplay.endTime.split("-")[2].substring(0,2)+"-"+this.state.jobDisplay.endTime.split("-")[1]+"-"+this.state.jobDisplay.endTime.split("-")[0]} {this.state.jobDisplay.endTime.substring(12,17)}</td></tr>
                            <tr><th>Job Address</th><td>{this.state.jobDisplay.jobAddress.split("\n").map((item, index) => {
                                return <span key={index} style={{display:"block"}}>{item}</span>
                            })}</td></tr>
                            <tr><th>Address Notes</th><td>{this.state.jobDisplay.addressNotes}</td></tr>
                            <tr><th>Own Car Required</th><td>{this.state.jobDisplay.ownCarRequired ? "Yes" : "No"}</td></tr>
                            <tr><th>Full License Required</th><td>{this.state.jobDisplay.fullLicenseRequired ? "Yes" : "No"}</td></tr>
                            <tr><th>Date Requested</th><td>{this.state.jobDisplay.dateRequested.split("-")[2].substring(0,2)+"-"+this.state.jobDisplay.dateRequested.split("-")[1]+"-"+this.state.jobDisplay.dateRequested.split("-")[0]} {this.state.jobDisplay.dateRequested.substring(11,17)}</td></tr>
                            <tr> { 
                                this.state.jobDisplay.status === "PENDING_PAYMENT" && 
                                <td colspan={2}>
                                    <div colSpan="2" onClick={(e) => {this.props.history.push("/admin/jobs/reassign?job="+this.state.jobDisplay.id)}} className="btn btn-success" style={{textAlign:"Center", width:"100%", border:"none", marginTop:30, marginBottom:10, cursor:"pointer"}}>Reassign</div>
                                </td>
                                }
                            </tr>
                            


                           
                        </tbody>
                    </table>

                </div>
                </div>
                </div>

                }


            
        </div>
        )
    }

    createTableRow(job){
        return (
            [<tr key={job.id} onClick={ e=> {this.showModal(e,job)}}>
                                                        
            <td style={{color:"gray"}}>#{job.id}</td>
            <td>{job.startTime.split("-")[2].substring(0,2)+"-"+job.startTime.split("-")[1]+"-"+job.startTime.split("-")[0]} <br/> <span className="text-primary">{job.startTime.substring(12,17)}</span></td>
            <td>{job.endTime.split("-")[2].substring(0,2)+"-"+job.endTime.split("-")[1]+"-"+job.endTime.split("-")[0]} <br/> <span className="text-primary" >{job.endTime.substring(12,17)}</span></td>
            <td><span style={{fontWeight:'bold'}}>{job.customer.familySurname}<br/></span>
                {job.customer.caregivers.map((caregiver) => {
                    return <span> {caregiver.firstName} {caregiver.lastName}<br /></span>
                })}
            
            </td>
            <td>{job.customer.email}</td>
            <td><span className={"badge badge-pill badge-" + (job.status==="PENDING_ASSIGNMENT" ? "danger" : "warning")}>{job.status.replace("_"," ")}</span></td>

            
        </tr> 
        ,
        
         
        job.assignedBabysitters && !job.assignedBabysitters.length < 1 ? (
        <tr style={{pointerEvents:'none'}}>
        <td style={{color:"gray"}}>Babysitters:</td>
        <td colSpan={5}> {job.assignedBabysitters.map((b,i) => {
                return <span> * {b.firstName} {b.lastName} &nbsp;&nbsp;&nbsp;</span>
                        })
            }
        <i className={"fa fa-sort-asc"} style={{float:'right'}}/>
        </td>
        </tr>
        ) : " "
        ]

        )
        
    }
}

export default withRouter(AdminIndexPage);