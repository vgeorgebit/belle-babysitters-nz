import React from 'react';
import AdminNavigation from './components/AdminNavigation.js';
import { ax } from './api.js'
import * as constants from './constants.js'
import { withRouter } from 'react-router-dom';

const nameRegex = RegExp(
    /^[ A-Za-z\\-]+$/
);

const usernameRegex = RegExp(
    /^[A-Za-z0-9]+$/
);

const passwordRegex = RegExp(
    /^[0-9A-Za-z!@#$%^&*/_-]+$/
);

class CreateAdmin extends React.Component {

    //constructor for CreateAdmin object
    constructor(props) {
        super(props);

        // if form has been submitted before
        this.submitted = false;

        // sets state upon construction
        // this.state = initialState;
        this.state = {
            firstName: '',
            lastName: '',
            username: '',
            password: '',
            confirmPassword: '',
            superAdmin: false,

            firstName: '',
            lastName: '',
            username: '',
            password: '',
            confirmPassword: '',

            submitError: '',
            processing: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        if (this.props.username != null) {
            ax.get(constants.API_BASE_URL + "admin/" + this.props.username).then(response => {
                const data = response.data;
                this.setState({
                    firstName: data.firstName,
                    lastName: data.lastName,
                    username: data.username,
                    superAdmin: data.superAdmin
                });

            }).catch(error => {
                this.props.history.replace("/admin/admin");

            });
        }

    }



    handleChange(event) {
        let name = event.target.name;
        this.setState({
            [event.target.name]: event.target.value
        }, () => {
            this.validate(name);
        });
    }

    validate = (inputName = null, callback = null) => {
        let firstNameError = null;
        let lastNameError = null;
        let usernameError = null;
        let passwordError = null;

        if (inputName === null || inputName === "firstName")
            if (!this.state.firstName || this.state.firstName.length < 1) {
                firstNameError = "First name is required";
            } else if (this.state.firstName.length > 64) {
                firstNameError = "First name is too long";
            } else if (!nameRegex.test(this.state.firstName)) {
                firstNameError = "First name may only contain letters";
            }

        if (inputName === null || inputName === "lastName")
            if (!this.state.lastName || this.state.lastName.length < 1) {
                lastNameError = "Last name is required";
            } else if (this.state.lastName.length > 64) {
                lastNameError = "Last name is too long";
            } else if (!nameRegex.test(this.state.lastName)) {
                lastNameError = "Last name may only contain letters";
            }

        if (inputName === null || inputName === "username")
            if (!this.state.username || this.state.username.length < 4) {
                usernameError = "Username is too short";
            } else if (this.state.username.length > 16) {
                usernameError = "Username is too long";
            } else if (!usernameRegex.test(this.state.username)) {
                usernameError = "Username may only contain alphanumeric characters";
            }

        if (inputName === null || inputName === "password" || inputName === "confirmPassword")
            if (this.state.password != this.state.confirmPassword) {
                passwordError = "Passwords must match";
            } else if (this.props.username && (!this.state.password || this.state.password.length < 1)) {
                passwordError = null;
            } else if (this.state.password.length < 6) {
                passwordError = "Password is too short";
            } else if (!passwordRegex.test(this.state.password)) {
                passwordError = "Password may only contain alphanumeric characters and !@#$%^&*/_-";
            } else if (this.state.password.length > 32) {
                passwordError = "Password is too long"
            }

        let s = {};
        if (firstNameError || inputName === "firstName")
            s["firstNameError"] = firstNameError;
        if (lastNameError || inputName === "lastName")
            s["lastNameError"] = lastNameError;
        if (usernameError || inputName === "username")
            s["usernameError"] = usernameError
        if (passwordError || inputName === "password" || inputName === "confirmPassword")
            s["passwordError"] = passwordError;

        if (inputName != null) {
            s["submitError"] = null;
        }

        this.setState(s, () => {
            if (callback)
                callback();
        });
    };

    handleSubmit(event) {
        event.preventDefault();

        this.validate(null, () => {
            if (this.state.firstNameError || this.state.lastNameError || this.state.usernameError || this.state.passwordError) {
                this.setState({
                    submitError: "Please fix the highlighted errors in the form"
                });
            } else {
                const admin = {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    username: this.state.username,
                    password: this.state.password,
                    superAdmin: this.state.superAdmin,
                }

                if (!this.props.username) {
                    ax.post(constants.API_BASE_URL + "admin", admin).then(response => {
                        this.props.history.push("/admin/admin");
                    }).catch(error => {
                        let msg = '';
                        if (error.response && error.response.data && error.response.data.constraintViolations) {
                            error.response.data.constraintViolations.map(v => {
                                msg += v + '  ';
                            });
                        } else if (error.response && error.response.data && error.response.data.error_description) {
                            msg = error.response.data.error_description;
                        } else {
                            msg = "An unknown error occurred";
                        }

                        this.setState({
                            submitError: msg
                        });
                    });
                } else {

                    const updated = {
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        username: this.state.username,
                        password: this.state.password ? this.state.password : null,
                        superAdmin: this.state.superAdmin,
                    }

                    ax.put(constants.API_BASE_URL + "admin", updated).then(response => {
                        this.props.history.push("/admin/admin");
                    }).catch(error => {
                        let msg = '';
                        if (error.response && error.response.data && error.response.data.constraintViolations) {
                            error.response.data.constraintViolations.map(v => {
                                msg += v + '  ';
                            });
                        } else if (error.response && error.response.data && error.response.data.error_description) {
                            msg = error.response.data.error_description;
                        } else {
                            msg = "An unknown error occurred";
                        }

                        this.setState({
                            submitError: msg
                        });
                    });

                }
            }
        });

    }

    handleCheckChange(event) {
        this.setState({
            superAdmin: event.target.checked
        })
    }

    getInputClasses(formError) {
        return "form-control" + (formError === null ? " is-valid" : formError ? " is-invalid" : "");
    }

    deleteClicked(event, username) {
        event.preventDefault();

        const del = window.confirm("Are you sure you want to remove this admin?"); //dialog box

        if (del) {
                ax.delete(constants.API_BASE_URL + "admin/" + username).then(response => {
                this.props.history.push("/admin/admin"); // redirect to admin list
            }).catch(error => {
                alert("Error: Unable to delete admin");
            });
        }
    }

    render() {
        return (
            <div>
                <AdminNavigation />
                <div className="container createAdminContainer">
                    <div style={{width: "100%", textAlign: "right"}}>
                        <button className="btn btn-danger" style={{float: "none", marginTop: 0}} onClick={() => {this.props.history.push("/admin/admin")}}>Cancel</button>
                    </div>
                    <h1>{!this.props.username ? 'Create Admin Account' : 'Modify Admin Account'}</h1>

                    <form onSubmit={this.handleSubmit} className="form">
                        <div className="row">
                            <div className="col">
                                <label htmlFor="firstName">First Name:</label>
                                <input type="text" name="firstName" className={this.getInputClasses(this.state.firstNameError)} value={this.state.firstName} onChange={this.handleChange} placeholder="First Name" />
                                {this.state.firstNameError && <span className="formError">{this.state.firstNameError}</span>}
                            </div>
                            <div className="col">
                                <label htmlFor="lastName">Last Name:</label>
                                <input type="text" name="lastName" className={this.getInputClasses(this.state.lastNameError)} value={this.state.lastName} onChange={this.handleChange} placeholder="Surname" />
                                {this.state.lastNameError && <span className="formError">{this.state.lastNameError}</span>}

                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <label htmlFor="username">Username:</label>
                                <input type="text" name="username" className={this.getInputClasses(this.state.usernameError)} disabled={this.props.username} value={this.state.username} onChange={this.handleChange} placeholder="Username" />
                                {this.state.usernameError && <span className="formError">{this.state.usernameError}</span>}

                            </div>
                            <div className="col">
                                <div className="row">
                                    <input type="checkbox" name="superAdmin" checked={this.state.superAdmin} onChange={this.handleCheckChange} className="form-check-input" />
                                    <label className="form-check-label" htmlFor="superAdmin">Super Admin:</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <label htmlFor="password">Password:</label>
                                <input type="password" name="password" className={this.getInputClasses(this.state.passwordError)} value={this.state.password} onChange={this.handleChange} placeholder={this.props.username ? "(Unchanged)" : ""} />
                                {this.state.passwordError && <span className="formError">{this.state.passwordError}</span>}
                            </div>
                            <div className="col">
                                <label htmlFor="confirmPassword">Confirm Password:</label>
                                <input type="password" name="confirmPassword" className={this.getInputClasses(this.state.passwordError)} value={this.state.confirmPassword} onChange={this.handleChange} placeholder={this.props.username ? "(Unchanged)" : ""} />
                            </div>
                        </div>
                        <br />
                        <div className="row">
                            <div className="col">
                            { this.props.username && 
                                <input style={{ float: "left" }} type="submit" class="btn btn-danger" onClick={e => this.deleteClicked(e, this.props.username)} value = "Delete"/>
                            }
                                <input style={{ float: "right" }} disabled={this.state.processing} type="submit" value={this.state.processing ? "Processing..." : (this.props.username != null ? "Update Admin" : "Add Admin")} className="btn btn-success" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <p style={{ float: "right", fontSize: 16 }} className="formError">{this.state.errorMessage}</p>
                                <p style={{ float: "right", fontSize: 16 }} className="formError">{this.state.submitError} </p>
                            </div>
                        </div>


                    </form>
                </div>
            </div >
        );
    }
}
export default withRouter(CreateAdmin);