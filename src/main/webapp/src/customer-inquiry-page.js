import React from 'react';

import CustomerNavigation from './components/CustomerNavigation.js';
import CustomerInquiryForm from './components/CustomerInquiryForm.js';

class CustomerInquiryPage extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <div>
                <CustomerNavigation/>
                

                <CustomerInquiryForm/>
                
            </div>
        )
    }
}

export default CustomerInquiryPage;
