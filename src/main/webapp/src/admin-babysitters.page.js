import React from 'react';
import AdminNavigation from './components/AdminNavigation.js';
import { withRouter } from 'react-router-dom';
import { ax } from './api.js';
import * as constants from './constants.js';
import BadgeDisplay from './components/BadgeDisplay.js'

class AdminBabysittersPage extends React.Component {

    constructor(props, context) {
        super(props);

        this.state = {
            page: {},
            loading: true,
            pageNum: 0,
            search: '',
            show : false,
            accountDisplay : {},
            image : '',
            images : {},
            file: null
            
       };

        ax.get(constants.API_BASE_URL + "babysitters?page=0&search=" + this.state.search).then(response => {
            this.setState({
                page: response.data,
                loading: false
            });
            response.data.content.forEach(babysitter => {
                this.getImages(babysitter.username)
            });
        }).catch(e =>{
            alert("ERROR OCCURED");
        });

        this.loadPage = this.loadPage.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.getImages = this.getImages.bind(this);
        this.handleAddClick = this.handleAddClick.bind(this);
    }

    getImages(username){
        ax.get(constants.API_BASE_URL + "babysitter/image/"+username, { responseType: 'arraybuffer' },)
                .then(
                response => {
                    const base64 = btoa(
                        new Uint8Array(response.data).reduce(
                        (data, byte) => data + String.fromCharCode(byte),
                        '',
                        ),
                    );

                    this.setState(prevState => {
                        let images = Object.assign({}, prevState.images);  
                        images[username] = "data:;base64," + base64;                                     
                        return { images };  
                    });
                    }).catch(e =>{
                        alert("ERROR OCCURED");
                    });
        
    }

    showModal(e,babysitter){

        this.setState({ 
            accountDisplay: babysitter,
            show : true
        });
      };
    
    
    hideModal = (e) => {
        this.setState({ 
            show: false,
            file : null
         });
      };

    loadPage(pageNum) {
        this.setState({
            loading: true
        });

        ax.get(constants.API_BASE_URL + "babysitters?page=" + pageNum + "&search=" + this.state.search).then(response => {
            this.setState({
                page: response.data,
                loading: false,
                pageNum: pageNum,
                images : {}
            });
            response.data.content.forEach(babysitter => {
                this.getImages(babysitter.username)
            });
            
        }).catch(e =>{
            console.log("IMAGE ERROR");
        });
    }

    handleAddClick(event) {
        event.preventDefault();
        this.props.history.push("/admin/babysitter/form");
    }

    handleSearch(event) {
        this.setState({
            search: event.target.value,
            pageNum: 0
        }, () => {
            this.loadPage(this.state.pageNum);
        });
    }
    

    render() {
        return (
            <div>
                <AdminNavigation />
                
                <div className="container adminsContainer">
                    <div className="row">
                        <div className="col">
                            <h1>Babysitter Accounts</h1>
                        </div>
                        <div className="col">
                            <button onClick={this.handleAddClick} type="button" className="btn btn-success"><i className="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Add Babysitter</button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <input onChange={this.handleSearch} className="form-control" type="text" placeholder="Search" />
                        </div>
                    </div>
                    <div>
                        {
                            this.state.loading ?
                                (
                                    <div className="spinner-border text-primary" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                ) :
                                <div>
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>First Name</th>
                                                <th>Surname</th>
                                                <th>Username</th>
                                                <th>Badges</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.page.content.map(babysitter => {
                                                    return (
                                                        
                                                        <tr key={babysitter.username} onClick={ e=> {this.showModal(e,babysitter)}} style={{background: babysitter.disabled ? "#F0EFEF" : null}}>
                                                            
                                                            <td><img alt="Babysitter Profile" className="adminsProfile" src ={this.state.images[babysitter.username]} /></td>
                                                            <td>{babysitter.firstName}</td>
                                                            <td>{babysitter.lastName}</td>
                                                            <td>{babysitter.username}</td>
                                                            <td>
                                                                <BadgeDisplay babysitter={babysitter}/>
                                                            </td>
                                                        </tr>)
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <center>
                                        {!this.state.page.first && <span><a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum - 1) }} href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>}
                                        <span>Page {this.state.pageNum + 1} of {this.state.page.totalPages}</span>
                                        {!this.state.page.last && <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={e => { e.preventDefault(); this.loadPage(this.state.pageNum + 1) }} href="">Next</a></span>}
                                    </center>
                                </div>
                        }
                    </div>
                </div>
                
                { 

                
                    this.state.show &&
                    <div className="blackout" >
                    <div className="modalContainer" >
                        
                        <div className="row">
                            <div className="col col-6">
                                <h2>{this.state.accountDisplay.firstName} {this.state.accountDisplay.lastName}</h2>
                                
                            </div>
                            <div className="col col-4">
                                <a style={{height:'auto', width:'100%'}} id="modifyButton" className="btn btn-warning" href={"/admin/babysitter/edit?username=" + this.state.accountDisplay.username}><i className="fa fa-pencil" aria-hidden="true"/>&nbsp;&nbsp;Modify</a>

                            </div>
                            <div className="col col-2">
                                <button style={{height:'auto', width:'100%'}} type="button" className="btn btn-danger" onClick={this.hideModal}><i className="fa fa-times" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        
                        <div className="modalContent">
                        {/* <figure>
                            
                        <div class="row">
                            <img class="modalPicture" src={this.state.images[this.state.accountDisplay.username]} />
                            <span style={{width:"100px", float:"right"}}>{this.state.accountDisplay.bio}</span>
                        </div>
                        </figure> */}
                        
                        <table className="table">
                        {this.state.accountDisplay.disabled && <tr><th style={{textAlign:"Center", background:"#FE4040", color:"white"}} colSpan="2"> THIS ACCOUNT IS DISABLED </th></tr>}
{/**
 * Kurt Weston was here
 */}
                            <tbody>
                                
                                <tr><td><img class="modalPicture" src={this.state.images[this.state.accountDisplay.username]} /></td><td class="col col-4">{this.state.accountDisplay.bio.split("\n").map((value) => {return <span>{value}<br/></span>})}</td></tr>
                                <tr><th> First Name</th><td> {this.state.accountDisplay.firstName}</td></tr>
                                <tr><th> Last Name</th><td> {this.state.accountDisplay.lastName}</td></tr>
                                <tr><th> Date of Birth</th><td> {this.state.accountDisplay.dob &&  this.state.accountDisplay.dob.split("-")[2].substring(0, 2) + "-" + this.state.accountDisplay.dob.split("-")[1] + "-" + this.state.accountDisplay.dob.split("-")[0]}</td></tr>
                                <tr><th> Username</th><td> {this.state.accountDisplay.username}</td></tr>
                                <tr><th> Contact Phone</th><td> {this.state.accountDisplay.contactPhone}</td></tr>
                                <tr><th> Email</th><td> {this.state.accountDisplay.email}</td></tr>
                                <tr><th> Emergency Contact</th><td> <span>
                                        {this.state.accountDisplay.emgName} <br/>
                                        Relation: {this.state.accountDisplay.emgRelation} <br/>
                                        Ph: {this.state.accountDisplay.emgPhone} <br/>
                                        </span>
                                </td></tr>
                                
                                <tr><th> Contract Sign Date</th><td> {this.state.accountDisplay.contractSignDate && this.state.accountDisplay.contractSignDate.split("-")[2].substring(0, 2) + "-" + this.state.accountDisplay.contractSignDate.split("-")[1] + "-" + this.state.accountDisplay.contractSignDate.split("-")[0]}</td></tr>
                            </tbody>
                        </table>
                    
                    </div>
                    </div>
                    </div>
                                    
                }
            </div>
        )
    }
}

export default withRouter(AdminBabysittersPage);