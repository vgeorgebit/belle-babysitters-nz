import React from 'react';

import BabysitterNavigation from './components/BabysitterNavigation.js';

class BabysitterIndexPage extends React.Component {

    render() {
        return (
            <div>
                <BabysitterNavigation/>
                <h1>Babysitter Index</h1>
            </div>
        )
    }
}

export default BabysitterIndexPage;