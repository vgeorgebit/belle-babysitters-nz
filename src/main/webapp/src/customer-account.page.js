import React from 'react';
import { ax } from './api.js';
import * as constants from './constants.js';

import CustomerNavigation from './components/CustomerNavigation';

class CustomerAccountPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            // show: false,
            accountDisplay: {},
            // images: {},
        };

        // this.getImages = this.getImages.bind(this);
        this.nav = this.nav.bind(this);
    }

    nav(e) {
        e.preventDefault();
        this.props.history.push("/customer/account/edit");
    }

    getDetails() {
        ax.get(constants.API_BASE_URL + "customer/owndetails").then(response => {
            this.setState({
                accountDisplay: response.data
            });
            // this.getImages(response.data.username);
        });

    };

    componentDidMount() {
        this.getDetails()
    }

    render() {
        return (
            <div>
                <CustomerNavigation />

                {/* {this.viewDetails(e, babysitter.username) && */}

                <div className="conatiner babysitterViewcontainer">
                    <h1 className="myAccountHeader">Account Details</h1>

                    <div>
                        <div className="row">
                            <div className="col col-6">
                            {/* <a style={{height:'auto', width:'100%'}} id="modifyButton" className="btn btn-warning" href={"/customer/account/edit?username=" + this.state.accountDisplay.email}><i className="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Modify</a> */}

                                <button style={{ height: 'auto', width: '100%' }} className="btn addBtn" onClick={this.nav}><i className="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Modify</button>
                            </div>
                        </div>

                        <p>Click the modify button to view the full details of your account.</p>
                        <table className="table">

                            <tbody>
                                <tr><th>Family Surname</th><td> {this.state.accountDisplay.familySurname}</td></tr>
                                <tr><th>Email</th><td> {this.state.accountDisplay.email}</td></tr>
                                <tr><th>Membership Expiry Date </th><td> {this.state.accountDisplay.membershipEndDate && this.state.accountDisplay.membershipEndDate.split('T')[0]}</td></tr>
                                <tr><th>Emergency Contact Name </th><td> {this.state.accountDisplay.emgConFName} {this.state.accountDisplay.emgConLName}</td></tr>
                                <tr><th>Emergency Contact Number </th><td> {this.state.accountDisplay.emgConNo}</td></tr>
                                <tr><th>Emergency Contact Relation </th><td> {this.state.accountDisplay.emgConRel}</td></tr>
                                <tr><th>Pets </th><td> {this.state.accountDisplay.pets ? this.state.accountDisplay.pets.split("\n").map((pet, index) => {
                                    return <span key={index}>{pet}<br /></span>
                                }) : "No Pets"}</td></tr>
                               
                                <tr><th>Other Requirements </th><td> {this.state.accountDisplay.extraNotes ? this.state.accountDisplay.extraNotes : "None"}</td></tr>
                               
                                <tr><th>Caregivers</th><td>{this.state.accountDisplay.caregivers ? this.state.accountDisplay.caregivers.map((caregiver) => {
                                    return <span> {caregiver.firstName + " " + caregiver.lastName}<br /></span>
                                }) : "No Caregivers"}</td></tr>
                               
                                <tr><th>Children</th><td>{this.state.accountDisplay.children ? this.state.accountDisplay.children.map((child) => {
                                    return <span key={child.firstName + "," + child.lastName + "," + child.age
                                }> { child.firstName } ({ new Date().getFullYear() - new Date(child.dob).getFullYear() }   { child.gender[0] }) < br /></span>
                            }) : "No Children"}</td></tr>



                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        )
    }
}

export default CustomerAccountPage;