import React from 'react';
import { ax } from './api.js';
import * as constants from './constants.js';

import BabysitterNavigation from './components/BabysitterNavigation.js';

class BabsitterAccountPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            // show: false,
            accountDisplay: {},
            images: {},
        };

        this.getImages = this.getImages.bind(this);
        this.nav = this.nav.bind(this);
    }

    nav(e) {
        e.preventDefault();
        this.props.history.push("/babysitter/account/edit");
    }
    
    getDetails() {
        ax.get(constants.API_BASE_URL + "babysitter/owndetails").then(response => {
            this.setState({
                accountDisplay: response.data
            });
            this.getImages(response.data.username);
        }).catch(e =>{
            alert("ERROR OCCURED");
        });

    };

    getImages(username) {
        ax.get(constants.API_BASE_URL + "babysitter/image/" + username, { responseType: 'arraybuffer' })
            .then(
                response => {
                    const base64 = btoa(
                        new Uint8Array(response.data).reduce(
                            (data, byte) => data + String.fromCharCode(byte),
                            '',
                        ),
                    );

                    this.setState(prevState => {
                        let images = Object.assign({}, prevState.images);
                        images[username] = "data:;base64," + base64;
                        return { images };
                    });
                }).catch(e =>{
                    alert("ERROR OCCURED");
                });

    }

    componentDidMount() {
        this.getDetails()
    }

    render() {
        return (
            <div>
                <BabysitterNavigation />

                {/* {this.viewDetails(e, babysitter.username) && */}

                <div className="conatiner babysitterViewcontainer">
                <h1 className="myAccountHeader">Account Details</h1>

                        <div>
                            <div className="row">
                                <div className="col col-6">
                                    <h2>{this.state.accountDisplay.firstName} {this.state.accountDisplay.lastName}</h2>


                                </div>
                                <div className="col">
                                    {/* <a style={{ height: 'auto', width: '100%' }} id="modifyButton" className="btn btn-warning" href={"/admin/babysitter/account/edit="}><i className="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Modify</a> */}
                                    <button style={{ height: 'auto', width: '100%' }} className="btn addBtn" onClick={this.nav}><i className="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Modify</button>
                                </div>

                            </div>

                            <figure>

                                <img class="modalPicture" src={this.state.images[this.state.accountDisplay.username]} />

                            </figure>
                            <table className="table">

                                <tbody>
                                    <tr><th> First Name</th><td> {this.state.accountDisplay.firstName}</td></tr>
                                    <tr><th> Last Name</th><td> {this.state.accountDisplay.lastName}</td></tr>
                                    <tr><th> Date of Birth</th><td> {this.state.accountDisplay.dob && this.state.accountDisplay.dob.toString().split('T')[0]}</td></tr>
                                    <tr><th> Username</th><td> {this.state.accountDisplay.username}</td></tr>
                                    <tr><th> Contact Phone</th><td> {this.state.accountDisplay.contactPhone}</td></tr>
                                    <tr><th> Email</th><td> {this.state.accountDisplay.email}</td></tr>
                                    <tr><th> Emergency Person</th><td> {this.state.accountDisplay.emgName}</td></tr>
                                    <tr><th> Emergency Relation</th><td> {this.state.accountDisplay.emgRelation}</td></tr>
                                    <tr><th> Emergency Phone</th><td> {this.state.accountDisplay.emgPhone}</td></tr>
                                    <tr><th> Contract Sign Date</th><td> {this.state.accountDisplay.contractSignDate && this.state.accountDisplay.contractSignDate.toString().split('T')[0]}</td></tr>
                                    <tr><th> Address</th><td> {this.state.accountDisplay.address}</td></tr>
                                    <tr><th> Bio</th><td> {this.state.accountDisplay.bio && this.state.accountDisplay.bio.split("\n").map((value) => {
                                        return <span>{value}</span>
                                    })}</td></tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
    
            </div>
        )
    }
}

export default BabsitterAccountPage;