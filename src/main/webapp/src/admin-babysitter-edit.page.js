import React from 'react';

import CreateBabysitterPage from './admin-babysitter-form.page'

class AdminBabysittersViewPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: new URL(window.location.href).searchParams.get('username'),

        }

        
    }

    componentDidMount() {
        const url_string = window.location.href;
        const url = new URL(url_string);
        const username = url.searchParams.get('username');
        //  console.log(username);
        this.setState({
            username: username
        });

    }

    render() {
        return (
            <div>
                <CreateBabysitterPage username={this.state.username}/>
               
            </div>
        )
    }
}

export default AdminBabysittersViewPage;