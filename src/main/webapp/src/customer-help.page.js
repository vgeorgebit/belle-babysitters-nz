import React from 'react';

import CustomerNavigation from './components/CustomerNavigation';
import api, { ax } from './api.js';
import * as constants from './constants.js';
import { withRouter } from 'react-router-dom';

import { render } from "react-dom";
import Accordion from "./components/Accordion";


class CustomerHelpPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedIndex: -1
        }
    }

    render() {
        return (
            <div>
                <CustomerNavigation />

                <div className="container" style={{ marginTop: "30px" }}>
                    <h1 style={{ paddingBottom: "10px" }}> FAQ's </h1>

                    <Accordion className="accordion" selectedIndex={this.state.selectedIndex} >
                        <div data-header="What is Belle Babysitters Ltd?" className="accordionItem">
                            <div className="panel">
                                <p> We offer a range of babysitters who will come to your home during the day or evening.
                                    They will entertain, feed and bath your child/children and follow their usual routines/bedtime activities,
                                    settle them to sleep, and attend to them during the evening should they wake.
                                </p>
                                <p>
                                    We also provide Hotel/Motel sitters for those that are on holiday or business in Dunedin.
                                    We can also organise multiple babysitters and set up a child friendly area within corporate/sports/wedding and other such events.
                                </p>
                            </div>
                        </div>
                    </Accordion >

                    <Accordion className="accordion" selectedIndex={this.state.selectedIndex} >
                        <div data-header="What are our fees?" className="accordionItem">
                            <div className="panel">
                                <p> We have two parts to our pricing. A booking (agency) fee, and the babysitter rates will apply separately.</p>
                                <p>See our <a href="http://www.bellebabysitters.co.nz/for-families">"Pricing"</a> tab on the main website for further information.</p>
                            </div>
                        </div>
                    </Accordion >

                    <Accordion className="accordion" selectedIndex={this.state.selectedIndex} >
                        <div data-header="How to make a booking" className="accordionItem">
                            <p>Booking in advance at least 5-7 days is recommended. However we will do our best to ensure you have access to one of our babysitters at all times.</p>
                            <p> 1. Click on the <a href="/customer/jobs/form">create booking</a> tab in your navigation</p>
                            <p> 2. Fill in your start date and time & your end date and time</p>
                            <p> 3. Select an address from one of your cargegivers or enter a new address </p>
                            <p> 4. Select present children</p>
                            <p> 5. Enter any job notes if necessary </p>
                            <p> 6. Select transport requirements</p>
                            <p> 7. Submit your booking. </p>
                            {/* <p></p> */}
                            <p> Once you have submitted your booking you will recieve a confirmation email ....</p>
                        </div>
                    </Accordion >

                    <Accordion className="accordion" selectedIndex={this.state.selectedIndex} >
                        <div data-header="My Account" className="accordionItem">
                            <p>
                                As a customer you are able to view and modify your personal account details.
                            </p>
                        </div>
                    </Accordion>

                </div>
            </div>
        )
    }
}

export default withRouter(CustomerHelpPage);