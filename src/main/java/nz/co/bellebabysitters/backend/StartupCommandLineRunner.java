package nz.co.bellebabysitters.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import nz.co.bellebabysitters.backend.service.IAdminService;

@Component
public class StartupCommandLineRunner implements CommandLineRunner {

	@Autowired
	IAdminService adminService;
	
	@Override
	public void run(String... args) throws Exception {
		try 
		{
			adminService.createNewAdmin("admin", "Belle", "Babysitters", true, "belleadmin");
			
			
			BelleBabysittersApplication.getLogger().info("Created default admin account with username: 'admin' and password: 'belleadmin'");
		} catch (Exception e)
		{
			BelleBabysittersApplication.getLogger().error("Unable to create default admin account: " + e.getMessage());
		}
	}

	
}
