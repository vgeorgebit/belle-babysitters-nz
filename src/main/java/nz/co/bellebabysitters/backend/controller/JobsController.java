package nz.co.bellebabysitters.backend.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javassist.NotFoundException;
import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.entity.Job;
import nz.co.bellebabysitters.backend.entity.JobOffer;
import nz.co.bellebabysitters.backend.entity.JobOfferStatus;
import nz.co.bellebabysitters.backend.service.IJobsService;

@RestController
public class JobsController {

	@Autowired
	IJobsService jobsService;

	private String getCurrentBabysitterUsername() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return username.substring(11);
	}

	private String getCurrentCustomerUsername() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return username.substring(9);
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@PostMapping("/api/jobs/{customer}")
	public ResponseEntity<Job> createNewJobForCustomer(@RequestBody Job job,
			@PathVariable(name = "customer") String customerEmail) {
		Job newJob;
		try {
			newJob = jobsService.createNewJobForCustomer(job, customerEmail);
			return new ResponseEntity<>(newJob, HttpStatus.CREATED);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@Secured({ AuthUtils.ROLE_CUSTOMER })
	@PostMapping("/api/jobs/ascustomer")
	public ResponseEntity<Job> createNewJobAsCustomer(@RequestBody Job job) {
		Job newJob;
		try {
			newJob = jobsService.createNewJobForCustomer(job, getCurrentCustomerUsername());
			return new ResponseEntity<>(newJob, HttpStatus.CREATED);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Secured({ AuthUtils.ROLE_BABYSITTER })
	@PostMapping("/api/jobs/respond/{id}")
	public ResponseEntity<?> respondToJobOffer(@PathVariable(name = "id") Long offerId,
			@RequestParam boolean accepted) {
		try {
			jobsService.respondToJobOffer(getCurrentBabysitterUsername(), offerId, accepted);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@GetMapping("/api/jobs/{id}")
	public ResponseEntity<Job> getJobById(@PathVariable(name = "id") Long jobId) {
		Optional<Job> j = jobsService.getJobById(jobId);

		if (!j.isPresent())
			return new ResponseEntity<Job>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<Job>(j.get(), HttpStatus.OK);
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@PutMapping("/api/jobs/{id}")
	public ResponseEntity<?> updateJobById(@PathVariable(name = "id") Long jobId, @RequestBody Job job) {
		jobsService.updateJob(jobId, job);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@Secured({ AuthUtils.ROLE_CUSTOMER })
	@GetMapping("/api/jobs/customer/{id}")
	public ResponseEntity<Job> getJobByIdAsCustomer(@PathVariable(name = "id") Long jobId) {
		Optional<Job> j = jobsService.getJobById(jobId);

		if (!j.isPresent())
			return new ResponseEntity<Job>(HttpStatus.NOT_FOUND);

		Job job = j.get();

		if (!job.getCustomer().getEmail().equals(getCurrentCustomerUsername()))
			return new ResponseEntity<Job>(HttpStatus.NOT_FOUND);

		// Don't send personal data about babysitters to customers...
		job.setJobOffers(null);
		for (Babysitter b : job.getAssignedBabysitters()) {
			b.setAddress(null);
			b.setContractSignDate(null);
			b.setDob(null);
			b.setEmail(null);
			b.setContactPhone(null);
			b.setEmgName(null);
			b.setEmgPhone(null);
			b.setEmgRelation(null);
			b.setLastName(null);
			b.setSetupCode(null);
			b.setResetCode(null);
		}

		job.getCustomer().setAdminNotesAdmin(null);
		job.getCustomer().setAdminNotesBabysitter(null);

		return new ResponseEntity<Job>(job, HttpStatus.OK);
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@GetMapping("/api/jobs")
	public Page<Job> getAllJobsWithFilter(@RequestParam int page, @RequestParam String search,
			@RequestParam String filterBy) {
		// No filter will return all pending jobs
		return jobsService.getAllJobsPaginated(page, search, filterBy);
	}

	@Secured({ AuthUtils.ROLE_BABYSITTER })
	@GetMapping("/api/jobs/offers")
	public ResponseEntity<Page<JobOffer>> getAllJobOffersPaginated(@RequestParam int page,
			@RequestParam String filterBy) {
		try {
			return new ResponseEntity<Page<JobOffer>>(
					jobsService.getAllJobOffersPaginated(getCurrentBabysitterUsername(), page, filterBy),
					HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<Page<JobOffer>>(HttpStatus.NOT_FOUND);
		}
	}

	@Secured({ AuthUtils.ROLE_BABYSITTER })
	@GetMapping("/api/jobs/offers/{id}")
	public ResponseEntity<JobOffer> getOneJobOffer(@PathVariable(name = "id") Long id) {
		try {
			return new ResponseEntity<JobOffer>(jobsService.getJobOffer(getCurrentBabysitterUsername(), id),
					HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<JobOffer>(HttpStatus.NOT_FOUND);
		}
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@GetMapping("/api/jobs/{id}/offers/{status}")
	public List<JobOffer> getAllBabysittersOffersForJobByStatus(@PathVariable(name = "id") Long jobId, @PathVariable(name = "status") JobOfferStatus status) {
		return jobsService.getJobOffersForJobByStatus(jobId, status);
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@PostMapping("/api/jobs/{id}/assign")
	public ResponseEntity<?> assignBabysittersToJob(@PathVariable(name = "id") Long jobId,
			@RequestParam String[] babysitterUsernames, @RequestParam String bookingFee) {
		try {
			jobsService.assignBabysittersToJob(jobId, babysitterUsernames, bookingFee);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (IllegalStateException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@PutMapping("/api/jobs/{id}/reassign")
	public ResponseEntity<?> reassignBabysittersToJob(@PathVariable(name = "id") Long jobId,
			@RequestParam String[] babysitterUsernames) {
		try {
			jobsService.reassignBabysittersToJob(jobId, babysitterUsernames);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (IllegalStateException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@Secured({ AuthUtils.ROLE_ADMIN })
	@PostMapping("/api/jobs/{id}/confirm")
	public ResponseEntity<?> confirmJob(@PathVariable(name = "id") Long jobId) {
		try {
			jobsService.confirmJob(jobId);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Secured({ AuthUtils.ROLE_BABYSITTER })
	@PostMapping("/api/jobs/babysitter")
	public Page<List<Job>> getAllAssignedJobAsBabysitter(@RequestParam int page, @RequestParam String search,
			@RequestParam String filterBy) {
		return jobsService.getAllJobsPaginatedForBabysitter(page, search, filterBy, getCurrentBabysitterUsername());
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@PostMapping("/api/jobs/{id}/cancel")
	public ResponseEntity<?> cancelJobAsAdmin(@PathVariable(name = "id") Long jobId) {
		try {
			jobsService.cancelJob(jobId, null);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@Secured({ AuthUtils.ROLE_CUSTOMER })
	@PostMapping("/api/jobs/{id}/customercancel")
	public ResponseEntity<?> cancelJobAsCustomer(@PathVariable(name = "id") Long jobId) {
		try {
			jobsService.cancelJob(jobId, getCurrentCustomerUsername());
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@Secured({ AuthUtils.ROLE_CUSTOMER })
	@PostMapping("/api/jobs/customer")
	public Page<Job> getAllJobsAsCustomer(@RequestParam int page, @RequestParam String filterBy) {
		Page<Job> result = jobsService.getAllJobsPaginatedForCustomer(page, filterBy, getCurrentCustomerUsername());

		result.getContent().forEach(job -> {
				job.setJobOffers(null);
				job.getCustomer().setAdminNotesAdmin(null);
				job.getCustomer().setAdminNotesBabysitter(null);
				if(job.getAssignedBabysitters() != null && !job.getAssignedBabysitters().isEmpty()){
					job.getAssignedBabysitters().forEach(b -> {
						b.setAddress(null);
						b.setContractSignDate(null);
						b.setDisabled(null);
						b.setContactPhone(null);
						b.setDob(null);
						b.setEmail(null);
						b.setEmgName(null);
						b.setEmgPhone(null);
						b.setEmgRelation(null);
						b.setLastName(null);
					});
				}
			});
		
		
		return result;
	}
}