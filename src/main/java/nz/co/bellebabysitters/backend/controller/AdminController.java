package nz.co.bellebabysitters.backend.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Admin;
import nz.co.bellebabysitters.backend.service.IAdminService;

@RestController
public class AdminController {

	// service layer stuff goes here
	@Autowired
	IAdminService adminService;

	private String getCurrentUsername() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();

		return username.substring(6);
	}

	/**
	 * FOR TESTING PURPOSES
	 */
	@GetMapping("/status")
	public String getStatus() {
		return "I am Alive";
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@PostMapping("/api/admin")
	public ResponseEntity<Admin> createAdmin(@RequestBody Admin newAdmin) {

		adminService.createNewAdmin(newAdmin.getUsername(), newAdmin.getFirstName(), newAdmin.getLastName(),
				newAdmin.getSuperAdmin(), newAdmin.getPassword());

		return new ResponseEntity<>(newAdmin, HttpStatus.CREATED);

	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@PutMapping("/api/admin")
	public ResponseEntity<?> updateAdmin(@RequestBody Admin newAdmin) {
		adminService.updateAdmin(newAdmin);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@PutMapping("/api/admin/ownpassword")
	public ResponseEntity<Admin> updateAdminOwnPassword(@RequestBody Admin adminToUpdate) {
		adminService.updateAdminPassword(getCurrentUsername(), adminToUpdate.getPassword());

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@DeleteMapping("/api/admin/{username}")
	public ResponseEntity<?> deleteAdmin(@PathVariable String username) {
		adminService.deleteAdmin(username);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@GetMapping("/api/admin/{username}")
	public ResponseEntity<?> getAdmin(@PathVariable String username) {
		Optional<Admin> result = adminService.getAdminByUsername(username);
		if (result.isPresent())
			return new ResponseEntity<>(result.get(), HttpStatus.OK);
		return new ResponseEntity<Admin>(HttpStatus.NOT_FOUND);
	}

	@Secured({ AuthUtils.ROLE_ADMIN })
	@GetMapping("/api/admin/owndetails")
	public ResponseEntity<Admin> getOwnDetails() {
		Optional<Admin> admin = adminService.getAdminByUsername(getCurrentUsername());

		if (admin.isPresent())
			return new ResponseEntity<Admin>(admin.get(), HttpStatus.OK);
		return new ResponseEntity<Admin>(HttpStatus.NOT_FOUND);
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@GetMapping("/api/admins")
	public Page<Admin> getAllAdminPaginated(@RequestParam int page, @RequestParam String search) {
		return adminService.getAllPaginated(page, search);
	}

}