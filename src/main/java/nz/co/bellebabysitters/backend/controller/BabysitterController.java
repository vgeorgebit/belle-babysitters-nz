package nz.co.bellebabysitters.backend.controller;

import java.io.IOException;
import java.util.Optional;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.service.IBabysitterService;

@RestController
public class BabysitterController {

	// service layer stuff goes here
	@Autowired
	IBabysitterService babysitterService;

	private String getCurrentUsername() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return username.substring(11);
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@PostMapping("/api/babysitter")
	public ResponseEntity<Babysitter> createBabysitter(@RequestBody Babysitter newBabysitter) {
		babysitterService.createNewBabysitter(newBabysitter);
		return new ResponseEntity<>(newBabysitter, HttpStatus.CREATED);
	}

	@PostMapping("/publicapi/babysitter/setup")
	public ResponseEntity<?> babysitterSetup(@RequestParam String code, @RequestParam String password) {
		babysitterService.setupBabysitter(code, password);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@PutMapping("/api/babysitter")
	public ResponseEntity<?> updateBabysitter(@RequestBody Babysitter requestBabysitter) {
		babysitterService.updateBabysitter(requestBabysitter);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Secured({ AuthUtils.ROLE_BABYSITTER })
	@PutMapping("/api/babysitter/owndetails")
	public ResponseEntity<?> updateOwnBabysitterDetails(@RequestParam String email, @RequestParam String contactPhone,
			@RequestParam String address, @RequestParam String password)
			throws JsonParseException, JsonMappingException, JsonGenerationException, IOException {
		Optional<Babysitter> b = babysitterService.getBabysitterByUsername(getCurrentUsername());

		if (!b.isPresent())
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		Babysitter babysitter = b.get();

		ObjectMapper om = new ObjectMapper();

		Babysitter updatedBabysitter = om.readValue(om.writeValueAsString(babysitter), Babysitter.class);
		updatedBabysitter.setEmail(email);
		updatedBabysitter.setContactPhone(contactPhone);
		updatedBabysitter.setAddress(address);
		updatedBabysitter.setPassword(password);

		babysitterService.updateBabysitter(updatedBabysitter);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Secured({ AuthUtils.ROLE_BABYSITTER })
	@GetMapping("/api/babysitter/owndetails")
	public ResponseEntity<Babysitter> getOwnDetails() {
		Optional<Babysitter> babysitter = babysitterService.getBabysitterByUsername(getCurrentUsername());

		if (babysitter.isPresent())
			return new ResponseEntity<Babysitter>(babysitter.get(), HttpStatus.OK);
		return new ResponseEntity<Babysitter>(HttpStatus.NOT_FOUND);
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@GetMapping("/api/babysitters")
	public Page<Babysitter> getAllBabysittersPaginated(@RequestParam int page, @RequestParam String search) {
		return babysitterService.getAllPaginated(page, search);
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@DeleteMapping("/api/babysitter/{username}")
	public ResponseEntity<?> deleteBabysitter(@PathVariable String username) {
		babysitterService.deleteBabysitter(username);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@PutMapping("/api/babysitter/image/{username}")
	public ResponseEntity<?> putImage(@PathVariable String username, @RequestParam MultipartFile file)
			throws IOException {
		byte[] image = file.getBytes();
		babysitterService.setBabysitterImage(username, image);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@Secured({ AuthUtils.ROLE_ADMIN, AuthUtils.ROLE_BABYSITTER, AuthUtils.ROLE_CUSTOMER })
	@GetMapping(value = "/api/babysitter/image/{username}", produces = { MediaType.IMAGE_JPEG_VALUE })
	public @ResponseBody byte[] getImage(@PathVariable String username) {
		return babysitterService.getBabysitterImage(username);

	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@GetMapping("/api/babysitter/{username}")
	public ResponseEntity<Babysitter> getSingleBabysitter(@PathVariable String username) {
		return new ResponseEntity<>(babysitterService.getBabysitterByUsername(username).get(), HttpStatus.OK);
	}

	@PostMapping("/publicapi/babysitter/passwordreset")
	public ResponseEntity<?> babysitterPasswordResetRequest(@RequestParam String email) {
		babysitterService.setupPasswordReset(email);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PostMapping("/publicapi/babysitter/resetpassword")
	public ResponseEntity<?> babysitterResetPassword(@RequestParam String code, @RequestParam String password) {
		babysitterService.resetBabysitterPassword(code, password);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}