package nz.co.bellebabysitters.backend.controller;

import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Job;
import nz.co.bellebabysitters.backend.entity.JobStatus;
import nz.co.bellebabysitters.backend.service.IJobsService;
import nz.co.bellebabysitters.backend.service.IPaymentService;

@RestController
public class PaymentController {

	@Autowired
	private IPaymentService paymentService;
	
	@Autowired
	private IJobsService jobsService;

	private String getCurrentCustomerUsername() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return username.substring(9);
	}
	
	@Secured({ AuthUtils.ROLE_CUSTOMER })
	@PostMapping(value = "api/job/payment/{id}")
	public ResponseEntity<Map<String, Object>> makePayment(@PathVariable("id") Long paymentId) {
		Optional<Job> j = jobsService.getJobById(paymentId);
		
		if (!j.isPresent())
			return new ResponseEntity<Map<String,Object>>(HttpStatus.NOT_FOUND);
		
		Job job = j.get();
		
		if (!job.getCustomer().getEmail().equals(getCurrentCustomerUsername()))
			return new ResponseEntity<Map<String,Object>>(HttpStatus.NOT_FOUND);
		
		if (job.getStatus() != JobStatus.PENDING_PAYMENT)
			return new ResponseEntity<Map<String,Object>>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<>(paymentService.createJobPayment(job), HttpStatus.OK);
	}
	
	@PostMapping(value = "publicapi/job/payment/complete")
	public ResponseEntity<Map<String, Object>> completePayment(HttpServletRequest request) {
		return new ResponseEntity<>(paymentService.completeJobPayment(request), HttpStatus.OK);
	}

}
