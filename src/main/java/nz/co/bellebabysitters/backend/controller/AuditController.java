package nz.co.bellebabysitters.backend.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.entity.Caregiver;
import nz.co.bellebabysitters.backend.entity.Child;
import nz.co.bellebabysitters.backend.entity.Customer;
import nz.co.bellebabysitters.backend.entity.audit.AuditRevisionEntity;
import nz.co.bellebabysitters.backend.entity.audit.AuditRevisionResponse;
import nz.co.bellebabysitters.backend.service.IBabysitterService;
import nz.co.bellebabysitters.backend.service.ICustomerService;

@RestController
public class AuditController {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private IBabysitterService babysitterService;

	@Autowired
	private ICustomerService customerService;

	@SuppressWarnings("unchecked")
	private List<AuditRevisionResponse> getRevisionsForEntity(Class<?> clazz, Long id) {

		AuditReader reader = AuditReaderFactory.get(entityManager);
		List<Number> revisions = reader.getRevisions(clazz, id);

		List<AuditRevisionResponse> response = new ArrayList<>();

		for (Number revisionNumber : revisions) {
			List<Object[]> results = reader.createQuery().forRevisionsOfEntity(clazz, false, false)
					.add(AuditEntity.id().eq(id)).add(AuditEntity.revisionNumber().eq(revisionNumber)).getResultList();

			for (Object o : results) {
				Object[] data = (Object[]) o;
				Object entity = data[0];
				AuditRevisionEntity revisionEntity = (AuditRevisionEntity) data[1];
				String action = data[2].toString();

				response.add(new AuditRevisionResponse(revisionEntity.getId(), revisionEntity.getRevisionDate(),
						revisionEntity.getEditor(), action, entity));
			}
		}

		return response;
	}

	@SuppressWarnings("unchecked")
	private List<AuditRevisionResponse> getRevisionsForCustomer(Class<?> clazz, Long id, Number revisionNumber) {

		AuditReader reader = AuditReaderFactory.get(entityManager);

		List<AuditRevisionResponse> response = new ArrayList<>();

		Object[] data = (Object[])reader.createQuery().forRevisionsOfEntity(clazz, false, false)
				.add(AuditEntity.id().eq(id)).add(AuditEntity.revisionNumber().eq(revisionNumber)).getSingleResult();

		Object entity = data[0];
		AuditRevisionEntity revisionEntity = (AuditRevisionEntity) data[1];
		String action = data[2].toString();
		Customer cust = (Customer) entity;
		
		List<Object> dataChildren = reader.createQuery().forRevisionsOfEntity(Child.class, false, false)
				.add(AuditEntity.property("customer_id").eq(id)).add(AuditEntity.revisionNumber().eq(revisionNumber)).getResultList();
		
		cust.getChildren().clear();
		
		for (Object o : dataChildren) {
			Child c = (Child) ((Object[])o)[0];
			cust.getChildren().add(c);
		}
		
		List<Object> dataCaregivers = reader.createQuery().forRevisionsOfEntity(Caregiver.class, false, false)
				.add(AuditEntity.property("customer_id").eq(id)).add(AuditEntity.revisionNumber().eq(revisionNumber)).getResultList();
		
		cust.getCaregivers().clear();
		
		for (Object o : dataCaregivers) {
			Caregiver c = (Caregiver) ((Object[]) o)[0];
			cust.getCaregivers().add(c);
		}
		
		response.add(new AuditRevisionResponse(revisionEntity.getId(), revisionEntity.getRevisionDate(),
				revisionEntity.getEditor(), action, entity));
		
		return response;
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@GetMapping("/api/audit/babysitter/{username}")
	public ResponseEntity<List<AuditRevisionResponse>> getBabysitterAuditRevisions(@PathVariable String username) {
		Optional<Babysitter> b = babysitterService.getBabysitterByUsername(username);

		if (!b.isPresent())
			return new ResponseEntity<List<AuditRevisionResponse>>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<List<AuditRevisionResponse>>(getRevisionsForEntity(Babysitter.class, b.get().getId()),
				HttpStatus.OK);
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@GetMapping("/api/audit/customer/{username}")
	public ResponseEntity<List<AuditRevisionResponse>> getCustomerAuditRevisions(@PathVariable String username) {
		Optional<Customer> c = customerService.getCustomerByUsername(username);

		if (!c.isPresent())
			return new ResponseEntity<List<AuditRevisionResponse>>(HttpStatus.NOT_FOUND);

		Customer customer = c.get();

		List<Number> revisionNumbers = AuditReaderFactory.get(entityManager).getRevisions(Customer.class, customer.getId());
		
		List<AuditRevisionResponse> response = new ArrayList<>();
		
		for (Number revisionNumber : revisionNumbers) {
			response.addAll(getRevisionsForCustomer(Customer.class, customer.getId(), revisionNumber));
		}
		
		return new ResponseEntity<List<AuditRevisionResponse>>(response, HttpStatus.OK);
	}

}
