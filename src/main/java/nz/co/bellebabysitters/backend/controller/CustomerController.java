
package nz.co.bellebabysitters.backend.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javassist.NotFoundException;
import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Customer;
import nz.co.bellebabysitters.backend.service.ICustomerService;
import nz.co.bellebabysitters.backend.service.MailService;

@SuppressWarnings("unused")
@RestController
public class CustomerController {

	@Autowired
	private ICustomerService customerService;
	
	@Autowired
	private MailService mailService;
	
	private String getCurrentUsername() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return username.substring(9);
	}
	
	@Secured(AuthUtils.ROLE_SUPER_ADMIN)
	@PostMapping("/api/customer")
	public ResponseEntity<Customer> adminCreateCustomerAccount(@RequestBody Customer customer) {
		return new ResponseEntity<Customer>(customerService.createNewCustomer(customer), HttpStatus.CREATED);
	}
	
	@PostMapping("/publicapi/customer")
	public ResponseEntity<Customer> customerCreateCustomerAccount(@RequestBody Customer customer) {
		return new ResponseEntity<Customer>(customerService.createNewCustomerAsCustomer(customer), HttpStatus.CREATED);
	}

	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@PutMapping("/api/customer")
	public ResponseEntity<Customer> updateCustomer(@RequestBody Customer updateCustomer) {
		try {
			customerService.updateCustomer(updateCustomer);
			return new ResponseEntity<>(updateCustomer, HttpStatus.NO_CONTENT);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/publicapi/customer/setup")
	public ResponseEntity<?> customerSetup(@RequestParam String code, @RequestParam String password) {
		customerService.setupCustomer(code, password);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PostMapping("/publicapi/customer/passwordreset")
	public ResponseEntity<?> customerPasswordResetRequest(@RequestParam String email) {
		customerService.setupPasswordReset(email);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PostMapping("/publicapi/customer/resetpassword")
	public ResponseEntity<?> CustomerResetPassword(@RequestParam String code, @RequestParam String password) {
		customerService.resetCustomerPassword(code, password);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@Secured({ AuthUtils.ROLE_CUSTOMER })
	@GetMapping("/api/customer/owndetails")
	public ResponseEntity<Customer> getOwnDetails() {
		Optional<Customer> customer = customerService.getCustomerByUsername(getCurrentUsername());

		if (customer.isPresent()) {
			Customer c = customer.get();
			
			// Don't send admin only notes about the customer to them!
			c.setAdminNotesAdmin(null);
			c.setAdminNotesBabysitter(null);
			
			return new ResponseEntity<Customer>(c, HttpStatus.OK);
		}
		return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
	}
	
	
	@Secured({ AuthUtils.ROLE_CUSTOMER })
	@PutMapping("/api/customer/owndetails")
	public ResponseEntity<Customer> customerUpdateCustomer(@RequestBody Customer updateCustomer) {
		if (!updateCustomer.getEmail().equals(getCurrentUsername()))
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		
		try {
			customerService.updateCustomerAsCustomer(updateCustomer);
			return new ResponseEntity<>(updateCustomer, HttpStatus.NO_CONTENT);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@Secured({AuthUtils.ROLE_SUPER_ADMIN})
	@GetMapping("/api/customer/{username}")
	public ResponseEntity<Customer> getSingleBabysitter(@PathVariable String username){
		Optional<Customer> c = customerService.getCustomerByUsername(username);
		if (!c.isPresent())
			return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<Customer>(c.get(), HttpStatus.OK);
	}
	
	@Secured({ AuthUtils.ROLE_SUPER_ADMIN })
	@GetMapping("/api/customers")
	public Page<Customer> getAllAdminPaginated(@RequestParam int page, @RequestParam String search) {
		return customerService.getAllPaginated(page, search);
	}
	
	@Secured({ AuthUtils.ROLE_CUSTOMER })
	@PostMapping("/api/customer/inquiry")
	public ResponseEntity<Customer> getCustomerInquiry(@RequestParam String inquiryMessage) {
		if (inquiryMessage.length() < 20)
			throw new IllegalArgumentException("Inquiry message is too short");
		
		Customer customer = customerService.getCustomerByUsername(getCurrentUsername()).get();
		
		mailService.sendCustomerInquiry(customer, inquiryMessage);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
