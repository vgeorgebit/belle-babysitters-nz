package nz.co.bellebabysitters.backend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import javassist.NotFoundException;
import nz.co.bellebabysitters.backend.entity.Job;
import nz.co.bellebabysitters.backend.entity.JobOffer;
import nz.co.bellebabysitters.backend.entity.JobOfferStatus;

public interface IJobsService {

	public Job createNewJobForCustomer(Job job, String customerEmail) throws NotFoundException;

	public void respondToJobOffer(String babysitterUsername, Long offerId, boolean accepted) throws NotFoundException;

	public Optional<Job> getJobById(Long jobId);

	public Page<Job> getAllJobsPaginated(int page, String search, String filterBy);

	public Page<JobOffer> getAllJobOffersPaginated(String babysitterUsername, int page, String filterBy) throws NotFoundException;

	public JobOffer getJobOffer(String babysitterUsername, Long id) throws NotFoundException;

	public List<JobOffer> getJobOffersForJobByStatus(Long jobId, JobOfferStatus status);

	public void assignBabysittersToJob(Long jobId, String[] babysitterUsernames, String bookingFeePrice) throws NotFoundException;

	public void confirmJob(Long jobId) throws NotFoundException;

	public Page<List<Job>> getAllJobsPaginatedForBabysitter(int page, String search, String filterBy,
			String currentBabysitterUsername);
	
	public Page<Job> getAllJobsPaginatedForCustomer(int page, String filterBy,
			String customerUsername);

	void cancelJob(Long jobId, String customerUsername) throws NotFoundException;

	public void updateJob(Long jobId, Job job);

	public void reassignBabysittersToJob(Long jobId, String[] babysitterUsernames) throws NotFoundException;

}
