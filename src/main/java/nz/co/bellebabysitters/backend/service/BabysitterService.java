package nz.co.bellebabysitters.backend.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.exception.EntityConstraintViolationException;
import nz.co.bellebabysitters.backend.exception.UnhandledException;
import nz.co.bellebabysitters.backend.exception.UsernameInUseException;
import nz.co.bellebabysitters.backend.repository.DBErrorCodes;
import nz.co.bellebabysitters.backend.repository.IBabysitterRepository;

@Service
public class BabysitterService implements IBabysitterService {

	@Autowired
	IBabysitterRepository babysitterRepository;

	@Autowired
	MailService mailService;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	byte[] defaultImage;

	public BabysitterService() throws IOException {
		String path = System.getProperty("user.dir") + "/src/main/resources/default.png";
		try {
			defaultImage = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			throw new IOException("Default account image not found");
		}
	}

	@Override
	@Transactional
	public void createNewBabysitter(Babysitter babysitter) {
		babysitter.setPassword(null);
		babysitter.setSetupCode(UUID.randomUUID().toString());
		babysitter.setUsername(babysitter.getUsername().toLowerCase());
		save(babysitter);
		mailService.sendBabysitterSetupEmail(babysitter);
	}

	@Override
	public void setupBabysitter(String code, String password) {
		Optional<Babysitter> b = babysitterRepository.findBySetupCode(code);
		if (!b.isPresent())
			throw new IllegalArgumentException("Invalid setup code");

		Babysitter babysitter = b.get();
		AuthUtils.validatePassword(password);
		babysitter.setSetupCode(null);
		babysitter.setPassword(passwordEncoder.encode(password));
		save(babysitter);
	}
	
	@Override
	public void resetBabysitterPassword(String code, String password) {
		System.err.println(code);
		Optional<Babysitter> b = babysitterRepository.findByResetCode(code);

		if (!b.isPresent())
			throw new IllegalArgumentException("Invalid reset code");
		
		Babysitter babysitter = b.get();
		
		if(babysitter.getResetExpiryDate().before(Date.from(Instant.now())))
			throw new IllegalArgumentException("Password reset code has expired");
		
		AuthUtils.validatePassword(password);
		babysitter.setResetCode(null);
		babysitter.setResetExpiryDate(null);
		babysitter.setPassword(passwordEncoder.encode(password));
		save(babysitter);	
		
		mailService.sendBabysitterPasswordChangedEmail(babysitter);
	}

	@Override
	public Optional<Babysitter> getBabysitterByUsername(String username) {
		return babysitterRepository.findByUsername(username);
	}

	@Transactional
	@Override
	public void updateBabysitter(Babysitter updatedBabysitter) {
		Babysitter babysitter = getBabysitterByUsername(updatedBabysitter.getUsername()).get();
		
		updatedBabysitter.setId(babysitter.getId());
		updatedBabysitter.setImage(babysitter.getImage());

		if(updatedBabysitter.getPassword() == null || updatedBabysitter.getPassword().isEmpty()){
			updatedBabysitter.setPassword(babysitter.getPassword());
			updatedBabysitter.setSetupCode(babysitter.getSetupCode());
		} else {
			AuthUtils.validatePassword(updatedBabysitter.getPassword());
			updatedBabysitter.setSetupCode(null);
			updatedBabysitter.setPassword(passwordEncoder.encode(updatedBabysitter.getPassword()));
		}

		save(updatedBabysitter);
	}

	@Override
	public void updateBabysitterPassword(String username, String password) {
		// To DO
	}

	/**
	 * Saves the babysitter to the repository, if some conditions are thrown, the
	 * response is defined here. As the repository.save() method is used to update
	 * too (and likely other contexts), the code to save is put into this method to
	 * stop repeating itself.
	 * 
	 * @param babysitter the babysitter being saved / updated
	 */
	public void save(Babysitter babysitter) {
		try {
			babysitterRepository.save(babysitter);
		} catch (DataIntegrityViolationException e) {
			/*
			 * DataIntegrityViolationException is a Spring wrapper containing the Hibernate
			 * ConstraintViolationException.
			 */
			ConstraintViolationException cve = (ConstraintViolationException) e.getCause();

			switch (cve.getErrorCode()) {
			case DBErrorCodes.DUPLICATE_ENTRY:
				throw new UsernameInUseException(babysitter.getUsername());
			default:
				throw new UnhandledException(e.getMessage());
			}
		} catch (javax.validation.ConstraintViolationException e) {
			List<String> violationMessages = new ArrayList<String>();
			for (ConstraintViolation<?> cv : e.getConstraintViolations()) {
				violationMessages.add(cv.getMessage());
			}
			throw new EntityConstraintViolationException(violationMessages);
		}
	}

	@Override
	public Page<Babysitter> getAllPaginated(int page, String search) {
		PageRequest pageRequest = PageRequest.of(page, 5, Sort.by(Direction.ASC, "firstName", "lastName", "username"));

		return babysitterRepository.findAll(search, pageRequest);
	}

	@Transactional
	@Override
	public void deleteBabysitter(String username) {
		babysitterRepository.deleteByUsername(username);
	}

	@Override
	public void setBabysitterImage(String username, byte[] image) {
		Babysitter babysitter = getBabysitterByUsername(username).get();
		babysitter.setImage(image);
		save(babysitter);
	}

	@Override
	public byte[] getBabysitterImage(String username) {
		byte[] image = getBabysitterByUsername(username).get().getImage();
		return image == null ? defaultImage : image;
	}
	
	@Override
	@Transactional
	public void setupPasswordReset(String email) {
		Optional<Babysitter> b = babysitterRepository.findByEmail(email.toLowerCase());
		
		if (!b.isPresent())
			throw new UsernameNotFoundException("No account is associated with the given email.");
		
		Babysitter babysitter = b.get();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, 12);
		
		String resetCode = UUID.randomUUID().toString();
		Date resetExpiry = cal.getTime();
		babysitter.setResetCode(resetCode);
		babysitter.setResetExpiryDate(resetExpiry);
		
		babysitterRepository.setupPasswordResetForBabysitter(babysitter.getId(), resetCode, resetExpiry);
		mailService.sendBabysitterResetEmail(babysitter);
		
	}

}
