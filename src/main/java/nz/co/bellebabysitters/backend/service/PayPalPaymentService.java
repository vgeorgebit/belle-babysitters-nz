package nz.co.bellebabysitters.backend.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.InputFields;
import com.paypal.api.payments.Item;
import com.paypal.api.payments.ItemList;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.Presentation;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.api.payments.WebProfile;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

import javassist.NotFoundException;
import nz.co.bellebabysitters.backend.entity.Job;
import nz.co.bellebabysitters.backend.exception.PaymentFailedException;
import nz.co.bellebabysitters.backend.repository.IJobsRepository;

@Service
public class PayPalPaymentService implements IPaymentService {

	@Value("${belle.domain}")
	private String domain;

	@Value("${belle.paypal.client-id}")
	private String clientId;

	@Value("${belle.paypal.client-secret}")
	private String clientSecret;

	@Value("${belle.paypal.sandbox}")
	private boolean sandbox;

	@Autowired
	private IJobsRepository jobsRepository;
	
	@Autowired
	private IJobsService jobsService;

	private static final String WEB_PROFILE_NAME = "Belle Babysitters";

	@Override
	public Map<String, Object> createJobPayment(Job job) {
		Map<String, Object> response = new HashMap<String, Object>();

		if (!job.getBookingFeePrice().matches("\\d+\\.(\\d{2})"))
			throw new IllegalArgumentException("Invalid price value");

		Amount amount = new Amount("NZD", job.getBookingFeePrice());
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setReferenceId(job.getId().toString());

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm zzz");
		String bookingInfo = "(Booking ID: #" + job.getId() + ")\n" + dateFormat.format(job.getStartTime()) + " - "
				+ dateFormat.format(job.getEndTime());

		ItemList itemList = new ItemList();
		Item item = new Item("Belle Babysitters Booking Fee", "1", job.getBookingFeePrice(), "NZD");
		item.setDescription(bookingInfo);
		List<Item> items = new ArrayList<Item>();
		items.add(item);
		itemList.setItems(items);

		transaction.setItemList(itemList);
		transaction.setNoteToPayee(bookingInfo);
		transaction.setDescription(bookingInfo);

		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(transaction);

		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		Payment payment = new Payment();
		payment.setIntent("sale");
		payment.setPayer(payer);
		payment.setTransactions(transactions);

		RedirectUrls redirectUrls = new RedirectUrls();
		redirectUrls.setCancelUrl(domain + "/customer/paymentfail?id=" + job.getId());
		redirectUrls.setReturnUrl(domain + "/customer/processpayment?id=" + job.getId());
		payment.setRedirectUrls(redirectUrls);
		Payment createdPayment;
		try {
			APIContext context = new APIContext(clientId, clientSecret, this.sandbox ? "sandbox" : "live");

			List<WebProfile> profiles = WebProfile.getList(context);

			WebProfile profile = null;
			String profileId = "";

			for (WebProfile p : profiles) {
				if (p.getName().equals(WEB_PROFILE_NAME)) {
					profile = p;
					profileId = p.getId();
					break;
				}
			}

			if (profile == null) {
				profile = new WebProfile();
				profile.setPresentation(new Presentation().setBrandName("Belle Babysitters").setLocaleCode("NZ"));
				profile.setInputFields(new InputFields().setAllowNote(false).setNoShipping(1));
				profile.setName(WEB_PROFILE_NAME);
				profileId = profile.create(context).getId();
			}

			payment.setExperienceProfileId(profileId);

			String redirectUrl = "";
			createdPayment = payment.create(context);
			if (createdPayment != null) {
				List<Links> links = createdPayment.getLinks();
				for (Links link : links) {
					if (link.getRel().equals("approval_url")) {
						redirectUrl = link.getHref();
						break;
					}
				}
				response.put("status", "success");
				response.put("redirect_url", redirectUrl);
			}
		} catch (PayPalRESTException e) {
			throw new PaymentFailedException("Unable to create payment");
		}

		job.setPaymentId(createdPayment.getId());
		try {
			jobsRepository.save(job);
		} catch (Exception e) {
			throw new PaymentFailedException("Unable to store payment id");
		}
		return response;
	}

	@Transactional
	public Map<String, Object> completeJobPayment(HttpServletRequest req) {
		Map<String, Object> response = new HashMap<String, Object>();
		Payment payment = new Payment();
		String jobId = req.getParameter("id");
		String paymentId = req.getParameter("paymentId");

		Optional<Job> j = jobsRepository.findById(Long.parseLong(jobId));

		if (!j.isPresent() || !j.get().getPaymentId().equals(paymentId))
			throw new PaymentFailedException("Invalid payment details");

		try {
			jobsService.confirmJob(j.get().getId());
		} catch (NotFoundException e) {
			throw new PaymentFailedException("Failed to confirm job");
		}
		
		payment.setId(paymentId);
		PaymentExecution paymentExecution = new PaymentExecution();
		paymentExecution.setPayerId(req.getParameter("PayerID"));
		try {
			APIContext context = new APIContext(clientId, clientSecret, "sandbox");
			Payment createdPayment = payment.execute(context, paymentExecution);
			if (createdPayment == null) {
				throw new PaymentFailedException("Failed to execute payment");
			}
			
			response.put("id", jobId);
		} catch (PayPalRESTException e) {
			throw new PaymentFailedException("Failed to complete payment");
		}
		
		// TODO: Emails, confirm booking, etc...
		
		return response;
	}

}
