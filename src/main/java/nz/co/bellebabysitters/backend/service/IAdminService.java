package nz.co.bellebabysitters.backend.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import nz.co.bellebabysitters.backend.entity.Admin;

public interface IAdminService {

	public void createNewAdmin(String username, String firstName, String lastName, boolean superAdmin, String password);

	public Optional<Admin> getAdminByUsername(String username);

	public void updateAdminPassword(String username, String password);
	
	public void deleteAdmin(String username);

	public void updateAdmin(Admin admin);

	public Page<Admin> getAllPaginated(int page, String search);
	
}
