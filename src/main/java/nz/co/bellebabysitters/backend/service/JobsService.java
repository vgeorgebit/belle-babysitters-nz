package nz.co.bellebabysitters.backend.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import javassist.NotFoundException;
import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.entity.Car;
import nz.co.bellebabysitters.backend.entity.Customer;
import nz.co.bellebabysitters.backend.entity.Job;
import nz.co.bellebabysitters.backend.entity.JobOffer;
import nz.co.bellebabysitters.backend.entity.JobOfferStatus;
import nz.co.bellebabysitters.backend.entity.JobStatus;
import nz.co.bellebabysitters.backend.entity.License;
import nz.co.bellebabysitters.backend.repository.IBabysitterRepository;
import nz.co.bellebabysitters.backend.repository.ICustomerRepository;
import nz.co.bellebabysitters.backend.repository.IJobOfferRepository;
import nz.co.bellebabysitters.backend.repository.IJobsRepository;

@Service
public class JobsService implements IJobsService {

	@Autowired
	ICustomerRepository customerRepository;

	@Autowired
	IBabysitterRepository babysitterRepository;

	@Autowired
	IJobsRepository jobsRepository;

	@Autowired
	IJobOfferRepository jobOfferRepository;

	@Autowired
	MailService mailService;

	@Transactional
	@Override
	public Job createNewJobForCustomer(Job job, String customerEmail) throws NotFoundException {
		Job newJob = new Job();

		Optional<Customer> customer = customerRepository.findByEmail(customerEmail);

		if (!customer.isPresent())
			throw new NotFoundException("Customer not found");

		newJob.setAddressNotes(job.getAddressNotes());
		newJob.setAssignedBabysitters(null);
		newJob.setChildrenDetails(job.getChildrenDetails());
		newJob.setCustomer(customer.get());
		newJob.setEndTime(job.getEndTime());
		newJob.setId(null);
		newJob.setJobAddress(job.getJobAddress());
		newJob.setJobNotes(job.getJobNotes());
		newJob.setStartTime(job.getStartTime());
		newJob.setStatus(JobStatus.PENDING_ASSIGNMENT);
		newJob.setOwnCarRequired(job.isOwnCarRequired());
		newJob.setFullLicenseRequired(job.isFullLicenseRequired());
		newJob.setNumChildren(job.getNumChildren());
		newJob.setDateRequested(new Date());

		List<Babysitter> potentialBabysitters = job.isFullLicenseRequired() ? babysitterRepository.findByLicense(License.FULL)
				: babysitterRepository.findAll();

		List<JobOffer> jobOffers = new ArrayList<>(potentialBabysitters.size());
		for (Babysitter babysitter : potentialBabysitters) {
			if ((job.isOwnCarRequired() && (babysitter.getCar() == Car.ALWAYS || babysitter.getCar() == Car.SOMETIMES))
					|| !job.isOwnCarRequired()) {
				JobOffer offer = new JobOffer();
				offer.setBabysitter(babysitter);
				offer.setJob(newJob);
				offer.setStatus(JobOfferStatus.PENDING);
				jobOffers.add(offer);
			}
		}

		newJob.setJobOffers(jobOffers);

		jobsRepository.save(newJob);

		mailService.sendJobOfferEmails(jobOffers);
		mailService.sendNewJobAlertToAdmin(newJob);
		mailService.sendJobConfirmationToCustomer(newJob);

		return newJob;
	}

	@Override
	public void respondToJobOffer(String babysitterUsername, Long offerId, boolean accepted) throws NotFoundException {
		Optional<Babysitter> b = babysitterRepository.findByUsername(babysitterUsername);
		if (!b.isPresent())
			throw new NotFoundException("Babysitter not found");

		Babysitter babysitter = b.get();

		Optional<JobOffer> o = jobOfferRepository.findByIdAndBabysitter(offerId, babysitter);
		if (!o.isPresent())
			throw new NotFoundException("Job offer not found");

		JobOffer offer = o.get();
		offer.setStatus(accepted ? JobOfferStatus.AVAILABLE : JobOfferStatus.UNAVAILABLE);

		jobOfferRepository.save(offer);
	}

	@Override
	public Optional<Job> getJobById(Long jobId) {
		return jobsRepository.findById(jobId);
	}

	@Override
	public Page<Job> getAllJobsPaginated(int page, String search, String filterBy) {
		PageRequest pageRequest = PageRequest.of(page, 10, Sort.by(Direction.ASC, "startTime", "dateRequested"));

		PageRequest pastPageRequest = PageRequest.of(page, 10, Sort.by(Direction.DESC, "startTime", "dateRequested"));
		
		switch (filterBy) {
		case "PENDING_ASSIGNMENT":
			return jobsRepository.findAllByStatus(search, pageRequest, JobStatus.PENDING_ASSIGNMENT);
		case "PENDING_PAYMENT":
			return jobsRepository.findAllByStatus(search, pageRequest, JobStatus.PENDING_PAYMENT);
		case "CONFIRMED":
			return jobsRepository.findAllByStatus(search, pageRequest, JobStatus.CONFIRMED);
		case "COMPLETED":
			return jobsRepository.findAllByStatus(search, pastPageRequest, JobStatus.COMPLETED);
		case "EXPIRED":
			return jobsRepository.findAllByStatus(search, pastPageRequest, JobStatus.EXPIRED);
		case "CANCELLED":
			return jobsRepository.findAllByStatus(search, pastPageRequest, JobStatus.CANCELLED);
		case "PAST":
			return jobsRepository.findAllPast(search, pastPageRequest);
		default:
			return jobsRepository.findAllPending(search, pageRequest);
		}
	}

	@Override
	public Page<JobOffer> getAllJobOffersPaginated(String babysitterUsername, int page, String filterBy)
			throws NotFoundException {
		PageRequest pageRequest = PageRequest.of(page, 10,
				Sort.by(Direction.ASC, "job.startTime", "job.dateRequested"));

		Optional<Babysitter> b = babysitterRepository.findByUsername(babysitterUsername);
		if (!b.isPresent())
			throw new NotFoundException("Babysitter not found");

		Babysitter babysitter = b.get();

		switch (filterBy) {
		case "PENDING":
			return jobOfferRepository.findAllByStatusAndBabysitterId(pageRequest, JobOfferStatus.PENDING,
					babysitter.getId());
		case "AVAILABLE":
			return jobOfferRepository.findAllByStatusAndBabysitterId(pageRequest, JobOfferStatus.AVAILABLE,
					babysitter.getId());
		case "UNAVAILABLE":
			return jobOfferRepository.findAllByStatusAndBabysitterId(pageRequest, JobOfferStatus.UNAVAILABLE,
					babysitter.getId());
		default:
			return null;
		}
	}

	@Override
	public JobOffer getJobOffer(String babysitterUsername, Long id) throws NotFoundException {
		Optional<Babysitter> b = babysitterRepository.findByUsername(babysitterUsername);
		if (!b.isPresent())
			throw new NotFoundException("Babysitter not found");

		Babysitter babysitter = b.get();
		return jobOfferRepository.findByIdAndBabysitter(id, babysitter).get();
	}

	@Override
	public List<JobOffer> getJobOffersForJobByStatus(Long jobId, JobOfferStatus status) {
		return jobOfferRepository.findAllByJobIdAndStatus(jobId, status);
	}

	@Transactional
	@Override
	public void assignBabysittersToJob(Long jobId, String[] babysitterUsernames, String bookingFeePrice)
			throws NotFoundException {

		if (!bookingFeePrice.matches("\\d+\\.(\\d{2})"))
			throw new IllegalArgumentException("Invalid booking fee format");

		Optional<Job> j = jobsRepository.findById(jobId);
		if (!j.isPresent())
			throw new NotFoundException("Job not found");

		Job job = j.get();

		if (babysitterUsernames.length < 1)
			throw new IllegalArgumentException("At least one babysitter must be assigned to a job");

		if (job.getAssignedBabysitters().size() != 0)
			throw new IllegalStateException("Job has already been assigned. Make a reassign request");

		Set<Babysitter> babysitters = babysitterRepository.findAllByUsername(Arrays.asList(babysitterUsernames));
		if (babysitters.size() != babysitterUsernames.length)
			throw new NotFoundException("One or more of the given babysitters was not found");

		boolean noFee = bookingFeePrice.matches("0+\\.00");

		job.setAssignedBabysitters(babysitters);
		job.setStatus(noFee ? JobStatus.CONFIRMED : JobStatus.PENDING_PAYMENT);
		if (noFee)
			job.setDateConfirmed(new Date());
		
		job.setBookingFeePrice(bookingFeePrice);

		jobsRepository.save(job);

		
		mailService.sendBabysitterJobAssignedEmail(job);
		mailService.sendCustomerJobAssignedEmail(job);

		if (noFee) {
			mailService.sendCustomerJobConfirmedEmail(job);
			mailService.sendBabysittersJobConfirmedEmail(job);
		}
	}
	
	@Override
	@Transactional
	public void reassignBabysittersToJob(Long jobId, String[] babysitterUsernames) throws NotFoundException {
		Optional<Job> j = jobsRepository.findById(jobId);
		if (!j.isPresent())
			throw new NotFoundException("Job not found");

		Job job = j.get();

		if (babysitterUsernames.length < 1)
			throw new IllegalArgumentException("At least one babysitter must be assigned to a job");

		Set<Babysitter> babysitters = babysitterRepository.findAllByUsername(Arrays.asList(babysitterUsernames));
		if (babysitters.size() != babysitterUsernames.length)
			throw new NotFoundException("One or more of the given babysitters was not found");

		job.setAssignedBabysitters(babysitters);
		
		// jobsRepository.removeAssignedBabysittersForJob(jobId); <--- breaks if used, doesnt seem to be required
		jobsRepository.save(job);
		
		mailService.sendBabysitterJobAssignedEmail(job);
		if (job.getStatus() == JobStatus.CONFIRMED)
			mailService.sendBabysittersJobConfirmedEmail(job);

		mailService.sendCustomerReassignmentEmail(job);
	}

	@Override
	public void confirmJob(Long jobId) throws NotFoundException {
		Optional<Job> j = jobsRepository.findById(jobId);
		if (!j.isPresent())
			throw new NotFoundException("Job not found");

		Job job = j.get();
		job.setStatus(JobStatus.CONFIRMED);
		job.setDateConfirmed(new Date());
		
		jobsRepository.save(job);

		mailService.sendCustomerJobConfirmedEmail(job);
		mailService.sendBabysittersJobConfirmedEmail(job);
	}

	@Override
	public Page<List<Job>> getAllJobsPaginatedForBabysitter(int page, String search, String filterBy,
			String babysitterUsername) {
		PageRequest pageRequest = PageRequest.of(page, 10,
				Sort.by(Direction.ASC, "job.startTime", "job.dateRequested"));

		PageRequest pastPageRequest = PageRequest.of(page, 10,
				Sort.by(Direction.DESC, "job.startTime", "job.dateRequested"));
		
		switch (filterBy) {
		case "PENDING_PAYMENT":
			return jobsRepository.findAllByStatusAndBabysitter(search, pageRequest, JobStatus.PENDING_PAYMENT,
					babysitterUsername);
		case "CONFIRMED":
			return jobsRepository.findAllByStatusAndBabysitter(search, pageRequest, JobStatus.CONFIRMED,
					babysitterUsername);
		case "PAST":
			return jobsRepository.findAllPastForBabysitter(search, pastPageRequest, babysitterUsername);
		default:
			return jobsRepository.findAllUpcomingForBabysitter(search, pageRequest, babysitterUsername);
		}
	}

	@Override
	public void cancelJob(Long jobId, String customerUsername) throws NotFoundException {
		Optional<Job> j = jobsRepository.findById(jobId);
		if (!j.isPresent())
			throw new NotFoundException("Job not found");
		
		Job job = j.get();

		if (customerUsername != null && !job.getCustomer().getEmail().equals(customerUsername))
			throw new NotFoundException("Job not found");
		
		job.setStatus(JobStatus.CANCELLED);
		
		jobsRepository.save(job);
	
		mailService.sendJobCancellationEmailToBabysitters(job);
		mailService.sendJobCancellationEmailToCustomer(job);
		mailService.sendJobCancellationEmailToAdmin(job);
	}
	
	public Page<Job> getAllJobsPaginatedForCustomer(int page, String filterBy,
			String customerUsername) {
		PageRequest pageRequest = PageRequest.of(page, 10,
				Sort.by(Direction.ASC, "startTime", "dateRequested"));

		PageRequest pastPageRequest = PageRequest.of(page, 10,
				Sort.by(Direction.DESC, "startTime", "dateRequested"));
		
		switch (filterBy) {
		case "PENDING_PAYMENT":
			return jobsRepository.findAllByStatusAndCustomer(pageRequest, JobStatus.PENDING_PAYMENT, customerUsername);
		case "CONFIRMED":
			return jobsRepository.findAllByStatusAndCustomer(pageRequest, JobStatus.CONFIRMED, customerUsername);
		case "COMPLETED":
			return jobsRepository.findAllByStatusAndCustomer(pastPageRequest, JobStatus.COMPLETED, customerUsername);
		case "PENDING_ASSIGNMENT":
			return jobsRepository.findAllByStatusAndCustomer(pageRequest, JobStatus.PENDING_ASSIGNMENT,
					customerUsername);
		case "CANCELLED":
			return jobsRepository.findAllByStatusAndCustomer(pastPageRequest, JobStatus.CANCELLED, customerUsername);
		case "EXPIRED":
			return jobsRepository.findAllByStatusAndCustomer(pastPageRequest, JobStatus.EXPIRED, customerUsername);
		default:
			return jobsRepository.findAllPendingForCustomer(pageRequest, customerUsername);
		}
	}

	@Override
	public void updateJob(Long jobId, Job job) {
		Optional<Job> j = jobsRepository.findById(jobId);
		if (!j.isPresent())
			throw new IllegalArgumentException("Job does not exist");

		Job updatedJob = j.get();

		updatedJob.setAddressNotes(job.getAddressNotes());
		updatedJob.setEndTime(job.getEndTime());
		updatedJob.setJobAddress(job.getJobAddress());
		updatedJob.setJobNotes(job.getJobNotes());
		updatedJob.setStartTime(job.getStartTime());
		updatedJob.setOwnCarRequired(job.isOwnCarRequired());
		updatedJob.setFullLicenseRequired(job.isFullLicenseRequired());
	
		jobsRepository.save(updatedJob);
		
		mailService.sendBabysitterJobUpdatedEmail(updatedJob);
		mailService.sendCustomerJobUpdatedEmail(updatedJob);
	}

}