package nz.co.bellebabysitters.backend.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import nz.co.bellebabysitters.backend.entity.Job;

public interface IPaymentService {

	public Map<String, Object> createJobPayment(Job job);
	public Map<String, Object> completeJobPayment(HttpServletRequest req);
	
}
