package nz.co.bellebabysitters.backend.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolation;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Admin;
import nz.co.bellebabysitters.backend.exception.EntityConstraintViolationException;
import nz.co.bellebabysitters.backend.exception.UnhandledException;
import nz.co.bellebabysitters.backend.exception.UsernameInUseException;
import nz.co.bellebabysitters.backend.repository.DBErrorCodes;
import nz.co.bellebabysitters.backend.repository.IAdminRepository;

@Service
public class AdminService implements IAdminService {

	@Autowired
	IAdminRepository adminRepository;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	@Override
	public void createNewAdmin(String username, String firstName, String lastName, boolean superAdmin,
			String password) {
		AuthUtils.validatePassword(password);

		// Create an new instance of the Admin class with the given parameters.
		Admin admin = new Admin();
		admin.setUsername(username.toLowerCase());
		admin.setFirstName(firstName);
		admin.setLastName(lastName);
		admin.setSuperAdmin(superAdmin);
		// Hash the password using BCrypt before storage
		admin.setPassword(passwordEncoder.encode(password));
		save(admin);
	}

	@Override
	public Optional<Admin> getAdminByUsername(String username) {
		return adminRepository.findByUsername(username);
	}

	@Override
	public void updateAdminPassword(String username, String password) {
		AuthUtils.validatePassword(password);
		Admin admin = adminRepository.findByUsername(username).get();
		admin.setPassword(passwordEncoder.encode(password));
		save(admin); // need to update for errors

	}

	/**
	 * Saves the admin to the repository, if some conditions are thrown, the
	 * response is defined here. As the repository.save() method is used to update
	 * too (and likely other contexts), the code to save is put into this method to
	 * stop repeating itself.
	 * 
	 * @param admin the admin being saved / updated
	 */
	public void save(Admin admin) {
		try {
			adminRepository.save(admin);
		} catch (DataIntegrityViolationException e) {
			/*
			 * DataIntegrityViolationException is a Spring wrapper containing the Hibernate
			 * ConstraintViolationException.
			 */
			ConstraintViolationException cve = (ConstraintViolationException) e.getCause();
			switch (cve.getErrorCode()) {
			case DBErrorCodes.DUPLICATE_ENTRY:
				throw new UsernameInUseException(admin.getUsername());
			default:
				throw new UnhandledException(e.getMessage());
			}
		} catch (javax.validation.ConstraintViolationException e) {
			List<String> violationMessages = new ArrayList<String>();
			for (ConstraintViolation<?> cv : e.getConstraintViolations()) {
				violationMessages.add(cv.getMessage());
			}
			throw new EntityConstraintViolationException(violationMessages);
		}
	}

	@Transactional
	@Override
	public void deleteAdmin(String username) {
		if (username.equalsIgnoreCase("admin"))
			throw new IllegalArgumentException("You cannot delete the default admin account");
		
		adminRepository.deleteByUsername(username);
	}

	@Override
	public void updateAdmin(Admin admin) {
		Admin updatedAdmin = getAdminByUsername(admin.getUsername()).get();

		if (admin.getUsername().equalsIgnoreCase("admin") && !admin.getSuperAdmin())
			throw new IllegalArgumentException("You cannot demote the default admin account");
		
		updatedAdmin.setFirstName(admin.getFirstName());
		updatedAdmin.setLastName(admin.getLastName());
		updatedAdmin.setSuperAdmin(admin.getSuperAdmin());
		if (admin.getPassword() != null) {
			AuthUtils.validatePassword(admin.getPassword());
			updatedAdmin.setPassword(passwordEncoder.encode(admin.getPassword()));
		}

		save(updatedAdmin);
	}

	public Page<Admin> getAllPaginated(int page, String search) {
		PageRequest pageRequest = PageRequest.of(page, 5, Sort.by(Direction.ASC, "firstName", "lastName", "username"));

		return adminRepository.findAll(search, pageRequest);
	}

}