package nz.co.bellebabysitters.backend.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Admin;
import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.entity.Customer;
import nz.co.bellebabysitters.backend.repository.IAdminRepository;
import nz.co.bellebabysitters.backend.repository.IBabysitterRepository;
import nz.co.bellebabysitters.backend.repository.ICustomerRepository;

@Service(value = "userService")
public class AuthUserDetailsService implements UserDetailsService
{
	
	@Autowired
	private IAdminRepository adminRepository;
	
	@Autowired
	private IBabysitterRepository babysitterRepository;

	@Autowired
	private ICustomerRepository customerRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
	{
		if (username.startsWith("admin\\")) {
			Optional<Admin> admin = adminRepository.findByUsername(username.substring(6).toLowerCase());
			
			if (!admin.isPresent())
				throw new UsernameNotFoundException("Incorrect username or password");
			
			Set<SimpleGrantedAuthority> authorities = new HashSet<>();
			
			authorities.add(new SimpleGrantedAuthority(AuthUtils.ROLE_ADMIN));
			if (admin.get().getSuperAdmin())
				authorities.add(new SimpleGrantedAuthority(AuthUtils.ROLE_SUPER_ADMIN));

			return new User(username, admin.get().getPassword(), authorities);
		}
		else if (username.startsWith("babysitter\\")) {
			Optional<Babysitter> babysitter = babysitterRepository.findByUsername(username.substring(11).toLowerCase());
			
			if (!babysitter.isPresent() || babysitter.get().getPassword() == null || babysitter.get().getPassword().length() == 0)
				throw new UsernameNotFoundException("Incorrect username or password");
			
			if (babysitter.get().getDisabled())
				throw new AccessDeniedException("Your account has been temporarily disabled. Please contact Belle Babysitters directly for further assistance.");
			
			Set<SimpleGrantedAuthority> authorities = new HashSet<>();
			authorities.add(new SimpleGrantedAuthority(AuthUtils.ROLE_BABYSITTER));
			
			return new User(username, babysitter.get().getPassword(), authorities);
		} else if (username.startsWith("customer\\")) {
			Optional<Customer> customer = customerRepository.findByEmail(username.substring(9).toLowerCase());
			
			if (!customer.isPresent() || customer.get().getPassword() == null || customer.get().getPassword().length() == 0)
				throw new UsernameNotFoundException("Incorrect email address or password");
			
			if (customer.get().getDisabled())
				throw new AccessDeniedException("Your account has been temporarily disabled. Please contact Belle Babysitters directly for further assistance.");
			
			Set<SimpleGrantedAuthority> authorities = new HashSet<>();
			authorities.add(new SimpleGrantedAuthority(AuthUtils.ROLE_CUSTOMER));
			
			return new User(username, customer.get().getPassword(), authorities);
		}
		else
		{
			throw new IllegalArgumentException("Unknown account type");
		}

	}

}
