package nz.co.bellebabysitters.backend.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javassist.NotFoundException;
import nz.co.bellebabysitters.backend.auth.AuthUtils;
import nz.co.bellebabysitters.backend.entity.Caregiver;
import nz.co.bellebabysitters.backend.entity.Child;
import nz.co.bellebabysitters.backend.entity.Customer;
import nz.co.bellebabysitters.backend.exception.EntityConstraintViolationException;
import nz.co.bellebabysitters.backend.exception.UnhandledException;
import nz.co.bellebabysitters.backend.exception.UsernameInUseException;
import nz.co.bellebabysitters.backend.repository.DBErrorCodes;
import nz.co.bellebabysitters.backend.repository.ICustomerRepository;

@Service
public class CustomerService implements ICustomerService {

	@Autowired
	MailService mailService;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	@Autowired
	ICustomerRepository customerRepository;

	@Override
	@Transactional
	public Customer createNewCustomer(Customer customer) {
		if (customer.getChildren() == null || customer.getChildren().size() < 1)
			throw new IllegalArgumentException("Customer must have at least one child");
		if (customer.getChildren().size() > 20)
			throw new IllegalArgumentException("Customer cannot have more than 20 children");

		if (customer.getCaregivers() == null || customer.getCaregivers().size() < 1)
			throw new IllegalArgumentException("Customer must define at least one caregiver");
		if (customer.getCaregivers().size() > 6)
			throw new IllegalArgumentException("Customer cannot define more than 6 caregivers");

		customer.setPassword(null);
		customer.setSetupCode(UUID.randomUUID().toString());
		customer.setEmail(customer.getEmail().toLowerCase());

		for (Child c : customer.getChildren())
			c.setCustomer(customer);

		for (Caregiver c : customer.getCaregivers())
			c.setCustomer(customer);

		save(customer);
		mailService.sendCustomerSetupEmail(customer);

		return customer;
	}
	
	@Override
	public Customer createNewCustomerAsCustomer(Customer customer) {
		if (customer.getChildren() == null || customer.getChildren().size() < 1)
			throw new IllegalArgumentException("Customer must have at least one child");
		if (customer.getChildren().size() > 20)
			throw new IllegalArgumentException("Customer cannot have more than 20 children");

		if (customer.getCaregivers() == null || customer.getCaregivers().size() < 1)
			throw new IllegalArgumentException("Customer must define at least one caregiver");
		if (customer.getCaregivers().size() > 6)
			throw new IllegalArgumentException("Customer cannot define more than 6 caregivers");

		customer.setPassword(null);
		customer.setSetupCode(UUID.randomUUID().toString());
		customer.setEmail(customer.getEmail().toLowerCase());
		customer.setAdminNotesAdmin(null);
		customer.setAdminNotesBabysitter(null);
		customer.setId(null);
		customer.setMembershipEndDate(null);

		for (Child c : customer.getChildren())
			c.setCustomer(customer);

		for (Caregiver c : customer.getCaregivers())
			c.setCustomer(customer);

		save(customer);
		mailService.sendCustomerSetupEmail(customer);

		return customer;
	}

	@Transactional
	@Override
	public void setupCustomer(String code, String password) {
		Optional<Customer> c = customerRepository.findBySetupCode(code);
		if (!c.isPresent())
			throw new IllegalArgumentException("Invalid setup code");

		Customer customer = c.get();
		AuthUtils.validatePassword(password);
		customerRepository.setupCustomerPassword(customer.getId(), passwordEncoder.encode(password));
	}
	
	@Transactional
	@Override
	public void updateCustomer(Customer updatedCustomer) throws NotFoundException {
		Optional<Customer> c = customerRepository.findByEmail(updatedCustomer.getEmail());
		
		if (!c.isPresent()) {
			throw new NotFoundException("Customer not found");
		}
		
		Customer customer = c.get();

		updatedCustomer.setId(customer.getId());

		if(updatedCustomer.getPassword() == null || updatedCustomer.getPassword().isEmpty()){
			updatedCustomer.setPassword(customer.getPassword());
			updatedCustomer.setSetupCode(customer.getSetupCode());
		} else {
			AuthUtils.validatePassword(updatedCustomer.getPassword());
			updatedCustomer.setSetupCode(null);
			updatedCustomer.setPassword(passwordEncoder.encode(updatedCustomer.getPassword()));
		}
		
		for (Child child : updatedCustomer.getChildren())
			child.setCustomer(updatedCustomer);
		
		for (Caregiver cg : updatedCustomer.getCaregivers())
			cg.setCustomer(updatedCustomer);
		
		customerRepository.removeCaregiversForCustomer(customer.getId());
		customerRepository.removeChildrenForCustomer(customer.getId());
		save(updatedCustomer);
	}
	
	@Transactional
	@Override
	public void updateCustomerAsCustomer(Customer updatedCustomer) throws NotFoundException {
		Optional<Customer> c = customerRepository.findByEmail(updatedCustomer.getEmail());
		
		if (!c.isPresent()) {
			throw new NotFoundException("Customer not found");
		}
		
		Customer customer = c.get();

		updatedCustomer.setId(customer.getId());
		updatedCustomer.setEmail(customer.getEmail());
		updatedCustomer.setSetupCode(customer.getSetupCode());
		updatedCustomer.setMembershipEndDate(customer.getMembershipEndDate());
		updatedCustomer.setResetCode(customer.getResetCode());
		updatedCustomer.setResetExpiryDate(customer.getResetExpiryDate());
		updatedCustomer.setDisabled(customer.getDisabled());
		updatedCustomer.setAdminNotesAdmin(customer.getAdminNotesAdmin());
		updatedCustomer.setAdminNotesBabysitter(customer.getAdminNotesBabysitter());
		
		if(updatedCustomer.getPassword() == null || updatedCustomer.getPassword().isEmpty()){
			updatedCustomer.setPassword(updatedCustomer.getPassword());
			updatedCustomer.setSetupCode(updatedCustomer.getSetupCode());
		} else {
			AuthUtils.validatePassword(updatedCustomer.getPassword());
			updatedCustomer.setSetupCode(null);
			updatedCustomer.setPassword(passwordEncoder.encode(updatedCustomer.getPassword()));
		}
		
		for (Child child : updatedCustomer.getChildren())
			child.setCustomer(updatedCustomer);
		
		for (Caregiver cg : updatedCustomer.getCaregivers())
			cg.setCustomer(updatedCustomer);
		
		customerRepository.removeCaregiversForCustomer(customer.getId());
		customerRepository.removeChildrenForCustomer(customer.getId());
		save(updatedCustomer);
	}
	
	
	

	public void save(Customer customer) {
		try {
			customerRepository.save(customer);
		} catch (DataIntegrityViolationException e) {
			/*
			 * DataIntegrityViolationException is a Spring wrapper containing the Hibernate
			 * ConstraintViolationException.
			 */
			ConstraintViolationException cve = (ConstraintViolationException) e.getCause();
			switch (cve.getErrorCode()) {
			case DBErrorCodes.DUPLICATE_ENTRY:
				throw new UsernameInUseException(customer.getEmail());
			default:
				throw new UnhandledException(e.getMessage());
			}
		} catch (javax.validation.ConstraintViolationException e) {
			List<String> violationMessages = new ArrayList<String>();
			for (ConstraintViolation<?> cv : e.getConstraintViolations()) {
				violationMessages.add(cv.getMessage());
			}
			throw new EntityConstraintViolationException(violationMessages);
		}
	}

	@Override
	public Optional<Customer> getCustomerByUsername(String username) {
		return customerRepository.findByEmail(username.toLowerCase());
	}

	@Override
	@Transactional
	public void setupPasswordReset(String email) {
		Optional<Customer> c = customerRepository.findByEmail(email.toLowerCase());
		
		if (!c.isPresent())
			throw new UsernameNotFoundException("No account is associated with the given address.");
		
		Customer customer = c.get();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, 12);
		
		String resetCode = UUID.randomUUID().toString();
		Date resetExpiry = cal.getTime();
		customer.setResetCode(resetCode);
		customer.setResetExpiryDate(resetExpiry);
		
		customerRepository.setupPasswordResetForCustomer(customer.getId(), resetCode, resetExpiry);
		mailService.sendCustomerResetEmail(customer);
	}
	
	@Override
	public void resetCustomerPassword(String code, String password) {
		Optional<Customer> c = customerRepository.findByResetCode(code);
		if (!c.isPresent())
			throw new IllegalArgumentException("Invalid reset code");
		
		Customer customer = c.get();
		
		if(customer.getResetExpiryDate().before(Date.from(Instant.now())))
			throw new IllegalArgumentException("Password reset code has expired");
		
		AuthUtils.validatePassword(password);
		customer.setResetCode(null);
		customer.setResetExpiryDate(null);
		customer.setPassword(passwordEncoder.encode(password));
		save(customer);	
		
		mailService.sendCustomerPasswordChangedEmail(customer);
	}
	
	public Page<Customer> getAllPaginated(int page, String search) {
		PageRequest pageRequest = PageRequest.of(page, 5, Sort.by(Direction.ASC, "familySurname", "email"));

		return customerRepository.findAll(search, pageRequest);
	}

}
