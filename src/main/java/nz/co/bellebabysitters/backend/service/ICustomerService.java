package nz.co.bellebabysitters.backend.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import javassist.NotFoundException;
import nz.co.bellebabysitters.backend.entity.Customer;

public interface ICustomerService {

	public Customer createNewCustomer(Customer customer);

	public void setupCustomer(String code, String password);

	public Optional<Customer> getCustomerByUsername(String username);

	public void setupPasswordReset(String email);

	public void resetCustomerPassword(String code, String password);

	public Page<Customer> getAllPaginated(int page, String search);

	void updateCustomer(Customer updatedCustomer) throws NotFoundException;

	void updateCustomerAsCustomer(Customer updatedCustomer) throws NotFoundException;

	public Customer createNewCustomerAsCustomer(Customer customer);

}
