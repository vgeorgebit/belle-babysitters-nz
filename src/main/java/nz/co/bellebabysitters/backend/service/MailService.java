package nz.co.bellebabysitters.backend.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.mail.internet.MimeMessage;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import nz.co.bellebabysitters.backend.BelleBabysittersApplication;
import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.entity.Customer;
import nz.co.bellebabysitters.backend.entity.Job;
import nz.co.bellebabysitters.backend.entity.JobOffer;
import nz.co.bellebabysitters.backend.exception.MailException;

@Service
public class MailService {

	@Autowired
	private JavaMailSender mailSender;

	@Value("${belle.mail.from}")
	private String fromEmail;

	@Value("${belle.mail.admin}")
	private String adminEmail;

	@Value("${belle.mail.inquiry}")
	private String inquiryEmail;
	
	@Value("${belle.domain}")
	private String domain;

	public void sendBabysitterSetupEmail(Babysitter babysitter) {
		try {
			MimeMessage msg = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);

			helper.setTo(babysitter.getEmail());
			helper.setFrom(fromEmail);
			helper.setSubject("Belle Babysitters: New Babysitter Account Created");
			helper.setText("<p> Welcome to the Belle Babysitters Team! </p>\n" + "<br>\n"
					+ "<p>Your profile is now active and you can begin accepting jobs with us. Please complete these simple steps to begin accepting jobs: </p>\n"
					+ "<p> 1. <i><a href=\"" + domain + "/babysitter/setup?code=" + babysitter.getSetupCode()
					+ "\">Click here</a></i> to set up your password OR <i><a href=\"http://www.bellebabysitters.co.nz\">Click here</a></i> to open our web page and have a look around:</p>\n"
					+ "<blockquote> Username: <b>" + babysitter.getUsername() + "</b></blockquote>\n"
					+ "<p> 2.  <i><a href=\"" + domain
					+ "/babysitter_login\">Login</a></i> and click <b>My Profile</b> to check your profile information is correct. You can edit your phone number, email address, home address and password. If you want to update other information (e.g. First Aid or Drivers licence) email please email us.</p>    \n"
					+ "<br>\n" + "<p>We thank you for signing up with us!</p>\n" + "<br>\n" + "<b>Got a question?</b>\n"
					+ "<blockquote>Call: 0800 235532</blockquote>\n" + "<blockquote>Text: 0225 235532 </blockquote>\n"
					+ "<blockquote>Email: enquiry@bellebabysitters.co.nz</blockquote>\n" + "    ", true);

			mailSender.send(msg);
		} catch (Exception e) {
			BelleBabysittersApplication.getLogger().error(e.getMessage());
			e.printStackTrace();
			throw new MailException("Error sending setup email");
		}
	}

	public void sendCustomerSetupEmail(Customer customer) {
		try {
			MimeMessage msg = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);

			helper.setTo(customer.getEmail());
			helper.setFrom(fromEmail);
			helper.setSubject("Belle Babysitters: New Family Account Created ");
			helper.setText("<p> Welcome to Belle Babysitters! </p>\n" + "<br>\n"
					+ "<p>You have successfully created an account with us. Complete these simple steps to get started now:</p>\n"
					+ "<p> 1. <a href=\"" + domain
					+ "/customer_login\">Click here</a></i> go to your account and login with your details OR <i><a href=\"http://www.bellebabysitters.co.nz\">Click here</a></i> to open our web page and have a look around</p>\n"
					+ "<blockquote> Username: <b>" + customer.getEmail() + "</b></blockquote>\n"
					+ "<blockquote><b><i><a href=\"" + domain + "/customer/setup?code=" + customer.getSetupCode()
					+ "\">Click here</a></i></b> to setup your account password.</blockquote>\n"
					+ "<p>2. <i> <a href=\"" + domain
					+ "/customer/jobs/form\">Click here</a></i> to make your first booking. </p>\n" + "<br>\n"
					+ "<p>We thank you for signing up with us!</p> " + "<br>\n" + "<b>Got a question?</b>\n"
					+ "<blockquote>Call: 0800 235532</blockquote>\n" + "<blockquote>Text: 0225 235532 </blockquote>\n"
					+ "<blockquote>Email: enquiry@bellebabysitters.co.nz</blockquote>\n", true);

			mailSender.send(msg);
		} catch (Exception e) {
			BelleBabysittersApplication.getLogger().error(e.getMessage());
			e.printStackTrace();
			throw new MailException("Error sending setup email");
		}
	}

	public void sendBabysitterResetEmail(Babysitter babysitter) {
		try {
			MimeMessage msg = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);

			helper.setTo(babysitter.getEmail());
			helper.setFrom(fromEmail);
			helper.setSubject("Belle Babysitters Password Reset");
			helper.setText("<p>Hi " + babysitter.getFirstName()
					+ "!</p><p>Please use the following password reset link to reset your password. The reset link will expire in 12 hours.</p><p><a href=\""
					+ domain + "/babysitter/reset?code=" + babysitter.getResetCode()
					+ "\">Click here</a> to reset your password.</p>", true);

			mailSender.send(msg);
		} catch (Exception e) {
			BelleBabysittersApplication.getLogger().error(e.getMessage());
			e.printStackTrace();
			throw new MailException("Error sending password reset email");
		}

	}

	public void sendCustomerResetEmail(Customer customer) {
		try {
			MimeMessage msg = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);

			helper.setTo(customer.getEmail());
			helper.setFrom(fromEmail);
			helper.setSubject("Belle Babysitters Password Reset");
			helper.setText("<p>Hi " + customer.getFamilySurname() + " family,"
					+ "</p><p>Please use the following password reset link to reset your password. The reset link will expire in 12 hours.</p><p><a href=\""
					+ domain + "/customer/reset?code=" + customer.getResetCode()
					+ "\">Click here</a> to reset your password.</p>", true);

			mailSender.send(msg);
		} catch (Exception e) {
			BelleBabysittersApplication.getLogger().error(e.getMessage());
			e.printStackTrace();
			throw new MailException("Error sending password reset email");
		}

	}

	public void sendJobOfferEmails(List<JobOffer> jobOffers) {
		for (JobOffer offer : jobOffers) {
			if (!offer.getBabysitter().getDisabled()) {
				CompletableFuture.runAsync(() -> {
					try {
						MimeMessage msg = mailSender.createMimeMessage();
						MimeMessageHelper helper = new MimeMessageHelper(msg, true);
						helper.setFrom(fromEmail);
						helper.setSubject("Belle Babysitters: Job Available");
						Job job = offer.getJob();
						helper.setText("<p> Hi " + offer.getBabysitter().getFirstName() + ",</p>\n" + "<br/>\n"
								+ "<p>A customer has requested a new booking which you are eligible for.</p>\n"
								+ "<p>Please login to your <i><a href=\"" + domain
								+ "/babysitter_login\">Babysitters Portal</a></i> to tell us your availability or not for this job.</p>\n"
								+ "\n" + "<p>Job details are as follows:</p>\n" + "<ul> \n" + "    <li> Start Time: "
								+ job.getStartTime() + "</li>\n" + "    <li>End Time: " + job.getEndTime() + "</li>\n"
								+ "    <li> Suburb: " + job.getJobAddress().split("\n")[1] + "</li>\n"
								+ "    <li> Number of Children: " + job.getNumChildren() + "</li>"
								+ "	 <li><i>Children Details: </i><br>" + job.getChildrenDetails().replace("\n", "<br>")
								+ "</li>\n" + "</ul>\n" + "<br/>\n"
								+ "<p><b>Please Note:</b> accepting this job offer DOES NOT mean you are assigned to the job - it is simply an indication of willingness to be assigned the job. If you have partial availability or some other condition on being able to take on any job please text/phone us immediately via the numbers below for administration assistance.</p>\n"
								+ "<p><b>You will receive an email confirming the job if it is assigned to you.</b></p>\n"
								+ "<br/>\n" + "<p>Kind Regards,</p>\n"
								+ " <p>Administration (Annabelle &amp; Charlotte),</p>\n"
								+ "<p>Belle Babysitters Ltd.</p>\n" + "<br>\n" + "<b>Got a question?</b>\n"
								+ "<p>Call: 0800 235532</p>\n" + "<p>Text: 0225 235532 </p>\n"
								+ "<p>Email: enquiry@bellebabysitters.co.nz</p>", true);
						helper.setTo(offer.getBabysitter().getEmail());
						mailSender.send(msg);
					} catch (Exception e) {
						BelleBabysittersApplication.getLogger()
								.error("Unable to send babysitter job offer email: " + e.getMessage());
					}
				});
			}
		}
	}

	public void sendNewJobAlertToAdmin(Job job) {
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: New Booking Requests Pending");
				helper.setText("    <p>Please log into the <a href=\"" + domain
						+ "/admin_login\">Admin Portal</a> and check the new booking request information.</p>\n"
						+ "    <ul> \n" + "        <li> Start Time:" + job.getStartTime() + "</li>\n"
						+ "        <li> End Time:" + job.getEndTime() + "</li>\n" + "        <li> Suburb:"
						+ job.getJobAddress().split("\n")[1] + "</li>\n" + "        <li> Number of Children: "
						+ job.getNumChildren() + "</li> <li> Children Details: " + job.getChildrenDetails()
						+ "</li> </ul>\n" + "    <p>Please report to management if unsure about anything.</p>\n"
						+ "    <br/>\n"
						+ "    <p>Kind Regards,</p>   <p>Administration (Annabelle &amp; Charlotte),</p>\n"
						+ "    <p>Belle Babysitters Ltd.</p>", true);
				helper.setTo(adminEmail);
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send admin job alert email: " + e.getMessage());
			}
		});
	}

	public void sendJobConfirmationToCustomer(Job job) {
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: Booking Received ");
				helper.setText("   <p> Dear " + job.getCustomer().getFamilySurname() + " family,</p>\n"
						+ "    <p> We are currently searching for available babysitters for you and will update you shortly.</p>\n"
						+ "    <p> <i>Please Note:</i> If you are a returning customer, we will try your previous babysitters first and await to hear of their availability before confirming with you. If you would like us to process your booking more quickly and do not require this, please let us know via text on 0225 235532 for administration assistance.</p>\n"
						+ "\n" + "    <p><b>Some additional information for you: </b></p>\n"
						+ "    <p>We suggest you look at the following areas of our website if you haven't already to ensure you understand all the information for new clients: </p>\n"
						+ "    <ol>\n"
						+ "        <li>Family's Introduction To Belle Babysitters. - See <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">\"Homepage\"</a></li>\n"
						+ "        <li>Family Suggestions for Babysitters (when they arrive) - See <a href=\"http://www.bellebabysitters.co.nz/resources-for-familes\" target=\"_blank\">\"Useful Resources for families\"</a></li>\n"
						+ "        <li>Belle Babysitters LTD Terms and Conditions for Families - See <a href=\"http://www.bellebabysitters.co.nz/policy/1\" target=\"_blank\">\"Families Terms & Conditions\"</a><i> (Point 9. Relates to memberships only).</i></li>\n"
						+ "    </ol>\n"
						+ "    <p>Please use the contact information below if we can help you further with anything at all.</p>\n"
						+ "\n" + "\n" + "    <br/>\n" + "    <p>Kind Regards,</p>\n"
						+ "     <p>Administration (Annabelle &amp; Charlotte),</p>\n"
						+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
						+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
						+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
						+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
						+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter/\" target=\"_blank\">www.facebook.com/bellebabysitter</a></p>\n"
						+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>",
						true);
				helper.setTo(job.getCustomer().getEmail());
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send customer booking confirmation email: " + e.getMessage());
			}
		});
	}
	
	public void sendCustomerInquiry(Customer customer, String inquiryMessage) {
		CompletableFuture.runAsync(() -> {
		try {
			MimeMessage msg = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			
			helper.setTo((customer.getEmail()));
			helper.setTo(inquiryEmail);
			helper.setReplyTo(customer.getEmail());
			helper.setFrom(customer.getEmail());
			helper.setSubject("Belle Babysitters Customer Inquiry");
			helper.setText("<p>New enquiry from: " + customer.getFamilySurname() + " family (" + customer.getEmail() + ")</p>" + "<p>" + inquiryMessage + "</p>", true);
			
			mailSender.send(msg);
		} catch (Exception e) {
			BelleBabysittersApplication.getLogger().error(e.getMessage());
			e.printStackTrace();
			throw new MailException("Error sending customer inquiry");
		
		}
		});
	}

	public void sendBabysitterPasswordChangedEmail(Babysitter babysitter) {
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: Password Reset Activated");
				helper.setText("     <p> Hi " + babysitter.getFirstName() + ",</p>\n"
						+ "    <p><b>The password for your Belle Babysitters account, was recently changed.</b></p>\n"
						+ "    <p>If you made this change, you're all set.</p>\n" + "    <br>\n"
						+ "    <p><b><i>Didn’t change your password?</i></b></p>\n"
						+ "    <p>Please take these steps to secure your account.</p>\n"
						+ "    <p> Contact us immediately via your preferred method below.</p>\n" + "    <ul>\n"
						+ "        <li>Call: 0800 235532</li>\n" + "        <li>Text: 0225 235532 </li>\n"
						+ "        <li>Email: enquiry@bellebabysitters.co.nz</li>\n" + "    </ul>\n" + "    <br/>\n"
						+ "    <p>Kind Regards,</p>\n" + "     <p>Administration (Annabelle &amp; Charlotte),</p>\n"
						+ "    <p>Belle Babysitters Ltd.</p>\n" + "\n" + "    <br>\n"
						+ "    <p>We will never ask you for your password in an email. If you don’t trust a link in an email, go directly to the normal sign-in page via <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">bellebabysitters.co.nz</a></p>",
						true);
				helper.setTo(babysitter.getEmail());
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send babysitter password changed email: " + e.getMessage());
			}
		});
	}

	public void sendCustomerPasswordChangedEmail(Customer customer) {
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: Password Reset Activated");
				helper.setText("    <p> Hi " + customer.getFamilySurname() + " family,</p>\n"
						+ "    <p><b>The password for your Belle Babysitters account, was recently changed.</b></p>\n"
						+ "    <p>If you made this change, you're all set.</p>\n" + "    <br>\n"
						+ "    <p><b><i>Didn’t change your password?</i></b></p>\n"
						+ "    <p>Please take these steps to secure your account.</p> \n"
						+ "    <p> Contact us immediately via your preferred method below.</p>\n" + "    <ul>\n"
						+ "        <li>Call: 0800 235532</li>\n" + "        <li>Text: 0225 235532 </li>\n"
						+ "        <li>Email: enquiry@bellebabysitters.co.nz</li>\n" + "    </ul>\n" + "    <br/>\n"
						+ "    <p>Kind Regards,</p>\n" + "     <p>Administration (Annabelle &amp; Charlotte),</p>\n"
						+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n"
						+ "    <p>We will never ask you for your password in an email. If you don’t trust a link in an email, go directly to the normal sign-in page via <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">bellebabysitters.co.nz</a></p>",
						true);
				helper.setTo(customer.getEmail());
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send customer password changed email: " + e.getMessage());
			}
		});
	}

	public void sendCustomerJobAssignedEmail(Job job) {
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: Booking penciled in - Pending payment");

				String babysitterInfo = job.getAssignedBabysitters().stream().map(b -> b.getFirstName())
						.collect(Collectors.joining(", "));

				helper.setText("    <p>Dear " + job.getCustomer().getFamilySurname() + " family, </p>\n" + "    <br>\n"
						+ "    <p>Good news! We have babysitters available and " + babysitterInfo
						+ (job.getAssignedBabysitters().size() > 1 ? "  are " : "  is ")
						+ " currently pencilled in for you. If you wish to proceed and confirm your booking please pay the booking fee via any of the following methods:</p>\n"
						+ "    <ul>\n" + "        <li>Pay online via your <a href=\"" + domain
						+ "/customer\" target=\"_blank\"></a>customer portal</li>\n"
						+ "        <li>Make an online payment to: 06-0909-0430440-01 (ANZ) </li>\n" + "        <ul>\n"
						+ "            <li>Use your family name and invoice number <b>#" + job.getId()
						+ "</b> as a reference. Once your Payment is received we will send you one further confirmation email to ensure we have all the details for your booking correct and this is the point after which any last minute cancellation fees to the babysitter may apply.</li>\n"
						+ "        </ul>\n" + "    </ul>\n" + "    <br>\n"
						+ "    <p><i>Please Note: </i> Our current standard babysitter rate of $18 per hour ($22 p.h. Public Holidays/Short Notice), is always payable to the babysitter separately (unless otherwise pre-arranged). </p>\n"
						+ "    <p><i>Please note: </i> The minimum amount payable to your babysitter is $30 (or $20 if you are a current member). Also if you are combining families an additional $2 per hour per child fee for the additional family may apply.</p>\n"
						+ "    <br>\n" + "    <p><b>A couple of extra things about our policies:</b></p>\n"
						+ "    <ol>\n"
						+ "        <li> It is our policy for the babysitter to arrive 10 minutes prior to the booked start time (free of charge) of every job to ensure time for settling the children with you around and allowing you time to update the babysitter on your children's day and the point reached in their routines thus far.</li>\n"
						+ "        <li>  It is also our policy (a safety measure for our babysitters) to sometimes phone (at our discretion) the emergency contact you provided to verify that you are indeed a family with children. If we do this we will ask 4-5 general questions about how they know your family, the number, age of your children and if they know your address - the suburb in which you live/are staying (from the information you provided).</li>\n"
						+ "        <li> We do not usually give out the contact information of our babysitters but we are happy to answer any of your queries or provide further information about the babysitter we have matched you with. All of our babysitters (in alphabetical order) have their picture on our website (some are still developing their profile) and all have an official ID they should wear when they arrive. Our website is <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a> (for your reference).</li>\n"
						+ "    </ol>\n" + "    <br>\n"
						+ "    <p>Please let us know if we can help you further with anything at all.</p>\n"
						+ "    <br>\n" + "    <p>Kind Regards,</p>\n"
						+ "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
						+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
						+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
						+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
						+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
						+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n"
						+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>",
						true);
				helper.setTo(job.getCustomer().getEmail());
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send customer job assigned email: " + e.getMessage());
			}
		});
	}

	public void sendCustomerJobConfirmedEmail(Job job) {
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: Payment Received & Booking Confirmation");

				String babysitterInfo = job.getAssignedBabysitters().stream().map(b -> b.getFirstName())
						.collect(Collectors.joining(", "));

				helper.setText("    <p> Dear " + job.getCustomer().getFamilySurname() + " family, </p>\n" + "    <br>\n"
						+ "    <p>This is to acknowledge that your payment has been received and your babysitter "
						+ babysitterInfo + (job.getAssignedBabysitters().size() > 1 ? "  are  " : "  is")
						+ " has now been confirmed for " + job.getStartTime() + " till " + job.getStartTime()
						+ " (the finish time is usually flexible - in consultation with the babysitter) at the address "
						+ job.getJobAddress()
						+ ". (If your booking location is an accommodation provider your babysitter will report to reception on arrival, please leave any further instructions at reception).</p>\n"
						+ "    <p>If this is correct and you have no further queries you need do nothing more. If anything needs correcting or should your plans change unexpectedly please let us know as soon as possible. If you need to cancel your booking, you can do this via your <a href=\""
						+ domain
						+ "/customer\" target=\"_blank\">online customer portal</a>  however cancellation fees may apply. Alternatively, if you need to change your booking details, or have another urgent matter, please contact us via one of the following channels below.</p>\n"
						+ "    <br>\n"
						+ "    <p><i>Please Note: </i> Our current standard babysitter rate of $18 per hour ($22 p.h. Public Holidays/Short Notice), is always payable to the babysitter separately (unless otherwise pre-arranged). </p>\n"
						+ "    <p><i>Please note: </i> The minimum amount payable to your babysitter is $30 (or $20 if you are a current member). Also if you are combining families an additional $2 per hour per child fee for the additional family may apply.</p>\n"
						+ "\n" + "    <br>\n" + "    <p>Kind Regards,</p>\n"
						+ "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
						+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
						+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
						+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
						+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
						+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n"
						+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>",
						true);
				helper.setTo(job.getCustomer().getEmail());
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send customer job confirmed email: " + e.getMessage());
			}
		});
	}

	public void sendBabysitterJobAssignedEmail(Job job) {
		CompletableFuture.runAsync(() -> {
			for (Babysitter babysitter : job.getAssignedBabysitters()) {
				try {
					MimeMessage msg = mailSender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(msg, true);
					helper.setFrom(fromEmail);
					helper.setSubject("Belle Babysitters: Job now assigned to you");
					helper.setText("    <p> Hi " + babysitter.getFirstName() + ",</p>\n"
							+ "    <p>Great! You have been assigned to job <b>#" + job.getId() + "</b>.</p>\n"
							+ "    <p>Details:</p>\n" + "    <ul>\n" + "        <li> Suburb: "
							+ job.getJobAddress().split("\n")[1] + "</li>\n" + "        <li> Start Time: "
							+ job.getStartTime() + "</li>\n" + "    </ul>\n"
							+ "    <p>This means you are currently pencilled in for this job until full confirmation and payment is received from the family. You will receive another email when the job is confirmed. Please log into the app <a href=\""
							+ domain
							+ "/babysitter\" target=\"_blank\">here</a> to see your assigned job, and please put it in your own calendar. If you have any issues in being able to complete this job please contact us via any of the methods below at the earliest opportunity so we can replace you with the least disruption to the family (If it is less than 24 hours before the job start time please phone/text us).</p>\n"
							+ "    <br>\n" + "    <p>Kind Regards,</p>\n"
							+ "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
							+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
							+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
							+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
							+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
							+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n"
							+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>",
							true);
					helper.setTo(babysitter.getEmail());
					mailSender.send(msg);
				} catch (Exception e) {
					BelleBabysittersApplication.getLogger()
							.error("Unable to send babysitter job assigned email: " + e.getMessage());
				}
			}
		});
	}

	public void sendBabysittersJobConfirmedEmail(Job job) {
		CompletableFuture.runAsync(() -> {
			for (Babysitter babysitter : job.getAssignedBabysitters()) {
				try {
					MimeMessage msg = mailSender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(msg, true);
					helper.setFrom(fromEmail);
					helper.setSubject("Belle Babysitters: Babysitting Job Confirmed!");
					helper.setText("    <p> Hi " + babysitter.getFirstName() + ",</p>\n"
							+ "    <p> A job you are assigned to has now been confirmed. Please log into the app <a href=\""
							+ domain
							+ "/babysitter\" target=\"_blank\">here</a> to ensure you have read all of the job information.</p>\n"
							+ "    <p>Details:</p>\n" + "    <ul>\n" + "        <li> Suburb: "
							+ job.getJobAddress().split("\n")[1] + "</li>\n" + "        <li> Start Date/Time: "
							+ job.getStartTime() + "</li>\n" + "        <li> Address Notes: " + job.getAddressNotes()
							+ "</li>\n" + "        <li> Job Notes: " + job.getJobNotes() + "</li>\n"
							+ "        <li> Admin Notes: " + job.getCustomer().getAdminNotesBabysitter() + "</li>\n"
							+ "    </ul>\n"
							+ "    <p>If you have any issues in being able to complete this job please contact us via any of the methods below at the earliest opportunity so we can replace you with the least disruption to the family. (If it is less than 24 hours before the job start time please phone/text us.)</p>\n"
							+ "    <br>\n" + "    <p>Kind Regards,</p>\n"
							+ "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
							+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
							+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
							+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
							+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
							+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n"
							+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>\n"
							+ "", true);
					helper.setTo(babysitter.getEmail());
					mailSender.send(msg);
				} catch (Exception e) {
					BelleBabysittersApplication.getLogger()
							.error("Unable to send babysitter job confirmation email: " + e.getMessage());
				}
			}
		});
	}

	public void sendCustomerJobReminders(List<Job> jobsToRemind) {
		CompletableFuture.runAsync(() -> {
			for (Job job : jobsToRemind) {
				try {
					MimeMessage msg = mailSender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(msg, true);
					helper.setFrom(fromEmail);
					helper.setSubject("Belle Babysitters: Booking Reminder");

					String babysitterInfo = job.getAssignedBabysitters().stream().map(b -> b.getFirstName())
							.collect(Collectors.joining(", "));

					helper.setText("    <p> Dear " + job.getCustomer().getFamilySurname() + " family, </p>\n"
							+ "    <br>\n" + "    <p>This is just a courtesy reminder that " + babysitterInfo
							+ (job.getAssignedBabysitters().size() > 1 ? "  are  " : "  is  ") + " will see you at "
							+ job.getStartTime() + ", " + job.getJobAddress() + " .</p>\n"
							+ "    <p> If this is correct you need do nothing further. If there is an error or something needs changed unexpectedly, please phone/text us using one of the numbers below.</p>\n"
							+ "    <br>\n" + "    <p>Kind Regards,</p>\n"
							+ "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
							+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
							+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
							+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
							+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
							+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n"
							+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>",
							true);
					helper.setTo(job.getCustomer().getEmail());
					mailSender.send(msg);
				} catch (Exception e) {
					BelleBabysittersApplication.getLogger()
							.error("Unable to send customer job reminder email: " + e.getMessage());
				}
			}
		});

	}

	public void sendBabysitterJobReminders(List<Job> jobsToRemind) {
		for (Job job : jobsToRemind) {
			CompletableFuture.runAsync(() -> {
				for (Babysitter babysitter : job.getAssignedBabysitters()) {
					try {
						MimeMessage msg = mailSender.createMimeMessage();
						MimeMessageHelper helper = new MimeMessageHelper(msg, true);
						helper.setFrom(fromEmail);
						helper.setSubject("Belle Babysitters: Booking Reminder");
						helper.setText("    <p> Hi " + babysitter.getFirstName() + ",</p>\n" + "    <br>\n"
								+ "    <p>This is just a courtesy reminder that you are booked to complete a babysitting job at "
								+ job.getStartTime() + ", " + job.getJobAddress() + " .</p>\n"
								+ "    <p> If this is correct and you are all set to complete this job and you need do nothing further. If there is an error or you are no longer available, please text/phone us immediately via the numbers below for administration assistance.</p>\n"
								+ "    <br>\n" + "    <p>Kind Regards,</p>\n"
								+ "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
								+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n"
								+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>", true);
						helper.setTo(babysitter.getEmail());
						mailSender.send(msg);
					} catch (Exception e) {
						BelleBabysittersApplication.getLogger()
								.error("Unable to send babysitter job reminder email: " + e.getMessage());
					}
				}
			});

		}
	}

	public void sendBabysitterJobUpdatedEmail(Job job) {
		// Work around for Hibernate lazy loading
		for (Babysitter b : job.getAssignedBabysitters()) {

		}
		CompletableFuture.runAsync(() -> {
			for (Babysitter babysitter : job.getAssignedBabysitters()) {
				try {
					MimeMessage msg = mailSender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(msg, true);
					helper.setFrom(fromEmail);
					helper.setSubject("Belle Babysitters: Updated Job Details");
					helper.setText("    <p> Dear " + babysitter.getFirstName() + " ,</p>\n" + "    <br>\n"
							+ "    <p>Some changes to your job <b>#" + job.getId()
							+ "</b> have been requested by the family. To see the new confirmation information please log into the app <a href=\""
							+ domain
							+ "/babysitter\" target=\"_blank\">here</a> and ensure you have read all of the updated job information. </p>\n"
							+ "    <p><b>Please confirm you have seen this by texting 0225235532 at your earliest convenience. </b></p>\n"
							+ "    <p>If (due to these changes), you have any issues in being able to complete this job please contact us via any of the methods below at the earliest opportunity so we can find a solution with the least disruption to the family. (If it is less than 24 hours before the job start time please phone/text us.)</p>\n"
							+ "    <br>\n" + "    <p>Kind Regards,</p>\n"
							+ "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
							+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
							+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
							+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
							+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
							+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n"
							+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>",
							true);
					helper.setTo(babysitter.getEmail());
					mailSender.send(msg);
				} catch (Exception e) {
					BelleBabysittersApplication.getLogger()
							.error("Unable to send babysitter job updated email: " + e.getMessage());
				}
			}

		});

	}

	public void sendCustomerJobUpdatedEmail(Job job) {
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject(" Belle Babysitters: Booking Change Received ");

				String babysitterInfo = job.getAssignedBabysitters().stream().map(b -> b.getFirstName())
						.collect(Collectors.joining(", "));

				helper.setText("    <p> Dear " + job.getCustomer().getFamilySurname() + " family,</p>\n" + "    <br>\n"
						+ "    <p>This is to confirm that we have received your request to change your booking details. Your babysitter has been informed. Your new confirmation information is as follows.</p>\n"
						+ "    <ul>\n" + "        <li> Babysitter(s): " + babysitterInfo + " </li>\n"
						+ "        <li> Start date/time: " + job.getStartTime() + "</li>\n"
						+ "        <li> End date/time: " + job.getEndTime() + " </li>\n" + "        <li> Address: "
						+ job.getJobAddress() + "</li>\n" + "        <li> Booking Notes: " + job.getJobNotes()
						+ "</li>\n" + "    </ul>\n"
						+ "    <p>This is to confirm that we have received your request to change your booking details. Your babysitter has been informed and your booking remains confirmed with the updated information.</p>\n"
						+ "    <p>Please let us know if we can help with anything further at all. </p>\n"
						+ "        <br>\n" + "    <p>Kind Regards,</p>\n"
						+ "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
						+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
						+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
						+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
						+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
						+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n"
						+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>",
						true);
				helper.setTo(job.getCustomer().getEmail());
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send babysitter job updated email: " + e.getMessage());
			}
		});
	}

	public void sendJobCancellationEmailToBabysitters(Job job) {
		// Work around for Hibernate lazy loading
		for (Babysitter b : job.getAssignedBabysitters()) {

		}
		CompletableFuture.runAsync(() -> {
			for (Babysitter babysitter : job.getAssignedBabysitters()) {
				try {
					MimeMessage msg = mailSender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(msg, true);
					helper.setFrom(fromEmail);
					helper.setSubject("Belle Babysitters: CANCELLED Booking");
					helper.setText("    <p> Dear " + babysitter.getFirstName() + ",</p>\n" + "    <br>\n"
							+ "    <p>Unfortunately, the booking you were confirmed for " + job.getStartTime() + " in "
							+ job.getJobAddress().split("\n")[1]
							+ " has been cancelled by the family. Thanks for your understanding. We will let you know separately if a cancellation fee is payable to you. You should receive a text checking you have received this information or please text us to confirm you have received this cancellation. </p>\n"
							+ "    <br>\n" + "    <p>Kind Regards,</p>\n"
							+ "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
							+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
							+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
							+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
							+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
							+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n"
							+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>",
							true);
					helper.setTo(babysitter.getEmail());
					mailSender.send(msg);
				} catch (Exception e) {
					BelleBabysittersApplication.getLogger()
							.error("Unable to send babysitter job cancellation email: " + e.getMessage());
				}
			}
		});
	}

	public void sendJobCancellationEmailToCustomer(Job job) {
		Hibernate.initialize(job);
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: CANCELLED Booking Received");
				helper.setText("    <p> Dear " + job.getCustomer().getFamilySurname() + " family, </p>\n" + "    <br>\n"
						+ "    <p>We are sorry to see that you have cancelled your booking. This is confirmation that your cancellation has been received and your babysitter has been informed. We will let you know separately if a cancellation fee is payable by you as per our cancellation policy. </p>\n"
						+ "    <p>Please let us know if we can help you further at all.</p>\n" + "    <br>\n"
						+ "    <p>Kind Regards,</p>\n" + "    <p>Administration (Annabelle &amp; Charlotte),</p>\n"
						+ "    <p>Belle Babysitters Ltd.</p>\n" + "    <br>\n" + "    <b>Got a question?</b>\n"
						+ "    <p>Call: 0800 235532</p>\n" + "    <p>Text: 0225 235532 </p>\n"
						+ "    <p>Email: enquiry@bellebabysitters.co.nz</p>\n"
						+ "    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n"
						+ "    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n"
						+ "    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>",
						true);
				helper.setTo(job.getCustomer().getEmail());
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send customer job cancellation email: " + e.getMessage());
			}
		});
	}

	public void sendJobCancellationEmailToAdmin(Job job) {
		Hibernate.initialize(job);
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: CANCELLED Booking by a family");
				helper.setText("     <p>Unfortunately, the booking job <b># " + job.getId() + "</b> for  the " + job.getCustomer().getFamilySurname() + " family has been cancelled by the family. </p>\n" + 
						"    <p> Please contact the family at <b>" + job.getCustomer().getEmail() + "</b> to organise any cancellation fees that need to be paid. </p>\n" + 
						"    <br>\n" + 
						"    <p>Kind Regards,</p>\n" + 
						"    <p>Administration (Annabelle &amp; Charlotte),</p>\n" + 
						"    <p>Belle Babysitters Ltd.</p>",
						true);
				helper.setTo(adminEmail);
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send admin job cancellation email: " + e.getMessage());
			}
		});
	}

	public void sendCustomerReassignmentEmail(Job job) {
		Hibernate.initialize(job);
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: Replacement Babysitter");
				helper.setText("    <p> Dear " + job.getCustomer().getFamilySurname() + " family, </p>\n" + 
						"    <br>\n" + 
						"    <p>Unfortunately, your babysitter who was confirmed for " + job.getStartTime() + " in " + job.getJobAddress().split("\n")[1] + " has become unavailable. </p>\n" + 
						"    <p>We now have a new babysitter available instead. You can log into <a href=\"" + domain + "/customer\" target=\"_blank\">your portal</a> to check out more about them (including picture and profile).</p>\n" + 
						"    <br>\n" + 
						"    <p>Kind Regards,</p>\n" + 
						"    <p>Administration (Annabelle &amp; Charlotte),</p>\n" + 
						"    <p>Belle Babysitters Ltd.</p>\n" + 
						"    <br>\n" + 
						"    <b>Got a question?</b>\n" + 
						"    <p>Call: 0800 235532</p>\n" + 
						"    <p>Text: 0225 235532 </p>\n" + 
						"    <p>Email: enquiry@bellebabysitters.co.nz</p>\n" + 
						"    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n" + 
						"    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n" + 
						"    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>", true);
				helper.setTo(job.getCustomer().getEmail());
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send customer job reassignment email: " + e.getMessage());
			}
		});			
	}

	public void sendCustomerJobCompletedEmail(Job job) {
		Hibernate.initialize(job);
		CompletableFuture.runAsync(() -> {
			try {
				MimeMessage msg = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(msg, true);
				helper.setFrom(fromEmail);
				helper.setSubject("Belle Babysitters: Follow-Up Babysitting Occasion");
				helper.setText("    <p> Dear " + job.getCustomer().getFamilySurname() + " family,</p>\n" + 
						"    <br>\n" + 
						"    <p>We trust all went well with your recent babysitter booking. The following is a link to our feedback form containing seven questions regarding our service <a href=\"https://www.emailmeform.com/builder/form/HF643yqg7AZOpu13Kn9r\">https://www.emailmeform.com/builder/form/HF643yqg7AZOpu13Kn9r</a>. We very much appreciate you taking the time to answer them, especially if we have sent you a new babysitter.</p>\n" + 
						"    <br>\n" + 
						"    <p>As we are still growing our reputation if you are willing we would love to have your support by writing a review for us via customer feedback site (Ranked by Review) using this link <a href=\"http://rankedbyreview.co.nz/dunedin/business/296/belle-babysitters/\">http://rankedbyreview.co.nz/dunedin/business/296/belle-babysitters/</a>. We realise you are busy parents but we DO really appreciate hearing honest feedback from our customers. We want our service to meet your needs! Please let us know if we can do anything better.</p>\n" + 
						"    <br>\n" + 
						"    <p>Thank you again for your time and please let us know if we can assist you further in any way.</p>\n" + 
						"    <br>\n" + 
						"    <p>Please let us know if we can help you further at all.</p>\n" + 
						"    <br>\n" + 
						"    <p>Kind Regards,</p>\n" + 
						"    <p>Administration (Annabelle &amp; Charlotte),</p>\n" + 
						"    <p>Belle Babysitters Ltd.</p>\n" + 
						"    <br>\n" + 
						"    <b>Got a question?</b>\n" + 
						"    <p>Call: 0800 235532</p>\n" + 
						"    <p>Text: 0225 235532 </p>\n" + 
						"    <p>Email: enquiry@bellebabysitters.co.nz</p>\n" + 
						"    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n" + 
						"    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n" + 
						"    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>", true);
				helper.setTo(job.getCustomer().getEmail());
				mailSender.send(msg);
			} catch (Exception e) {
				BelleBabysittersApplication.getLogger()
						.error("Unable to send customer job completion email: " + e.getMessage());
			}
		});
	}

	public void sendBabysittersJobCompletedEmail(Job job, Babysitter babysitter) {
		Hibernate.initialize(job);
			CompletableFuture.runAsync(() -> {
				try {
					MimeMessage msg = mailSender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(msg, true);
					helper.setFrom(fromEmail);
					helper.setSubject(" Belle Babysitters: Follow-Up Babysitting Job");
					helper.setText("    <p> Dear " + babysitter.getFirstName() + ",</p>\n" + 
							"    <br>\n" + 
							"    <p>Please complete the following feedback form about your recent job <b># " + job.getId()+ "</b> to let us know anything about how the job went. Here is the link to our feedback form containing six questions regarding the family <a href=\"https://www.emailmeform.com/builder/form/ktxeM9ZHe9807Coq00\">https://www.emailmeform.com/builder/form/ktxeM9ZHe9807Coq00</a>. Please fill out this form for every new family you work for and periodically for any repeat families.</p>\n" + 
							"    <br>\n" + 
							"    <p>If there were any issues you would like to report please use the following form <a href=\"https://www.emailmeform.com/builder/form/92CFN38e3Xbz0QbM\">https://www.emailmeform.com/builder/form/92CFN38e3Xbz0QbM</a>.</p>\n" + 
							"    <br>\n" + 
							"    <p>If there is something you would like to discuss directly feel free to contact us via any of the methods below.</p>\n" + 
							"    <br>\n" + 
							"    <p>Kind Regards,</p>\n" + 
							"    <p>Administration (Annabelle &amp; Charlotte),</p>\n" + 
							"    <p>Belle Babysitters Ltd.</p>\n" + 
							"    <br>\n" + 
							"    <b>Got a question?</b>\n" + 
							"    <p>Call: 0800 235532</p>\n" + 
							"    <p>Text: 0225 235532 </p>\n" + 
							"    <p>Email: enquiry@bellebabysitters.co.nz</p>\n" + 
							"    <p>Website: <a href=\"http://www.bellebabysitters.co.nz\" target=\"_blank\">www.bellebabysitters.co.nz</a></p>\n" + 
							"    <p>Facebook: <a href=\"https://www.facebook.com/bellebabysitter\" target=\"_blank\">www.facebook.com/bellebabysitter/</a></p>\n" + 
							"    <p>Twitter: <a href=\"https://twitter.com/Bellebabysitter\" target=\"_blank\">twitter.com/Bellebabysitter</a></p>", true);
					helper.setTo(babysitter.getEmail());
					mailSender.send(msg);
				} catch (Exception e) {
					BelleBabysittersApplication.getLogger()
							.error("Unable to send babysitter job completion email: " + e.getMessage());
				}
			});		
	}
}
