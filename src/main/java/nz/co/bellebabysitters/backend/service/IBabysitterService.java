package nz.co.bellebabysitters.backend.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import nz.co.bellebabysitters.backend.entity.Babysitter;

public interface IBabysitterService {

	public void createNewBabysitter(Babysitter babysitter);

	public void setupBabysitter(String code, String password);

	public Optional<Babysitter> getBabysitterByUsername(String currentUsername);

	public void updateBabysitter(Babysitter babysitter);

	public void updateBabysitterPassword(String username, String password);

	public Page<Babysitter> getAllPaginated(int page, String search);

	public void deleteBabysitter(String username);

	public void setBabysitterImage(String username, byte[] image);

	public byte[] getBabysitterImage(String username);
	
	public void setupPasswordReset(String username);

	public void resetBabysitterPassword(String code, String password);
}

