package nz.co.bellebabysitters.backend;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableWebSecurity
@EnableResourceServer
@EnableScheduling
@EnableSwagger2
public class BelleBabysittersApplication {

	private static Logger logger = LoggerFactory.getLogger(BelleBabysittersApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(BelleBabysittersApplication.class, args);
	}

	@PostConstruct
	public void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("NZ"));
	}

	public static Logger getLogger() {
		return logger;
	}

}
