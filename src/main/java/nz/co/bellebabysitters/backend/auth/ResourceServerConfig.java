package nz.co.bellebabysitters.backend.auth;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter
{

	@Override
	public void configure(ResourceServerSecurityConfigurer configurer) throws Exception
	{
		configurer.resourceId(AuthUtils.RESOURCE_ID);
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception
	{
		http.anonymous().disable()
		.csrf().disable()
        .authorizeRequests()
        .antMatchers("/api/**").authenticated()
        .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());		
	}

}