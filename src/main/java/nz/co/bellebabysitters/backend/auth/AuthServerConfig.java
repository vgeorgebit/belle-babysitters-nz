package nz.co.bellebabysitters.backend.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter
{

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private Environment env;
	
	@Bean
	public JwtAccessTokenConverter accessTokenConverter()
	{
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		
		// Load JWT signing key from the environment as we never want to share this information!
		converter.setSigningKey(env.getProperty("belle.auth-key"));

		return converter;
	}
	
	@Bean
	protected TokenStore tokenStore()
	{
		return new JwtTokenStore(accessTokenConverter());
	}
	
	@Bean
	public DefaultTokenServices defaultTokenServices()
	{
		DefaultTokenServices dts = new DefaultTokenServices();
		dts.setTokenStore(tokenStore());
		
		return dts;
	}
	
	@Override
	public void configure(ClientDetailsServiceConfigurer configurer) throws Exception
	{
		configurer.inMemory()
		.withClient(AuthUtils.CLIENT_ID).secret(AuthUtils.CLIENT_SECRET)
		.authorizedGrantTypes("password", "refresh_token")
		.accessTokenValiditySeconds(AuthUtils.ACCESS_TOKEN_VALIDITY)
		.refreshTokenValiditySeconds(AuthUtils.REFRESH_TOKEN_VALIDITY)
		.scopes("read", "write", "trust")
		.resourceIds(AuthUtils.RESOURCE_ID);
		
	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer configurer)
	{
		configurer.authenticationManager(authenticationManager)
		.accessTokenConverter(accessTokenConverter())
		.tokenStore(tokenStore()).reuseRefreshTokens(false);
	}
	
}
