package nz.co.bellebabysitters.backend.auth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthUtils {
 
	public static final String RESOURCE_ID = "BELLE-API-RESOURCE";
 
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
	public static final String ROLE_BABYSITTER = "ROLE_BABYSITTER";
	public static final String ROLE_CUSTOMER = "ROLE_CUSTOMER";
 
	/*
	 * It doesn't matter that we store these in plaintext, as they're included in
	 * the webapp. We really only have them here because we can't remove the basic
	 * auth from our OAuth2 library.
	 */
	public static final String CLIENT_ID = "bellebabysitters-client";
	// BCrypt hashed client secret (b3lleB4bysiTTer$)
	public static final String CLIENT_SECRET = "$2a$10$fmDI4q0uFVwOXn706.JZEutc6qSNdy4SV2x4C12yZTjcWYfiBrs2u";
 
	// Token validity is given in seconds.
	public static final int ACCESS_TOKEN_VALIDITY = 5 * 60; // 5 minutes
	public static final int REFRESH_TOKEN_VALIDITY = 60 * 60 * 24; // 1 day
 
	/*
	 * We cannot enforce password constraints in tables as we only store
	 * hashed passwords. This is a utility method which can be used to
	 * check password constraints.
	 */
	public static final int PASSWORD_MIN_LENGTH = 6, PASSWORD_MAX_LENGTH = 32;
	public static final String PASSWORD_REGEXP = "[0-9A-Za-z!@#$%^&*/_-]+";
	public static final String PASSWORD_LENGTH_MESSAGE = "Password must contain between 6 and 32 characters.";
	public static final String PASSWORD_REGEXP_MESSAGE = "Password may only contain alphanumeric characters and !@#$%^&*/_-";
	
	public static void validatePassword(String password) {
		if (password == null)
			throw new IllegalArgumentException(PASSWORD_LENGTH_MESSAGE);

		// Validate password length.
		int passLength = password.length();
		if (passLength > PASSWORD_MAX_LENGTH || passLength < PASSWORD_MIN_LENGTH) {
			throw new IllegalArgumentException(PASSWORD_LENGTH_MESSAGE);
		}

		// Validate password consists of valid characters.
		Pattern p = Pattern.compile(PASSWORD_REGEXP);
		Matcher m = p.matcher(password);
		if (!m.matches()) {
			throw new IllegalArgumentException(PASSWORD_REGEXP_MESSAGE);
		}
	}
}