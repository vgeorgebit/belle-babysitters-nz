package nz.co.bellebabysitters.backend.tasks;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.entity.Job;
import nz.co.bellebabysitters.backend.entity.JobStatus;
import nz.co.bellebabysitters.backend.repository.IJobsRepository;
import nz.co.bellebabysitters.backend.service.MailService;

@Component
public class JobStatusTasks {

	@Autowired
	private IJobsRepository jobsRepository;
	
	@Autowired
	private MailService mailService;

	// Job expiry (every 5 mins)
	@Scheduled(fixedRate = 1000 * 60 * 5)
	public void updateJobExpiry() {
		Date now = Date.from(Instant.now());
		List<Job> jobsToExpire = jobsRepository.findByStartTimeBeforeAndStatus(now,
				JobStatus.PENDING_ASSIGNMENT);
		jobsToExpire.addAll(jobsRepository.findByStartTimeBeforeAndStatus(now, JobStatus.PENDING_PAYMENT));

		jobsToExpire.forEach(j -> {
			j.setStatus(JobStatus.EXPIRED);
		});

		jobsRepository.saveAll(jobsToExpire);
	}

	// Job completion & feedback
	@Scheduled(fixedRate = 1000 * 60 * 5, initialDelay = 1000 * 30 * 5) // every 5 mins
	public void updateJobCompleted() {
		List<Job> jobsToComplete = jobsRepository.findByEndTimeBeforeAndStatus(Date.from(Instant.now()), JobStatus.CONFIRMED);

		jobsToComplete.forEach(j -> {
			j.setStatus(JobStatus.COMPLETED);
		});

		jobsRepository.saveAll(jobsToComplete);

		jobsToComplete.forEach(j -> {
			Hibernate.initialize(j);
				mailService.sendCustomerJobCompletedEmail(j);
				for (Babysitter babysitter : j.getAssignedBabysitters()) {
					Hibernate.initialize(babysitter);
					mailService.sendBabysittersJobCompletedEmail(j, babysitter);
				}
		});

	}

	// Job reminders
	@Scheduled(fixedRate = 1000 * 60 * 30) // every 30 mins
	public void sendJobReminders() {
		Date oneDayFromNow = Date.from(Instant.now().plusSeconds(TimeUnit.HOURS.toSeconds(24)));
		List<Job> jobsToRemind = jobsRepository.findByRemindersSentAndStatusAndStartTimeBefore(false, JobStatus.CONFIRMED, oneDayFromNow);

		jobsToRemind.forEach(j -> {
			j.setRemindersSent(true);
		});

		mailService.sendCustomerJobReminders(jobsToRemind);
		mailService.sendBabysitterJobReminders(jobsToRemind);
		
		jobsRepository.saveAll(jobsToRemind);
	}
}
