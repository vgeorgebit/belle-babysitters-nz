package nz.co.bellebabysitters.backend.entity.audit;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

@RevisionEntity(AuditRevisionListener.class)
@Entity
@Table(name = "REVISION_INFO")
public class AuditRevisionEntity extends DefaultRevisionEntity {

	private static final long serialVersionUID = 8479500506925137974L;
	
	private String editor;

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }
	
}
