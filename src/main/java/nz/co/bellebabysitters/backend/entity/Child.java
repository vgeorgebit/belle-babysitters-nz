package nz.co.bellebabysitters.backend.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Audited
@Table(name = Child.TABLE_NAME)
public class Child {
	
	public static final String TABLE_NAME = "CHILD";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name = "ID")
	private Long id;
	
	@NotNull(message = "Child first name is required")
	@Column(name = "FIRST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "First name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "First name may only contain alphabetic characters.")
	private String firstName;
	
	@NotNull(message = "Child surname is required")
	@Column(name = "LAST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "Last name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Last name may only contain alphabetic characters.")
	private String lastName;
	
	@NotNull(message = "Child gender is required")
	@Column(name = "GENDER")
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	@NotNull(message = "Child date of birth is required")
	@Column(name = "DOB", length = 10)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Past(message = "Child date of birth must be in the past") // To ensure the date is a past date
	private Date dob;
	
	@Column(name = "MEDICAL", length = 500)
	@Size(min = 0, max = 500, message = "Medical notes must be less than 500 characters.")
	private String medical;
	
	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(name = "CUSTOMER_ID", nullable = false)
	private Customer customer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getMedical() {
		return medical;
	}

	public void setMedical(String medical) {
		this.medical = medical;
	}
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Child other = (Child) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
