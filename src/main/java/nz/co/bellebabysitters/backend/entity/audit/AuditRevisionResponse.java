package nz.co.bellebabysitters.backend.entity.audit;

import java.util.Date;

public class AuditRevisionResponse {

	private Integer revisionId;
	private Date revisionDate;
	private String editor;
	private String action;
	private Object entity;

	public AuditRevisionResponse() {
	}

	public AuditRevisionResponse(Integer revisionId, Date revisionDate, String editor, String action, Object entity) {
		this.revisionId = revisionId;
		this.revisionDate = revisionDate;
		this.editor = editor;
		this.action = action;
		this.entity = entity;
	}

	public Integer getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(Integer revisionId) {
		this.revisionId = revisionId;
	}

	public Date getRevisionDate() {
		return revisionDate;
	}

	public void setRevisionDate(Date revisionDate) {
		this.revisionDate = revisionDate;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Object getEntity() {
		return entity;
	}

	public void setEntity(Object entity) {
		this.entity = entity;
	}

}
