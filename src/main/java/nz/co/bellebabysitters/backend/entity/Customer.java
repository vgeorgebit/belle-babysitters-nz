package nz.co.bellebabysitters.backend.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Audited
@Table(name = Customer.TABLE_NAME)
public class Customer {

	public static final String TABLE_NAME = "CUSTOMER";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name = "ID")
	private Long id;

	@NotNull(message = "Customer family surname is required")
	@Column(name = "SURNAME", length = 64)
	@Size(min = 1, max = 64, message = "Surname must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Surname may only contain alphabetic characters.")
	private String familySurname;

	@NotNull(message = "Customer email address is required")
	@Pattern(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
	@Column(name = "EMAIL", length = 255, unique = true)
	@Size(min = 1, max = 255, message = "Email name must contain between 1 and 255 characters.")
	private String email;

	@Column(name = "PASSWORD", length = 60)
	@Size(min = 59, max = 60, message = "BCrypt hashed password length should be 59 or 60 bytes.")
	// Do not serialise this field to JSON - we never want to send the password in a
	// response.
	@JsonIgnore
	private String password;

	@Column(name = "SETUP_CODE", unique = true, length = 36)
	@Size(min = 36, max = 36)
	@JsonIgnore
	private String setupCode;
	
	@Column(name = "RESET_CODE", unique = true, length = 36)
	@Size(min = 36, max = 36)
	@JsonIgnore
	private String resetCode;
	
	@Column(name = "RESET_EXPIRY")
	@JsonIgnore
	private Date resetExpiryDate;

	@Column(name = "MEMBERSHIP_END_DATE", length = 10)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date membershipEndDate;

	@NotNull(message = "Customer emergency contact first name is required")
	@Column(name = "EMG_CONTACT_FIRST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "First name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Emergency Contact Name may only contain alphabetic characters.")
	private String emgConFName;

	@NotNull(message = "Customer emergency contact surname is required")
	@Column(name = "EMG_CONTACT_LAST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "Last name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Emergency Contact Name may only contain alphabetic characters.")
	private String emgConLName;

	@NotNull(message = "Customer emergency contact phone number is required")
	@Column(name = "EMG_CONTACT_NUM", length = 15)
	@Size(min = 1, max = 15, message = "Emergency contact phone number must be between 1 to 15 numeric values")
	@Pattern(regexp = "^[0-9]*$", message = "Emergency contact phone may only contain numeric values.")
	private String emgConNo;

	@NotNull(message = "Customer emergency contact relationship is required")
	@Column(name = "EMG_RELATION", length = 64)
	@Size(min = 1, max = 64, message = "Emergency Relationship must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Emergency Relationship may only contain alphabetic characters.")
	private String emgConRel;

	@Column(name = "PETS", length = 500)
	@Size(min = 1, max = 500, message = "Pets must contain between 1 and 500 characters.")
	private String pets;

	@Column(name = "EXTRA_NOTES", length = 500)
	@Size(min = 0, max = 500, message = "Extra notes must be less than 500 characters.")
	private String extraNotes;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private List<Caregiver> caregivers;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private List<Child> children;
	
	@NotNull
	@Column(name = "DISABLED")
	private Boolean disabled = false;
	
	// These notes are only visible to administrators.
	@Column(name = "ADMIN_NOTES_ADMIN", length = 4096)
	private String adminNotesAdmin;
	
	// These notes will be sent to babysitters before a job.
	@Column(name = "ADMIN_NOTES_BABYSITTER", length = 4096)
	private String adminNotesBabysitter;
	
	@Column(name = "HOW_DID_YOU_HEAR", length = 64)
	private String howDidYouHear;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFamilySurname() {
		return familySurname;
	}

	public void setFamilySurname(String surname) {
		this.familySurname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email != null ? email.toLowerCase() : null;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public String getSetupCode() {
		return setupCode;
	}

	public void setSetupCode(String setupCode) {
		this.setupCode = setupCode;
	}

	public Date getMembershipEndDate() {
		return membershipEndDate;
	}

	public void setMembershipEndDate(Date membershipEndDate) {
		this.membershipEndDate = membershipEndDate;
	}

	public String getEmgConFName() {
		return emgConFName;
	}

	public void setEmgConFName(String emgConFName) {
		this.emgConFName = emgConFName;
	}

	public String getEmgConLName() {
		return emgConLName;
	}

	public void setEmgConLName(String emgConLName) {
		this.emgConLName = emgConLName;
	}

	public String getEmgConNo() {
		return emgConNo;
	}

	public void setEmgConNo(String emgConNo) {
		this.emgConNo = emgConNo;
	}

	public String getEmgConRel() {
		return emgConRel;
	}

	public void setEmgConRel(String emgConRel) {
		this.emgConRel = emgConRel;
	}

	public String getPets() {
		return pets;
	}

	public void setPets(String pets) {
		this.pets = pets;
	}

	public String getExtraNotes() {
		return extraNotes;
	}

	public void setExtraNotes(String extraNotes) {
		this.extraNotes = extraNotes;
	}

	public List<Caregiver> getCaregivers() {
		return caregivers;
	}

	public void setCaregivers(List<Caregiver> caregivers) {
		this.caregivers = caregivers;
	}

	public List<Child> getChildren() {
		return children;
	}

	public void setChildren(List<Child> children) {
		this.children = children;
	}
	
	public String getResetCode() {
		return resetCode;
	}

	public void setResetCode(String resetCode) {
		this.resetCode = resetCode;
	}

	public Date getResetExpiryDate() {
		return resetExpiryDate;
	}

	public void setResetExpiryDate(Date resetExpiryDate) {
		this.resetExpiryDate = resetExpiryDate;
	}
	
	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public String getAdminNotesAdmin() {
		return adminNotesAdmin;
	}

	public void setAdminNotesAdmin(String adminNotesAdmin) {
		this.adminNotesAdmin = adminNotesAdmin;
	}

	public String getAdminNotesBabysitter() {
		return adminNotesBabysitter;
	}

	public void setAdminNotesBabysitter(String adminNotesBabysitter) {
		this.adminNotesBabysitter = adminNotesBabysitter;
	}
	
	public String getHowDidYouHear() {
		return howDidYouHear;
	}

	public void setHowDidYouHear(String howDidYouHear) {
		this.howDidYouHear = howDidYouHear;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



}
