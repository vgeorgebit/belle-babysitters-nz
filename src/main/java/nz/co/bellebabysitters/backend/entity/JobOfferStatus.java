package nz.co.bellebabysitters.backend.entity;

public enum JobOfferStatus {

	PENDING, AVAILABLE, UNAVAILABLE
}
