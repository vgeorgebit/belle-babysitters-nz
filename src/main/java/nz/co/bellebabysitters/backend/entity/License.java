package nz.co.bellebabysitters.backend.entity;

public enum License {
	
	FULL, RESTRICTED, LEARNERS, NONE;
	
}
