package nz.co.bellebabysitters.backend.entity;

public enum Gender {

	MALE, FEMALE, OTHER;
	
}
