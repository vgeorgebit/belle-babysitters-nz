package nz.co.bellebabysitters.backend.entity;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
 
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
 
@Entity
@Table(name = Admin.TABLE_NAME)
public class Admin {
 
	public static final String TABLE_NAME = "ADMIN";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name = "ID")
	private Long id;
	
	@NotNull
	@Column(name = "FIRST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "First name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z]*", message = "First name may only contain alphabetic characters.")
	private String firstName;
 
	@NotNull
	@Column(name = "LAST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "Last name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z]*", message = "Last name may only contain alphabetic characters.")
	private String lastName;
 
	@NotNull
	// Ensure usernames are unique & cannot be changed.
	@Column(name = "USERNAME", updatable = false, unique = true, length = 16)
	@Size(min = 4, max = 16, message = "Username must contain between 4 and 16 characters.")
	@Pattern(regexp = "[A-Za-z0-9]*", message = "Username may only contain alphanumeric characters.")
	private String username;
 
	@NotNull
	@Column(name = "PASSWORD", length = 60)
	@Size(min = 59, max = 60, message = "BCrypt hashed password length should be 59 or 60 bytes.")
	@JsonIgnore // Do not serialise this field to JSON - we never want to send the password in a response.
	private String password;
 
	@NotNull
	@Column(name = "SUPER_ADMIN")
	private Boolean superAdmin = false;
 
	public Long getId() {
		return id;
	}
 
	public void setId(Long id) {
		this.id = id;
	}
 
	public String getFirstName() {
		return firstName;
	}
 
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
 
	public String getLastName() {
		return lastName;
	}
 
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
 
	public String getUsername() {
		return username;
	}
 
	public void setUsername(String username) {
		this.username = username;
	}
 
	@JsonIgnore
	public String getPassword() {
		return password;
	}
 
	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}
 
	public Boolean getSuperAdmin() {
		return superAdmin;
	}
 
	public void setSuperAdmin(Boolean superAdmin) {
		this.superAdmin = superAdmin;
	}
 
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}
 
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Admin other = (Admin) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
}