package nz.co.bellebabysitters.backend.entity;

public enum JobStatus {

	PENDING_ASSIGNMENT, PENDING_PAYMENT, CONFIRMED, COMPLETED, CANCELLED, EXPIRED
	
}
