package nz.co.bellebabysitters.backend.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Audited
@Table(name = Babysitter.TABLE_NAME)
public class Babysitter {

	public static final String TABLE_NAME = "BABYSITTER";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name = "ID")
	private Long id;

	@NotNull
	@Column(name = "FIRST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "First name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "First name may only contain alphabetic characters.")
	private String firstName;

	@NotNull
	@Column(name = "LAST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "Last name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Last name may only contain alphabetic characters.")
	private String lastName;

	@NotNull
	@Column(name = "DOB", length = 10)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Past // To ensure the date is a past date
	private Date dob;

	@NotNull
	@Column(name = "ADDRESS", length = 512)
	@Size(min = 1, max = 512, message = "Address must contain between 1 and 512 characters.")
	@Pattern(regexp = "^[ a-zA-Z0-9,.\\-\\s]*$", message = "Address may only contain alphanumeric characters, spaces, commas, hyphens, and full stops.")
	private String address;

	@NotNull
	@Column(name = "CONTACT_NUM", length = 15)
	@Size(min = 1, max = 15, message = "Contact phone number must be between 1 to 15 numeric values")
	@Pattern(regexp = "^[0-9]*$", message = "Contact Phone may only contain numeric values.")
	private String contactPhone;

	@NotNull
	@Pattern(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
	@Column(name = "EMAIL", length = 255, unique = true)
	@Size(min = 1, max = 255, message = "Email name must contain between 1 and 255 characters.")
	private String email;

	@NotNull
	@Column(name = "EMG_CONTACT_NAME", length = 64)
	@Size(min = 1, max = 64, message = "First name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Emergency Contact Name may only contain alphabetic characters.")
	private String emgName;

	@NotNull
	@Column(name = "EMG_PHONE", length = 15)
	@Size(min = 1, max = 15, message = "Emergency contact phone number must be between 1 to 15 numeric values")
	@Pattern(regexp = "^[0-9]*$", message = "Emergency contact Phone may only contain numeric values.")
	public String emgPhone;

	@NotNull
	@Column(name = "EMG_RELATION_NAME", length = 64)
	@Size(min = 1, max = 64, message = "Emergency Relationship name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Emergency Relationship name may only contain alphabetic characters.")
	private String emgRelation;

	@NotNull
	@Column(name = "CONTRACT_SIGN_DT", length = 10)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date contractSignDate;

	@NotNull
	// Ensure usernames are unique & cannot be changed.
	@Column(name = "USERNAME", updatable = false, unique = true, length = 16)
	@Size(min = 4, max = 16, message = "Username must contain between 4 and 16 characters.")
	@Pattern(regexp = "[A-Za-z0-9]*", message = "Username may only contain alphanumeric characters.")
	private String username;

	@Column(name = "PASSWORD", length = 60)
	@Size(min = 59, max = 60, message = "BCrypt hashed password length should be 59 or 60 bytes.")
	@JsonIgnore // Do not serialise this field to JSON - we never want to send the password in a
				// response.
	private String password;

	@Column(name = "SETUP_CODE", unique = true, length = 36)
	@Size(min = 36, max = 36)
	@JsonIgnore
	private String setupCode;

	@Column(name = "RESET_CODE", unique = true, length = 36)
	@Size(min = 36, max = 36)
	@JsonIgnore
	private String resetCode;
	
	@Column(name = "RESET_EXPIRY")
	@JsonIgnore
	private Date resetExpiryDate;
	
	@Column(name = "IMAGE")
	@JsonIgnore
	@Lob
	private byte[] image;
	
	@NotNull
	@Column(name = "DISABLED")
	private Boolean disabled = false;

	@NotNull
	@Column(name = "LICENSE")
	private License license;

	@NotNull(message = "Car access is required")
	@Column(name = "CAR")
	@Enumerated(EnumType.STRING)
	private Car car;

	@Column(name = "POLICE_CHECK_DT", length = 10)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date policeCheck = null;

	@Column(name = "MEDICAL_CERT_DT", length = 10)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date medicalCertDate = null;

	@Column(name = "BIO", length = 1024)
	private String bio;

	public String getBio(){
		return bio;
	}

	public void setBio(String bio){
		this.bio = bio;
	}
	
	public License getLicense(){
		return license;
	}
	
	public void setLicense(License license){
		this.license = license;
	}

	public Car getCar(){
		return car;
	}

	public void setCar(Car car){
		this.car = car;
	}

	public Date getPoliceCheck(){
		return policeCheck;
	}

	public void setPoliceCheck(Date policeCheck){
		this.policeCheck = policeCheck;
	}

	public Date getMedicalCertDate(){
		return medicalCertDate;
	}

	public void setMedicalCertDate(Date medicalCertDate){
		this.medicalCertDate = medicalCertDate;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Babysitter other = (Babysitter) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
			this.email = email != null ? email.toLowerCase() : null;
	}

	public String getEmgName() {
		return emgName;
	}

	public void setEmgName(String emgName) {
		this.emgName = emgName;
	}

	public String getEmgRelation() {
		return emgRelation;
	}

	public void setEmgRelation(String emgRelation) {
		this.emgRelation = emgRelation;
	}

	public Date getContractSignDate() {
		return contractSignDate;
	}

	public void setContractSignDate(Date contractSignDate) {
		this.contractSignDate = contractSignDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public String getSetupCode() {
		return setupCode;
	}

	public void setSetupCode(String setupCode) {
		this.setupCode = setupCode;
	}

	public byte[] getImage(){
		return image;
	}

	public void setImage(byte[] image){
		this.image = image;
	}

	public void setEmgPhone(String emgPhone){
		this.emgPhone = emgPhone;
	}

	public String getEmgPhone(){
		return emgPhone;
	}

	public String getResetCode() {
		return resetCode;
	}

	public void setResetCode(String resetCode) {
		this.resetCode = resetCode;
	}

	public Date getResetExpiryDate() {
		return resetExpiryDate;
	}

	public void setResetExpiryDate(Date resetExpiryDate) {
		this.resetExpiryDate = resetExpiryDate;
	} 
	
}