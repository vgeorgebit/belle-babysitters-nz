package nz.co.bellebabysitters.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Audited
@Table(name = Caregiver.TABLE_NAME)
public class Caregiver {

	public static final String TABLE_NAME = "CAREGIVER";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name = "ID")
	private Long id;

	@NotNull(message = "Caregiver first name is required")
	@Column(name = "FIRST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "Parent / cargiver first name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Parent / caregiver first name may only contain alphabetic characters.")
	private String firstName;

	@NotNull(message = "Caregiver surname is required")
	@Column(name = "LAST_NAME", length = 64)
	@Size(min = 1, max = 64, message = "Parent / caregiver last name must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Parent / caregiver last name may only contain alphabetic characters.")
	private String lastName;

	@NotNull(message = "Caregiver phone number is required")
	@Column(name = "PHONE_NUMBER", length = 15)
	@Size(min = 1, max = 15, message = "Parent / caregiver contact phone number must be between 1 to 15 numeric values")
	@Pattern(regexp = "^[0-9]*$", message = "Parent / caregiver contact Phone may only contain numeric values.")
	private String phoneNumber;

	@NotNull(message = "Caregiver street address is required")
	@Column(name = "STREET_ADDRESS", length = 64)
	@Size(min = 1, max = 512, message = "Parent / caregiver address must contain between 1 and 512 characters.")
	@Pattern(regexp = "^[a-zA-Z0-9,.\\-\\s]*$", message = "Parent / caregiver address may only contain alphanumeric characters, spaces, commas, hyphens, and full stops.")
	private String streetAddress;

	@NotNull(message = "Caregiver address line 2 is required")
	@Column(name = "ADDRESS_LINE_2", length = 64)
	@Size(min = 0, max = 512, message = "Parent / caregiver address line 2 must contain between 1 and 512 characters.")
	@Pattern(regexp = "^[a-zA-Z0-9,.\\-\\s]*$", message = "Parent / caregiver address line 2 may only contain alphanumeric characters, spaces, commas, hyphens, and full stops.")
	private String addressLine2;

	@NotNull(message = "Caregiver city is required")
	@Column(name = "CITY", length = 64)
	@Size(min = 1, max = 64, message = "Parent / caregiver city must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Parent / caregiver city may only contain alphabetic characters.")
	private String city;

	@NotNull(message = "Caregiver region is required")
	@Column(name = "REGION", length = 64)
	@Size(min = 1, max = 64, message = "Parent / caregiver region must contain between 1 and 64 characters.")
	@Pattern(regexp = "[ A-Za-z\\-]*", message = "Parent / caregiver region may only contain alphabetic characters.")
	private String region;

	@NotNull(message = "Caregiver postcode is required")
	@Column(name = "POST_CODE", length = 10)
	@Size(min = 4, max = 10, message = "Parent / caregiver post code must be between 4 and 10 characters")
	@Pattern(regexp = "^[0-9\\-A-Za-z]*", message = "Parent / caregiver post code may only contain alphanumeric values.")
	private String postCode;

	@Column(name = "ADDRESS_NOTES", length = 500)
	@Size(min = 0, max = 500, message = "Parent / caregiver address notes must be less than 500 characters.")
	private String addressNotes;

	@NotNull(message = "Caregiver relationship to child is required")
	@Column(name = "RELATION_TO_CHILD")
	@Enumerated(EnumType.STRING)
	private CaregiverRelationship relationshipToChild;

	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(name = "CUSTOMER_ID", nullable = false)
	private Customer customer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getAddressNotes() {
		return addressNotes;
	}

	public void setAddressNotes(String addressNotes) {
		this.addressNotes = addressNotes;
	}

	public CaregiverRelationship getRelationshipToChild() {
		return relationshipToChild;
	}

	public void setRelationshipToChild(CaregiverRelationship relationshipToChild) {
		this.relationshipToChild = relationshipToChild;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Caregiver other = (Caregiver) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
