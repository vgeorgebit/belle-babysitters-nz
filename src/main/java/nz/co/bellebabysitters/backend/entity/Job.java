package nz.co.bellebabysitters.backend.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = Job.TABLE_NAME)
public class Job {

	public static final String TABLE_NAME = "JOB";
	
	@Id
	@SequenceGenerator(initialValue = 1000, name = "job_id_generator")
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "job_id_generator")
	@Column(name = "ID")
	private Long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "CUSTOMER_ID")
	private Customer customer;
	
	@NotNull
	@Column(name = "CHILDREN_DETAILS")
	private String childrenDetails;
	
	@NotNull
	@Column(name = "NUM_CHILDREN")
	private Integer numChildren;
	
	@JsonIgnore
	@OneToMany(mappedBy = "job", cascade = CascadeType.ALL)
	private List<JobOffer> jobOffers;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private Set<Babysitter> assignedBabysitters;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "STATUS")
	private JobStatus status;
	
	@NotNull
	@Column(name = "FULL_LICENSE_REQUIRED")
	private boolean fullLicenseRequired;
	
	@NotNull
	@Column(name = "OWN_CAR_REQUIRED")
	private boolean ownCarRequired;
	
	@Column(name = "DATE_CONFIRMED")
	private Date dateConfirmed;

	@NotNull
	@Column(name = "START_TIME")
	private Date startTime;
	
	@NotNull
	@Column(name = "END_TIME")
	private Date endTime;
	
	@NotNull
	@Column(name = "DATE_REQUESTED")
	private Date dateRequested; // The date the booking was actually requested (not the date it was for)
	
	@NotNull
	@Column(name = "JOB_ADDRESS", length = 1024)
	private String jobAddress;
	
	@Column(name = "ADDRESS_NOTES", length = 1024)
	private String addressNotes;
	
	@Column(name = "JOB_NOTES", length = 1024)
	private String jobNotes;

	@Pattern(regexp = "\\d+\\.(\\d{2})", message = "Invalid booking fee format")
	@Column(name = "BOOKING_FEE_PRICE")
	private String bookingFeePrice;
	
	@Column(name = "PAYMENT_ID")
	private String paymentId;
	
	@Column(name = "REMINDER_EMAILS_SENT")
	private boolean remindersSent = false;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getChildrenDetails() {
		return childrenDetails;
	}

	public void setChildrenDetails(String childrenDetails) {
		this.childrenDetails = childrenDetails;
	}

	public Integer getNumChildren() {
		return numChildren;
	}

	public void setNumChildren(Integer numChildren) {
		this.numChildren = numChildren;
	}

	public List<JobOffer> getJobOffers() {
		return jobOffers;
	}

	public void setJobOffers(List<JobOffer> jobOffers) {
		this.jobOffers = jobOffers;
	}

	public Set<Babysitter> getAssignedBabysitters() {
		return assignedBabysitters;
	}

	public void setAssignedBabysitters(Set<Babysitter> assignedBabysitters) {
		this.assignedBabysitters = assignedBabysitters;
	}

	public JobStatus getStatus() {
		return status;
	}

	public void setStatus(JobStatus status) {
		this.status = status;
	}

	public boolean isFullLicenseRequired() {
		return fullLicenseRequired;
	}

	public void setFullLicenseRequired(boolean fullLicenseRequired) {
		this.fullLicenseRequired = fullLicenseRequired;
	}

	public boolean isOwnCarRequired() {
		return ownCarRequired;
	}

	public void setOwnCarRequired(boolean ownCarRequired) {
		this.ownCarRequired = ownCarRequired;
	}

	@JsonFormat(pattern="yyyy-MMM-dd HH:mm z", timezone="NZ")
	public Date getDateConfirmed() {
		return dateConfirmed;
	}

	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	public void setDateConfirmed(Date dateConfirmed) {
		this.dateConfirmed = dateConfirmed;
	}

	@JsonFormat(pattern="yyyy-MMM-dd HH:mm z", timezone="NZ")
	public Date getStartTime() {
		return startTime;
	}

	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@JsonFormat(pattern="yyyy-MMM-dd HH:mm z", timezone="NZ")
	public Date getEndTime() {
		return endTime;
	}

	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getJobAddress() {
		return jobAddress;
	}

	public void setJobAddress(String jobAddress) {
		this.jobAddress = jobAddress;
	}

	public String getAddressNotes() {
		return addressNotes;
	}

	public void setAddressNotes(String addressNotes) {
		this.addressNotes = addressNotes;
	}

	public String getJobNotes() {
		return jobNotes;
	}

	public void setJobNotes(String jobNotes) {
		this.jobNotes = jobNotes;
	}

	@JsonFormat(pattern="yyyy-MMM-dd HH:mm z", timezone="NZ")
	public Date getDateRequested() {
		return dateRequested;
	}

	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	public void setDateRequested(Date dateRequested) {
		this.dateRequested = dateRequested;
	}

	public String getBookingFeePrice() {
		return bookingFeePrice;
	}

	public void setBookingFeePrice(String bookingFeePrice) {
		this.bookingFeePrice = bookingFeePrice;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public boolean isRemindersSent() {
		return remindersSent;
	}

	public void setRemindersSent(boolean remindersSent) {
		this.remindersSent = remindersSent;
	}
		
}
