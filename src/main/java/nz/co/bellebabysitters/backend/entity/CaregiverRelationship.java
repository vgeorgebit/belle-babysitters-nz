package nz.co.bellebabysitters.backend.entity;

public enum CaregiverRelationship {
	
	MOTHER, FATHER, STEP_FATHER, STEP_MOTHER, GRANDPARENT, OTHER_FAMILY, OTHER;
	
}
