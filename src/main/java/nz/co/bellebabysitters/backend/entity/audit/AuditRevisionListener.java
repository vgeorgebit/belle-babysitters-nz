package nz.co.bellebabysitters.backend.entity.audit;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuditRevisionListener implements RevisionListener {

	@Override
	public void newRevision(Object revisionEntity) {
		AuditRevisionEntity auditedRevisionEntity = (AuditRevisionEntity) revisionEntity;

		try {
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			auditedRevisionEntity.setEditor(username);
		} catch (NullPointerException e) {
			auditedRevisionEntity.setEditor("SYSTEM");
		}
	}

}
