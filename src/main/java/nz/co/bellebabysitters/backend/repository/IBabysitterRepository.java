package nz.co.bellebabysitters.backend.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.entity.License;

@Repository
public interface IBabysitterRepository extends JpaRepository<Babysitter, Long> {

	public Optional<Babysitter> findByUsername(String username);

	public Optional<Babysitter> findBySetupCode(String code);

	@Query("SELECT b FROM Babysitter b WHERE LOWER(b.firstName) LIKE CONCAT(LOWER(:search), '%') OR LOWER(b.lastName) LIKE CONCAT(LOWER(:search), '%') OR LOWER(b.username) LIKE CONCAT(LOWER(:search), '%') OR LOWER(CONCAT(b.firstName, ' ', b.lastName)) LIKE CONCAT(LOWER(:search), '%')")
	public Page<Babysitter> findAll(@Param(value = "search") String search, Pageable pageable);
	
	public void deleteByUsername(String username);
	
	public Optional<Babysitter> findByEmail(String email);
	
	@Modifying
	@Query(value = "UPDATE Babysitter b SET b.resetCode = :resetCode, b.resetExpiryDate = :expiry, b.setupCode = NULL WHERE b.id = :id")
	public void setupPasswordResetForBabysitter(@Param(value = "id") Long id, @Param(value = "resetCode") String setupCode, @Param(value = "expiry") Date expiry);
	
	public Optional<Babysitter> findByResetCode(String code);
	
	public List<Babysitter> findByLicense(License license);

	@Query("FROM Babysitter WHERE username in :usernames")
	public Set<Babysitter> findAllByUsername(@Param("usernames") List<String> babysitterUsernames);
}
