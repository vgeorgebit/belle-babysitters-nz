package nz.co.bellebabysitters.backend.repository;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import nz.co.bellebabysitters.backend.entity.Customer;

@Repository
public interface ICustomerRepository extends JpaRepository<Customer, Long> {

	public Optional<Customer> findBySetupCode(String code);

	@Modifying
	@Query(value = "UPDATE Customer c SET c.setupCode = NULL, c.password = :password WHERE c.id = :id")
	public void setupCustomerPassword(@Param(value = "id") Long id, @Param(value = "password") String password);

	public Optional<Customer> findByEmail(String email);

	@Modifying
	@Query(value = "UPDATE Customer c SET c.resetCode = :resetCode, c.resetExpiryDate = :expiry, c.setupCode = NULL WHERE c.id = :id")
	public void setupPasswordResetForCustomer(@Param(value = "id") Long id, @Param(value = "resetCode") String setupCode, @Param(value = "expiry") Date expiry);
	
	public Optional<Customer> findByResetCode(String code);
	
	@Query("SELECT a FROM Customer a WHERE LOWER(a.familySurname) LIKE CONCAT(LOWER(:search), '%') OR LOWER(a.email) LIKE CONCAT(LOWER(:search), '%')")
	public Page<Customer> findAll(@Param(value = "search") String search, Pageable pageable);

	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM caregiver WHERE customer_id = :id")
	public void removeCaregiversForCustomer(@Param(value = "id") Long id);
	
	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM child WHERE customer_id = :id")
	public void removeChildrenForCustomer(@Param(value = "id") Long id);

}
