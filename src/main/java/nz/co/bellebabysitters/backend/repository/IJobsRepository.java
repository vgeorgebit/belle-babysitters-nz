package nz.co.bellebabysitters.backend.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import nz.co.bellebabysitters.backend.entity.Job;
import nz.co.bellebabysitters.backend.entity.JobStatus;

@Repository
public interface IJobsRepository extends JpaRepository<Job, Long> {

	@Query("SELECT j FROM Job j WHERE (LOWER(j.customer.familySurname) LIKE CONCAT(LOWER(:search), '%') OR LOWER(j.customer.email) LIKE CONCAT(LOWER(:search), '%') OR CONCAT('\\#', j.id) LIKE CONCAT(:search, '%')) AND (j.status = 'PENDING_ASSIGNMENT' OR j.status = 'PENDING_PAYMENT')")
	public Page<Job> findAllPending(@Param(value = "search") String search, Pageable pageable);

	@Query("SELECT j FROM Job j WHERE (LOWER(j.customer.familySurname) LIKE CONCAT(LOWER(:search), '%') OR LOWER(j.customer.email) LIKE CONCAT(LOWER(:search), '%') OR CONCAT('\\#', j.id) LIKE CONCAT(:search, '%')) AND (j.status = 'COMPLETED' OR j.status = 'CANCELLED' OR j.status = 'EXPIRED')")
	public Page<Job> findAllPast(@Param(value = "search") String search, Pageable pageable);
	
	@Query("SELECT j FROM Job j WHERE (LOWER(j.customer.familySurname) LIKE CONCAT(LOWER(:search), '%') OR LOWER(j.customer.email) LIKE CONCAT(LOWER(:search), '%') OR CONCAT('\\#', j.id) LIKE CONCAT(:search, '%')) AND j.status = :status")
	public Page<Job> findAllByStatus(@Param(value = "search") String search, Pageable pageable, JobStatus status);

	@Query("SELECT j.job FROM JobOffer j WHERE (LOWER(j.job.customer.familySurname) LIKE CONCAT(LOWER(:search), '%') OR LOWER(j.job.customer.email) LIKE CONCAT(LOWER(:search), '%') OR CONCAT('\\#', j.job.id) LIKE CONCAT(:search, '%')) AND j.job.status = :status AND j.babysitter.username = :babysitterUsername")
	public Page<List<Job>> findAllByStatusAndBabysitter(String search, Pageable pageable, JobStatus status,
			String babysitterUsername);

	@Query("SELECT j.job FROM JobOffer j WHERE (LOWER(j.job.customer.familySurname) LIKE CONCAT(LOWER(:search), '%') OR LOWER(j.job.customer.email) LIKE CONCAT(LOWER(:search), '%') OR CONCAT('\\#', j.job.id) LIKE CONCAT(:search, '%')) AND (j.job.status = 'PENDING_PAYMENT' OR j.job.status = 'CONFIRMED') AND j.babysitter.username = :babysitterUsername")
	public Page<List<Job>> findAllUpcomingForBabysitter(String search, Pageable pageable, String babysitterUsername);
	
	@Query("SELECT j FROM Job j WHERE j.status = :status AND j.customer.email= :customerUsername")
	public Page<Job> findAllByStatusAndCustomer(Pageable pageable, JobStatus status, String customerUsername);

	@Query("SELECT j FROM Job j WHERE (j.status = 'PENDING_PAYMENT' OR j.status = 'PENDING_ASSIGNMENT' OR j.status = 'CONFIRMED') AND j.customer.email = :customerUsername")
	public Page<Job> findAllPendingForCustomer(Pageable pageable, String customerUsername);
	
	public List<Job> findByStartTimeBeforeAndStatus(Date date, JobStatus status);
	public List<Job> findByEndTimeBeforeAndStatus(Date date, JobStatus status);

	public List<Job> findByRemindersSentAndStatusAndStartTimeBefore(boolean remindersSent, JobStatus status, Date date);

	@Modifying
	@Query(nativeQuery = true, value = "DELETE FROM job_assigned_babysitters WHERE job_id = :id")
	public void removeAssignedBabysittersForJob(@Param(value = "id") Long jobid);

	@Query("SELECT j.job FROM JobOffer j WHERE CONCAT('\\#', j.job.id) LIKE CONCAT(:search, '%') AND (j.job.status = 'CANCELLED' OR j.job.status = 'COMPLETED') AND j.babysitter.username = :babysitterUsername")
	public Page<List<Job>> findAllPastForBabysitter(String search, Pageable pageable, String babysitterUsername);

}
