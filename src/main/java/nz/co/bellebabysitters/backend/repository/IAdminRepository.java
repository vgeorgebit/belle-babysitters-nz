package nz.co.bellebabysitters.backend.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import nz.co.bellebabysitters.backend.entity.Admin;

@Repository
public interface IAdminRepository extends JpaRepository<Admin, Long> {

	public Optional<Admin> findByUsername(String username);

	public void deleteByUsername(String username);

	@Query("SELECT a FROM Admin a WHERE LOWER(a.firstName) LIKE CONCAT(LOWER(:search), '%') OR LOWER(a.lastName) LIKE CONCAT(LOWER(:search), '%') OR LOWER(a.username) LIKE CONCAT(LOWER(:search), '%') OR LOWER(CONCAT(a.firstName, ' ', a.lastName)) LIKE CONCAT(LOWER(:search), '%')")
	public Page<Admin> findAll(@Param(value = "search") String search, Pageable pageable);
}
