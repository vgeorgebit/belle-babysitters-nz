package nz.co.bellebabysitters.backend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import nz.co.bellebabysitters.backend.entity.Babysitter;
import nz.co.bellebabysitters.backend.entity.JobOffer;
import nz.co.bellebabysitters.backend.entity.JobOfferStatus;

@Repository
public interface IJobOfferRepository extends JpaRepository<JobOffer, Long> {

	public Optional<JobOffer> findByIdAndBabysitter(Long id, Babysitter babysitter);

	@Query("SELECT o FROM JobOffer o WHERE o.status = :status and o.babysitter.id = :babysitterId AND o.job.status = 'PENDING_ASSIGNMENT'")
	public Page<JobOffer> findAllByStatusAndBabysitterId( Pageable pageable, JobOfferStatus status, Long babysitterId);

	public List<JobOffer> findAllByJobIdAndStatus(Long jobId, JobOfferStatus available);

}
