package nz.co.bellebabysitters.backend.exception;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import nz.co.bellebabysitters.backend.BelleBabysittersApplication;

@ControllerAdvice
public class ExceptionController {

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleIllegalArgumentException(IllegalArgumentException e) {
		return new ExceptionResponse(HttpStatus.BAD_REQUEST.toString(), e.getMessage());
	}

	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
		return new ExceptionResponse(HttpStatus.BAD_REQUEST.toString(), e.getMessage());
	}

	@ExceptionHandler(UsernameInUseException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.CONFLICT)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleUsernameInUseException(UsernameInUseException e) {
		return new ExceptionResponse(HttpStatus.CONFLICT.toString(), e.getMessage());
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
		return new ExceptionResponse(HttpStatus.METHOD_NOT_ALLOWED.toString(), e.getMessage());
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleAccessDeniedException(AccessDeniedException e) {
		return new ExceptionResponse(HttpStatus.FORBIDDEN.toString(), "Access denied");
	}

	@ExceptionHandler(AuthenticationCredentialsNotFoundException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleAuthenticationCredentialsNotFoundException(
			AuthenticationCredentialsNotFoundException e) {
		return new ExceptionResponse(HttpStatus.UNAUTHORIZED.toString(), "Authentication credentials must be supplied");
	}

	@ExceptionHandler(BadCredentialsException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleBadCredentialsException(BadCredentialsException e) {
		return new ExceptionResponse(HttpStatus.FORBIDDEN.toString(), e.getMessage());
	}

	@ExceptionHandler(NotFoundException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleNotFoundException(NotFoundException e) {
		return new ExceptionResponse(HttpStatus.NOT_FOUND.toString(), e.getMessage());
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
		return new ExceptionResponse(HttpStatus.BAD_REQUEST.toString(), "Invalid arguments");
	}
	
	@ExceptionHandler(MailException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleMailException(MailException e) {
		return new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), e.getMessage());
	}
	
	@ExceptionHandler(EntityConstraintViolationException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleEntityConstraintViolationException(EntityConstraintViolationException e) {
		return new ExceptionResponse(HttpStatus.BAD_REQUEST.toString(), "Entity constraints violated", e.getViolationMessages());
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleConstraintViolationException(ConstraintViolationException e) {
		return new ExceptionResponse(HttpStatus.BAD_REQUEST.toString(), "Entity constraint violated: " + e.getConstraintName());
	}
	
	@ExceptionHandler(UsernameNotFoundException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleUsernameNotFoundException(UsernameNotFoundException e) {
		return new ExceptionResponse(HttpStatus.NOT_FOUND.toString(), e.getMessage());
	}

	// Exceptions we have specifically chosen not to do anything with.
	@ExceptionHandler(UnhandledException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public ExceptionResponse handleUnhandledException(UnhandledException e) {
		return new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), "An unhandled error occured");
	}
	
	// Default exception handler for all other exceptions (ones we didn't even try
	// to handle).
	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@Order(Ordered.LOWEST_PRECEDENCE)
	public ExceptionResponse handleException(Exception e) {
		BelleBabysittersApplication.getLogger().error(e.toString());
		return new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), "An unknown error occured");
	}

}
