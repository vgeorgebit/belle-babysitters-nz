package nz.co.bellebabysitters.backend.exception;

public class UnhandledException extends RuntimeException
{
	private static final long serialVersionUID = -8833980992914305207L;

	public UnhandledException(String message) {
		super(message);
	}

}
