package nz.co.bellebabysitters.backend.exception;

public class PaymentFailedException extends RuntimeException {

	private static final long serialVersionUID = -2076722670781662407L;

	public PaymentFailedException(String message) {
		super(message);
	}
}
