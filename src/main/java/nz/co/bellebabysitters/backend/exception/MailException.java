package nz.co.bellebabysitters.backend.exception;

public class MailException extends RuntimeException {

	private static final long serialVersionUID = 2138667228416565171L;

	public MailException(String message) {
		super(message);
	}
	
}
