package nz.co.bellebabysitters.backend.exception;

import java.util.List;

public class EntityConstraintViolationException extends RuntimeException {

	private static final long serialVersionUID = 7634451546939468541L;
	private List<String> violationMessages;

	public EntityConstraintViolationException(List<String> violationMessages) {
		super(violationMessages.size() + " entity constraint violations: " + String.join(", ", violationMessages));
		this.violationMessages = violationMessages;
	}

	public List<String>getViolationMessages() {
		return this.violationMessages;
	}

}
