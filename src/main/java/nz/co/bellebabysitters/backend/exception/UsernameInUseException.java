package nz.co.bellebabysitters.backend.exception;

public class UsernameInUseException extends RuntimeException {

	private static final long serialVersionUID = 4502268439315314580L;
	private String username;
	
	public UsernameInUseException(String username)
	{
		super("An account with the username '" + username + "' already exists.");
		this.username = username;
	}
	
	public String getUsername()
	{
		return username;
	}
}
