# Web Application for Belle Babysitters

For getting started please read the following file in the docs folder:
```
Belle Babysitters Web System.pdf
```

For information on how to deploy the application please read the following file in the docs folder:
```
Belle-Babysitters Deployment Documentation.pdf
```
